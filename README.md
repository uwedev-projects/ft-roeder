# Fenstertechnik Röder Webapp

![alt text](http://www.fenster-tip.de/images/rehau-fenstertechnik.jpg)

## Server

### Start compiling

* [STRG] + [SHIFT] + B
	* startet TypeScript Compiler mit watch flag

### Start Nodemon

* TypeScript Compiler starten

```
cd server
npm run dev
```

* Nodemon wird gestartet 

### VS Code Server Debugging

* server.ts im Editor öffnen
* TypeScript Compiler starten
* [F5]
	* startet den Script im debug mode (hier können Breakpoints etc. genutzt werden)

## Client

### Start dev server

```
cd client
npm run serve
```

### Create production build

```
npm build-prod
```