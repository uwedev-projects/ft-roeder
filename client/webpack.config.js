const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const HTMLClientConfig = {
	template: 'index.ejs',
	filename: path.join(__dirname, 'dist', 'index.html') ,
	title: 'Röder Fenstertechnik',
	inject: 'body',
	hash: true
};

const commonChunkConfig = {
	name: 'commons',
	filename: 'commons.[hash].js'
};

const isDev = process.env.NODE_ENV !== 'production';

if(isDev) {
	console.log('Webpack bundling in dev mode..');
}
else {
	console.log('Webpack bundling in prod mode..');
}

const webpackConfig = {
	context: path.join(__dirname, 'src'),
	entry: {
		index: './App.tsx',
		vendor: [
			'core-js',
			'react',
			'react-dom',
			'react-motion',
			'react-redux',
			'react-router-redux',
			'redux',
			'redux-persist',
			'redux-persist-transform-filter',
			'redux-thunk',
			'localforage',
			'uuid'
		]
	},
	cache: true,
	
	output: {
		filename: '[name].[hash].js',
		chunkFilename: '[name].[hash].[chunkhash].js',
		path: path.resolve(__dirname, 'dist'),
		publicPath: isDev ? '/' : 'http://134.255.252.48:4200/'
		// publicPath: 'dist/'
	},

	// Enable sourcemaps for debugging webpack's output
	devtool: isDev ? 'eval-source-map' : false,
	
	resolve: {
		extensions: ['.ts', '.tsx', '.js', '.json']
	},

	externals: {
    'cheerio': 'window',
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },
	
	module: {
		rules: [
			{ test: /\.tsx?$/, loader: 'awesome-typescript-loader' },
			{ enforce: 'pre', test: /\.js$/, loader: 'source-map-loader' },
			{ exclude: /node_modules/},
			isDev ? { test: /\.s?css$/, loaders: ['style-loader', 'css-loader', 'sass-loader'] } : 
			{
				test: /\.s?css$/, 
				use: ExtractTextPlugin.extract({fallback: "style-loader", use: "css-loader!sass-loader"})
			},
			{ test: /\.(jpe?g|png|gif|svg|xml)$/i, loader: "file-loader?name=resources/images/[name].[ext]" },
		]
	},

	devServer: isDev ? {
		inline: true,
		hot: true,
		publicPath: 'http://localhost:3000/',
		contentBase: path.join(__dirname, 'dist'),
		port: 3000,
		watchContentBase: true,
		historyApiFallback: true,
		compress: !isDev,
	} : {
		historyApiFallback: true
	},
	target: "web",
	plugins: isDev ? 
	[
		new ExtractTextPlugin({ filename: '[contenthash].style.css', allChunks: true }),
		new HtmlWebpackPlugin(HTMLClientConfig),
		new webpack.SourceMapDevToolPlugin(),
		new webpack.DefinePlugin({
  		'process.env': {
    		NODE_ENV: JSON.stringify('dev')
  		}
		}),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.optimize.CommonsChunkPlugin(commonChunkConfig),
		new CopyWebpackPlugin([
			{from: '../resources/icons'},
			{from: '../resources/images', to: 'images'}
		]),
	] :
	[
		new webpack.optimize.CommonsChunkPlugin(commonChunkConfig),
		new ExtractTextPlugin({ filename: '[contenthash].style.css', allChunks: true }),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.DefinePlugin({
  		'process.env': {
    		NODE_ENV: JSON.stringify('production')
  		}
		}),
		new HtmlWebpackPlugin(HTMLClientConfig),
		new CopyWebpackPlugin([
			{from: '../resources/icons'},
			{from: '../resources/images', to: 'images'}
		]),
		new UglifyJsPlugin({ sourcemap: false, compress: true })
	]
};

module.exports = webpackConfig;
