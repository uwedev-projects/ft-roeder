import { Reducer, combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { appReducer } from './Application';
import { userReducer } from './User';
import { uiReducer } from './UI';
import { imagesReducer } from './Images';
import { servicesReducer } from './Services';
import { postsReducer } from './Posts';

const dataReducer: Reducer<DataState> = combineReducers<DataState>({
	posts: postsReducer,
	services: servicesReducer,
	images: imagesReducer
});

const rootReducer: Reducer<any> = combineReducers<any>({
	router: (routerReducer as any),
	application: appReducer,
	data: dataReducer,
	ui: uiReducer,
	user: userReducer
});

export default rootReducer;
