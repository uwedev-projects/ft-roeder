import { Reducer } from 'redux';
import { User } from '../constants/ActionTypes';

export const userReducer: Reducer<User> = (state: User = null, action: UserAction) => {
	switch(action.type) {
		case User.RECEIVE_USER_DATA:
			return Object.assign({}, action.user);
		case User.SIGN_OUT_USER:
			return null;
		default:
			return state;
	};
};