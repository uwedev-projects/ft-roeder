import { Reducer } from 'redux';
import { Data } from '../constants/ActionTypes';
import { ImageTypes } from '../constants/ImageTypes';

const defaultState = new Map();
for(let imageType in ImageTypes) {
	defaultState.set(imageType, []);
};

export const imagesReducer: Reducer<any> = (state = defaultState, action: ImagesAction) => {
	let images: Image[] = null;
	switch(action.type) {
		case Data.CREATE_IMAGE:
			images = state.get(action.image.type) || [];
			return new Map(Array.from(state))
				.set(action.image.type, images.concat(action.image));
		case Data.CREATE_IMAGES:
			images = state.get(action.images[0].type) || [];
			return new Map(Array.from(state))
				.set(action.images[0].type, images.concat(action.images));
		case Data.RECEIVE_ALL_IMAGES:
			return new Map(state)
				.set(action.imgType, action.images);
		case Data.REMOVE_IMAGE:
			return new Map(Array.from(state))
				.set(action.imgType, state.get(action.imgType)
				.filter(img => img._id !== action.id));
		default:
			return new Map(state);
	}
};