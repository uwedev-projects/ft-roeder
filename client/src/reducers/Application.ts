import { Reducer, combineReducers } from 'redux';
import { Application, User } from '../constants/ActionTypes';

import { appConfig } from '../config';

const defaultApiServer = appConfig.apiServerBaseUrl;
export const apiServer: Reducer<string> = (state = defaultApiServer, action: ApiServerAction) => {
	switch(action.type){
		case Application.SET_API_SERVER:
			return action.apiServer;
		default:
			return state;
	};
};

export const jwt: Reducer<string> = (state: string = null, action: JwtAction) => {	
	switch(action.type) {
		case Application.RECEIVE_JWT:
			return action.jwt;
		case User.SIGN_OUT_USER:
			return null;
		default:
			return state;
	};
};

export const networkRequests: Reducer<NetworkRequest[]> = (state = [], action: NetworkRequestAction) => {
	switch(action.type) {
		case Application.NETWORK_REQUEST_START:
			return state.concat({
				id: action.id
			});
		case Application.NETWORK_REQUEST_FINISH:
		case Application.NETWORK_REQUEST_FAILURE:
			return state.filter(req => req.id !== action.id);
		default:
			return state;
	};
};

export const isVisible: Reducer<boolean> = (state: boolean = true, action: VisibilityAction) => {
	switch(action.type) {
		case Application.SET_VISIBILITY_STATE:
			return action.isVisible;
		default:
			return state;
	};
};

export const isConnected: Reducer<boolean> = (state: boolean = false, action: ConnectionAction) => {
	switch(action.type) {
		case Application.SET_CONNECTION_STATE:
			return action.isConnected;
		default:
			return state;
	};
};

export const appReducer: Reducer<AppState> = combineReducers({
	networkRequests,
	isVisible,
	isConnected,
	apiServer,
	jwt
});