import { Reducer } from 'redux';
import { Data } from '../constants/ActionTypes';

export const servicesReducer: Reducer<Service[]> = (state = [], action: ServicesAction) => {
	switch(action.type) {
		case Data.CREATE_SERVICE:
			return state.concat(action.service);
		case Data.REMOVE_SERVICE:
			return state.filter(service => service._id !== action.id);
		case Data.REMOVE_ALL_SERVICES:
			return [];
		case Data.UPDATE_SERVICE:
			return state.map(service => {
				if(service._id === action.service._id)
					return action.service;
				return service;
			});
		case Data.RECEIVE_ALL_SERVICES:
			return action.services;
		case Data.RECEIVE_SERVICE:
			if(state.includes(action.service))
				return state.map(service => service.name === action.service.name ?
					action.service : service);
			else return state.concat([action.service]);
		default:
			return state;
	};
};