import { Reducer } from 'redux';
import { Data } from '../constants/ActionTypes';

export const postsReducer: Reducer<Post[]> = (state = [], action: PostsAction) => {
	switch(action.type) {
		case Data.RETRIEVE_NEXT_POSTS:
			return state.concat(action.posts);
		case Data.CREATE_POST:
			return state.concat([action.post]);
		case Data.UPDATE_POST:
			return state.map(post => {
				if(post._id === action.post._id)
					return action.post;
				return post;
			});
		case Data.REMOVE_POST:
			return state.filter(post => post._id !== action.id);
		case Data.REMOVE_POSTS:
			return [];
		default:
			return state;
	};
};