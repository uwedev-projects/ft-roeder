import { Reducer, combineReducers } from 'redux';
import { UI } from '../constants/ActionTypes';

export const statusMessages: Reducer<StatusMessage[]> = (state = [], action: StatusMessageAction) => {
	switch(action.type) {
		case UI.CREATE_STATUS_MESSAGE:
			const isUnique = state.every(message => message.text !== action.message.text);
			if(isUnique)
				return state.concat([action.message]);
			else
				return state;
		case UI.DISMISS_STATUS_MESSAGE:
			return state.filter(message => message.id !== action.id);
		default:
			return state;
	};
};

export const uiReducer: Reducer<UIState> = combineReducers<UIState>({
	statusMessages
});