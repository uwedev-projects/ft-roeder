export namespace Admin {
	export const MANAGE_IMAGES_TITLE = 'Bilder verwalten';
	export const IMAGE_DETAILS_NO_IMG_SELECTED = 'Kein Bild ausgewählt.';
	export const IMAGE_DELETION_TITLE = 'Bild dauerhaft entfernen';
	export const IMAGE_DELETION_MESSAGE = name => `Sind Sie sicher, dass Sie das Bild "${name}" löschen möchten?`;
	export const IMAGE_UPLOADER_DESCRIPTION = 'Hier können Sie Bilder hochladen, die Sie anschließend für verschiedene Inhalte auf Ihrer Seite benutzen können.';

	export const SERVICE_DELETION_TITLE = 'Leistung löschen';
	export const SERVICE_DELETION_MESSAGE = name => `Sind Sie sicher, dass die Leistung "${name}" dauerhaft entfernt werden soll?`;
};

export namespace UI {
	export const CONFIRM_DIALOGUE = 'Bestätigen';
	export const ABORT_DIALOGUE = 'Abbrechen';
}

export namespace Tooltip {
	export const SERVICE_EDITOR_NAME_INPUT = 'Bitte den Namen der Leistung eingeben.';
	export const SERVICE_EDITOR_DESC_IMG_INPUT = 'Wählen Sie das Hauptanzeigebild für diese Leistung aus. Dieses Bild wird in der Übersicht Ihrer Leistungen auf der Home Seite angezeigt.';
	export const POST_EDITOR_TITLE_INPUT = 'Bitte geben Sie hier einen Titel für den Blogeintrag an.';
	export const POST_EDITOR_BODY_IMG_INPUT = 'Wählen Sie das Hauptanzeigebild für diesen Blogeintrag aus. Dieses Bild wird in der Übersicht aller Blogeinträge dargestellt.';
};

export const BusinessInfo = {
	name: 'Röder Fenstertechnik',
	owner: 'Bernd Röder',
	street: 'Gielsdorfer Str. 39',
	zipcode: 53123,
	city: 'Bonn',
	umsatzSteuerId: 'XXXXXXX'
};

export const ContactInfo = {
	phoneNumber: '+49 228 9649546',
	eMail: 'roeder-fenstertechnik@gmx.de'
};