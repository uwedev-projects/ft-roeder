export namespace Application {
	export const NETWORK_REQUEST_START = 'NETWORK_REQUEST_START';
	export const NETWORK_REQUEST_FINISH = 'NETWORK_REQUEST_FINISH';
	export const NETWORK_REQUEST_FAILURE = 'NETWORK_REQUEST_FAILURE';
	export const SET_VISIBILITY_STATE = 'SET_VISIBILITY_STATE';
	export const SET_CONNECTION_STATE = 'SET_CONNECTION_STATE';
	export const SET_API_SERVER = 'SET_API_SERVER';

	export const RECEIVE_JWT = 'RECEIVE_JWT';
};

export namespace Data {
	export const RECEIVE_ALL_SERVICES 					= 'RECEIVE_ALL_SERVICES';
	export const RECEIVE_SERVICE 								= 'RECEIVE_SERVICE';
	export const CREATE_SERVICE 								= 'CREATE_SERVICE';
	export const UPDATE_SERVICE 								= 'UPDATE_SERVICE';
	export const REMOVE_ALL_SERVICES 						= 'REMOVE_ALL_SERVICES';
	export const REMOVE_SERVICE 								= 'REMOVE_SERVICE';

	export const RECEIVE_ALL_IMAGES 						= 'RECEIVE_ALL_IMAGES';
	export const CREATE_IMAGE 									= 'CREATE_IMAGE';
	export const CREATE_IMAGES									=	'CREATE_IMAGES';
	export const REMOVE_ALL_IMAGES 							= 'REMOVE_ALL_IMAGES';
	export const REMOVE_IMAGE 									= 'REMOVE_IMAGE';

	export const RECEIVE_SERVICE_CATEGORY_DATA	= 'RECEIVE_SERVICE_CATEGORY_DATA';
	export const CREATE_SERVICE_CATEGORY				= 'CREATE_SERVICE_CATEGORY';
	export const UPDATE_SERVICE_CATEGORY 				= 'UPDATE_SERVICE_CATEGORY';
	export const REMOVE_SERVICE_CATEGORY 				= 'REMOVE_SERVICE_CATEGORY';

	export const RETRIEVE_NEXT_POSTS						= 'RETRIEVE_NEXT_POSTS';
	export const CREATE_POST										= 'CREATE_POST';
	export const UPDATE_POST										= 'UPDATE_POST';
	export const REMOVE_POST										= 'REMOVE_POST';
	export const REMOVE_POSTS										= 'REMOVE_POSTS';
};

export namespace UI {
	export const TOGGLE_SIDE_MENU = 'TOGGLE_SIDE_MENU';
	export const CREATE_STATUS_MESSAGE = 'CREATE_STATUS_MESSAGE';
	export const DISMISS_STATUS_MESSAGE = 'DISMISS_STATUS_MESSAGE';
};

export namespace User {
	export const SIGN_IN_USER = 'SIGN_IN_USER';
	export const SIGN_OUT_USER = 'SIGN_OUT_USER';
	export const RECEIVE_USER_DATA = 'RECEIVE_USER_DATA';
};