export namespace MessageType { 
	export const SUCCESS = 'SUCCESS';
	export const INFO = 'INFO';
	export const ERROR = 'ERROR';
};

export namespace StatusMessages {
	export const DEFAULT_DURATION 					=	7500;
	export const DEFAULT_NETWORK_ERROR 			= `Ein Netzwerkfehler ist aufgetreten.\nDie Anfrage konnte nicht übermittelt werden.`;
	export const BAD_CREDENTIALS_ERROR 			= 'Tut uns leid!\nDer Benutzername und/oder das Passwort sind falsch.';

	export const SERVICE_CREATION_FAILURE 	= 'Ein Fehler ist aufgetreten. Die Leistung konnte nicht erstellt werden.';
	export const SERVICE_UPDATE_FAILURE			= 'Ein Fehler ist aufgetreten. Die Leistung konnte nicht gespeichert werden.';
	export const SERVICE_DELETION_FAILURE 	= 'Ein Fehler ist aufgetreten. Die Leistung konnte nicht gelöscht werden.';
	export const SERVICE_DELETE_ALL_FAILRE 	=	'Ein Fehler ist aufgetreten. Die Leistungen konnten nicht gelöscht werden.';

	export const SERVICE_CREATION_SUCCESS		= 'Leistung erfolgreich erstellt.';
	export const SERVICE_UPDATE_SUCCESS			= 'Leistung erfolgreich gespeichert.';
	export const SERVICE_DELETION_SUCCESS		= 'Leistung erfolgreich gelöscht.';
	export const SERVICE_DELETE_ALL_SUCCESS	= 'Leistungen erfolgreich gelöscht.';

	export const POST_FETCH_FAILURE					= 'Es tut uns leid. Beim Laden der Blogeinträge ist ein Fehler aufgetreten.';
	export const POST_CREATION_FAILRURE 		= 'Ein Fehler ist aufgetreten. Der Blogeintrag konnte nicht erstellt werden.';
	export const POST_UPDATE_FAILURE				= 'Ein Fehler ist aufgetreten. Der Blogeintrag konnte nicht gespeichert werden.';
	export const POST_DELETION_FAILURE 			= 'Ein Fehler ist aufgetreten. Der Blogeintrag konnte nicht gelöscht werden.';
	export const POST_DELETE_ALL_FAILURE 		=	'Ein Fehler ist aufgetreten. Die Blogeinträge konnten nicht gelöscht werden.';

	export const POST_CREATION_SUCCESS			= 'Blogeintrag erfolgreich erstellt.';
	export const POST_UPDATE_SUCCESS				= 'Blogeintrag erfolgreich gespeichert.';
	export const POST_DELETION_SUCCESS			= 'Blogeintrag erfolgreich gelöscht.';
	export const POST_DELETE_ALL_SUCCESS		= 'Blogeinträge erfolgreich gelöscht.';

	export const IMAGES_DELETION_FAILURE		= 'Ein Fehler ist aufgetreten. Das Bild konnte nicht gelöscht werden';
	
	export const IMAGES_UPLOAD_SUCESS				= 'Bilder erfolgreich gespeichert.';
	export const IMAGES_UPLOAD_FAILURE			= 'Ein Fehler ist aufgetreten. Die Bilder konnten nicht gespeichert werden.';
	export const IMAGES_DELETION_SUCCESS		= 'Bild erfolgreich gelöscht.'
};

export namespace ValidationMessages {
	export const SERVICE_INVALID_NAME 				= 'Bitte geben Sie einen Namen aus mindestens 4 Zeichen an.';
	export const SERVICE_INVALID_DESCRIPTION 	= 'Bitte geben Sie eine Beschreibung für diese Leistung an.';
	export const SERVICE_INVALID_IMAGE				= 'Bitte wählen Sie ein Bild für diese Leistung aus.';

	export const POST_INVALID_TITLE 					= 'Bitte geben Sie einen Titel für diesen Blogeintrag an.';
};

export namespace Breakpoints {
	export const XS = 480;
	export const SM = 768;
	export const MD = 992;
	export const LG = 1200;
}

export const TOOLTIP_DELAY = 2000;