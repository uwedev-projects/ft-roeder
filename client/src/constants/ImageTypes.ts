export namespace ImageTypes {
	export const DEFAULT = 'DEFAULT';
	export const POST = 'POST';
	export const SERVICE = 'SERVICE';
	export const REFERENCE = 'REFERENCE';
	export const SLIDER = 'SLIDER';
	export const TEST = 'TEST';
};

export const typeTranslationMap = {
	DEFAULT: 'Sonstiges',
	POST: 'Blogeinträge',
	SERVICE: 'Leistungen',
	REFERENCE: 'Referenzen',
	SLIDER: 'Bildergallerie',
	TEST: 'Test'
};