import * as React from 'react';
import { NavRoute } from '../components/nav-controller/NavController';

export const TestPage: React.StatelessComponent<void> = () => (
	<div className="test-page">
		I am a test page.
	</div>
);

export const testRoutes: NavRoute[] = [
	{ path: '/test', Component: TestPage }
];

export const testEntries: NavMenuEntry[] = [
	{ name: 'TestLink01', path: '/test01', iconClass: 'test' },
	{ name: 'TestLink02', path: '/test02', iconClass: 'test' },
	{ name: 'TestLink03', path: '/test03', iconClass: 'test' }
];