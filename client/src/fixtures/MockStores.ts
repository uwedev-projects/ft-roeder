import { appConfig } from '../config';
import { createMemoryHistory, History } from 'history';
import { createMockStore } from 'redux-test-utils';

export const defaultState = {
	router: null,
	application: {
		isConnected: true,
		networkRequests: [],
		isVisible: true,
		jwt: null,
		apiServer: appConfig.apiServerBaseUrl
	},
	data: null,
	ui: null,
	user: null
};

export const createDefaultMockStore = () => {
	return createMockStore(defaultState);
};

export const createRouterMockStore = (history?: History) => {
	if(!history)
		history = createMemoryHistory();
	const routerMockState = {
		router: {
			location: history.location
		},
		application: {
			isConnected: true,
			networkRequests: [],
			isVisible: true,
			jwt: null,
			apiServer: appConfig.apiServerBaseUrl
		},
		data: null,
		ui: {
			statusMessages: []
		},
		user: null
	};

	return createMockStore(routerMockState);
};

export const createAuthenticatedMockStore = (history: History) => {
	const authenticatedMockStore = {
		router: {
			location: history.location
		},
		application: {
			isConnected: true,
			networkRequests: [],
			isVisible: true,
			jwt: null,
			apiServer: appConfig.apiServerBaseUrl
		},
		data: null,
		ui: {
			statusMessages: []
		},
		user: {
			credentials: {
				userName: 'Test123',
				password: 'password'
			},
			userInfo: {
				firstName: 'Test',
				lastName: 'Test'
			}
		}
	};

	return createMockStore(authenticatedMockStore);
};
