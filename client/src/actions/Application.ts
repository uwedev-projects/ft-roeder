import { Application } from '../constants/ActionTypes';
import { v4 } from 'uuid';

export const networkRequest = (error: string, isFetching?: boolean, id?: string) => {
	let action: NetworkRequestAction;
	if(error) {
		action = {
			type: Application.NETWORK_REQUEST_FAILURE,
			id,
			error
		}
	}
	else if(isFetching)
		action = {
			type: Application.NETWORK_REQUEST_START,
			id: v4()
		};
	else
		action = {
			type: Application.NETWORK_REQUEST_FINISH,
			id
		}
	
	return action;
};

export const deferredFinish = (action: NetworkRequestAction, delay: number) => {
	return Object.assign(action, {
		meta: { delay }
	});
};

export const receiveJwt = (jwt: string) => {
	return {
		type: Application.RECEIVE_JWT,
		jwt
	};
};

export const setVisibilityState = (isVisible: boolean) => {
	return {
		type: Application.SET_VISIBILITY_STATE,
		isVisible
	};
};

export const setConnectionState = (isConnected: boolean) => {
	return {
		type: Application.SET_CONNECTION_STATE,
		isConnected
	};
};

export const setApiServer = (apiServer: string) => {
	return {
		type: Application.SET_API_SERVER,
		apiServer
	};
};