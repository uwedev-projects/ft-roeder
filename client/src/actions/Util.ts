import { ThunkAction } from 'redux-thunk';

export const deferAction = (action: BaseAction, delay: number) => {
	return (dispatch) => {
		setTimeout(() => {
			dispatch(action);
		}, delay);
	};
};