import { v4 } from 'uuid';
import { UI } from '../constants/ActionTypes';
import { MessageType, StatusMessages } from '../constants/UIConstants';
import { Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

export const createStatusMessage = (id: string, text: string, duration: number, messageType: string) => {
	return {
		type: UI.CREATE_STATUS_MESSAGE,
		message: {
			id,
			text,
			duration,
			type: messageType
		}
	};
};

export const dismissStatusMessage = (id: string) => {
	return {
		type: UI.DISMISS_STATUS_MESSAGE,
		id
	};
};

const defaultDuration = StatusMessages.DEFAULT_DURATION;

export const pushStatusMessage = (message: string, messageType = MessageType.INFO, duration = defaultDuration) => {
	return async (dispatch: Dispatch<any>, getState: Function) => {
		const uid = v4();
		const isUnique = getState().ui.statusMessages.every(
			statusMessage => statusMessage.text !== message
		);
		
		if(isUnique) {
			dispatch(createStatusMessage(uid, message, duration, messageType));
			setTimeout(() => {
				dispatch(dismissStatusMessage(uid));
			}, duration);
		}
	};
};

export const defaultNetworkErrMessage = () => dispatch => {
		dispatch(pushStatusMessage(
			StatusMessages.DEFAULT_NETWORK_ERROR,
			MessageType.ERROR
	));
}