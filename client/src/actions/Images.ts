import { Data } from '../constants/ActionTypes';
import { Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { fetchHttp, postFormData, fetchAuthenticated, getHttpConfig } from '../core/Http';
import { pushStatusMessage, defaultNetworkErrMessage } from '../actions/UI';
import { networkRequest } from '../actions/Application';
import { StatusMessages, MessageType } from '../constants/UIConstants';

export const receiveAllImages = (images: Image[], imgType: string) => {
	return {
		type: Data.RECEIVE_ALL_IMAGES,
		images,
		imgType
	};
};

export const createImage = (image: Image) => {
	return {
		type: Data.CREATE_IMAGE,
		image
	};
};

export const createImages = (images: Image[]) => {
	return {
		type: Data.CREATE_IMAGES,
		images
	};
};

export const removeImage = (id: string, imgType: string) => {
	return {
		type: Data.REMOVE_IMAGE,
		id,
		imgType
	};
};

export const removeAllImages = () => {
	return { type: Data.REMOVE_ALL_IMAGES };
};

export const postImages = (files: File[], imageType: string) => async (dispatch, getState) => {
	const { id } = dispatch(networkRequest(null, true));

	try {
		const formData = new FormData();
		for(let i = 0; i < files.length; i++) {
			formData.append(imageType , files[i]);
		}
		
		let httpConfig: RequestInit = getHttpConfig('POST', formData);
		const jwt = getState().application.jwt;

		const response = await postFormData('/images', jwt, formData);
		dispatch(networkRequest(null, false, id));
		if(response.ok && response.status < 400) {
			const data = await response.json();
			if(data.images) {
				dispatch(createImages(data.images));
				dispatch(
					pushStatusMessage(StatusMessages.IMAGES_UPLOAD_SUCESS, MessageType.SUCCESS)
				);
				return { images: data.images };
			}
		}
		else {
			dispatch(
				pushStatusMessage(StatusMessages.IMAGES_UPLOAD_FAILURE, MessageType.ERROR)
			);
		}
	} catch (err) {
		dispatch(networkRequest('Failed to post Images data.', false, id));
		dispatch(defaultNetworkErrMessage());
		console.log(err);
		console.error(err.stack);
	}
};

export const getImagesByType = (imgType: string) => async dispatch => {
	const { id } = dispatch(networkRequest(null, true));
	try {
		const response = await fetchHttp(`/images?type=${imgType}`);
		dispatch(networkRequest(null, false, id));
		if(response.ok &&  response.status < 400) {
			const data = await response.json();
			if(data.images) 
				dispatch(receiveAllImages(data.images, imgType));
		}
	} catch (err) {
		console.error(err);
		dispatch(networkRequest(`Failed to fetch Images data for type = ${imgType}`, false, id));
		dispatch(defaultNetworkErrMessage());
	}
};

export const deleteImageById = (imgId: string, imgType: string) => async (dispatch, getState) => {
	const { id } = dispatch(networkRequest(null, true));
	
	try {
		const jwt = getState().application.jwt;
		const httpConfig: RequestInit = { method: 'DELETE' };
		const url = `/images/${imgId}?type=${imgType}`;

		const response = await fetchAuthenticated(url, jwt, httpConfig)	;
		dispatch(networkRequest(null, false, id));
		
		if(response.ok && response.status < 400) {
			const data = await response.json();
			if(data.result && data.result.ok === 1) {
				dispatch(removeImage(imgId, imgType));
				dispatch(pushStatusMessage(
					StatusMessages.IMAGES_DELETION_SUCCESS,
					MessageType.SUCCESS
				));
			}
		} else {
			dispatch(pushStatusMessage(
				StatusMessages.IMAGES_DELETION_FAILURE,
				MessageType.ERROR
			));
		}
	} catch (err) {
		dispatch(networkRequest('Failed to delete Image data.', false, id));
		dispatch(defaultNetworkErrMessage());
	}
};