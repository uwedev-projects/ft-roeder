import { Data } from '../constants/ActionTypes';

import { fetchHttp, fetchAuthenticated, getHttpConfig } from '../core/Http';
import { pushStatusMessage, defaultNetworkErrMessage } from '../actions/UI';
import { networkRequest } from '../actions/Application';
import { StatusMessages, MessageType } from '../constants/UIConstants';

export const receiveAllServices = (services: Service[]) => {
	return {
		type: Data.RECEIVE_ALL_SERVICES,
		services
	};
};

export const receiveService = (service: Service) => {
	return {
		type: Data.RECEIVE_SERVICE,
		service
	};
};

export const updateService = (service: Service) => {
	return {
		type: Data.UPDATE_SERVICE,
		service
	};
};

export const removeService = (id: string) => {
	return {
		type: Data.REMOVE_SERVICE,
		id
	};
};

export const removeAllServices = () => {
	return { type: Data.REMOVE_ALL_SERVICES }
};

export const createService = (service: Service) => {
	return {
		type: Data.CREATE_SERVICE,
		service
	};
};

export const getAllServices = (orderBySignificance?: boolean) => async dispatch => {
	const { id } = dispatch(networkRequest(null, true));

	try {
		const url = orderBySignificance ? '/services?orderby=significance' : '/services';
		const response = await fetchHttp(url);

		dispatch(networkRequest(null, false, id));
		if(response.ok && response.status < 400) {
			const data = await response.json();
			if(data.services)
				dispatch(receiveAllServices(data.services));
		}
	} catch (err) {
		dispatch(networkRequest('Failed to fetch Services data.', false, id));
		dispatch(defaultNetworkErrMessage());
	}
};

export const getServiceByName = (name: string) => async dispatch => {
	const { id } = dispatch(networkRequest(null, true));
		try {
			const response = await fetchHttp(`/services/${name}`);
			dispatch(networkRequest(null, false, id));
			if(response.ok && response.status < 400) {
				const data = await response.json();
				if(data.service)
					dispatch(receiveService(data.service));
			}
		} catch (err) {
			console.log(err);
			dispatch(networkRequest(`Failed to fetch data for Service ${name}.`, false, id));
			dispatch(defaultNetworkErrMessage());
		}
};

export const patchServiceById = (service: Service) => async (dispatch, getState) => {
	const { id } = dispatch(networkRequest(null, true));

	if(!service._id)
		throw new Error('Cannot patch service. No valid id was found.');

	try {
		const httpConfig: RequestInit = getHttpConfig('PATCH', {service});
		const jwt = getState().application.jwt;

		const response = await fetchAuthenticated(`/services/${service._id}`, jwt, httpConfig);
		
		dispatch(networkRequest(null, false, id));

		if(response.ok && response.status < 400) {
			const data = await response.json();
			if(data.result && data.result.ok === 1) {
				dispatch(updateService(service));
				dispatch(pushStatusMessage(StatusMessages.SERVICE_UPDATE_SUCCESS, MessageType.SUCCESS));
			}
		}
		else {
			dispatch(pushStatusMessage(StatusMessages.SERVICE_UPDATE_FAILURE, MessageType.ERROR));
		}
	} catch (err) {
		dispatch(networkRequest('Failed to patch Service data.', false, id));
		dispatch(defaultNetworkErrMessage());
	}
};

export const postService = (service: Service) => async (dispatch, getState) => {
	const { id } = dispatch(networkRequest(null, true));

	try {
		const httpConfig: RequestInit = getHttpConfig('POST', {service});
		const jwt = getState().application.jwt;

		const response = await fetchAuthenticated('/services', jwt, httpConfig);
		dispatch(networkRequest(null, false, id));

		if(response.ok && response.status < 400) {
			const data = await response.json();
			if(data.service) {
				dispatch(createService(data.service));
				dispatch(pushStatusMessage(StatusMessages.SERVICE_CREATION_SUCCESS, MessageType.SUCCESS));
			}
		}
		else {
			dispatch(pushStatusMessage(StatusMessages.SERVICE_CREATION_FAILURE, MessageType.ERROR));
		}
	} catch (err) {
		dispatch(networkRequest('Failed to post Service data.', false, id));
		dispatch(defaultNetworkErrMessage());
	}
};

export const deleteServiceById = serviceId => async (dispatch, getState) => {
	const { id } = dispatch(networkRequest(null, true));

	try {
		const jwt = getState().application.jwt;
		const httpConfig: RequestInit = { method: 'DELETE' };

		const response = await fetchAuthenticated(`/services/${serviceId}`, jwt, httpConfig);
		dispatch(networkRequest(null, false, id));

		if(response.ok && response.status < 400) {
			const data = await response.json();

			if(data.result && data.result.ok === 1) {
				dispatch(removeService(serviceId));
				dispatch(pushStatusMessage(StatusMessages.SERVICE_DELETION_SUCCESS, MessageType.SUCCESS));
			}
		} else {
			dispatch(pushStatusMessage(StatusMessages.SERVICE_DELETION_FAILURE, MessageType.ERROR));
		}
	} catch (err) {
		dispatch(networkRequest('Failed to delete Service data.', false, id));
		dispatch(defaultNetworkErrMessage());
	}
};

export const deleteAllServices = () => async (dispatch, getState) => {
	const { id } = dispatch(networkRequest(null, true));

	try {
		const jwt = getState().application.jwt;
		const httpConfig: RequestInit = { method: 'DELETE' };
		const response = await fetchAuthenticated('/services', jwt, httpConfig);
		dispatch(networkRequest(null, false, id));

		if(response.ok && response.status < 400) {
			const data = await response.json();

			if(data.result && data.result.ok === 1) {
				dispatch(removeAllServices());
				dispatch(pushStatusMessage(StatusMessages.SERVICE_DELETE_ALL_SUCCESS, MessageType.SUCCESS));
			}
		} else {
			dispatch(pushStatusMessage(StatusMessages.SERVICE_DELETE_ALL_FAILRE, MessageType.ERROR));
		}

	} catch (err) {
		dispatch(networkRequest('Failed to delete Services data.', false, id));
		dispatch(defaultNetworkErrMessage());
	}
};