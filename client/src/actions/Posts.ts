import { Data } from '../constants/ActionTypes';

import { fetchHttp, fetchAuthenticated, getHttpConfig } from '../core/Http';
import { pushStatusMessage, defaultNetworkErrMessage } from '../actions/UI';
import { networkRequest } from '../actions/Application';
import { StatusMessages, MessageType } from '../constants/UIConstants';

export const retrieveNextPosts = (posts: Post[]) => {
	return {
		type: Data.RETRIEVE_NEXT_POSTS,
		posts
	};
};

export const createPost = (post: Post) => {
	return {
		type: Data.CREATE_POST,
		post
	};
};

export const updatePost = (post: Post) => {
	return {
		type: Data.UPDATE_POST,
		post
	};
};

export const removePost = (id: string) => {
	return {
		type: Data.REMOVE_POST,
		id
	};
};

export const removeAllPosts = () => {
	return { type: Data.REMOVE_POSTS };
};

export const getPostsLazily = (skip: number, limit = 5) => async (dispatch, getState) => {
	const { id } = dispatch(networkRequest(null, true));
	try {
		const response = await fetchHttp(`/posts?skip=${skip}&limit=${limit}`);
		dispatch(networkRequest(null, false, id));
		if(response.ok && response.status < 400) {
			const data = await response.json();
			if(data.posts)
				dispatch(retrieveNextPosts(data.posts));
		}
		else {
			dispatch(pushStatusMessage(StatusMessages.POST_FETCH_FAILURE, MessageType.ERROR));
		}
	} catch (err) {
		console.log(err);
		dispatch(networkRequest(`Failed to fetch data for Posts.`, false, id));
		dispatch(defaultNetworkErrMessage());
	}
};

export const postPost = (post: Post) => async (dispatch, getState) => {
	const { id } = dispatch(networkRequest(null, true));

	try {
		const httpConfig: RequestInit = getHttpConfig('POST', { post });
		const jwt = getState().application.jwt;

		const response = await fetchAuthenticated('/posts', jwt, httpConfig);
		dispatch(networkRequest(null, false, id));

		if(response.ok && response.status < 400) {
			const data = await response.json();
			if(data.post) {
				dispatch(createPost(data.post));
				dispatch(pushStatusMessage(StatusMessages.POST_CREATION_SUCCESS, MessageType.SUCCESS));
			}
		}
		else {
			dispatch(pushStatusMessage(StatusMessages.POST_CREATION_FAILRURE, MessageType.ERROR));
		}
	} catch (err) {
		dispatch(networkRequest('Failed to post Post data.', false, id));
		dispatch(defaultNetworkErrMessage());
	}
};

export const patchPostById = (post: Post) => async (dispatch, getState) => {
	const { id } = dispatch(networkRequest(null, true));

	if(!post._id)
		throw new Error('Cannot patch post. No valid id was found.');

	try {
		const httpConfig: RequestInit = getHttpConfig('PATCH', {post});
		const jwt = getState().application.jwt;

		const response = await fetchAuthenticated(`/posts/${post._id}`, jwt, httpConfig);
		
		dispatch(networkRequest(null, false, id));

		if(response.ok && response.status < 400) {
			const data = await response.json();
			if(data.result && data.result.ok === 1) {
				dispatch(updatePost(post));
				dispatch(pushStatusMessage(StatusMessages.POST_UPDATE_SUCCESS, MessageType.SUCCESS));
			}
		}
		else {
			dispatch(pushStatusMessage(StatusMessages.POST_UPDATE_FAILURE, MessageType.ERROR));
		}
	} catch (err) {
		dispatch(networkRequest('Failed to patch Post data.', false, id));
		dispatch(defaultNetworkErrMessage());
	}
};

export const deletePostById = postId => async (dispatch, getState) => {
	const { id } = dispatch(networkRequest(null, true));

	try {
		const jwt = getState().application.jwt;
		const httpConfig: RequestInit = { method: 'DELETE' };

		const response = await fetchAuthenticated(`/posts/${postId}`, jwt, httpConfig);
		dispatch(networkRequest(null, false, id));

		if(response.ok && response.status < 400) {
			const data = await response.json();

			if(data.result && data.result.ok === 1) {
				dispatch(removePost(postId));
				dispatch(pushStatusMessage(StatusMessages.POST_DELETION_SUCCESS, MessageType.SUCCESS));
			}
		} else {
			dispatch(pushStatusMessage(StatusMessages.POST_DELETION_FAILURE, MessageType.ERROR));
		}
	} catch (err) {
		dispatch(networkRequest('Failed to delete Post data.', false, id));
		dispatch(defaultNetworkErrMessage());
	}
};

export const deleteAllPosts = () => async (dispatch, getState) => {
	const { id } = dispatch(networkRequest(null, true));

	try {
		const jwt = getState().application.jwt;
		const httpConfig: RequestInit = { method: 'DELETE' };
		const response = await fetchAuthenticated('/posts', jwt, httpConfig);
		dispatch(networkRequest(null, false, id));

		if(response.ok && response.status < 400) {
			const data = await response.json();

			if(data.result && data.result.ok === 1) {
				dispatch(removeAllPosts());
				dispatch(pushStatusMessage(StatusMessages.POST_DELETE_ALL_SUCCESS, MessageType.SUCCESS));
			}
		} else {
			dispatch(pushStatusMessage(StatusMessages.POST_DELETE_ALL_FAILURE, MessageType.ERROR));
		}

	} catch (err) {
		dispatch(networkRequest('Failed to delete Posts data.', false, id));
		dispatch(defaultNetworkErrMessage());
	}
};