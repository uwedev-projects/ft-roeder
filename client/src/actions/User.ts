import { User } from '../constants/ActionTypes';
import { networkRequest, deferredFinish, receiveJwt } from './Application';
import { deferAction } from './Util';
import { pushStatusMessage } from './UI';
import { push } from 'react-router-redux';
import { Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { fetchHttp, fetchAuthenticated } from '../core/Http';
import { MessageType, StatusMessages } from '../constants/UIConstants';

export const receiveUser = (user: User) => {
	return {
		type: User.RECEIVE_USER_DATA,
		user
	};
};

export const signOut = () => {
	return (dispatch: Dispatch<any>) => {
		dispatch({ type: User.SIGN_OUT_USER });
		dispatch(push('/'));
	};
};

export const login = (credentials: Credentials) => {
	return async (dispatch: Dispatch<any>, getState: Function) => {
		try {
			dispatch({ type: User.SIGN_IN_USER });
			await dispatch(authenticate(credentials));
			await dispatch(fetchAdminUser(credentials.userName));
			dispatch(push('/admin/dashboard'));
		} catch (err) {
			let errorMessage = null;
			switch(err.message) {
				case '400':
				case '401':
				case '404':
					errorMessage = StatusMessages.BAD_CREDENTIALS_ERROR;
					break;
				default:
					errorMessage = StatusMessages.DEFAULT_NETWORK_ERROR;
			};
			dispatch(pushStatusMessage(
				errorMessage, 
				MessageType.ERROR
			));
		}
	};
};

export const authenticate = (credentials: Credentials) => {
	return async (dispatch: any, getState: Function) => {
		const { id } = dispatch(networkRequest(null, true));
		const httpConfig: RequestInit = {
			method: 'POST',
			body: JSON.stringify({credentials})
		};

		try {
			const response: Response = await fetchHttp('/auth', httpConfig);
			if(response.ok) {
				dispatch(
					deferAction(networkRequest(null, false, id), 500)
				);
				const data = await response.json();
				dispatch(receiveJwt(data.jwt));
			}
			else if(response.status >= 400 && response.status <= 500) {
				throw new Error(`${response.status}`);
			}
		} catch (err) {
			dispatch(networkRequest('Failed to fetch JSON webtoken.', false, id));
			throw err;
		}
	};
};

export const fetchAdminUser = (userName: string) => {
	return async (dispatch: Dispatch<any>, getState: Function) => {
		const startNetworkRequestAction = networkRequest(null, true);
		const { id } = startNetworkRequestAction;
		dispatch(startNetworkRequestAction);

		const jwt = getState().application.jwt;
		try {
			const response: Response = await fetchAuthenticated(`/admin-users/${userName}`, jwt);
			if(response.ok) {
				dispatch(networkRequest(null, false, id));
				const data = await response.json();
				dispatch(receiveUser(data.adminUser));
			}
			else
				throw new Error(`${response.status}`);
		} catch (err) {
			dispatch(networkRequest('Failed to fetch AdminUser data.', false, id));
			throw err;
		}
	};
};