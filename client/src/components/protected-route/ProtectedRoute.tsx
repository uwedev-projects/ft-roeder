import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Location } from 'history';

export interface ProtectedRouteProps {
	user?: AdminUser;
	location?: Location;
};

const mapStateToProps = (state: GlobalState) => {
	return {
		user: state.user,
		location: state.router.location
	};
};

export const protectedRoute = (WrappedComponent) => {
	class ProtectedComponent extends React.Component<ProtectedRouteProps, any> {
		constructor(props) { super(props) };

		public render(): JSX.Element {
			if(this.props.user)
				return <WrappedComponent {... this.props}/>;
			else
				return <Redirect to="/sign-in"/>;
		};
	};

	return connect(mapStateToProps, null, null)(ProtectedComponent);
};