import './NavBar.scss';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { navBarEntries, navBarAdminEntries } from '../../core/Routes';

export interface NavBarProps {
	activeRoute: string;
	isAuthenticated: boolean;
}

export const NavBar: React.StatelessComponent<NavBarProps> = props => {
	const navEntries = props.isAuthenticated ? 
		navBarAdminEntries : navBarEntries;
	const entries: JSX.Element[] = navEntries.map((entry) => {
		const key = entry.path;
		let className = 'nav-bar-entry';
		if(entry.path === props.activeRoute)
			className += ' active';
		return (
			<li className={className} key={key}> 
					<Link to={entry.path}> {entry.name} </Link> 
			</li>
		);
	});

	return (
		<nav className="nav-bar-wrapper">
			<ul className="nav-bar">
				{ entries }
			</ul>
		</nav>
	);
};