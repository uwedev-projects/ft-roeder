import './RangeSlider.scss';
import * as React from 'react';
import { Motion, spring } from 'react-motion';
import { NumberInput } from '../number-input/NumberInput';

export interface RangeSliderProps {
	min: number;
	max: number;
	initialValue?: number;
	step?: number;
	onChange?: (value: number) => void;
	disabled?: boolean;
};

export interface RangeSliderState {
	value: number;
	lastValue: number;
	width: number;
	left: number;
	movementStart: number;
	steppedValue: number;
	resizeThrottleTimer: any;
};

export class RangeSlider extends React.PureComponent<RangeSliderProps, RangeSliderState> {
	private static readonly HORIZONTAL_PADDING = 10;
	private areaRef: HTMLElement = null;
	private sliderRef: HTMLElement = null;

	constructor(props) {
		super(props);
		const { initialValue, min, step } = props;
		const value = props.disabled ? min : initialValue || min;
		this.state = {
			value,
			lastValue: null,
			width: 1,
			left: 0,
			movementStart: null,
			steppedValue: initialValue && step ? initialValue - initialValue % step : null,
			resizeThrottleTimer: null
		};
	};

	public componentDidMount(): void {
		window.addEventListener('resize', ev => this.handleResize(ev));
		this.updateWidthAndLeft();
	};

	public componentWillReceiveProps(nextProps, nextState): void {
		if(nextState.value !== this.state.value)
			this.setState({ lastValue: this.state.value });
	};

	// public shouldComponentUpdate(nextProps: RangeSliderProps, nextState: RangeSliderState): boolean {
	// 	if(nextProps.initialValue !== this.props.initialValue && Math.abs(this.props.initialValue - nextProps.initialValue) >)
	// 	return true;
	// };

	private updateWidthAndLeft(): void {
		let { width, left } = this.areaRef.getBoundingClientRect();
		width -= RangeSlider.HORIZONTAL_PADDING;
		if(width !== this.state.width || left !== this.state.left)
			this.setState({ width, left });
	};

	private updateValueFromPosition(xPos: number, useSteps?: boolean): void {
		const { min, max, step } = this.props;
		const { width, left } = this.state;

		const progress = (xPos - left) /width;
		let value = Math.round(
			Math.min(max, (max - min) * progress + min)
		);

		if(useSteps) {
			if(value % step < step / 2)
				value -= value % step;
			else
				value += step - value % step;
		}
		if(value !== this.state.value) {
			if(this.props.onChange)
				this.props.onChange(value);
			this.setState({ value });
		}
	};

	private getSliderTranslation(value: number): number {
		const { width } = this.state;
		const { min, max } = this.props;
		const translation = Math.max(0, value - min) / (max - min) * width
		 + .5 * RangeSlider.HORIZONTAL_PADDING;
		return Math.round(translation * 100) / 100;
	};

	private handleSliderMoveStart(xPos: number): void {
		if(!this.props.disabled) {
			this.setState({ movementStart: xPos });
			this.areaRef.focus();
			this.sliderRef.focus();
		}
	};

	private handleSliderMove(xPos: number): void {
		if(!this.props.disabled && this.state.movementStart !== null)
			this.updateValueFromPosition(xPos);
	};

	private handleSliderMoveEnd(xPos: number): void {
		this.setState({ movementStart: null });
		if(!this.props.disabled && this.props.step)
			this.updateValueFromPosition(xPos, true);
	};

	private handleAreaClick(xPos: number): void {
		this.setState({ movementStart: null });
		if(!this.props.disabled) {
			if(this.props.step)
				this.updateValueFromPosition(xPos, true);
			else
				this.updateValueFromPosition(xPos);
		}
	};

	private handleSliderValueChange(enteredValue: number): void {
		const { step } = this.props;
		let value = enteredValue - enteredValue % step;
		if(enteredValue % step >= .5 * step) {
			value = enteredValue + step - enteredValue % step;
		}
		
		if(value !== this.state.value) {
			this.setState({ value });
			if(this.props.onChange)
				this.props.onChange(value);
		}
	};

	private handleKeyDown(key: string): void {
		if(!this.props.disabled) {
			switch(key) {
				case 'ArrowRight':
					this.incrementByOneStep();
					break;
				case 'ArrowLeft':
					this.decrementByOneStep();
					break;
			};
		}
	};

	private handleWheel(ev: React.WheelEvent<HTMLElement>): void {
		if(!this.props.disabled) {
			ev.preventDefault();
			if(ev.deltaY > 0)
				this.decrementByOneStep();
			else
				this.incrementByOneStep();
		}
	};

	private handleResize(ev): void {
		if(!this.props.disabled && !this.state.resizeThrottleTimer) {
			const timer = setTimeout(() => {
				this.updateWidthAndLeft();
				this.setState({ resizeThrottleTimer: null });
			}, 66);
			this.setState({ resizeThrottleTimer: timer });
		}
	};

	private incrementByOneStep(): void {
		const { value } = this.state;
		this.setState({ value: Math.min(this.props.max, value + this.props.step) });
		this.sliderRef.focus();
		this.areaRef.focus();
	};

	private decrementByOneStep(): void {
		const { value } = this.state;
		this.setState({ value: Math.max(this.props.min, value - this.props.step) });
		this.sliderRef.focus();
		this.areaRef.focus();
	};

	public render(): JSX.Element {
		const rangeSliderClass = this.props.disabled ? 'range-slider disabled' : 'range-slider';
		return (
			<span
				ref={node => this.sliderRef = node}
				className={rangeSliderClass}
				tabIndex={0}
				onKeyDown={ev => this.handleKeyDown(ev.key)}
				onWheel={ev => this.handleWheel(ev)}
			>
				<span className="area-wrapper" ref={node => this.areaRef = node}>
					<RangeArea
						sliderTranslation={this.getSliderTranslation(this.state.value)}
						lastTranslation={this.getSliderTranslation(this.state.lastValue)}
						onSliderMoveStart={xPos => this.handleSliderMoveStart(xPos)}
						onSliderMove={xPos => this.handleSliderMove(xPos)}
						onSliderMoveEnd={xPos => this.handleSliderMoveEnd(xPos)}
						onAreaClick={xPos => this.handleAreaClick(xPos)}
					/>
				</span>
				<section className="value-wrapper">
					  <SliderValue
						value={this.state.value}
						min={this.props.min}
						max={this.props.max}
						onChange={value => this.handleSliderValueChange(value)}
					/>  
				</section>
			</span>
		);
	};
};

export interface RangeAreaProps {
	sliderTranslation: number;
	lastTranslation: number;
	onSliderMoveStart: (xPos: number) => void;
	onSliderMove: (xPos: number) => void;
	onSliderMoveEnd: (xPos: number) => void;
	onAreaClick: (xPos: number) => void;
};

export const RangeArea: React.StatelessComponent<RangeAreaProps> = props => {
	const springConfig = { stiffness: 120, damping: 17 };
	const initialStyle = { translation: props.lastTranslation };
	const destStyle = { translation: spring(props.sliderTranslation, springConfig) };

	return (
		<section className="range-area"
			onMouseDown={(ev) => props.onSliderMoveStart(ev.clientX)}
			onTouchStart={ev =>	props.onSliderMoveStart(ev.changedTouches[0].clientX)}
			onMouseMove={ev => props.onSliderMove(ev.clientX)}
			onTouchMove={ev => props.onSliderMove(ev.changedTouches[0].clientX)}
			onMouseUp={ev => props.onAreaClick(ev.clientX)}
			onTouchEnd={ev => props.onSliderMoveEnd(ev.changedTouches[0].clientX)}
		>
			<section className="background-area"/>
			<Motion defaultStyle={initialStyle} style={destStyle}>
				{ interpolated => 
					<span>
						<SliderCircle xTranslation={interpolated.translation}/>
						<section className="slider-progress" style={{ width: `${interpolated.translation}px` }}/>
					</span>
				}
			</Motion>
		</section>
	);
};

export interface SliderCircleProps {
	xTranslation: number;
};

export const SliderCircle: React.StatelessComponent<SliderCircleProps> = props => {
	const circleStyle = { transform: `translateX(${props.xTranslation}px)`};
	return (
		<section className="slide-circle" style={circleStyle}/>
	);
};

export interface SliderValueProps {
	value: number;
	min: number;
	max: number;
	onChange: (value: number) => void;
};

export const SliderValue: React.StatelessComponent<SliderValueProps> = props => {
	return (
		<section className="slider-value">
			<NumberInput
				min={props.min}
				max={props.max}
				initialValue={props.value}
				onChange={value => props.onChange(value)}
			/>
		</section>
	);
};