/// <reference path="../../index.d.ts"/>

import './Footer.scss';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { footerEntries } from '../../core/Routes';

export const Footer: React.StatelessComponent<undefined> = () => {
	const navEntries: JSX.Element[] = footerEntries
		.map(entry => <FooterNavEntry entry={entry} key={entry.path}/>);
	return (
		<footer className="footer row">
			<ul className="footer-entry-list"> { navEntries } </ul>
		</footer>
	);
};

export interface FooterNavEntryProps {
	entry: NavMenuEntry;
};

export const FooterNavEntry: React.StatelessComponent<FooterNavEntryProps> = props => {
	return (
		<li className="footer-nav-entry">
			<Link to={props.entry.path}> { props.entry.name } </Link>
		</li>
	);
};