import * as React from 'react';
import { createDevTools, IDevTools } from 'redux-devtools';
import LogMonitor from 'redux-devtools-log-monitor';
import DockMonitor from 'redux-devtools-dock-monitor';

export const DevTools: IDevTools = createDevTools(
	<DockMonitor 	toggleVisibilityKey="ctrl-x"
								changePositionKey="alt-x"
								defaultIsVisible={false}
								defaultPosition="bottom">
		<LogMonitor theme="twilight" />
	</DockMonitor>
);