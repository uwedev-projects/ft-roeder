import './NumberInput.scss';
import * as React from 'react';

export interface NumberInputProps {
	onChange?: (value: number) => void;
	initialValue?: number;
	min?: number;
	max?: number;
	disabled?: boolean;
};

export interface NumberInputState {
	value: number;
};

export class NumberInput extends React.Component<NumberInputProps, NumberInputState> {
	private inputRef: HTMLElement = null;

	constructor(props) {
		super();
		const { initialValue, min } = props;
		this.state = { value: initialValue || min || null };
	};

	public focus(): void {
		this.inputRef.focus();
	};

	public componentWillReceiveProps(nextProps: NumberInputProps): void {
		if(nextProps.initialValue !== this.props.initialValue) {
			this.setValue(nextProps.initialValue);
		}
	};

	private setValue(value: number): void {
		value = Math.min(this.props.max, value);
		value = Math.max(this.props.min, value);
		
		this.setState({ value });
		
		if(this.props.onChange)
			this.props.onChange(value);
	};

	private handleChange(ev: React.ChangeEvent<HTMLInputElement>): void {
		if(!this.props.disabled) {
			if(ev.target.value === '')
				this.setState({ value: null });

			const value = Number.parseInt(ev.target.value);
			if(Number.isInteger(value)) {
				this.setValue(value);
			}
		}
	};

	public render(): JSX.Element {
		return (
			<span className="number-input">
				<input
					ref={node => this.inputRef = node}
					onChange={ev => this.handleChange(ev)}
					value={this.state.value || ''}
				/>
			</span>
		);
	};
};