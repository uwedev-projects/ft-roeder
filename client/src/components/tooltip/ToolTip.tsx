import './ToolTip.scss';
import * as React from 'react';
import { TOOLTIP_DELAY } from '../../constants/UIConstants';

export interface ToolTipState {
	xPos: number;
	yPos: number;
	isVisible: boolean;
	timerId: any;
};

export interface ToolTipProps {
	text: string;
};

export class ToolTip extends React.PureComponent<ToolTipProps, ToolTipState> {
	private tooltipRef: HTMLElement = null;
	private screenWidth: number;

	constructor() {
		super();
		this.state = {
			isVisible: false,
			timerId: null,
			xPos: null,
			yPos: null
		};
	};

	public componentDidMount(): void {
		this.screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	};
	
	private handleMouseEnter(ev) {
		const { clientX, clientY } = ev;
		const xPos = clientX;
		const yPos = clientY;
		const timerId = setTimeout(() => {
			this.setState({
				xPos, yPos,
				isVisible: true,
				timerId: null
			});	
		}, TOOLTIP_DELAY);
		this.setState({ timerId });
	};

	private handleMouseMove(ev) {
		const { clientX, clientY } = ev;
		this.setState({ xPos: clientX, yPos: clientY });
	};

	private handleMouseLeave(ev) {
		if(this.state.timerId)
			clearTimeout(this.state.timerId);
		this.setState({ isVisible: false, timerId: null });
	};

	public render(): JSX.Element {
		let tooltipClass = 'tooltip-main';
		let tooltipStyle = {
			left: `${Math.min(this.state.xPos, this.screenWidth - 180)}px`,
			top: `${this.state.yPos}px`
		};

		if(this.state.isVisible) 
			tooltipClass += ' visible';
			
		return (
			<aside 
				className="tooltip-wrapper"
				onMouseEnter={ev => this.handleMouseEnter(ev)}
				onMouseMove={ev => this.handleMouseMove(ev)}
				onMouseLeave={ev => this.handleMouseLeave(ev)}
			>
				<section className="content-wrapper"> { this.props.children } </section>
				<section className={tooltipClass} style={tooltipStyle} ref={node => this.tooltipRef = node}>
					<span className="icon-wrapper">
						<i className="fa fa-info-circle info-icon"/>
					</span>
					<span className="text-wrapper">
				 		<p className="tooltip-text">	{ this.props.text } </p>
					</span>
				</section>
			</aside>
		);
	};
};