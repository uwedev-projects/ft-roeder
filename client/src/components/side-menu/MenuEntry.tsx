/// <reference path="../../index.d.ts"/>

import * as React from 'react';
import { Link } from 'react-router-dom';

export interface MenuEntryProps {
	entry: NavMenuEntry;
	onClick: (ev: React.MouseEvent<any>) => void;
};

export const MenuEntry: React.StatelessComponent<MenuEntryProps> = (props) => {
	let className = "row menu-entry ";
	if(props.entry.active)
		className += 'active';
	return (
		<section
			className={className}
			role="sidemenu"
			onMouseDown={ev => props.onClick(ev)}
			itemProp="itemListElement"
		>
			<Link to={props.entry.path}> 
				<section className="icon">
					<i className={props.entry.iconClass} aria-hidden="true"/>
				</section>
				<section className="entry">
				 <span>	{props.entry.name} </span>
				</section>
			</Link>
		</section>
	);
};