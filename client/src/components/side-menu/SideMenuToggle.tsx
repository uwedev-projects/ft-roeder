import * as React from 'react';
import { IconButton } from '../icon-button/IconButton';
import { SideMenu } from './SideMenu';

export interface SideMenuToggleProps {
	isAuthenticated: boolean;
	activeRoute: string;
};

export interface SideMenuToggleState {
	isExpanded: boolean;
};

export class SideMenuToggle extends React.Component<SideMenuToggleProps, SideMenuToggleState> {
	constructor(props) {
		super(props);
		this.state = { isExpanded: false };
	};

	private toggleMenu(): void {
		this.setState({ isExpanded: !this.state.isExpanded });
	};

	public render(): JSX.Element {
		return (
			<section>
				<IconButton
					iconClass="fa fa-bars fa-2x menu-icon"
					onClick={() => this.toggleMenu()}
				/>
				<SideMenu
					isExpanded={this.state.isExpanded}
					hideMenu={() => this.toggleMenu()}
					activeRoute={this.props.activeRoute}
					isAuthenticated={this.props.isAuthenticated}
				/>
			</section>
		);
	};
};