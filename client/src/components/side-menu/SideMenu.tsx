import './SideMenu.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { Motion, spring, presets, SpringHelperConfig } from 'react-motion';
import { MenuEntry } from './MenuEntry';
import { IconButton } from '../icon-button/IconButton';
import { sideMenuEntries, adminSideMenuEntries } from '../../core/Routes';

export interface SideMenuProps {
	isAuthenticated: boolean;
	hideMenu: () => void;
	isExpanded: boolean;
	activeRoute: string;
};

export interface SideMenuState {
	destTranslation: number;
	prevTranslation: number;
	renderedOnce: boolean;
	isMoving: boolean;
	touchStartTranslation: number;
	touchTranslation: number;
	entries: JSX.Element[];
	defaultStyle: any;
	destStyle: any;
};

export class SideMenu extends React.Component<SideMenuProps, SideMenuState> {
	private static readonly MENU_WIDTH = 320;
	private static readonly SPRING_CONFIG_EXPAND = {
		stiffness: 55,
		damping: 13
	};

	private static readonly SPRING_CONFIG_COLLAPSE = {
		stiffness: 40,
		damping: 8
	};
	private static readonly COLLAPSED_TRANSLATION = -1 * SideMenu.MENU_WIDTH - 10;

	constructor(props) {
		super(props);

		const navEntries = props.isAuthenticated ? adminSideMenuEntries : sideMenuEntries;
		const entries = this.getEntries(navEntries, props.activeRoute);

		const defaultTranslation = SideMenu.COLLAPSED_TRANSLATION;

		this.state = {
			destTranslation: defaultTranslation,
			prevTranslation: defaultTranslation,
			renderedOnce: false,
			isMoving: false,
			touchStartTranslation: 0,
			touchTranslation: 0,
			entries,
			defaultStyle: {
				translation: defaultTranslation
			},
			destStyle: {
				translation: spring(defaultTranslation, SideMenu.SPRING_CONFIG_COLLAPSE)
			}
		};
	};

	private getEntries(navEntries: NavMenuEntry[], activeRoute: string): JSX.Element[] {
		return navEntries.map((entry, index) => {
			entry = Object.assign({}, entry, {active: entry.path === activeRoute});
			return (
				<MenuEntry
					key={`${entry.name}${index}`}
					entry={entry}
					onClick={(ev) => this.handleEntryClicked(ev)}
				/>
			);
		});
	};

	private collapseMenuFrom(prevTranslation: number): void {
		this.setState({
			destTranslation: SideMenu.COLLAPSED_TRANSLATION,
			touchTranslation: SideMenu.COLLAPSED_TRANSLATION,
			prevTranslation,
			defaultStyle: {
				translation: prevTranslation
			},
			destStyle: {
				translation: spring(SideMenu.COLLAPSED_TRANSLATION, SideMenu.SPRING_CONFIG_COLLAPSE)
			}
		});
	};

	private expandMenuFrom(prevTranslation: number): void {
		this.setState({
			destTranslation: 0,
			touchTranslation: 0,
			prevTranslation,
			defaultStyle: {
				translation: SideMenu.COLLAPSED_TRANSLATION
			},
			destStyle: {
				translation: spring(0, SideMenu.SPRING_CONFIG_EXPAND)
			}
		});
	};

	public componentWillReceiveProps(nextProps: SideMenuProps): void {
		if(!this.state.isMoving) {
			if(nextProps.isExpanded)
				this.expandMenuFrom(SideMenu.COLLAPSED_TRANSLATION);
			else if(this.state.prevTranslation === SideMenu.COLLAPSED_TRANSLATION)
				this.collapseMenuFrom(0);
		}

		if(nextProps.isAuthenticated !== this.props.isAuthenticated ||
			nextProps.activeRoute !== this.props.activeRoute) {
			const navEntries = nextProps.isAuthenticated ? adminSideMenuEntries : sideMenuEntries;
			const entries = this.getEntries(navEntries, nextProps.activeRoute);
			this.setState({ entries });
		}
	};

	public componentDidMount(): void {
		this.setState({ renderedOnce: true });
	};

	private handleTouchStart(ev: React.TouchEvent<HTMLElement>): void {
		if(!this.state.isMoving && this.props.isExpanded) {
			const xPos = ev.changedTouches[0].clientX;
			this.setState({
				isMoving: true,
				touchStartTranslation: xPos
			});
		}
	};

	private handleTouchMove(ev: React.TouchEvent<HTMLElement>): void {
		if(this.state.isMoving) {
			const xPos = ev.changedTouches[0].clientX;
			const touchTranslation = Math.min(xPos - this.state.touchStartTranslation, 0);
			this.setState({ touchTranslation });
		}
	};

	private handleTouchCancel(ev: React.TouchEvent<HTMLElement>): void {
		this.setState({ isMoving: false });
	};

	private handleTouchEnd(ev: React.TouchEvent<HTMLElement>): void {
		if(this.state.isMoving && this.props.isExpanded) {
			const xPos = ev.changedTouches[0].clientX;
			let translation = xPos - this.state.touchStartTranslation;
			console.log(translation);
			if(translation >= -200) {
				console.log('Resetting ...');
				this.setState({
					touchTranslation: 0,
					prevTranslation: SideMenu.COLLAPSED_TRANSLATION
				});
			}
			else {
				translation = Math.max(translation, SideMenu.COLLAPSED_TRANSLATION);
				this.collapseMenuFrom(translation);
				this.props.hideMenu();
			}

			this.setState({ isMoving: false });
		}
	};

	private handleEntryClicked(ev: React.MouseEvent<HTMLElement>): void {
		if(this.state.isMoving)
			this.setState({ isMoving: false });
			
		// if(this.props.isExpanded) {
		// 	this.collapseMenuFrom(0);
		// 	this.props.hideMenu();
		// }
	};

	private getMenuStyle(translation: number): any {
		return {
			width: SideMenu.MENU_WIDTH,
			transform: `translateX(${this.state.isMoving ? 
				this.state.touchTranslation : translation}px)`
		};
	};

	public render(): JSX.Element {
		const defaultStyle = this.state.defaultStyle;
		const destStyle = this.state.destStyle;
		const overlayClass = this.state.destTranslation > SideMenu.COLLAPSED_TRANSLATION ? 
			'dark-overlay visible' : 'dark-overlay';

		return (
			<section>
				 <section className={overlayClass} onClick={() => this.props.hideMenu()}/> 
				<Motion
					defaultStyle={defaultStyle}
					style={destStyle}
					key={`${this.props.isExpanded}`}
				>
					{ interpolated => this.renderMenu(interpolated.translation) }
				</Motion>
			</section>
		);
	};

	private renderMenu(translation): JSX.Element {
		const menuStyle = this.getMenuStyle(translation);

		return (
			<nav
				className="side-menu"
				style={menuStyle}
				onTouchStart={(ev) => this.handleTouchStart(ev)}
				onTouchMove={(ev) => this.handleTouchMove(ev)}
				onTouchCancel={(ev) => this.handleTouchCancel(ev)}
				onTouchEnd={(ev) => this.handleTouchEnd(ev)}
			>
				<section className="wrapper">
					<header className="nav-header">
						<section className="nav-title">
							<h3> Navigation </h3>
						</section>
						<section className="close-menu" onTouchStart={ev => ev.stopPropagation()}>
							<IconButton iconClass="fa fa-times" onClick={() => this.props.hideMenu()}/>
						</section>
					</header>
					<main className="nav-content">
						{ this.state.entries } 
					</main>
				</section>
			</nav>
		);
	};
};