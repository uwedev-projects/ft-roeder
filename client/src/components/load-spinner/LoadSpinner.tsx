import * as React from 'react';
import { Motion, spring, SpringHelperConfig } from 'react-motion';

import { SpinnerShape } from './SpinnerShape';

export interface LoadSpinnerProps {
	radius: number;
	strokeWidth: number;
}

export interface LoadSpinnerState {
	motionKey: number;
	start: number;
	end: number;
	nextStart: number;
	nextEnd: number;
	iteration: number;
};

export class LoadSpinner extends React.PureComponent<LoadSpinnerProps, LoadSpinnerState> {
	private center: Vector2D;
	
	constructor(props) {
		super(props);
		this.state = {
			motionKey: 0,
			iteration: 0,
			start: 0,
			end: 0,
			nextStart: 0,
			nextEnd: .5
		};

		this.center = { x: props.radius, y: props.radius };
	};

	private mapProgressToVector(progress: number): Vector2D {
		let vector: Vector2D = { x: null, y: null };
		// using the parametric form
		// see https://en.wikipedia.org/wiki/Circle#Equations
		const angle = progress * 2 * Math.PI;

		const x: number = this.center.x + this.props.radius * Math.sin(angle);
		const y: number = this.center.y + this.props.radius * Math.cos((progress + .5) * 2 * Math.PI);
		vector.x = Math.round(x * 100) / 100;
		vector.y = Math.round(y * 100) / 100;

		return vector;
	};

	private toggleAnimation(): void {
		let start, end, nextStart, nextEnd;
		const iteration = (this.state.iteration + 1) % 4;

		if(iteration % 2 === 0) {
			start = this.state.start === 1 ? 0 : this.state.nextStart;
			end = this.state.end === 1 ? 0 : this.state.end;
			nextStart = this.state.nextStart;
			nextEnd = this.state.nextEnd === 1 ? .5 : this.state.nextEnd + .5;
		}
		else {
			start = this.state.start === 1 ? 0 : this.state.start;
			end = this.state.end === 1 ? 0 : this.state.nextEnd;
			nextStart = this.state.nextStart === 1 ? .5 : this.state.nextStart + .5;
			nextEnd = this.state.nextEnd;
		}

		this.setState({
			motionKey: this.state.motionKey ? 0 : 1,
			start,
			end,
			nextStart,
			nextEnd,
			iteration
		});
	};

	private getSpringConfig(): SpringHelperConfig {
		let config = { stiffness: 0, damping: 0, precision: 1 };
		if(this.state.iteration % 2 === 0) {
			config.stiffness = 180;
			config.damping = 25;
		}
		else {
			config.stiffness = 200;
			config.damping = 20;
		}
		return config;
	}

	public render(): JSX.Element {
		const springConfig = this.getSpringConfig();

		const initialStyle = {
			start: this.state.start,
			end: this.state.end
		};
		const destinationStyle = {
			start: spring(this.state.nextStart, springConfig),
			end: spring(this.state.nextEnd, springConfig)
		};
		return (
			<Motion
				key={this.state.motionKey}
				defaultStyle={initialStyle}
				style={destinationStyle}
				onRest={() => this.toggleAnimation()}
			>
				{ interpolatedStyle => 
					<SpinnerShape
						radius={this.props.radius}
						center={this.center}
						start={this.mapProgressToVector(interpolatedStyle.start)}
						end={this.mapProgressToVector(interpolatedStyle.end)}
						strokeWidth={this.props.strokeWidth}
					/> 
				}
			</Motion>
		);
	}
};