/// <reference path="../../index.d.ts"/>

import './LoadSpinner.scss';
import * as React from 'react';

export interface SpinnerShapeProps {
	center: Vector2D;
	radius: number;
	start: Vector2D;
	end: Vector2D;
	strokeWidth: number;
};

export const SpinnerShape: React.StatelessComponent<SpinnerShapeProps> = (props) => {
	const start: Vector2D = offsetByStrokeWidth(props.start, props.strokeWidth);
	const end: Vector2D = offsetByStrokeWidth(props.end, props.strokeWidth);
	let path: string = '';

	path += `M ${start.x} ${start.y} `;
	path += `A ${props.radius} ${props.radius} 0 0 1 `;
	path += `${end.x} ${end.y}`;

	const spinnerStyle = {
		width: `${props.radius*2 + props.strokeWidth*2}px`,
		height: `${props.radius*2 + props.strokeWidth*2}px`,
		left: `calc(50vw - ${props.radius}px)`,
		top: '40%'
	};

	return (
		<div className="load-spinner" style={spinnerStyle}>
			<svg>
				<path d={path} strokeWidth={props.strokeWidth}/>
			</svg>
		</div>
	);
};

const offsetByStrokeWidth = (vector: Vector2D, strokeWidth: number) => {
	return {
		x: vector.x + strokeWidth,
		y: vector.y + strokeWidth
	};
};