import './Slider.scss';
import * as React from 'react';
import { IconButton } from '../icon-button/IconButton';
import { RoundButton } from '../round-button/RoundButton';
import { Motion, spring, presets } from 'react-motion';

export interface SliderProps {
	slides: JSX.Element[];
	initialIndex?: number;
	wrap?: boolean;
	onSlide?: (movingForward: boolean) => void;
};

export interface SliderState {
	index: number;
	isMovingForward: boolean;
	isTransitioning: boolean;
};

export class Slider extends React.PureComponent<SliderProps, SliderState> {
	private readonly slideCount: number = this.props.slides.length;

	constructor(props) {
		super(props);
		this.state = {
			index: props.initialIndex || 0,
			isMovingForward: false,
			isTransitioning: false
		};
	};

	private getPreviousIndex(): number {
		return (this.state.index + this.slideCount - 1) % this.slideCount;
	};

	private getNextIndex(): number {
		return (this.state.index + 1) % this.slideCount;
	};

	private startTransition(isMovingForward = true): void {
		let index = isMovingForward ? this.getNextIndex() : this.getPreviousIndex();
		this.setState({
			index,
			isMovingForward,
			isTransitioning: true
		});
		if(this.props.onSlide)
			this.props.onSlide(isMovingForward);
	};

	private stopTransition(): void {
		this.setState({ isTransitioning: false });
	};

	private shouldPrevButtonRender(): boolean {
		if(this.props.slides.length > 1)
			return this.props.wrap || this.state.index !== 0;
		else return false;
	};

	private shouldNextButtonRender(): boolean {
		if(this.props.slides.length > 1)
			return this.props.wrap || this.state.index + 1 < this.props.slides.length;
		else return false;
	};

	private shouldSlideRender(index: number): boolean {
		return index === this.state.index || index === this.getNextIndex() || index === this.getPreviousIndex();
	};

	public render(): JSX.Element {
		const navButtons: JSX.Element = (
			<nav className="nav-buttons">
				<span className="prev-wrapper">
				{
					this.shouldPrevButtonRender() && 
						<RoundButton
							className="prev-btn"
							iconClass="fa fa-chevron-left"
							onClick={() => this.startTransition(false)}
						/>
				}
				</span>
				<span className="next-wrapper">
				{
					this.shouldNextButtonRender() &&
						<RoundButton
							className="next-btn"
							iconClass="fa fa-chevron-right"
							onClick={() => this.startTransition()}
						/>
				}
				</span>
			</nav>
		);

		let slides: JSX.Element[] = this.props.slides
			.map((_, index) => index)
			.filter(index => this.shouldSlideRender(index))
			.map(index => this.renderSlide(index));

		return (
			<section className="slider">
				<section className="slides">
					{ slides }
				</section>
				<section>
					{ navButtons }
				</section>
			</section>
		);
	};

	private renderMotion(index: number, defaultStyle, destStyle, onRest?: Function): JSX.Element {
		return (
			<Motion
				key={index}
				defaultStyle={defaultStyle}
				style={destStyle}
				onRest={() => onRest ? onRest() : null}
			>
				{ interpolated => 
					<Slide
						index={index}
						translation={interpolated.translation}
						activeIndex={this.state.index}
					> 
						{ this.props.slides[index] } 
					</Slide> 
				}
			</Motion>
		);
	};

	private renderSlide(index: number): JSX.Element {
		let defaultStyle = { translation: -100 };
		let destStyle = { translation: spring(0, presets.noWobble) };

		if(index === this.state.index) {
			if(this.state.isMovingForward)
				defaultStyle = { translation: 100 };
			return this.renderMotion(index, defaultStyle, destStyle, () => this.stopTransition());
		}
		else {
			if(this.state.isMovingForward) {
				if(index === this.getPreviousIndex()) {
					defaultStyle = { translation: 0 };
					destStyle = { translation: spring(-100, presets.noWobble) };
					return this.renderMotion(index, defaultStyle, destStyle);
				}
			}
			else if(index === this.getNextIndex()) {
				defaultStyle = { translation: 0 };
				destStyle = { translation: spring(100, presets.noWobble) };
				return this.renderMotion(index, defaultStyle, destStyle);
			}
			else return (
				<Slide
					index={index}
					translation={this.getPreviousIndex() === index ? 100 : -100}
					activeIndex={this.state.index}
					key={index}
				>
					{ this.props.slides[index] }
				</Slide>
			);
		}
	};
};

export interface SlideProps {
	index: number;
	activeIndex: number;
	translation: number;
};

export const Slide: React.StatelessComponent<SlideProps> = props => {
	const style = { transform: `translateX(${props.translation}%)` };
	let className = 'slide '
		
	if(props.index === props.activeIndex)
		className += 'active';
	else if(props.index < props.activeIndex)
		className += 'prev'
	else className += 'next'

	return (
		<section className={className} style={style}>
			{ props.children }
		</section>
	);
};