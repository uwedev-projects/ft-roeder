import './TimedContentSwitch.scss';
import * as React from 'react';
import { spring, TransitionMotion } from 'react-motion';

export interface TimedContentSwitchProps {
	content: JSX.Element[];
	switchDelay: number;
};

export interface TimedContentSwitchState {
	activeIndex: number;
	intervalId: any;
};

export class TimedContentSwitch extends React.Component<TimedContentSwitchProps, TimedContentSwitchState> {
	private static readonly springConfig = { stiffness: 42, damping: 12 };
	constructor() {
		super();
		this.state = { activeIndex: 0, intervalId: null };
	};

	public componentDidMount(): void {
		const intervalId = setInterval(() => {
			const activeIndex = (this.state.activeIndex + 1) % this.props.content.length;
			this.setState({ activeIndex });
		}, this.props.switchDelay * 1000);

		this.setState({ intervalId });
	};

	public componentWillUnmount(): void {
		clearInterval(this.state.intervalId);
	}

	private getDefaultStyles(): any {
		return [
			{
				key: this.state.activeIndex.toString(),
				style: { translation: 100 } 
			}
		];
	};

	private getStyles(): any {
		return [
			{
				key: this.state.activeIndex.toString(),
				style: { translation: spring(0, TimedContentSwitch.springConfig) } 
			}
		];
	};

	private willLeave(): any {
		return { translation: spring(-100, TimedContentSwitch.springConfig) };
	};

	private willEnter(): any {
		return { translation: 100 };
	};

	public render(): JSX.Element {

		return (
			<TransitionMotion
				defaultStyles={this.getDefaultStyles()}
				styles={this.getStyles()}
				willEnter={this.willEnter}
				willLeave={this.willLeave}
			>
			{
				interpolated => 
					<section className="timed-content-switch">
						{ interpolated.map(config => (
							<section
								key={config.key}
								className="content-wrapper"
								style={{transform: `translateX(${config.style.translation}%)`}}
							>
								{ this.props.content[this.state.activeIndex] }
							</section>
						)) }
					</section>
			}
			</TransitionMotion>
		);
	};
};