import './IconButton.scss';
import * as React from 'react';

export interface IconButtonProps {
	iconClass: string;
	onClick?: (event?: React.MouseEvent<HTMLSpanElement>) => void;
};

export const IconButton: React.StatelessComponent<IconButtonProps> = props => {
	return (
		<button className="icon-button" onClick={(event) => {
			event.preventDefault();
			props.onClick(event)}
		}> 
			<i 
				className={`${props.iconClass}`}
				aria-hidden="true"
			/>
		</button>
	);
};

export interface LabeledButtonProps {
	label: string;
	iconClass: string;
	onClick: (event?: React.MouseEvent<HTMLElement>) => void;
};

export const LabeledButton: React.StatelessComponent<LabeledButtonProps> = props => {
	return (
		<button className="labeled-button" onClick={ev => {
			ev.preventDefault();
			props.onClick(ev)}
		}>
			<span className="icon"> <i className={props.iconClass} aria-hidden="true"/> </span>
			<span className="button-label"> { props.label } </span>
		</button>
	);
};

// onTouchStart={ev => ev.stopPropagation()}