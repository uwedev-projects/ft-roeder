import * as React from 'react';

export type ReactComponent = React.Component<any, any> | React.StatelessComponent<any> | React.ComponentClass<any> | React.PureComponent<any, any>;

export interface SwipableComponentState {

};

export const swipableComponent = (WrappedComponent: ReactComponent) => {
	return class extends React.PureComponent<any, undefined> {
		public render(): JSX.Element {
			const Component = WrappedComponent as React.ComponentClass<any>;
			return <Component {...this.props}/>
		};
	};
};