import './MainLayout.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import NavController from '../nav-controller/NavController';
import Header from '../header/Header';
import MessagePanel from '../message-panel/MessagePanel';
import { Footer } from '../footer/Footer';
import { LoadSpinner } from '../load-spinner/LoadSpinner';
import { DevTools } from '../dev-tools/DevTools';

export interface MainLayoutProps {
	isFetching?: boolean;
};

export const MainLayout: React.StatelessComponent<MainLayoutProps> = (props) => {
	const isDev = process.env.NODE_ENV !== 'production';
	return (
		<section className="main-layout container-fluid">
			<Header/>
			{isDev && <DevTools/>}  
			<NavController/>
			<Footer/>
			<MessagePanel/>
			{ props.isFetching && <LoadSpinner radius={25} strokeWidth={8}/> } 
		</section>
	);
};

const mapStateToProps = (state: GlobalState, ownProps: MainLayoutProps) => {
	return {
		isFetching: state.application.networkRequests.length > 0,
	};
};

export default connect(mapStateToProps, null, null)(MainLayout);