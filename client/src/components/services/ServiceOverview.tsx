import './ServiceOverview.scss';
import * as React from 'react';
import { ServiceCard } from './ServiceCard';

export interface ServiceOverviewProps {
	services: Service[];
};

export const ServiceOverview: React.StatelessComponent<ServiceOverviewProps> = props => {
	const serviceCards: JSX.Element[] = props.services
		.map((service, index) => (
			<section
				key={service.name + index}
				className="col-xs-12 col-sm-6 col-md-4 no-pad card-wrapper"
				itemProp="itemListElement"
			>
				<ServiceCard
					name={service.name}
					imgUrl={service.descImageUrl}
				/>
			</section> 
		));
	return (
		<section className="service-overview" itemScope itemType="http://schema.org/ItemList">
			<main className="row card-container">
				{ serviceCards }
			</main>
		</section>
	);
};