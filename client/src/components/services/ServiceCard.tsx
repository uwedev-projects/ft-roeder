import './ServiceCard.scss';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { getThumbnailUrlFromImage } from '../../util/ImageUtils';

export interface ServiceCardProps {
	name: string;
	imgUrl: string;
};

export const ServiceCard: React.StatelessComponent<ServiceCardProps> = props => {
	const cardStyle = { backgroundImage: `url(${getThumbnailUrlFromImage(props.imgUrl, 'md')})` };
	return (
		<section className="service-card" itemScope itemType="http://schema.org/Service">
			<Link to={`/leistungen/${props.name}`} itemProp="url">
				<main className="card-content" style={cardStyle}>
					<header>
						<h4 className="service-name" itemProp="name"> { props.name } </h4> 
					</header>
					<aside className="link-flag">
						<section className="icon"> <i className="fa fa-info"/> </section>
						<section className="link-text"> Mehr erfahren </section> 
					</aside>
				</main>
			</Link>
		</section>
	);
};