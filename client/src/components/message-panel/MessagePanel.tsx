import './MessagePanel.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { MessageType } from '../../constants/UIConstants';
import { Motion, spring, presets } from 'react-motion';

export interface MessageProps {
	text: string;
	type: string;
	duration: number;
};

export interface MessageState {
	isFading: boolean;
	startTime: number;
	fadeOutTime: number;
};

export interface MessagePanelProps {
	messages?: StatusMessage[];
};

export class Message extends React.Component<MessageProps, MessageState> {
	constructor() {
		super();
		this.state = {
			isFading: false,
			startTime: 0,
			fadeOutTime: 0
		};
	};

	public componentDidMount(): void {
		this.setState({ startTime: Date.now() });
	};

	private handleMotionRested(): void {
		this.setState({
			isFading: true,
			fadeOutTime: this.props.duration - (Date.now() - this.state.startTime)
		});
	};

	public render(): JSX.Element {
		const springConfig = Object.assign(presets.wobbly, { precision: .001 });
		const initalStyle = { left: -300 };
		const destinationStyle = { left: spring(0, springConfig) };

		let msgClassName = `box-md message ${this.props.type.toLowerCase()}`;
		if(this.state.isFading)
			msgClassName += ' fading';

		let msgStyle = {};
		if(this.state.isFading)
			msgStyle = { transition: `${Math.round(this.state.fadeOutTime * 100) / 100000}s opacity ease` };

		return (
			<Motion
				defaultStyle={initalStyle}
				style={destinationStyle}
				onRest={() => this.handleMotionRested()}>
				{ interpolated => 
					<section
						className={msgClassName}
						style={{...interpolated, ...msgStyle}}> 
						{this.props.text} 
					</section> 
				}
			</Motion>
		);
	};
}

export const MessagePanel: React.StatelessComponent<MessagePanelProps> = (props) => {
	const messages: JSX.Element[] = props.messages.map(message => (
		<Message key={message.id} {... message}/>
	));
	return (
		<section className="message-panel">
			{ messages }
		</section>
	);
};

const mapStateToProps = (state: GlobalState) => {
	return { messages: state.ui.statusMessages };
};

export default connect(mapStateToProps, null)(MessagePanel);