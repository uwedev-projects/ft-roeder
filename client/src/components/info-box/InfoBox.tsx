import './InfoBox.scss';
import * as React from 'react';
import { getFullImageUrl } from '../../util/ImageUtils';

export interface InfoBoxProps {
	lines: InfoLine[];
	backgroundImage?: string;
};

export const InfoBox: React.StatelessComponent<InfoBoxProps> = props => {
	const style = props.backgroundImage ? 
		{ backgroundImage: `url(${getFullImageUrl(props.backgroundImage)})` } : {};

	const lines: JSX.Element[] = props.lines
		.map(line => (
			<li key={line.text} className="info-line">
				<p>
					<i className={line.icon || 'fa fa-check'} aria-hidden="true"/>
					{ line.text }
				</p>
			</li>
		));

	return (
		<section className="info-box" style={style}>
			<ul className="info-list">
				<span> { lines } </span>
			</ul>
		</section>
	);
};