import * as React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { Location } from 'history';
import { routes } from '../../core/Routes';
import { protectedRoute } from '../../components/protected-route/ProtectedRoute';

export interface NavControllerProps {
	location?: Location;
}

export type Component = 
	React.StatelessComponent<any> | 
	React.ComponentClass<any>;

export interface NavRoute {
	path: string;
	exact?: boolean;
	Component: Component;
	isProtected?: boolean;
};

export const NavController: React.StatelessComponent<undefined> = (props) => {
	let navRoutes: JSX.Element[] = routes.map((route, index) => {
		let { Component } = route;
		if(typeof(Component) === 'function')
		if(route.isProtected)
			Component = protectedRoute(Component);
		return (
			<Route 
				key={`/${index}${route.path}`}
				path={route.path}
				component={Component}
				
				exact={route.exact}
			/>
		);
	});

	return (
		<section className="nav-controller">
			<Switch {...props}>
				{navRoutes} 
			</Switch>
		</section>
	);
};

const mapStateToProps = (state: GlobalState) => {
	return {
		location: state.router.location
	};
};

export default connect(mapStateToProps, null, null)(NavController);