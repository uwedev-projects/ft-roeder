import './ImageUploader.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { postImages } from '../../../actions/Images';
import { pushStatusMessage } from '../../../actions/UI';

export interface ImageUploaderState {
	files: File[];
};

export interface ImageUploaderProps {
	imageType: string;
	idSuffix?: string;
	submitImages?: (images: File[], type: string) => Image[];
	onImagesSubmitted: (images: Image[]) => void;
	multiple?: boolean;
};

export class ImageUploader extends React.PureComponent<ImageUploaderProps, ImageUploaderState> {
	private uploaderRef: HTMLElement = null;

	constructor() {
		super();
		this.state = { files: [] };
	};

	private handleChange(ev: React.ChangeEvent<HTMLInputElement>): void {
		ev.preventDefault();
		const fileList = ev.target.files;
		
		this.setState({ files: [] });

		for(let i = 0; i < fileList.length; i++) {
			const reader = new FileReader();
			reader.onloadend = () => {
				this.setState({ files: this.state.files.concat(fileList[i]) });
			};
			reader.readAsBinaryString(fileList[i]);
		}
	};

	private handleSubmit(ev: React.FormEvent<HTMLFormElement>): void {
		ev.preventDefault();
		ev.stopPropagation();
		const images = this.props.submitImages(this.state.files, this.props.imageType);
		this.props.onImagesSubmitted(images ? images : []);
	};
	
	public render(): JSX.Element {
		const fileNames = this.state.files.map(file => file.name);
		const id =  this.props.imageType + (this.props.idSuffix || '');

		let imageType = this.props.imageType.charAt(0).toUpperCase();
		imageType += this.props.imageType.slice(1, this.props.imageType.length);
		return (
			<section className="image-uploader" onClick={ev => ev.stopPropagation()}>
				<form onSubmit={(ev) => this.handleSubmit(ev)}>
					<input
						type="file"
						id={id}
						className="img-input"
						multiple={this.props.multiple}
						accept="image/*"
						onChange={ev => this.handleChange(ev)}
						onFocus={ev => ev.stopPropagation()}
						onClick={ev => ev.stopPropagation()}
						ref={node => this.uploaderRef = node}
					/>
					<section className="label-wrapper">
						<label htmlFor={id} onMouseUp={() => this.uploaderRef.focus()}> Dateien auswählen </label>
					</section>
					<input type="submit" value="Speichern" className="button-primary submit"/>
					{ fileNames.length > 0 && <FileList names={fileNames}/> }
				</form>
			</section>
		);
	};
};

const mapDispatchToProps = (dispatch, ownProps: ImageUploaderProps) => {
	return {
		submitImages: async (files, type) => {
			const { images } = await dispatch(postImages(files, type.toLowerCase()));
			return images;
		}
	};
};

export default connect(null, mapDispatchToProps, null)(ImageUploader) as React.ComponentClass<ImageUploaderProps>;

export interface FileListProps {
	names: string[];
};

export const FileList: React.StatelessComponent<FileListProps> = (props) => {
	const files = props.names.map((name, i) => 
		<li key={name + i} className="file"> {i+1}. {name} </li>
	);
	return (
	<footer className="list-wrapper">
		<section className="file-list">
			<ul>
				{ files }
			</ul>
		</section>
	</footer>
	);
};