import './ImageDetails.scss';
import * as React from 'react';
import { Admin } from '../../../constants/Labels';
import { RoundButton } from '../../../components/round-button/RoundButton';
import { getThumbnailUrlFromImage, getFullImageUrl } from '../../../util/ImageUtils';
import { toNoun } from '../../../util/StringUtils';

export interface ImageDetailsProps {
	image: Image;
	onImageRemoved: () => void;
	onAddImageButtonClicked: () => void;
	onImageChosen: () => void;
};

export const ImageDetails: React.StatelessComponent<ImageDetailsProps> = props => {
	const previewClass = props.image ? 'preview' : 'preview none-selected';
	const controlsClass = props.image ? 'controls' : 'controls none-selected';
	return (
		<section className="row image-details">
			<section className="wrapper">
				<section className={'col-xs-9 col-sm-10 no-pad ' + previewClass}>
					<Preview image={props.image}/>
				</section>
				<aside className={'col-xs-3 col-sm-2 ' + controlsClass}>
					<section className="col-xs-12 no-pad remove-wrapper">
						<RoundButton
							className="remove-image"
							iconClass="fa fa-trash"
							onClick={ev => {
								ev.stopPropagation();
								if(props.image)
									props.onImageRemoved();
							}}
						/>
					</section>
					<section className="col-xs-12 no-pad add-wrapper">
						<RoundButton
							className="add-image"
							iconClass="fa fa-plus"
							onClick={ev => {
								ev.stopPropagation();
								props.onAddImageButtonClicked();
							}}
						/>
					</section>
					<section className="col-xs-12 no-pad choose-wrapper">
						<RoundButton
							className="choose-image"
							iconClass="fa fa-check"
							onClick={ev => {
								ev.stopPropagation();
								props.onImageChosen();
							}}
						/>
					</section>
				</aside>
			</section>
		</section>
	);
};

export interface PreviewProps {
	image: Image;
};

export const Preview: React.StatelessComponent<PreviewProps> = props => {
	return (
		<aside className="image-preview-sm">
			{ props.image ?
				<section className="image-preview">
					<section className="col-sm-6 no-pad preview-wrapper">
						<img
							src={getThumbnailUrlFromImage(props.image.url, 'sm')}
							alt={props.image.name}
						/>
					</section>
					<section className="hidden-xs col-sm-6 no-pad description">
						<ul>
							<li className="row"> 
								<span className="label"> Dateiname: </span>
								<span className="file-name"> { props.image.name } </span>
							</li>
							<li className="row">
								<span className="label"> Bildtyp: </span>
								<span> { toNoun(props.image.type) } </span>
							</li>
							<li className="row"> 
								<span className="label"> Upload URL: </span>
							</li>
							<li className="row">
								<a target="_blank" href={getFullImageUrl(props.image.url)}> 
									<p className="url"> { getFullImageUrl(props.image.url) } </p>
								</a>
							</li>
						</ul>
					</section>
				</section> :
				<p> {Admin.IMAGE_DETAILS_NO_IMG_SELECTED} </p>
			}
		</aside>
	);
};

export const ImagePreviewLarge: React.StatelessComponent<PreviewProps> = props => {
	return (
		<aside className="image-preview-lg">
			<section className="image-wrapper">
				<img src={getFullImageUrl(props.image.url)} alt={`Bildvorschau für: ${props.image.name}`}/>
			</section>
		</aside>
	);
};