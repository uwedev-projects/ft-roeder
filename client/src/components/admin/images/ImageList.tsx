import './ImageList.scss';
import * as React from 'react';
import { getThumbnailUrlFromImage } from '../../../util/ImageUtils';
import { ImageTypes } from '../../../constants/ImageTypes';

export interface ImageListProps {
	images: Image[];
	onImageClicked?: (image: Image) => void;
};

export interface ImageListState {
	activeIndex: number;
};

const imageTypes = Object.keys(ImageTypes);

export class ImageList extends React.PureComponent<ImageListProps, ImageListState> {
	constructor() {
		super();
		this.state = { activeIndex: null };
	};

	public shouldComponentUpdate(nextProps: ImageListProps, nextState: ImageListState): boolean {
		if(nextProps.images.length !== this.props.images.length || nextState.activeIndex !== this.state.activeIndex)
			return true;
		else
			return this.props.images.some((image, index) => image._id !== nextProps.images[index]._id);
	};

	private handleImageClicked(image: Image): void {
		const activeIndex = this.props.images.indexOf(image);
		this.setState({ activeIndex });
		this.props.onImageClicked(image);
	};

	private handleKeyUp(ev: React.KeyboardEvent<HTMLElement>): void {
		console.log(ev.charCode);
		console.log(ev.keyCode);
	};

	public render(): JSX.Element {
		const images = this.props.images.map((image, index) =>  {
			const imageClass = index === this.state.activeIndex ? 'row image active' : 'row image';
			return (
				<li
					className={imageClass}
					key={image._id.toString()}
					onClickCapture={() => this.handleImageClicked(image)}
				>
					<section className="col-xs-3 col-md-2 no-pad image-xs">
						<img src={getThumbnailUrlFromImage(image.url, 'xs')} alt={image.name}/>
					</section>
					<section className="col-xs-9 col-md-10 no-pad name">
						<section> { image.name } </section>
					</section>
				</li>
			);
		});
	return (
		<ul className="image-list">
			{ images }
			{ images.length === 0 &&
				<section className="no-images-available">
					<p> Keine Bilder vorhanden. </p>
				</section>
			}
		</ul>
	);
	}
};