import './ImageManager.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { Panel } from '../../panel/Panel';
import { SimpleDialogue } from '../../dialogue/SimpleDialogue';
import { Slider } from '../../slider/Slider';
import { ImageUploadOverlay } from './ImageUploadOverlay';
import { ImageList } from './ImageList';
import { ImageDetails, ImagePreviewLarge } from './ImageDetails';
import { ImageTypes, typeTranslationMap } from '../../../constants/ImageTypes';
import { getImagesByType, deleteImageById } from '../../../actions/Images';
import { Admin } from '../../../constants/Labels';

export interface ImageManagerProps {
	images?: Map<string, Image[]>;
	initialType: string;
	onImageChosen?: (image: Image) => void;
	onManagerDismissed?: () => void;
	getImagesByType?: (type: string) => void;
	deleteImageById?: (id: string, type: string) => void;
	isVisible?: boolean;
};

export interface ImageManagerState {
	activeIndex: number;
	hasFetched: any;
	selectedImage: Image;
	renderConfirmationDialogue: boolean;
	renderUploadOverlay: boolean;
	id: string;
};

const imageTypes = Object.keys(ImageTypes);

export class ImageManager extends React.PureComponent<ImageManagerProps, ImageManagerState> {
	private static managerCount: number = 0;

	constructor(props: ImageManagerProps) {
		super(props);
		ImageManager.managerCount++;

		const hasFetched = {};
		for(let type of imageTypes) {
			hasFetched[type] = false;
		};

		this.state = {
			activeIndex: imageTypes.indexOf(props.initialType.toUpperCase()),
			selectedImage: null,
			hasFetched,
			renderConfirmationDialogue: false,
			renderUploadOverlay: false,
			id: `${ImageManager.managerCount}`
		};
	};

	public componentWillMount(): void {
		this.loadImagesOfAdjacentTypes(this.state.activeIndex);
	};

	private updateImageType(type: string): void {
		const hasFetched = Object.assign({}, this.state.hasFetched, { [type]: true });
		this.setState({ hasFetched });
	};

	private loadImagesOfAdjacentTypes(index: number): void {
		const activeType = imageTypes[index];
		const nextType = imageTypes[this.getNext(index)];
		const prevType = imageTypes[this.getPrevious(index)];

		if(!this.state.hasFetched[activeType]) {
			this.getImagesByType(activeType);
			this.updateImageType(activeType);
		}
		if(!this.state.hasFetched[nextType]) {
			this.getImagesByType(nextType);
			this.updateImageType(nextType);
		}
		if(!this.state.hasFetched[prevType]) {
			this.getImagesByType(prevType);
			this.updateImageType(prevType);
		}
	};

	private getImagesByType(type: string) {
		this.updateImageType(type);
		this.props.getImagesByType(type);
	};

	public shouldComponentUpdate(nextProps: ImageManagerProps, nextState: ImageManagerState): boolean {
		if(nextState.activeIndex !== this.state.activeIndex 
			|| nextState.selectedImage !== this.state.selectedImage	
			|| nextProps.images !== this.props.images
			|| nextProps.isVisible !== this.props.isVisible
			|| nextState.renderConfirmationDialogue !== this.state.renderConfirmationDialogue
			|| nextState.renderUploadOverlay !== this.state.renderUploadOverlay
		) return true;
		
		return false;
	};

	private getNext(index: number): number {
		return (index + 1) % imageTypes.length;
	};

	private getPrevious(index: number): number {
		return (index - 1 + imageTypes.length) % imageTypes.length;
	};

	private isNext(index: number): boolean {
		return index === this.getNext(index);
	};

	private isPrevious(index: number): boolean {
		return index === this.getPrevious(index);
	};

	private isAdjacent(index: number): boolean {
		return this.isNext(index) || this.isPrevious(index) || 
			index === this.state.activeIndex;
	};

	private dismissUploadOverlay(): void {
		this.setState({ renderUploadOverlay: false });
	};

	public handleSlide(movingForward: boolean): void {
		const activeIndex = movingForward ?
			this.getNext(this.state.activeIndex) : this.getPrevious(this.state.activeIndex);
		this.setState({ activeIndex });
		this.loadImagesOfAdjacentTypes(activeIndex);
	};

	private handleImageClicked(image: Image): void {
		this.setState({ selectedImage: image });
	};
	
	private handleAddNewImage(): void {
		this.setState({ renderUploadOverlay: true });
	};

	private handleImageRemoved(): void {
		this.setState({ renderConfirmationDialogue: true });
	};

	private handleImageDeletionConfirmed(): void {
		const imgId = this.state.selectedImage._id.toString();
		this.setState({
			selectedImage: null,
			renderConfirmationDialogue: false
		});
		this.props.deleteImageById(imgId, imageTypes[this.state.activeIndex]);
	};

	private handleImageDeletionAbborted(): void {
		this.setState({ renderConfirmationDialogue: false });
	};

	private handleImageChosen(): void {
		if(this.props.onImageChosen)
			this.props.onImageChosen(this.state.selectedImage);
	};

	private renderSlide(type: string): JSX.Element {
		return (
			<section key={type}>
				<ImageList
					images={this.props.images.get(type) || []}
					onImageClicked={image => this.handleImageClicked(image)}
				/>
			</section>
		);
	};

	public render(): JSX.Element {
		const managerClass = `image-manager ${this.props.isVisible ? 'visible' : ''}`;
		const slides = imageTypes
			.map(type => this.renderSlide(type));
		const activeType = imageTypes[this.state.activeIndex];
		const displayedType = typeTranslationMap[imageTypes[this.state.activeIndex]];
		const initialSliderIndex = imageTypes.indexOf(this.props.initialType.toUpperCase());
		const panelTitle = `${Admin.MANAGE_IMAGES_TITLE} - ${displayedType}`;
		const selectedImageName = this.state.selectedImage ? 
			this.state.selectedImage.name : '';

		return (
			<section className={managerClass}>
				<Panel
					title={panelTitle}
					className="image-manager-panel"
					onPanelDismiss={() => this.props.onManagerDismissed()}
				>
					<section className="image-slider">
						<Slider
							wrap
							slides={slides}
							onSlide={movingForward => this.handleSlide(movingForward)}
							initialIndex={initialSliderIndex}
						/>
					</section>
					<section className="selected-image">
						<ImageDetails
							image={this.state.selectedImage}
							onImageRemoved={() => this.handleImageRemoved()}
							onAddImageButtonClicked={() => this.handleAddNewImage()}
							onImageChosen={() => this.handleImageChosen()}
						/>
						{ this.state.selectedImage !== null && 
							<section className="large-preview hidden-xs hidden-sm">
								<ImagePreviewLarge image={this.state.selectedImage}/>
							</section>
						}
					</section>
				</Panel>
				<SimpleDialogue
					title={Admin.IMAGE_DELETION_TITLE}
					message={Admin.IMAGE_DELETION_MESSAGE(selectedImageName)}
					onConfirmation={() => this.handleImageDeletionConfirmed()}
					onAbortion={() => this.handleImageDeletionAbborted()}
					isVisible={this.state.renderConfirmationDialogue}
				/>
				<ImageUploadOverlay
					onImageUploadComplete={() => this.dismissUploadOverlay()}
					onOverlayDismiss={() => this.dismissUploadOverlay()}
					isVisible={this.state.renderUploadOverlay}
					imageType={activeType.toLowerCase()}
					uploaderId={this.state.id}
				/>
			</section>
		);
	};
};


const mapStateToProps = (state: GlobalState) => {
	return {
		images: new Map(state.data.images)
	};
};

const mapDispatchToProps = (dispatch, ownProps: ImageManagerProps) => {
	return {
		getImagesByType: type => {
			dispatch(getImagesByType(type));
		},
		deleteImageById: (id, type) => {
			dispatch(deleteImageById(id, type));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps, null)(ImageManager) as React.ComponentClass<ImageManagerProps>;