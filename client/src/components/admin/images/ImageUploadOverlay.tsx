import './ImageUploadOverlay.scss';
import * as React from 'react';
import { Panel } from '../../panel/Panel';
import ImageUploader from './ImageUploader';
import { Admin } from '../../../constants/Labels';

export interface ImageUploadOverlayProps {
	onImageUploadComplete: () => void;
	onOverlayDismiss: () => void;
	isVisible: boolean;
	imageType: string;
	uploaderId?: string;
};

export const ImageUploadOverlay: React.StatelessComponent<ImageUploadOverlayProps> = props => {
	const overlayClass = `image-upload-overlay-wrapper ${props.isVisible ? 'visible' : ''}`;
	return (
		<section className={overlayClass}>
			<section className="image-upload-overlay">
				<Panel title="Bilder hochladen" onPanelDismiss={() => props.onOverlayDismiss()}>
					<section className="row description">
						<p> { Admin.IMAGE_UPLOADER_DESCRIPTION } </p>
					</section>
					<section className="row uploader">
						<ImageUploader
							multiple
							imageType={props.imageType}
							idSuffix={props.uploaderId || ''}
							onImagesSubmitted={() => props.onImageUploadComplete()}
						/>
					</section>
				</Panel>
			</section>
			<aside className="transparent-overlay"/>
		</section>
	);
};