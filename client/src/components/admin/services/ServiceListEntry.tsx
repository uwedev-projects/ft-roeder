/// <reference-types="node"/>

import * as React from 'react';
import { IconButton } from '../../icon-button/IconButton';
import { StaticMarkup } from '../../static-markup/StaticMarkup';
import { Link } from 'react-router-dom';
import { getThumbnailUrlFromImage } from '../../../util/ImageUtils';

export interface ServiceListEntryProps {
	name: string;
	description: string;
	descImageUrl: string;
	index: number;
	onServiceEdit: () => void;
	onServiceRemove: () => void;
};

export const ServiceListEntry: React.StatelessComponent<ServiceListEntryProps> = (props) => {
	return (
		<section className="row service-list-entry">
			<section className="col-xs-12 box-sm name">
				<Link to={`/leistungen/${props.name}`}>
					<h5> { props.name } </h5>
				</Link>
			</section>
			<section className="row no-pad entry-body">
				<section className="hidden-md hidden-lg col-xs-5 col-sm-2 no-pad controls-wrapper">
					<ServiceControls
						onServiceEdit={() => props.onServiceEdit()}
						onServiceRemove={() => props.onServiceRemove()}
					/>
				</section>
				<section className="col-xs-7 col-sm-3 col-lg-2 box-sm thumbnail-sm">
					<section className="img-wrapper">
						<img 
							src={getThumbnailUrlFromImage(props.descImageUrl, 'sm')}
							alt={props.name}
						/>
					</section>
					<section className="hidden-xs hidden-sm">
						<ServiceControls
							onServiceEdit={() => props.onServiceEdit()}
							onServiceRemove={() => props.onServiceRemove()}
						/>
					</section>
				</section>
				<section className="col-xs-12 col-sm-7 col-md-9 col-lg-10 no-pad description-wrapper">
					<ServiceDescription description={props.description}/>
				</section>
			</section>
		</section>
	);
};

export interface ServiceDescriptionProps {
	description: any;
};

export const ServiceDescription: React.StatelessComponent<ServiceDescriptionProps> = (props) => (
		<section className="col-xs-12 no-pad service-description">
			<section className="col-xs-12 box-sm description">
				<StaticMarkup content={props.description}/>
			</section>
		</section>
);

export interface ServiceControlsProps {
	onServiceEdit: () => void;
	onServiceRemove: () => void;
};

export const ServiceControls: React.StatelessComponent<ServiceControlsProps> = props => {
	return (
		<section className="col-xs-12 no-pad service-controls">
			<section className="col-xs-12 no-pad edit-service">
				<IconButton
					iconClass="fa fa-pencil"
					onClick={() => props.onServiceEdit()}
				/>
			</section>
			<section className="col-xs-12 no-pad remove-service">
				<IconButton
					iconClass="fa fa-trash"
					onClick={() => props.onServiceRemove()}
				/>
			</section>
		</section>
	);
};