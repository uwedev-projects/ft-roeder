import './AdminServiceList.scss';
import * as React from 'react';
import { ServiceListEntry } from './ServiceListEntry';
import { Admin } from '../../../constants/InfoMessages';

export interface ServiceListProps {
	services: ServiceDescription[];
	onServiceRemove: (index: number) => void;
	onServiceEdit: (index: number) => void;
};

export const AdminServiceList: React.StatelessComponent<ServiceListProps> = (props) => {
	const entries: JSX.Element[] = props.services.map((service, index) => 
		<ServiceListEntry
			name={service.name}
			index={index}
			description={service.description}
			descImageUrl={service.descImageUrl}
			key={service.name + index.toString()}
			onServiceEdit={() => props.onServiceEdit(index)}
			onServiceRemove={() => props.onServiceRemove(index)}
		/>
	);
	if(entries.length > 0)
		return <section className="service-list"> { entries } </section>;
	else
		return <section className="service-list none-available"> <p> { Admin.NO_SERVICES_AVAILABLE } </p> </section>;
};