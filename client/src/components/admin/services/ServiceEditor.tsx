import './ServiceEditor.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { RichTextEditor } from '../RichTextEditor';
import { RawDraftContentState } from 'draft-js';
import ImageManager from '../images/ImageManager';
import { RangeSlider } from '../../range-slider/RangeSlider';
import { LabeledButton } from '../../icon-button/IconButton';
import { StaticMarkup } from '../../static-markup/StaticMarkup';
import { ToolTip } from '../../tooltip/ToolTip';
import { pushStatusMessage } from '../../../actions/UI';
import { push } from 'react-router-redux';
import { ValidationMessages, MessageType } from '../../../constants/UIConstants';
import { Admin, Tooltip } from '../../../constants/Labels';
import { getFullImageUrl, getThumbnailUrlFromImage } from '../../../util/ImageUtils';

export interface ServiceEditorProps {
	onSubmit: (service: Service) => void;
	pushStatusMessage?: (message: string, type?: string, duration?: number) => void;
	push?: (path: string) => void;
	service?: Service;
};

export interface ServiceEditorState {
	service: Service;
	imageManagerVisible: boolean;
	initialDescriptionState: RawDraftContentState;
};

export class ServiceEditor extends React.Component<ServiceEditorProps, ServiceEditorState> {
	private isNewService: boolean = false;

	constructor(props) {
		super(props);
		let service: Service = props.service;
		if (!service) {
			service = {
				name: '',
				category: '',
				description: null,
				descImageUrl: ''
			};
			this.isNewService = true;
		}
		
		const initialDescriptionState = service.description ?
			JSON.parse(service.description) : null;

		this.state = {
			service,
			imageManagerVisible: false,
			initialDescriptionState
		};
	}

	public componentWillReceiveProps(nextProps: ServiceEditorProps) {
		if (nextProps.service !== this.state.service) {
			const { service } = nextProps;
			this.setState({
				service,
				initialDescriptionState: JSON.parse(service.description)
			});
		}
	};

	private handleEditorChange(rawState: RawDraftContentState): void {
		this.setState(state => {
			const service = Object.assign({}, state.service, { description: rawState });
			return { service };
		});
	};

	private handleSubmit(ev: React.FormEvent<HTMLFormElement>): void {
		ev.preventDefault();
		if (this.isValid()) {
			const service = Object.assign({}, this.state.service, {
				description: JSON.stringify(this.state.service.description)
			});
			this.props.onSubmit(service);
			this.props.push('/admin/leistungen');
		}
		else {
			const { name, descImageUrl, description } = this.state.service;
			const { pushStatusMessage } = this.props;

			if (!this.isNameValid(name))
				pushStatusMessage(ValidationMessages.SERVICE_INVALID_NAME, MessageType.ERROR);
			if (description === '')
				pushStatusMessage(ValidationMessages.SERVICE_INVALID_DESCRIPTION, MessageType.ERROR);
			if (descImageUrl === '')
				pushStatusMessage(ValidationMessages.SERVICE_INVALID_IMAGE, MessageType.ERROR);
		}
	};

	private handleImageManagerToggleClicked(ev: React.MouseEvent<HTMLElement>): void {
		ev.preventDefault();
		this.expandImageManager();
	};

	private handleImageChosen(image: Image): void {
		const service = Object.assign({}, this.state.service, { descImageUrl: image.url });
		this.setState({ service, imageManagerVisible: false });
	};

	private handleSignificanceChange(significance: number): void {
		const service = Object.assign({}, this.state.service, { significance });
		this.setState({ service });
	};

	private handleNameChanged(ev: React.ChangeEvent<HTMLInputElement>): void {
		const name = ev.target.value;
		const service = Object.assign({}, this.state.service, { name });
		this.setState({ service });
	};

	private expandImageManager(): void {
		this.setState({ imageManagerVisible: true });
	};

	private collapseImageManager(): void {
		this.setState({ imageManagerVisible: false });
	};

	private isValid(): boolean {
		const { name, description, descImageUrl } = this.state.service;
		return this.isNameValid(name) && description !== '' && descImageUrl !== '';
	};

	private isNameValid(name: string): boolean {
		return name.length > 3;
	};

	public render(): JSX.Element {
		const { name } = this.state.service;
		const { category } = this.state.service;
		const { description } = this.state.service;
		const { descImageUrl } = this.state.service;
		const { significance } = this.state.service;

		return (
			<section className="service-editor">
				<ImageManager
					initialType="SERVICE"
					isVisible={this.state.imageManagerVisible}
					onImageChosen={image => this.handleImageChosen(image)}
					onManagerDismissed={() => this.collapseImageManager()}
				/>
				<main className="editor-content">
					<section className="row editor-row">
						<section className="col-xs-6 col-md-3 no-pad editor-label">
							<label htmlFor="name"> Name der Leistung: </label>
						</section>
						<section className="col-xs-6 col-md-4 no-pad editor-input">
							<ToolTip text={Tooltip.SERVICE_EDITOR_NAME_INPUT}>
								<input id="name" placeholder="..." value={name} onChange={ev => this.handleNameChanged(ev)} />
							</ToolTip>
						</section>
						<section className="col-xs-6 col-md-3 no-pad editor-label">
							<label htmlFor="category"> Kategorie: </label>
						</section>
						<section className="col-xs-6 col-md-2 no-pad editor-input">
							<select id="category" className="disabled" disabled>
								<option> Noch nicht verfügbar </option>
							</select>
						</section>
						<section className="col-xs-6 col-md-3 no-pad editor-label">
							<label> Priorität: </label>
						</section>
						<section className="col-xs-6 col-md-4 no-pad editor-input">
							<RangeSlider
								min={1} max={5}
								initialValue={significance} step={1}
								onChange={value => this.handleSignificanceChange(value)}
							/>
						</section>
					</section>

					<section className="row editor-row">
						<section className="col-xs-5 col-sm-2 no-pad desc-img-label">
							<label> Anzeigebild: </label>
						</section>
						<section className="col-xs-7 col-sm-7 no-pad desc-img-url">
							<p>
								{descImageUrl &&
									<a target="_blank" href={getFullImageUrl(descImageUrl)}>
											{getFullImageUrl(descImageUrl)}
									</a>
								}
								{!descImageUrl && 'Kein Bild ausgewählt.'}
							</p>
						</section>
						<section className="col-sm-3 hidden-xs no-pad desc-img-toggle">
							<ToolTip text={Tooltip.SERVICE_EDITOR_DESC_IMG_INPUT}>
								<LabeledButton
									label="Bild auswählen"
									iconClass="fa fa-file-image-o"
									onClick={ev => this.handleImageManagerToggleClicked(ev)}
								/>
							</ToolTip>
						</section>
						{ descImageUrl &&
							<section className="col-xs-12 desc-img-preview">
								<img src={getThumbnailUrlFromImage(descImageUrl, 'md')} />
							</section>
						}
						<section className="col-xs-12 visible-xs no-pad desc-img-toggle">
							<LabeledButton
								label="Bild auswählen"
								iconClass="fa fa-file-image-o"
								onClick={ev => this.handleImageManagerToggleClicked(ev)}
							/>
						</section>
					</section>

					<section className="row editor-row">
						<section className="col-xs-12 no-pad editor-label">
							<label> Verfassen Sie einen Artikel: </label>
						</section>
						<section className="col-xs-12 no-pad text-editor">
							<RichTextEditor
								onChange={rawState => this.handleEditorChange(rawState)}
								initialContent={this.state.initialDescriptionState}
								placeholder="Bitte geben Sie eine ausführliche Beschreibung für Ihre Leistung ein ..."
								initialImgType="SERVICE"
							/>
						</section>
					</section>

					<section className="row editor-row">
						<section className="col-xs-12 col-sm-12 no-pad save-service">
							<LabeledButton
								iconClass="fa fa-floppy-o"
								label="Leistung speichern"
								onClick={ev => this.handleSubmit((ev as any))}
							/>
						</section>
					</section>

				</main>
			</section>
		);
	};
};

const mapDispatchToProps = dispatch => {
	return {
		pushStatusMessage: (message, type?, duration?) => {
			dispatch(pushStatusMessage(message, type, duration));
		},
		push: path => {
			dispatch(push(path));
		}
	};
};

export default connect(null, mapDispatchToProps, null)(ServiceEditor) as React.ComponentClass<ServiceEditorProps>;