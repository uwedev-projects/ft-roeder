import './PostEditor.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { RichTextEditor } from '../RichTextEditor';
import ImageManager from '../images/ImageManager';
import { LabeledButton } from '../../icon-button/IconButton';
import { ToolTip } from '../../tooltip/ToolTip';
import { StaticMarkup } from '../../static-markup/StaticMarkup';
import { getFullImageUrl, getThumbnailUrlFromImage } from '../../../util/ImageUtils';
import { pushStatusMessage } from '../../../actions/UI';
import { push } from 'react-router-redux';

import { Admin, Tooltip } from '../../../constants/Labels';
import { ValidationMessages, MessageType } from '../../../constants/UIConstants';

export interface PostEditorProps {
	post?: Post;
	onSubmit: (post: Post) => void;
	push?: (path: string) => void;
	pushStatusMessage?: (message: string, type?: string, duration?: number) => void;
};

export interface PostEditorState {
	initialBodyContent: string;
	imageManagerVisible: boolean;
	post: Post;
};

export class PostEditor extends React.Component<PostEditorProps, PostEditorState> {
	constructor(props: PostEditorProps) {
		super(props);
		const post = props.post || {
			title: '',
			author: 'Admin',
			body: '',
			bodyImageUrl: '',
			paragraphs: []
		};

		this.state = {
			post,
			initialBodyContent: post.body ? JSON.parse(post.body) : null,
			imageManagerVisible: false
		};
	};

	private handleSubmit(ev: React.FormEvent<HTMLFormElement>): void {
		ev.preventDefault();
		if(this.isValid) {
			const post: Post = Object.assign({}, this.state.post, {
				body: JSON.stringify(this.state.post.body)
			});
	
			this.props.onSubmit(post);
			this.props.push('/admin/aktuelles');
		}
		else {
			this.props.pushStatusMessage(ValidationMessages.POST_INVALID_TITLE, MessageType.ERROR);
		}
	};

	private handleEditorContentChange(content: string): void {
		const post: Post = Object.assign({}, this.state.post, { body: content });
		this.setState({ post });
	};

	private handleTitleChange(ev: React.ChangeEvent<HTMLInputElement>): void {
		const post: Post = Object.assign({}, this.state.post, { title: ev.target.value });
		this.setState({ post });
	};

	private handleImageManagerToggleClicked(ev: React.MouseEvent<HTMLElement>): void {
		ev.preventDefault();
		this.expandImageManager();
	};

	private handleImageChosen(image: Image): void {
		const post: Post = Object.assign({}, this.state.post, {
			bodyImageUrl: image.url
		});
		this.setState({ post, imageManagerVisible: false });
	};

	private expandImageManager(): void {
		this.setState({ imageManagerVisible: true });
	};

	private collapseImageManager(): void {
		this.setState({ imageManagerVisible: false });
	};

	private isValid(): boolean {
		const { post } = this.state;
		return post && post.title && post.title !== '';
	};

	public render(): JSX.Element {
		const { title } = this.state.post;
		const displayImgUrl = this.state.post.bodyImageUrl;

		return (
			<section className="post-editor">
				<ImageManager
					initialType="POST"
					isVisible={this.state.imageManagerVisible}
					onImageChosen={image => this.handleImageChosen(image)}
					onManagerDismissed={() => this.collapseImageManager()}
				/>
				<main className="editor-content">
					<section className="row editor-row">
						<section className="col-xs-6 no-pad editor-label">
							<label htmlFor="title"> Titel: </label>
						</section>
						<section className="col-xs-6 no-pad editor-input title-wrapper">
							<ToolTip text={Tooltip.POST_EDITOR_TITLE_INPUT}>
								<input id="title" placeholder="..." value={title} onChange={ev => this.handleTitleChange(ev)}/>
							</ToolTip>
						</section>
					</section>

					<section className="row editor-row">
						<section className="col-xs-5 col-sm-2 no-pad display-img-label">
							<label> Anzeigebild: </label>
						</section>
						<section className="col-xs-7 col-sm-7 no-pad display-img-url">
							<p>
								{displayImgUrl &&
									<a target="_blank" href={getFullImageUrl(displayImgUrl)}>
											{getFullImageUrl(displayImgUrl)}
									</a>
								}
								{!displayImgUrl && 'Kein Bild ausgewählt.'}
							</p>
						</section>
						<section className="col-sm-3 hidden-xs no-pad display-img-toggle">
							<ToolTip text={Tooltip.SERVICE_EDITOR_DESC_IMG_INPUT}>
								<LabeledButton
									label="Bild auswählen"
									iconClass="fa fa-file-image-o"
									onClick={ev => this.handleImageManagerToggleClicked(ev)}
								/>
							</ToolTip>
						</section>
						{ displayImgUrl &&
							<section className="col-xs-12 display-img-preview">
								<img src={getThumbnailUrlFromImage(displayImgUrl, 'md')} />
							</section>
						}
						<section className="col-xs-12 visible-xs no-pad display-img-toggle">
							<LabeledButton
								label="Bild auswählen"
								iconClass="fa fa-file-image-o"
								onClick={ev => this.handleImageManagerToggleClicked(ev)}
							/>
						</section>
					</section>

					<section className="row editor-row">
						<RichTextEditor
							placeholder="Verfassen Sie einen Blogeintrag ..."
							onChange={htmlString => this.handleEditorContentChange(htmlString)}
							initialImgType="POST"
						/>
					</section>

					<section className="row editor-row">
						<section className="col-xs-12 col-sm-12 no-pad save-post">
							<LabeledButton
								label="Blogeintrag speichern"
								iconClass="fa fa-floppy-o"
								onClick={ev => this.handleSubmit(ev as any)}
							/>
						</section>
					</section>

				</main>
			</section>
		);
	};
};

const mapDispatchToProps = dispatch => {
	return {
		push: (path: string) => {
			dispatch(push(path));
		},
		pushStatusMessage: (message, type?, duration?) => {
			dispatch(pushStatusMessage(message, type, duration));
		}
	};
};

export default connect(null, mapDispatchToProps, null)(PostEditor) as React.ComponentClass<PostEditorProps>;