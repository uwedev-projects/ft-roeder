import '../../../node_modules/draft-js/dist/Draft.css';
import './RichTextEditor.scss';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { 
	Editor, EditorState, RichUtils, KeyBindingUtil, 
	getDefaultKeyBinding, ContentState, ContentBlock,
	AtomicBlockUtils, convertToRaw, CompositeDecorator,
	convertFromRaw, RawDraftContentState, Modifier
} from 'draft-js';

import ImageManager from './images/ImageManager';
import { ToolTip } from '../tooltip/ToolTip';
import { getImageUrlForScreenWidth } from '../../util/ImageUtils';

export interface RichTextEditorProps {
	onChange?: (rawState: any) => void;
	onFocus?: () => void;
	initialContent?: RawDraftContentState;
	placeholder?: string;
	initialImgType?: string;
};

export interface RichTextEditorState {
	editorState: EditorState;
	isEditing: boolean;
	showImageManager: boolean;
	showUrlInput: boolean;
	linkUrl: string;
};

export class RichTextEditor extends React.Component<RichTextEditorProps, RichTextEditorState> {
	private editorRef: any = null;
	private editorWrapperRef: HTMLElement = null;
	private readonly decorator = new CompositeDecorator([{
		strategy: findLinkEntities,
		component: LinkEntity
	}]);

	constructor(props) {
		super(props);
		const { initialContent } = props;
		let editorState: EditorState = null;

		if(initialContent) {
			editorState = this.createEditorStateFromRaw(initialContent);
		}
		else
			editorState = EditorState.createEmpty(this.decorator);

		this.state = {
			editorState,
			isEditing: false,
			showImageManager: false,
			showUrlInput: false,
			linkUrl: ''
		};
	};

	private logState(editorState: EditorState): void {
		const contentState = editorState.getCurrentContent();
		console.log(convertToRaw(contentState));
	};

	public componentWillReceiveProps(nextProps: RichTextEditorProps): void {
		if(nextProps.initialContent !== this.props.initialContent)
			this.handleEditorChange(
				this.createEditorStateFromRaw(nextProps.initialContent)
			);
	};

	private createEditorStateFromRaw(rawContent: any): EditorState {
		const contentState: ContentState = convertFromRaw(rawContent);
		return EditorState.createWithContent(contentState, this.decorator);
	};

	private handleEditorChange(editorState: EditorState): void {
		const contentState = editorState.getCurrentContent();
		this.setState({ editorState });
		this.props.onChange(convertToRaw(contentState));
	};

	private handleKeyCommand(command): any {
		const nextState = RichUtils.handleKeyCommand(this.state.editorState, command);
		if(nextState) {
			this.handleEditorChange(nextState);
			return 'handled';
		}
		return 'not-handled';
	};

	private handleTab(ev: React.KeyboardEvent<any>): void {
		this.handleEditorChange(RichUtils.onTab(ev, this.state.editorState, 2));
	};

	private handleInlineStylesToggle(inlineStyle: string): void {
		this.setState(state => {
			return {
				editorState: RichUtils.toggleInlineStyle(state.editorState, inlineStyle)
			};
		});
	};

	private handleBlockTypeToggle(blockType: string): void {
		this.setState(state => {
			return {
				editorState: RichUtils.toggleBlockType(state.editorState, blockType)
			};
		});
	};

	private handleLinkUrlChange(linkUrl: string): void {
		this.setState({ linkUrl });
	};

	private handleInsertImageButtonClick(): void {
		if(!this.state.showUrlInput)
			this.editorRef.blur();
		this.setState({ showImageManager: !this.state.showImageManager });
	};

	private handleInsertLinkButtonClick(): void {
		const selection = this.state.editorState.getSelection();
		if(!selection.isCollapsed()) {
			this.setState({ showUrlInput: !this.state.showUrlInput });
		}
	};

	private handleLinkConfirm(): void {
		const { editorState, linkUrl } = this.state;
		const contentState = editorState.getCurrentContent();
		const contentStateWithEntity = contentState.createEntity('LINK', 'IMMUTABLE', { url: linkUrl });
		const entityKey = contentStateWithEntity.getLastCreatedEntityKey();

		const stateWithEntity = EditorState.set(editorState, { currentContent: contentStateWithEntity });
		const stateWithLink = RichUtils.toggleLink(
			stateWithEntity,
			stateWithEntity.getSelection(),
			entityKey
		);

		const selection = stateWithLink.getSelection();
		const collapsed: any = selection.merge({
			anchorOffset: selection.getEndOffset(),
			focusOffset: selection.getEndOffset()
		});

		const stateWithSelection = EditorState.forceSelection(stateWithLink, collapsed);
		const contentStateWithInsertion = Modifier.insertText(
			stateWithSelection.getCurrentContent(),
			collapsed,
			' '
		);

		const nextState = EditorState.set(stateWithSelection, { currentContent: contentStateWithInsertion });

		this.handleEditorChange(nextState);

		this.setState({
			showUrlInput: false,
			linkUrl: ''
		});
		
		setTimeout(() => this.startEditing(), 0);
	};

	private handleImageChosen(image: Image): void {
		const src = getImageUrlForScreenWidth(image.url);
		const alt = image.name;

		const { editorState } = this.state;
		const contentState = editorState.getCurrentContent();

		const contentStateWithEntity = contentState.createEntity('IMAGE', 'IMMUTABLE', { src });
		const entityKey: string = contentStateWithEntity.getLastCreatedEntityKey();

		const stateWithEntity = EditorState.set(editorState, { currentContent: contentStateWithEntity });

		this.handleEditorChange(
			AtomicBlockUtils.insertAtomicBlock(
				stateWithEntity,
				entityKey,
				' '
			)
		);

		this.handleImageManagerDismiss();
	};

	private handleImageManagerDismiss(): void {
		this.setState({ showImageManager: false });
	};

	private handleUrlInputCollapse(): void {
		this.setState({ showUrlInput: false });
	};

	private handleFocus(ev: React.MouseEvent<HTMLElement>): void {
		ev.stopPropagation();
		this.startEditing(); 
		if(this.props.onFocus)
			this.props.onFocus();
	};

	private handleBlur(ev: React.FocusEvent<HTMLElement>): void {
		if(!ev.relatedTarget && !this.state.showImageManager && !this.state.showUrlInput) {
			this.stopEditing();
			if(this.state.showUrlInput)
				this.setState({ showUrlInput: false });
			if(this.state.showImageManager)
				this.setState({ showImageManager: false });
		}
	};

	private startEditing(): void {
		this.setState({ isEditing: true });
		this.editorRef.focus();
	};

	private stopEditing(): void {
		this.setState({ isEditing: false });
		this.editorRef.blur();
	}

	private getKeyBinding(ev: React.KeyboardEvent<any>): any {
		const { hasCommandModifier } = KeyBindingUtil;
		switch(ev.keyCode) {
			case 83: // "S" Key
				if(hasCommandModifier(ev))
					return 'editor-saved';
				break;
			case 13: // "Enter" Key
				break;
		};
		
		return getDefaultKeyBinding(ev);
	};

	private getBlockStyle(block: ContentBlock): string {
		const type = block.getType();
		switch(type) {
			case 'blockquote':
				return 'article-quote';
			default:
				return null;
		};
	};

	public render(): JSX.Element {
		let editorClass = 'rich-text-editor';
		if(this.state.isEditing)
			editorClass += ' editing';

		return (
			<section
				ref={node => this.editorWrapperRef = node}
				className="rich-text-editor-wrapper"
				tabIndex={0}
				onClick={ev => this.handleFocus(ev)}
				onBlur={ev => this.handleBlur(ev)}
			>
				<ImageManager
					isVisible={this.state.showImageManager}
					initialType={this.props.initialImgType || 'DEFAULT'}
					onImageChosen={image => this.handleImageChosen(image)}
					onManagerDismissed={() => this.handleImageManagerDismiss()}
				/>
				<EditorToolbar
					visible={this.state.isEditing}
					editorState={this.state.editorState}
					linkUrl={this.state.linkUrl}
					onInlineStyleToggle={style => this.handleInlineStylesToggle(style)}
					onBlockTypeToggle={blockType => this.handleBlockTypeToggle(blockType)}
					selectingImage={this.state.showImageManager}
					enteringLink={this.state.showUrlInput}
					onInsertImageButtonClick={() => this.handleInsertImageButtonClick()}
					onInsertLinkButtonClick={() => this.handleInsertLinkButtonClick()}
					onLinkUrlChange={url => this.handleLinkUrlChange(url)}
					onLinkConfirm={() => this.handleLinkConfirm()}
					onLinkInputCollapse={() => this.handleUrlInputCollapse()}
				/>
				<section className={editorClass}>
					<Editor
						tabIndex={-1}
						ref={editor => this.editorRef = editor }
						editorState={this.state.editorState}
						keyBindingFn={ev => this.getKeyBinding(ev)}
						blockRendererFn={blockRenderer}
						blockStyleFn={block => this.getBlockStyle(block)}
						placeholder={this.props.placeholder || '...'}
						onTab={ev => this.handleTab(ev)}
						onChange={state => this.handleEditorChange(state)}
						handleKeyCommand={command => this.handleKeyCommand(command)}
					/>
				</section>
			</section>
		)
	};
};

function blockRenderer(block: ContentBlock): any {
	if(block.getType() === 'atomic') {
		return {
			component: ImageBlock,
			editable: false
		};
	}
};

function findLinkEntities(contentBlock: ContentBlock, callback, contentState: ContentState): any {
	contentBlock.findEntityRanges(
		character => {
			const entityKey = character.getEntity();
			return (
				entityKey !== null &&
				contentState.getEntity(entityKey).getType() === 'LINK'
			);
		},
		callback
	);
};

export interface LinkSpanProps {
	contentState: ContentState;
	entityKey: string;
};

const LinkEntity: React.StatelessComponent<LinkSpanProps> = props => {
	const { url } = props.contentState.getEntity(props.entityKey).getData();
	return (
		<span className="link-entity">
			<a href={url}> { props.children } </a>
		</span>
	);
};

export interface EditorToolbarProps {
	visible?: boolean;
	editorState: EditorState;
	linkUrl: string;
	onInlineStyleToggle: (style: string) => void;
	onBlockTypeToggle: (blockType: string) => void;
	selectingImage: boolean;
	enteringLink: boolean;
	onInsertImageButtonClick: () => void;
	onInsertLinkButtonClick: () => void;
	onLinkUrlChange:(url: string) => void;
	onLinkConfirm:() => void;
	onLinkInputCollapse: () => void;
};

const EditorToolbar: React.StatelessComponent<EditorToolbarProps> = props => (
	<section className={props.visible ? 'editor-toolbar visible' : 'editor-toolbar'}>
		<UrlInput
			visible={props.enteringLink}
			value={props.linkUrl}
			onUrlChange={url => props.onLinkUrlChange(url)}
			onUrlConfirm={() => props.onLinkConfirm()}
			onUrlInputCollapse={() => props.onLinkInputCollapse()}
		/>
		<InlineStyleControls
			editorState={props.editorState}
			onToggle={style => props.onInlineStyleToggle(style)}
		/>
		<AdditionalControls
			selectingImage={props.selectingImage}
			onInsertImageButtonClick={() => props.onInsertImageButtonClick()}
			onInsertLinkButtonClick={() => props.onInsertLinkButtonClick()}
		/>
		<BlockTypeControls
			editorState={props.editorState}
			onToggle={blockType => props.onBlockTypeToggle(blockType)}
		/>
	</section>
);

interface StyleControlProps {
	editorState: EditorState;
	onToggle: (style: string) => void;
};

const INLINE_CONTROLS = [
	{ label: 'Bold', iconClass: 'fa fa-bold', style: 'BOLD' },
	{ label: 'Italic', iconClass: 'fa fa-italic', style: 'ITALIC' },
	{ label: 'Underline', iconClass: 'fa fa-underline', style: 'UNDERLINE' }
];

const InlineStyleControls: React.StatelessComponent<StyleControlProps> = props => {
	const currentStyle = props.editorState.getCurrentInlineStyle();
	const controls = INLINE_CONTROLS.map(control => {
		return (
			<StyleButton
				iconClass={control.iconClass}
				style={control.style}
				onToggle={style => props.onToggle(style)}
				isActive={currentStyle.has(control.style)}
				key={control.label}
			/>
		);
	});
	return (
		<aside className="inline-style-controls">
			{ controls }
		</aside>
	);
};

const BLOCKTYPE_CONTROLS = [
	{ label: 'H1', iconClass: null, style: 'header-one' },
	{ label: 'H2', iconClass: null, style: 'header-two' },
	{ label: 'H3', iconClass: null, style: 'header-three' },
	{ label: 'H4', iconClass: null, style: 'header-four' },
	{ label: 'H5', iconClass: null, style: 'header-five' },
	{ label: 'H6', iconClass: null, style: 'header-six' },
	{ label: 'Zitat', iconClass: 'fa fa-quote-right', style: 'blockquote' },
	{ label: 'UL', iconClass: 'fa fa-list-ul', style: 'unordered-list-item' },
	{ label: 'OL', iconClass: 'fa fa-list-ol', style: 'ordered-list-item' }
];

const BlockTypeControls: React.StatelessComponent<StyleControlProps> = props => {
	const selection = props.editorState.getSelection();
	const blockType = props.editorState.getCurrentContent()
		.getBlockForKey(selection.getStartKey())
		.getType();
	
	const controls = BLOCKTYPE_CONTROLS.map(control => {
		return (
			<StyleButton
				iconClass={control.iconClass}
				onToggle={style => props.onToggle(style)}
				style={control.style}
				label={control.label}
				isActive={control.style === blockType}
				key={control.label}
			/>
		);
	});

	return (
		<aside className="block-type-controls">
			{ controls }
		</aside>
	);
};

interface StyleButtonProps {
	onToggle: (style: string) => void;
	label?: string;
	style: string;
	iconClass?: string;
	isActive?: boolean;
};

class StyleButton extends React.Component<StyleButtonProps, undefined> {
	private handleButtonClicked(ev): void {
		ev.preventDefault();
		this.props.onToggle(this.props.style);
	};

	public render(): JSX.Element {
		let buttonClass = 'style-button';
		if(this.props.isActive)
			buttonClass += ' active';
		return (
			<button className={buttonClass} onMouseDown={(ev) => this.handleButtonClicked(ev)} onClick={ev => ev.preventDefault()}>
				{this.props.iconClass && <i className={'control-icon ' + this.props.iconClass}/>}
				{!this.props.iconClass && <span className="label"> {this.props.label} </span> }
			</button>
		);
	};
};

export interface EditorButtonProps {
	isActive?: boolean;
	iconClass: string;
	tooltip?: string;
	onClick: (ev: React.MouseEvent<HTMLElement>) => void;
};

const EditorButton: React.StatelessComponent<EditorButtonProps> = props => {
	const icon = (
		<i className={props.iconClass}/>
	);

	return (
		<button
			className={props.isActive ? 'editor-button active' : 'editor-button'}
			onMouseDown={ev => props.onClick(ev)}
			onClick={ev => ev.preventDefault()}
		>
		{
			props.tooltip ? 
				<ToolTip text={props.tooltip}> { icon } </ToolTip> : { icon }
		}
		</button>
	)
};

export interface AdditionalControlsProps {
	selectingImage?: boolean;
	enteringLink?: boolean;
	onInsertImageButtonClick: () => void;
	onInsertLinkButtonClick: () => void;
};

const AdditionalControls: React.StatelessComponent<AdditionalControlsProps> = props => (
	<aside className="additional-controls">
		<EditorButton
			isActive={props.selectingImage}
			iconClass="fa fa-picture-o"
			tooltip="Bild einfügen"
			onClick={() => props.onInsertImageButtonClick()}
		/>
		<EditorButton
			isActive={props.enteringLink}
			iconClass="fa fa-link"
			tooltip="Hyperlink einfügen"
			onClick={ev => {
				ev.stopPropagation();
				props.onInsertLinkButtonClick();
			}}
		/>
	</aside>
);

export interface ImageBlockProps {
	contentState: ContentState;
	block: ContentBlock;
};

const ImageBlock: React.StatelessComponent<ImageBlockProps> = props => {
	const entity = props.contentState.getEntity(
		props.block.getEntityAt(0)
	);
	const { src } = entity.getData();
	const type = entity.getType();
	
	if(type === 'IMAGE') {
		return (
			<img className="image-block" src={src}/>
		);
	}

	return null;
};

export interface UrlInputProps {
	onUrlChange: (url: string) => void;
	onUrlConfirm: () => void;
	onUrlInputCollapse: () => void;
	value: string;
	visible?: boolean;
};

class UrlInput extends React.Component<UrlInputProps, undefined> {
	private inputRef: HTMLElement = null;

	public componentWillReceiveProps(nextProps: UrlInputProps): void {
		if(nextProps.visible) {
			setTimeout(() => this.inputRef.focus(), 0);
		}
	};

	private handleKeyDown(key: string): void {
		if(this.props.visible) {
			switch(key) {
				case 'Enter':
					this.props.onUrlConfirm();
					break;
				case 'Escape':
					this.props.onUrlInputCollapse();
					break;
			}
		}
	}

	public render(): JSX.Element {
		return (
			<section
				className={this.props.visible ? 'url-input visible' : 'url-input'}
				onClick={ev => ev.stopPropagation()}
				onKeyDown={ev => this.handleKeyDown(ev.key)}
			>
				<h4> Hyperlink einfügen </h4>
				<input
					ref={node => this.inputRef = node}
					value={this.props.value}
					onChange={ev => this.props.onUrlChange(ev.target.value)}
					placeholder="URL eingeben ..."
				/>
			</section>
		);
	};
};