import * as React from 'react';
import { Parser as HtmlParser } from 'html-to-react';
import { ContentState, RawDraftContentState, convertFromRaw } from 'draft-js';
import { stateToHTML } from 'draft-js-export-html';

export interface StaticMarkupProps {
	content: any;
};

export const StaticMarkup: React.StatelessComponent<StaticMarkupProps> = props => {
	const html = stateToHTML(
		convertFromRaw(JSON.parse(props.content))
	);

	return (
		<article className="static-markup">
			{ new HtmlParser().parse(html) }
		</article>
	);
};