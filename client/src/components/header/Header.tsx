import './Header.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { spring } from 'react-motion';
import { NavBar } from '../nav-bar/NavBar';
import { SideMenuToggle } from '../side-menu/SideMenuToggle';

export interface HeaderProps {
	isAuthenticated?: boolean;
	activeRoute?: string;
	onMenuIconClick?: () => void;
};

export const Header: React.StatelessComponent<HeaderProps> = (props) => {
	return (
		<section className="header-wrapper">
			<header className="header row">
				<div className="sidemenu-wrapper">
					<SideMenuToggle isAuthenticated={props.isAuthenticated} activeRoute={props.activeRoute}/>
				</div>
				<div className="home-wrapper hidden-xs"> 
					<Link className="home-link" to="/"> <i className="fa fa-2x fa-home" aria-hidden="true"/> </Link>
				</div>
				<div className="title-wrapper">
					<h1 className="title">
						{ props.isAuthenticated ? 'RFT Admin Area' : 'Röder Fenstertechnik' }
					</h1>
				</div>
				<div className="visible-md visible-lg col-md-8 nav-bar-wrapper">
					<NavBar isAuthenticated={props.isAuthenticated} activeRoute={props.activeRoute}/>
				</div>
			</header>
		</section>
	);
};

const mapStateToProps = (state: GlobalState) => {
	return {
		activeRoute: state.router.location.pathname,
		isAuthenticated: state.user !== null
	};
};

export default connect(mapStateToProps, null, null)(Header);