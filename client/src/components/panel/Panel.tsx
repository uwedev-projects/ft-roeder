import './Panel.scss';
import * as React from 'react';
import { IconButton } from '../icon-button/IconButton';

export interface PanelProps {
	title: string
	className?: string;
	onPanelDismiss?: () => void;
};

export const Panel: React.StatelessComponent<PanelProps> = props => {
	const hasCloseButton = props.onPanelDismiss !== undefined;
	const panelClass = 'panel-wrapper ' + (props.className ? props.className : '');

	return (
		<section className={panelClass}>
			<header className="title-bar">
				<h4> {props.title} </h4>
				{ hasCloseButton &&
					<span className="dismiss-panel">
						<IconButton
							iconClass="fa fa-2x fa-times"
							onClick={() => props.onPanelDismiss()}
						/>
					</span>
				}
			</header>
			<main className="panel-content"> {props.children} </main>
		</section>
	);
};