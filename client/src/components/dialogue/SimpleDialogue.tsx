import './SimpleDialogue.scss';
import * as React from 'react';
import { UI } from '../../constants/Labels';

export interface SimpleDialogueProps {
	title: string;
	message: string;
	onConfirmation: () => void;
	onAbortion: () => void;
	confirmText?: string;
	abortText?: string;
	isVisible: boolean;
};

export const SimpleDialogue: React.StatelessComponent<SimpleDialogueProps> = props => {
	const dialogueClass = `simple-dialogue ${props.isVisible ? 'visible' : ''}`;
	return (
		<section className={dialogueClass}>
			<section className="dialogue-content">
				<header className="row title">
					<h5> { props.title } </h5>
				</header>
				<section className="row message"> 
					<p> { props.message} </p>
				</section>
				<footer className="row controls">
					<button className="col-xs-6 confirm" onClick={() => props.onConfirmation()}>
						{ props.confirmText || UI.CONFIRM_DIALOGUE }
					</button>
					<button className="col-xs-6 abort" onClick={() => props.onAbortion()}>
						{ props.abortText || UI.ABORT_DIALOGUE }
					</button>
				</footer>
			</section>
			<aside className="transparent-overlay" onClick={() => props.onAbortion()}/>
		</section>
	);
};