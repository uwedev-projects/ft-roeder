import './RoundButton.scss';
import * as React from 'react';

export interface RoundButtonProps {
	iconClass: string;
	className?: string;
	onClick: (event: React.MouseEvent<HTMLElement>) => void;
};

export const RoundButton: React.StatelessComponent<RoundButtonProps> = (props) => {
	return (
		<section className={`round-button ${props.className || ''}`} onClick={(event) => props.onClick(event)}>
			<i className={props.iconClass} />
		</section>
	);
};