const baseUrl = process.env.NODE_ENV === 'production' ?
 'http://134.255.252.48:4200' : 'http://localhost:4200';

export const appConfig: AppConfig = {
	baseUrl,
	apiServerBaseUrl: `${baseUrl}/api`
};