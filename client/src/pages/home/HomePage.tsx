import * as React from 'react';

const HomePage: React.StatelessComponent<undefined> = () => {
	return (
		<section className="home-page row">
			<header className="page-title">
				<h2> TODO: Home Page </h2>
			</header>
			<main className="col-xs-12 content-container">
			</main>
		</section>
	);
};

export default HomePage;