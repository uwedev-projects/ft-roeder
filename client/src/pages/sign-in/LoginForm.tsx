import * as React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { login } from '../../actions/User';
import { pushStatusMessage } from '../../actions/UI';
import { MessageType } from '../../constants/UIConstants';

export interface LoginFormState {
	credentials: Credentials;
};

export interface LoginFormProps {
	onLogin?: (credentials: Credentials) => void;
	onValidationFailure?: (text: string, duration?: number) => void;
};

export class LoginForm extends React.PureComponent<LoginFormProps, LoginFormState> {
	constructor() {
		super();
		this.state = { credentials: null };
	};

	public componentWillMount(): void {
		this.setState({
			credentials: {
				userName: '',
				password: ''
			}
		});
	};

	private handleClick(): void {
		if(this.state.credentials.userName && this.state.credentials.password)
			this.props.onLogin(this.state.credentials);
		else {
			const errorMsg = `Bitte geben Sie sowohl Benutzernamen als auch Kennwort an.`;
			this.props.onValidationFailure(errorMsg, 5000);
		}
	};

	private handleChange(ev: React.ChangeEvent<HTMLInputElement>): void {
		let credentials: Credentials;
		switch(ev.target.id) {
			case 'admin-name':
				credentials = Object.assign({}, this.state.credentials, { userName: ev.target.value });
				break;
			case 'password':
				credentials = Object.assign({}, this.state.credentials, { password: ev.target.value });
				break;
		};
		this.setState({ credentials });
	};
	
	public render(): JSX.Element {
		return (
			<section className="login-form">
				<form onSubmit={(e) => {e.preventDefault()}} autoComplete="off">
					<section className="row">
						<input style={{display: 'none'}} name="password" type="password" autoComplete="on"/>
						<label htmlFor="admin-name"> Admin: </label>
						<input 
							id="admin-name"
							value={this.state.credentials.userName}
							placeholder="..."
							onChange={(ev) => this.handleChange(ev)}
							autoComplete="on"
							autoFocus
						/>
					</section>
					<section className="row">
						<label htmlFor="password"> Password: </label>
						<input 
							id="password"
							type="password"
							value={this.state.credentials.password}
							placeholder="..."
							onChange={(ev) => this.handleChange(ev)}
							autoComplete="off"
						/>
					</section>
					<section className="row">
						<button className="button-primary" onClick={() => this.handleClick()}> Anmelden </button>
					</section>
				</form>
			</section>
		);
	};
};

const mapDispatchToProps = (dispatch: Dispatch<any>, ownProps: LoginFormProps) => {
	return {
		onLogin: (credentials) => {
			dispatch(login(credentials));
		},
		onValidationFailure: (text, duration) => {
			dispatch(pushStatusMessage(text, MessageType.ERROR, duration));
		}
	};
};

export default connect(null, mapDispatchToProps, null)(LoginForm);