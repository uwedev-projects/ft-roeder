import './SignInPage.scss';
import * as React from 'react';
import { Panel } from '../../components/panel/Panel';
import LoginForm from './LoginForm';

const SignInPage: React.StatelessComponent<undefined> = () => (
	<section className="sign-in-page row">
		<main className="col-xs-12 content-container">
			<Panel title="Admin Login">
				<LoginForm/>
			</Panel>
		</main>
	</section>
);

export default SignInPage;