import './DisclaimerPage.scss';
import * as React from 'react';
import { BusinessInfo, ContactInfo } from '../../constants/Labels';

const DisclaimerPage: React.StatelessComponent<undefined> = props => {
	return (
		<section className="disclaimer-page row" itemScope itemType="http://schema.org/WebPage">
			<header className="page-title">
				<h2 itemProp="name"> Impressum </h2>
			</header>
			<main className="col-xs-12 disclaimer" itemProp="mainContentOfPage">
				<section className="col-xs-12 col-sm-6 block-container">
					<section className="info-container">
						<h4 className="block-title"> Angaben gemäß § 5 TMG </h4>
						<p> { BusinessInfo.name } </p>
						<p> { BusinessInfo.owner } </p>
						<p> { BusinessInfo.street } </p>
						<p> { BusinessInfo.zipcode + ' ' + BusinessInfo.city } </p>
					</section>
				</section>

				<section className="col-xs-12 col-sm-6 block-container">
					<section className="info-container">
						<h4 className="block-title"> Kontaktdaten </h4>
						<p> Telefon: { ContactInfo.phoneNumber } </p>
						<p> E-Mail: { ContactInfo.eMail } </p>
						<p> Umsatzsteuer-Identifikationsnummer gemäß $27a UStG: { BusinessInfo.umsatzSteuerId } </p>
					</section>
				</section>

				<section className="col-xs-12 block-container">
					<section className="info-container">
						<h4 className="block-title"> Haftung für Inhalte </h4>
						<p>
							Als Diensteanbieter sind wir gem&auml;&szlig;&sect; 7 Abs.1 TMG f&uuml;r 
							eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen
							verantwortlich. Nach &sect;&sect; 8 bis 10 TMG sind wir als Diensteanbieter 
							jedoch nicht verpflichtet, &uuml;bermittelte oder gespeicherte fremde 
							Informationen zu &uuml;berwachen oder nach Umst&auml;nden zu forschen, die 
							auf eine rechtswidrige T&auml;tigkeit hinweisen.
						</p>
						<p>
							Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach 
							den allgemeinen Gesetzen bleiben hiervon unber&uuml;hrt. Eine diesbez&uuml;gliche 
							Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten 
							Rechtsverletzung m&ouml;glich. Bei Bekanntwerden von entsprechenden 
							Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.
						</p>
					</section>
				</section>

				<section className="col-xs-12 block-container">
					<section className="info-container">
						<h4 className="block-title"> Haftung für Links </h4>
						<p>
							Unser Angebot enth&auml;lt Links zu externen Webseiten Dritter, auf deren
							Inhalte wir keinen Einfluss haben. Deshalb k&ouml;nnen wir f&uuml;r diese fremden 
							Inhalte auch keine Gew&auml;hr &uuml;bernehmen. F&uuml;r die Inhalte der verlinkten 
							Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. 
							Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf m&ouml;gliche 
						  Rechtsverst&ouml;&szlig;e &uuml;berpr&uuml;ft. Rechtswidrige Inhalte waren zum
							Zeitpunkt der Verlinkung nicht erkennbar.
						</p>
						<p>
							Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete 
							Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von 
							Rechtsverletzungen werden wir derartige Links umgehend entfernen.
						</p>
					</section>
				</section>

				<section className="col-xs-12 block-container">
					<section className="info-container">
						<h4 className="block-title"> Urheberrecht: </h4>
						<p>
							Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten
							unterliegen dem deutschen Urheberrecht. Die Vervielf&auml;ltigung, Bearbeitung, 
							Verbreitung und jede Art der Verwertung au&szlig;erhalb der Grenzen des Urheberrechtes 
							bed&uuml;rfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. 
							Downloads und Kopien dieser Seite sind nur f&uuml;r den privaten, nicht kommerziellen 
							Gebrauch gestattet.
						</p>
						<p>
							Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die 
							Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche 
							gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, 
							bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen
							werden wir derartige Inhalte umgehend entfernen.
						</p>
					</section>
				</section>

				<section className="col-xs-12 block-container">
					<section className="info-container">
						<h4 className="block-title"> Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV: </h4>
						<p> { BusinessInfo.owner} </p>
						<p> Anschrift: siehe oben </p>
						<p>
							Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor 
							einer Verbraucherschlichtungsstelle teilzunehmen.
						</p>

						<p> Quelle: </p>
						<p>
							<a href="https://www.e-recht24.de/impressum-generator.html">
								https://www.e-recht24.de/impressum-generator.html
							</a>
						</p>
					</section>
				</section>
			</main>
		</section>
	);
};

export default DisclaimerPage;