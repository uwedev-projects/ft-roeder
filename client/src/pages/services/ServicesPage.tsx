import './ServicesPage.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { getAllServices } from '../../actions/Services';
import { InfoBox } from '../../components/info-box/InfoBox';
import { ServiceOverview } from '../../components/services/ServiceOverview';
import { TimedContentSwitch } from '../../components/timed-content-switch/TimedContentSwitch';

const infoBoxLineSets = [
	[
		{ text: 'Langjährige Erfahrung' },
		{ text: 'Hohe Produktqualität' },
		{ text: 'Ihre Wünsche stehen im Vordergrund' }
	],
	[
		{ text: 'Sie haben Fragen oder Unklarheiten?', icon: 'fa fa-question-circle' },
		{ text: 'Lassen Sie sich fachlich beraten', icon: 'fa fa-lightbulb-o' },
		{ text: 'Fordern Sie ein unverbindliches Angebot an', icon: 'fa fa-handshake-o' }
	]
];

export interface ServicesPageProps {
	getAllServices?: (orderBySignificance: boolean) => void;
	services: Service[];
};

export class ServicesPage extends React.Component<ServicesPageProps, undefined> {
	public componentWillMount(): void {
		this.props.getAllServices(true);
	};

	public render(): JSX.Element {
		return (
			<section className="services-page row" itemScope itemType="http://schema.org/CollectionPage">
				<header className="page-title">
					<h2 itemProp="name"> Leistungen </h2>
				</header>
				<main className="col-xs-12 no-pad" itemProp="mainContentOfPage">
					<section className="info-container">
						<section className="col-xs-12 col-sm-6 content-switch">
							<TimedContentSwitch
								content={infoBoxLineSets.map(lineSet => <InfoBox lines={lineSet}/>)}
								switchDelay={10}
							/>
						</section>
						<section className="col-xs-12 col-sm-6 info-img" itemProp="image"/>
					</section>	
					<ServiceOverview services={this.props.services}/>
				</main>
			</section>
		);
	};
};

const mapStateToProps = (state: GlobalState) => {
	return {
		services: state.data.services
	};
};

const mapDispatchToProps = dispatch => {
	return {
		getAllServices: (orderBySignificance: boolean) => {
			dispatch(getAllServices(orderBySignificance));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps, null)(ServicesPage);