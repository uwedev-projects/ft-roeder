import './ServiceDetailPage.scss';
import * as React from 'react';
import { match } from 'react-router';
import { connect } from 'react-redux';
import { getServiceByName } from '../../actions/Services';
import { getImageUrlForScreenWidth } from '../../util/ImageUtils';

import { StaticMarkup } from '../../components/static-markup/StaticMarkup';

export interface ServiceDetailPageProps {
	match: match<ServiceParam>;
	getServiceByName?: (name: string) => void;
	service: Service;
};

export interface ServiceDetailPageState {

};

export class ServiceDetailPage extends React.Component<ServiceDetailPageProps, ServiceDetailPageState> {
	public componentWillMount(): void {
		this.props.getServiceByName(this.props.match.params.name);
	};

	public render(): JSX.Element {
		const { service } = this.props;
		if(!service) {
			return null;
		}

		const descImageUrl = getImageUrlForScreenWidth(service.descImageUrl);
		return (
			<section className="service-detail-page row" itemScope itemType="http://schema.org/Service">
				<header className="page-title">
					<h2 itemProp="name"> { service.name } </h2>
				</header>
				<main itemProp="mainEntityOfPage">
					<section
						className="col-xs-12 description-img"
						style ={{backgroundImage: `url(${descImageUrl})`}}
						itemProp="logo"
					/>
					<section className="col-xs-12 description" itemProp="description">
						<StaticMarkup content={service.description}/>
					</section>
				</main>
			</section>
		);
	};
};

const mapStateToProps = (state: GlobalState, ownProps: ServiceDetailPageProps) => {
	return {
		service: state.data.services
			.find(service => service.name === ownProps.match.params.name)
	};
};

const mapDispatchToProps = dispatch => {
	return {
		getServiceByName: name => {
			dispatch(getServiceByName(name));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps, null)(ServiceDetailPage);