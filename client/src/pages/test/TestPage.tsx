/// <reference types="node"/>

import * as React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { pushStatusMessage } from '../../actions/UI';
import { MessageType } from '../../constants/UIConstants';

import { AdminServiceList } from '../../components/admin/services/AdminServiceList';
import { RoundButton } from '../../components/round-button/RoundButton';
import ImageUploader from '../../components/admin/images/ImageUploader';
import ImageManager from '../../components/admin/images/ImageManager';
import { Slider } from '../../components/slider/Slider';
import { SimpleDialogue } from '../../components/dialogue/SimpleDialogue';
import { RangeSlider } from '../../components/range-slider/RangeSlider';
import { ServiceCard } from '../../components/services/ServiceCard';

const { services } = require('../../fixtures/Services');

export interface TestPageState {
	nextMessageType: number;
};

export interface TestPageProps {
	onMessageButtonClick?: (type: string) => void;
};

const TestSlide = props => 
	<section style={{ height: '300px', background: props.background }}>
		I am Test Slide number {props.index} 
	</section>;

class TestPage extends React.PureComponent<TestPageProps, TestPageState> {
	constructor() {
		super();
		this.state = { nextMessageType: 0 };
	};

	private pushMessage(): void {
		const messageType = this.mapIndexToType(this.state.nextMessageType);
		this.props.onMessageButtonClick(messageType);
		this.setState({ nextMessageType: (this.state.nextMessageType + 1) % 3});
	};

	private mapIndexToType(index: number): string {
		return Object.keys(MessageType)[index];
	};

	private handleFileUpload(images: Image[]): void {
		console.log(images);
	};
	
	public render(): JSX.Element {
		return (
			<div className="content-container">
				<h1> Testpage </h1>
				<h3> Service Card </h3>
				<ServiceCard name="Fenstertechnik" imgUrl="/service/1500759034661.jpg"/>
				<ServiceCard name="Fenstertechnik" imgUrl="/service/1500750198554.jpg"/>
				<h3> Range Slider </h3>
				<RangeSlider min={1} max={5} initialValue={3} step={1}/>
				<h3> Image Upload </h3>
				 <br/>
				<ImageUploader imageType="test" multiple onImagesSubmitted={images => this.handleFileUpload(images)}/>
				<RoundButton iconClass="fa fa-plus" onClick={(ev) => { console.log(ev) }}/>
				<div>
					<h3> Status Message System </h3>
					<button onClick={() => this.pushMessage()}> Push Message </button>
					<h3> File Upload </h3>
					<form
						encType="multipart/form-data"
						action="http://localhost:4200/api/images"
						method="post">
						<input name="paragraph" type="file" accept="image/*" multiple/>
						<button type="submit"> Upload Images </button>
					</form>
				</div>
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch: Dispatch<any>, getState: Function) => {
	return {
		onMessageButtonClick: (type: string) => {
			dispatch(pushStatusMessage(`${type}: I am a Test Message !!! Whats up?!`, type, 5000));
		}
	};
};

export default connect(null, mapDispatchToProps, null)(TestPage);