import './NewsPage.scss';
import * as React from 'react';
import * as Waypoint from 'react-waypoint';
import { connect } from 'react-redux';
import { getPostsLazily } from '../../actions/Posts';

import { StaticMarkup } from '../../components/static-markup/StaticMarkup';
import { LabeledButton } from '../../components/icon-button/IconButton';

export interface NewsPageProps {
	getPostsLazily?: (skip: number, limit?: number) => void;
	posts: Post[];
};

export class NewsPage extends React.Component<NewsPageProps, undefined> {
	private loadNextPosts(): void {
		console.log(this.props.posts);
		this.props.getPostsLazily(this.props.posts.length, 2);
	};

	public render(): JSX.Element {
		const posts: JSX.Element[] = this.props.posts
			.map(post => (
				<section className="post-test" key={post._id.toString()}>
					<h4> { post.title } </h4>
					<StaticMarkup content={post.body}/>
					<p> Erstellt am: {new Date(post.date).toLocaleDateString()} </p>
				</section>
			));

		const waypointKey: string = this.props.posts.length.toString();

		return (
			<section className="news-page row" itemScope itemType="http://schema.org/CollectionPage">
					<header className="page-title">
						<h2 itemProp="name"> Was gibt's Neues ? </h2>
					</header>
					<main className="col-xs-12 no-pad" itemProp="mainContentOfPage">
						{ posts }
						<Waypoint
							key={waypointKey}
							onEnter={() => this.loadNextPosts()}
						/>
					</main>
			</section>
		);
	};
};

const mapStateToProps = (state: GlobalState) => {
	return {
		posts: state.data.posts
	};
};

const mapDispatchToProps = dispatch => {
	return {
		getPostsLazily: (skip, limit?) => {
			dispatch(getPostsLazily(skip, limit));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps, null)(NewsPage);