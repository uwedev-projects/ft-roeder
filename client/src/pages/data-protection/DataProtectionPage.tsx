import './DataProtectionPage.scss';
import * as React from 'react';

const DataProtectionPage: React.StatelessComponent<undefined> = props => (
	<section className="data-protection-page row" itemScope itemType="http://schema.org/WebPage">
	<header className="page-title">
		<h2 itemProp="name"> Datenschutz </h2>
	</header>
	<main className="col-xs-12 no-pad" itemProp="mainContentOfPage">
		<section className="col-xs-12 block-container">
			<section className="info-container">
				<h4 className="block-title"> Datenschutzerklärung </h4>
				<p>
					Die Betreiber dieser Seiten nehmen den Schutz Ihrer persönlichen Daten sehr ernst. 
					Wir behandeln Ihre personenbezogenen Daten vertraulich und entsprechend der 
					gesetzlichen Datenschutzvorschriften sowie dieser Datenschutzerklärung.
				</p>
				<p>
					Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten 
					möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, 
					Anschrift oder E-Mail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets 
					auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht 
					an Dritte weitergegeben.
				</p>
				<p>
					Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der 
					Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz 
					der Daten vor dem Zugriff durch Dritte ist nicht möglich.
				</p>
			</section>
		</section>

		<section className="col-xs-12 block-container">
			<section className="info-container">
				<h4 className="block-title"> Cookies </h4>
				<p>
					Die Internetseiten verwenden teilweise so genannte Cookies. Cookies richten auf 
					Ihrem Rechner keinen Schaden an und enthalten keine Viren. Cookies dienen dazu, 
					unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen. Cookies sind 
					kleine Textdateien, die auf Ihrem Rechner abgelegt werden und die Ihr Browser 
					speichert.
				</p>
				<p>
					Die meisten der von uns verwendeten Cookies sind so genannte „Session-Cookies“. Sie 
					werden nach Ende Ihres Besuchs automatisch gelöscht. Andere Cookies bleiben auf Ihrem 
					Endgerät gespeichert, bis Sie diese löschen. Diese Cookies ermöglichen es uns, Ihren 
					Browser beim nächsten Besuch wiederzuerkennen.
				</p>
				<p>
					Sie können Ihren Browser so einstellen, dass Sie über das Setzen von Cookies 
					informiert werden und Cookies nur im Einzelfall erlauben, die Annahme von Cookies 
					für bestimmte Fälle oder generell ausschließen sowie das automatische Löschen der 
					Cookies beim Schließen des Browser aktivieren. Bei der Deaktivierung von Cookies kann 
					die Funktionalität dieser Website eingeschränkt sein.
				</p>
			</section>
		</section>

		<section className="col-xs-12 block-container">
			<section className="info-container">
				<h4 className="block-title"> SSL-Verschlüsselung </h4>
				<p>
					Diese Seite nutzt aus Gründen der Sicherheit und zum Schutz der Übertragung 
					vertraulicher Inhalte, wie zum Beispiel der Anfragen, die Sie an uns als Seitenbetreiber 
					senden, eine SSL-Verschlüsselung. Eine verschlüsselte Verbindung erkennen Sie daran, 
					dass die Adresszeile des Browsers von "http://" auf "https://" wechselt und an dem Schloss-
					Symbol in Ihrer Browserzeile.
				</p>
				<p>
					Wenn die SSL Verschlüsselung aktiviert ist, können die Daten, die Sie an uns übermitteln, 
					nicht von Dritten mitgelesen werden.
				</p>
			</section>
		</section>

		<section className="col-xs-12 col-sm-6 block-container">
			<section className="info-container">
				<h4 className="block-title"> Kontaktformular </h4>
				<p>
					Wenn Sie uns per Kontaktformular Anfragen zukommen lassen, werden Ihre Angaben aus dem
					Anfrageformular inklusive der von Ihnen dort angegebenen Kontaktdaten zwecks Bearbeitung der
					Anfrage und für den Fall von Anschlussfragen bei uns gespeichert. Diese Daten geben wir nicht 
					ohne Ihre Einwilligung weiter.
				</p>
			</section>
		</section>

		<section className="col-xs-12 col-sm-6 block-container">
			<section className="info-container">
				<h4 className="block-title"> Widerspruch Werbe-Mails </h4>
				<p>
					Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten zur Übersendung 
					von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit widersprochen. 
					Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten 
					Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.
				</p>
			</section>
		</section>
		<section className="col-xs-12">
			<p>
				Quelle <a href="https://www.e-recht24.de"> https://www.e-recht24.de </a>
			</p>
		</section>
	</main>
	</section>
);

export default DataProtectionPage;