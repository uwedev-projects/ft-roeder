import * as React from 'react';
import { connect } from 'react-redux';
import { match } from 'react-router';
import ServiceEditor from '../../../components/admin/services/ServiceEditor';
import { getServiceByName, patchServiceById } from '../../../actions/Services';

export interface EditServicePageProps {
	match: match<ServiceParam>;
	user: AdminUser;
	getServiceByName?: (name: string) => void;
	patchService?: (service: Service) => void;
	service: Service;
};

export class EditServicePage extends React.PureComponent<EditServicePageProps, undefined> {

	public componentWillMount(): void {
		this.props.getServiceByName(this.props.match.params.name);
	};

	public render(): JSX.Element {
		return (
			<section className="edit-service-page row">
				<header className="page-title">
					<h2> Leistung bearbeiten </h2>
				</header>
				<main className="col-xs-12 content-container">
					<ServiceEditor
						service={this.props.service}
						onSubmit={service => this.props.patchService(service)}
					/>
				</main>
			</section>
		);
	};
};

const mapStateToProps = (state: GlobalState, ownProps: EditServicePageProps) => {
	return {
		service: state.data.services
			.find(service => service.name === ownProps.match.params.name)
	};
};

const mapDispatchToProps = dispatch => {
	return {
		getServiceByName: (name: string) => {
			dispatch(getServiceByName(name));
		},
		patchService: service => {
			dispatch(patchServiceById(service));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps, null)(EditServicePage);