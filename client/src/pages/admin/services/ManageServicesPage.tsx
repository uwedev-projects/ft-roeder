import './ManageServicesPage.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { AdminServiceList } from '../../../components/admin/services/AdminServiceList';
import { SimpleDialogue } from '../../../components/dialogue/SimpleDialogue';
import { RoundButton } from '../../../components/round-button/RoundButton';
import { getAllServices, deleteServiceById } from '../../../actions/Services';
import { push } from 'react-router-redux';
import { Admin } from '../../../constants/Labels';

export interface ManageServicePageProps {
	user: AdminUser;
	services: Service[];
	getAllServices: () => void;
	deleteService: (id: string) => void;
	push: (path: string) => void;
};

export interface ManageServicePageState {
	removalDialogueVisible: boolean;
	selectedService: number;
};

export class ManageServicesPage extends  React.PureComponent<ManageServicePageProps, ManageServicePageState> {
	constructor() {
		super();
		this.state = {
			removalDialogueVisible: false,
			selectedService: null
		};
	};

	public componentWillMount(): void {
		this.props.getAllServices();
	};

	private handleServiceRemove(index: number): void {
		this.setState({ selectedService: index, removalDialogueVisible: true });
	};

	private handleServiceEdit(index: number): void {
		const { name } = this.props.services[index];
		this.props.push(`/admin/leistungen/bearbeiten/${name}`);
	};

	private handleDeletionConfirmed(): void {
		const serviceId = this.props.services[this.state.selectedService]._id.toString();
		this.props.deleteService(serviceId);
		this.setState({ selectedService: null, removalDialogueVisible: false });
	};

	private handleDeletionAbborted(): void {
		this.setState({ selectedService: null, removalDialogueVisible: false });
	};

	public render(): JSX.Element {
		const serviceDescriptions = this.props.services.map(service => {
			return {
				name: service.name,
				description: service.description,
				descImageUrl: service.descImageUrl
			};
		});
		
		const selectedServiceName = this.state.selectedService !== null ? 
			this.props.services[this.state.selectedService].name : '';
		const dialogueMessage = Admin.SERVICE_DELETION_MESSAGE(selectedServiceName);

		return (
			<section className="services-admin-page row">
				<header className="page-title">
					<h2> Leistungen verwalten </h2>
				</header>
				<main className="col-xs-12 content-container">
					<AdminServiceList
						services={serviceDescriptions}
						onServiceEdit={index => this.handleServiceEdit(index)}
						onServiceRemove={index => this.handleServiceRemove(index)}
					/>
				</main>
				<RoundButton
					className="add-button"
					iconClass="fa fa-plus"
					onClick={() => this.props.push('/admin/leistungen/neu')}
				/>
				<SimpleDialogue
					title={Admin.SERVICE_DELETION_TITLE}
					message={dialogueMessage}
					isVisible={this.state.removalDialogueVisible}
					onConfirmation={() => this.handleDeletionConfirmed()}
					onAbortion={() => this.handleDeletionAbborted()}
				/>
			</section>
		);
	};
};

const mapStateToProps = (state: GlobalState) => {
	return {
		services: state.data.services
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		push: path => {
			dispatch(push(path));
		},
		getAllServices: () => {
			dispatch(getAllServices());
		},
		deleteService: id => {
			dispatch(deleteServiceById(id));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps, null)(ManageServicesPage);