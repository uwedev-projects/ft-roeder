import * as React from 'react';
import { connect } from 'react-redux';
import { postService } from '../../../actions/Services';
import ServiceEditor from '../../../components/admin/services/ServiceEditor';

export interface CreateServicePageProps {
	user: AdminUser;
	onServiceEditorSubmit?: (service: Service) => void;
};

export const CreateServicePage: React.StatelessComponent<CreateServicePageProps> = (props) => {
	return (
		<section className="create-service-page row">
			<header className="page-title">
				<h2> Leistung erstellen</h2>
			</header>
			<main className="col-xs-12 content-container">
				<ServiceEditor onSubmit={service => props.onServiceEditorSubmit(service)}/>
			</main>
		</section>
	);
};

const mapDispatchToProps = (dispatch) => {
	return {
		onServiceEditorSubmit: (service) => {
			dispatch(postService(service));
		}
	};
};

export default connect(null, mapDispatchToProps, null)(CreateServicePage);