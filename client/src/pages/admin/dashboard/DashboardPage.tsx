import './DashboardPage.scss';
import * as React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { signOut } from '../../../actions/User';

export interface DashboardProps {
	user: AdminUser;
	onSignOut?: () => void;
};

export const DashboardPage: React.StatelessComponent<DashboardProps> = (props) => (
	<section className="admin-dashboard-page row">
		<header className="page-title">
			<h2> Admin Dashboard </h2>
		</header>
		<main className="col-xs-12 content-container">
			<h5> Willkommen im Dashboard, {props.user.userInfo.firstName}! </h5>
			<button className="sign-out button-primary" onClick={() => props.onSignOut()}> Abmelden </button>
		</main>
	</section>
);

const mapDispatchToProps = (dispatch: Dispatch<any>) => {
	return {
		onSignOut: () => dispatch(signOut())
	};
};

export default connect(null, mapDispatchToProps, null)(DashboardPage);