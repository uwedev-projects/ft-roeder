import * as React from 'react';

export interface ManageReferencesPageProps {
	user: AdminUser;
};

const ManageReferencesPage: React.StatelessComponent<ManageReferencesPageProps> = (props) => (
	<section className="references-admin-page row">
		<header className="page-title">
			<h2> TODO: Referenzen verwalten </h2>
		</header>
		<main className="col-xs-12 content-container">
		</main>
	</section>
);

export default ManageReferencesPage;