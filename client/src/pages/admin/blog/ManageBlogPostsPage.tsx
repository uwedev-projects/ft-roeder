import './ManageBlogPostsPage.scss';
import * as React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { RoundButton } from '../../../components/round-button/RoundButton';

export interface ManageBlogPostsPageProps {
	user: AdminUser;
	push?: (path: string) => void;
};

export class ManageBlogPostsPage extends React.Component<ManageBlogPostsPageProps, undefined> {
	public render(): JSX.Element {
		return (
			<section className="blog-admin-page row">
				<header className="page-title">
					<h2> Blogeinträge verwalten </h2>
				</header>
				<main className="col-xs-12 content-container">
				</main>
				<RoundButton
					className="add-button"
					iconClass="fa fa-plus"
					onClick={() => this.props.push('/admin/aktuelles/neu')}
				/>
			</section>
		);
	};
};

const mapStateToProps = (state: GlobalState) => {
	return {
		posts: state.data.posts
	};
};

const mapDispatchToProps = dispatch => {
	return {
		push: (path: string) => {
			dispatch(push(path));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps, null)(ManageBlogPostsPage);