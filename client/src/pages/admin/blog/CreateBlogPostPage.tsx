import * as React from 'react';
import { connect } from 'react-redux';
import PostEditor from '../../../components/admin/posts/PostEditor';
import { postPost } from '../../../actions/Posts';

export interface CreateBlogPostPageProps {
	user: AdminUser;
	onPostEditorSubmit: (post: Post) => void;
};

export const CreateBlogPostPage: React.StatelessComponent<CreateBlogPostPageProps> = props => {
	return (
		<section className="create-post-page row">
			<header className="page-title">
				<h2> Blogeintrag erstellen </h2>
			</header>
			<main className="col-xs-12 content-container">
				<PostEditor onSubmit={post => props.onPostEditorSubmit(post)}/>
			</main>
		</section>
	);
};

const mapDispatchToProps = dispatch => {
	return {
		onPostEditorSubmit: (post: Post) => {
			dispatch(postPost(post));
		}
	};
};

export default connect(null, mapDispatchToProps, null)(CreateBlogPostPage);