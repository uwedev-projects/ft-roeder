declare interface AppConfig {
	baseUrl: string;
	apiServerBaseUrl: string;
}

declare interface GlobalState {
	router: RouterState;
	application: AppState;
	data: DataState;
	ui: UIState;
	user: User;
}

declare interface AppState {
	networkRequests: NetworkRequest[];
	isVisible: boolean;
	isConnected: boolean;
	apiServer: string;
	jwt: string;
}

declare interface RouterState {
	location: {
		pathname: string;
	}
}

declare interface DataState {
	services: Service[];
	posts: Post[];
	images: Map<string, Image[]>;
}

declare interface UIState {
	sideMenuIsVisible: boolean;
	statusMessages: StatusMessage[];
}

declare interface NetworkRequest {
	id: string;
}

declare interface BaseAction {
	type: any;
	meta?: ActionMetaData;
}

declare interface ActionMetaData {
	delay?: number;
}

declare interface NetworkRequestAction extends BaseAction {
	error?: string;
	id?: string;
}

declare interface ConnectionAction extends BaseAction {
	isConnected?: boolean;
}

declare interface ApiServerAction extends BaseAction {
	apiServer?: string;
}

declare interface JwtAction extends BaseAction {
	jwt?: string;
}

declare interface VisibilityAction extends BaseAction {
	isVisible?: boolean;
}

declare interface UserAction extends BaseAction {
	user?: User;
}

declare interface ServicesAction extends BaseAction {
	service?: Service;
	services?: Service[];
	id?: string;
}

declare interface PostsAction extends BaseAction {
	post?: Post;
	posts?: Post[];
	id?: string;
}

declare interface ImagesAction extends BaseAction {
	image?: Image;
	images?: Image[];
	id?: string;
	imgType?: string;
}

declare interface StatusMessageAction extends BaseAction {
	id?: string;
	message?: StatusMessage;
}

// Documents

declare interface BaseDocument {
	_id?: Object;
}

declare interface Image extends BaseDocument {
	name: string;
	url: string;
	type: string;
}

declare interface AdminUser extends BaseDocument {
	credentials: Credentials;
	userInfo: UserInfo;
}

declare interface Post extends BaseDocument {
	title: string;
	author?: string;
	body: any;
	bodyImageUrl?: string;
	date?: string;
}

declare interface ServiceCategory extends BaseDocument {
	name: string;
	significance: number;
}

declare interface Service extends BaseDocument {
	name: string;
	category: string;
	significance?: number;
	description: any;
	descImageUrl?: string;
	adTexts?: string[];
}

declare type User = AdminUser;

// SubDocs

declare interface Credentials {
	userName: string,
	password: string
}

declare interface UserInfo {
	firstName: string;
	lastName: string;
}

declare interface NavLink {
	path: string;
	name: string;
}

declare interface NavMenuEntry extends NavLink {
	iconClass?: string;
	active?: boolean;
}

declare interface ContactData {
	phoneNumber: string;
	email: string;
	address: Address;
}

declare interface Address {
	fullName: string;
	street: string;
	city: string;
	zipcode: number;
}

// Sub Types

declare interface ServiceDescription {
	name: string;
	description: string;
	descImageUrl: string;
}

declare interface Vector2D {
	x: number;
	y: number;
}

declare interface StatusMessage {
	id: string;
	text: string;
	duration: number;
	type: string;
}

// Route params

declare interface ServiceParam {
	name: string;
}

// enums

declare enum ImageType { PARAGRAPH, POST, SERVICE, REFERENCE, SLIDER, DEFAULT }

// Other types

declare interface InfoLine {
	text: string;
	icon?: string;
}