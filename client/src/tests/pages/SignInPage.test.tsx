/// <reference types="mocha"/>

import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { componentDebugLog } from '../TestUtil';
import { spy, SinonSpy } from 'sinon';

import SignInPage from '../../pages/sign-in/SignInPage';
import { LoginForm } from '../../pages/sign-in/LoginForm';
import ConnectedLoginForm from '../../pages/sign-in/LoginForm';
import * as uiActionCreators from '../../actions/UI';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<SignInPage/>', () => {
	let signInPage: ShallowWrapper<any, any>;

	it('should render without problems', () => {
		signInPage = shallow(<SignInPage/>);
		expect(signInPage).to.be.present();
	});

	it('should contain a <ConnectedLoginForm/>', () => {
		const loginForm = signInPage.find(ConnectedLoginForm);
		expect(loginForm).to.be.present();
	});
});

describe('<LoginForm/>', () => {
	let loginForm: ShallowWrapper<any, any>;
	const loginHandler: SinonSpy = spy();
	const validationFailureHandler: SinonSpy = spy();

	it('should render without problems', () => {
		loginForm = shallow(
			<LoginForm
				onLogin={loginHandler}
				onValidationFailure={validationFailureHandler}
			/>
		);
		expect(loginForm).to.be.present();
	});

	it('should have an <input/> element for admin name and password', () => {
		expect(loginForm.find('#admin-name'))
			.to.be.present();
		expect(loginForm.find('#password'))
			.to.be.present();
	});

	it('should update its state on change', () => {
		const userNameField = loginForm.find('#admin-name');
		const passwordField = loginForm.find('#password');

		userNameField.simulate('change', { 
			target: {
				id: 'admin-name',
				value: 'test123'
			}
		});

		expect(loginForm.state()).to.have.property('credentials')
			.that.has.property('userName', 'test123');
		
		passwordField.simulate('change', {
			target: {
				id: 'password',
				value: 'TestPassword123'
			}
		});

		expect(loginForm.state()).to.have.property('credentials')
			.that.has.property('password', 'TestPassword123');
	});

	it('should call the login handler on click', () => {
		const loginButton = loginForm.find('button');
		expect(loginButton).to.be.present();

		loginButton.simulate('click');
		expect(loginHandler).to.have.property('callCount', 1);
		expect(loginHandler.calledWith(loginForm.state().credentials))
			.to.be.true;
	});

	it('should not call the login handler if 1 input is empty', () => {
		const userNameField = loginForm.find('#admin-name');
		userNameField.simulate('change', {
			target: {
				id: 'admin-name',
				value: ''
			}
		});

		const loginButton = loginForm.find('button');
		loginHandler.reset();
		loginButton.simulate('click');
		
		expect(loginHandler.called).to.be.false;
	});

	it('should call the onValidationFailure handler if 1 input field is empty', () => {
		validationFailureHandler.reset();
		const passwordField = loginForm.find('#password');
		passwordField.simulate('change', {
			target: {
				id: 'password',
				value: ''
			}
		});

		const loginButton = loginForm.find('button');
		loginButton.simulate('click');
		expect(validationFailureHandler.called).to.be.true;
	});
});