/// <reference types="mocha"/>

import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { spy, SinonSpy } from 'sinon';
import { componentDebugLog } from '../TestUtil';
import { getFullImageUrl, getThumbnailUrlFromImage } from '../../util/ImageUtils';

const { services } = require('../../fixtures/Services');

import { ServicesPage } from '../../pages/services/ServicesPage';
import { ServiceOverview } from '../../components/services/ServiceOverview';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<ServicesPage/>', () => {
	let servicesPage: ShallowWrapper<any, any> = null;
	const getAllServicesSpy: SinonSpy = spy();

	it('should render without problems', () => {
		servicesPage = shallow(
			<ServicesPage
				services={services}
				getAllServices={getAllServicesSpy}
			/>
		);			
		expect(servicesPage).to.be.present();
	});

	it('should render a <ServiceOverview/>', () => {
		const serviceOverview = servicesPage.find(ServiceOverview);
		expect(serviceOverview).to.be.present();
		expect(serviceOverview.props())
			.to.have.property('services')
				.that.is.deep.equal(services);
	});

	it('should fetch all services ordered by significance on mount', () => {
		expect(getAllServicesSpy.called).to.be.true;
		expect(getAllServicesSpy.calledWith(true)).to.be.true;
	});
});