/// <reference types="mocha"/>

import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { spy, SinonSpy } from 'sinon';
import { componentDebugLog } from '../TestUtil';

const { services } = require('../../fixtures/Services');
const { paragraphs } = require('../../fixtures/Paragraphs');

import { ServiceDetailPage } from '../../pages/services/ServiceDetailPage';
import { StaticMarkup } from '../../components/static-markup/StaticMarkup';
import { getImageUrlForScreenWidth } from '../../util/ImageUtils';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<ServiceDetailPage/>', () => {
	let serviceDetailPage: ShallowWrapper<any, any> = null;
	const getServiceByNameSpy: SinonSpy = spy();
	const match = {
		params: { name: services[0].name },
		isExact: true,
		path: `/leistungen/${services[0].name}`,
		url: ''
	};

	before(() => {
		services[0].paragraphs = paragraphs;
	});

	it('should render without problems', () => {
		serviceDetailPage = shallow(
			<ServiceDetailPage
				match={match}
				getServiceByName={getServiceByNameSpy}
				service={services[0]}
			/>
		);
		expect(serviceDetailPage).to.be.present();
	});

	it('should render the service name as the page title', () => {
		const nodeWithTitle = serviceDetailPage.find('.page-title')
			.filterWhere(node => node.text().trim() === services[0].name);
		expect(nodeWithTitle).to.be.present();
	});

	it('should render the description image', () => {
		const expectedImgUrl = getImageUrlForScreenWidth(services[0].descImageUrl);
		const nodeWithImage = serviceDetailPage.find('.description-img');
		expect(nodeWithImage)
			.to.have.style('background-image', `url(${expectedImgUrl})`);
	});

	it('should render <StaticMarkup/> with a custom article', () => {
		const markup = serviceDetailPage.find(StaticMarkup)
			.filterWhere(node => node.props().content === services[0].description);
		expect(markup).to.be.present();
	});

	it('should fetch the service specified by route parameter on mount', () => {
		expect(getServiceByNameSpy.called).to.be.true;
		expect(getServiceByNameSpy.calledWith(services[0].name)).to.be.true;
	});
});