/// <reference types="mocha"/>
/// <reference types="node"/>
/// <reference path="../../../index.d.ts"/>

import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { componentDebugLog } from '../../TestUtil';

import { DashboardPage } from '../../../pages/admin/dashboard/DashboardPage';
import { LoginForm } from '../../../pages/sign-in/LoginForm';
import ConnectedLoginForm from '../../../pages/sign-in/LoginForm';
import * as uiActionCreators from '../../../actions/UI';

const { adminUsers } = require('../../../fixtures/AdminUsers');
const admin1 = adminUsers[0];

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<DashboardPage/>', () => {
	let dashboard: ShallowWrapper<any, any> = null;

	it('should render without problems', () => {
		dashboard = shallow(<DashboardPage user={admin1}/>);
		expect(dashboard).to.be.present();
	});

	it('should have a sign out button', () => {
		const signOutButton = dashboard.find('.sign-out');
		expect(signOutButton).to.be.present();
	});

});