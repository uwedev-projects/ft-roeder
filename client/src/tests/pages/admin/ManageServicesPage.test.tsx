/// <reference types="mocha"/>
/// <reference types="node"/>
/// <reference path="../../../index.d.ts"/>

import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { componentDebugLog } from '../../TestUtil';
import { spy } from 'sinon';

import { ManageServicesPage } from '../../../pages/admin/services/ManageServicesPage';
import { AdminServiceList } from '../../../components/admin/services/AdminServiceList';
import { SimpleDialogue } from '../../../components/dialogue/SimpleDialogue';
import { RoundButton } from '../../../components/round-button/RoundButton';

import { Admin } from '../../../constants/Labels';

const { adminUsers } = require('../../../fixtures/AdminUsers');
const { services } = require('../../../fixtures/Services');

const admin1 = adminUsers[0];

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<ManageServicesPage/>', () => {
	let manageServicesPage: ShallowWrapper<any, any> = null;
	let confirmationDialogue: ShallowWrapper<any, any> = null;
	let instance = null;
	const getAllServicesSpy = spy();
	const deleteServiceByIdSpy = spy();
	const pushSpy = spy();

	it('should render the page without problems', () => {
		manageServicesPage = shallow(
			<ManageServicesPage
				user={admin1}
				services={services}
				getAllServices={getAllServicesSpy}
				deleteService={deleteServiceByIdSpy}
				push={pushSpy}
			/>
		);
		expect(manageServicesPage).to.be.present();
		instance = manageServicesPage.instance();
	});

	it('should setup its initial state', () => {
		expect(manageServicesPage.state())
			.to.have.property('removalDialogueVisible', false);
		expect(manageServicesPage.state())
			.to.have.property('selectedService', null);
	});

	it('should display a list of service descriptions', () => {
		const serviceList = manageServicesPage.find(AdminServiceList);
		expect(serviceList).to.be.present();
		expect(serviceList.someWhere(node => node.props()['paragraphs']))
			.to.be.false;
	});

	it('should render a <RoundButton/> to add new services', () => {
		const addButton = manageServicesPage.find(RoundButton);
		expect(addButton).to.be.present();
	});

	it('should render a <SimpleDialogue/>', () => {
		confirmationDialogue = manageServicesPage.find(SimpleDialogue);
		expect(confirmationDialogue).to.be.present();
	});

	it('should pass the admin labels as title and message through to the dialogue', () => {
		expect(confirmationDialogue.props())
			.to.have.property('message')
				.that.is.equal(Admin.SERVICE_DELETION_MESSAGE(''));
		expect(confirmationDialogue.props())
			.to.have.property('title')
				.that.is.equal(Admin.SERVICE_DELETION_TITLE);
	});

	it('should call onAddButtonClick', () => {
		const addButton = manageServicesPage.find(RoundButton);
		addButton.simulate('click');
		expect(pushSpy.called).to.be.true;
	});

	it('should have fetched all services on mount', () => {
		expect(getAllServicesSpy.called).to.be.true;
	});

	it('should handle a edit service event', () => {
		pushSpy.reset();
		const handleServiceEditSpy = spy(instance, 'handleServiceEdit');
		handleServiceEditSpy.call(instance, 0);
		expect(pushSpy.called).to.be.true;
		expect(pushSpy.calledWith(`/admin/leistungen/bearbeiten/${services[0].name}`))
			.to.be.true;
	});

	it('should handle a service removed event', () => {
		const handleServiceRemoveSpy = spy(instance, 'handleServiceRemove');
		handleServiceRemoveSpy.call(instance, 1);
		expect(manageServicesPage.state())
			.to.have.property('selectedService')
				.that.is.equal(1);
		expect(manageServicesPage.state())
			.to.have.property('removalDialogueVisible')
				.that.is.true;
	});

	it('should handle a deletion confirmed event', () => {
		manageServicesPage.setState({ selectedService: 1 });
		const handleDeletionConfirmedSpy = spy(instance, 'handleDeletionConfirmed');
		handleDeletionConfirmedSpy.call(instance);

		expect(manageServicesPage.state())
			.to.have.property('selectedService', null);
		expect(manageServicesPage.state())
			.to.have.property('removalDialogueVisible')
				.that.is.false;

		expect(deleteServiceByIdSpy.called).to.be.true;
		expect(deleteServiceByIdSpy.calledWith(services[1]._id))
			.to.be.true;
	});

	it('should handle a deletion abborted event', () => {
		manageServicesPage.setState({ selectedService: 0, removalDialogueVisible: true });
		const handleDeletionAbbortedSpy = spy(instance, 'handleDeletionAbborted');
		handleDeletionAbbortedSpy.call(instance);
		manageServicesPage.update();
		expect(manageServicesPage.state())
			.to.have.property('removalDialogueVisible', false);
		expect(manageServicesPage.state())
			.to.have.property('selectedService', null);
	});

});