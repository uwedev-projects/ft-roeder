/// <reference types="mocha"/>
/// <reference types="node"/>
/// <reference path="../../../index.d.ts"/>

import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { spy } from 'sinon';
import { componentDebugLog } from '../../TestUtil';

import { ManageBlogPostsPage } from '../../../pages/admin/blog/ManageBlogPostsPage';
import { RoundButton } from '../../../components/round-button/RoundButton';

const { adminUsers } = require('../../../fixtures/AdminUsers');
const { posts } = require('../../../fixtures/Posts');

const admin1 = adminUsers[0];

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<CreateBlogPostPage/>', () => {
	let manageBlogPostsPage: ShallowWrapper<any, any> = null;
	

	it('should render without problems', () => {
		manageBlogPostsPage = shallow(<ManageBlogPostsPage user={admin1}/>);
		expect(manageBlogPostsPage).to.be.present();
	});

	it('should render a <RoundButton/> to add a new post', () => {
		const addButton = manageBlogPostsPage.find(RoundButton);
		expect(addButton).to.be.present();
	});
});