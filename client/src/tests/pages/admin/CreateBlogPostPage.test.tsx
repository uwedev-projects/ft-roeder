/// <reference types="mocha"/>
/// <reference types="node"/>
/// <reference path="../../../index.d.ts"/>

import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { spy } from 'sinon';
import { componentDebugLog } from '../../TestUtil';

import { CreateBlogPostPage } from '../../../pages/admin/blog/CreateBlogPostPage';
import PostEditor from '../../../components/admin/posts/PostEditor';

const { adminUsers } = require('../../../fixtures/AdminUsers');
const { posts } = require('../../../fixtures/Posts');

const admin1 = adminUsers[0];

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<CreateBlogPostPage/>', () => {
	let createBlogPostPage: ShallowWrapper<any, any> = null;
	const onPostSubmitSpy = spy();

	it('should render without problems', () => {
		createBlogPostPage = shallow(<CreateBlogPostPage user={admin1} onPostEditorSubmit={onPostSubmitSpy}/>);
		expect(createBlogPostPage).to.be.present();
	});

	it('should render a <PostEditor/>', () => {
		const postEditor = createBlogPostPage.find(PostEditor);
		expect(postEditor).to.be.present();
	});

	it('should call the submit handler function', () => {
		const postEditor = createBlogPostPage.find(PostEditor);
		postEditor.props().onSubmit(posts[1]);
		expect(onPostSubmitSpy.called).to.be.true;
	});
});