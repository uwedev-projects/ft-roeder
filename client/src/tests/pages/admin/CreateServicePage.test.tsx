/// <reference types="mocha"/>
/// <reference types="node"/>
/// <reference path="../../../index.d.ts"/>

import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { componentDebugLog } from '../../TestUtil';

import { CreateServicePage } from '../../../pages/admin/services/CreateServicePage';
import ServiceEditor from '../../../components/admin/services/ServiceEditor';

const { adminUsers } = require('../../../fixtures/AdminUsers');
const { services } = require('../../../fixtures/Services');

const admin1 = adminUsers[0];

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<CreateServicePage/>', () => {
	let createServicePage: ShallowWrapper<any, any> = null;

	it('should render without problems', () => {
		createServicePage = shallow(<CreateServicePage user={admin1}/>);
		expect(createServicePage).to.be.present();
	});

	it('should render a <ServiceEditor/>', () => {
		const serviceEditor = createServicePage.find(ServiceEditor);
		expect(serviceEditor).to.be.present();
	});

});