/// <reference types="mocha"/>
/// <reference types="node"/>
/// <reference path="../../index.d.ts"/>

import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { componentDebugLog } from '../TestUtil';

import { InfoBox } from '../../components/info-box/InfoBox';
import { getFullImageUrl } from '../../util/ImageUtils';

chai.use(chaiEnzyme(componentDebugLog));

const expect: Chai.ExpectStatic = chai.expect;

describe('<InfoBox/>', () => {
	let infoBox: ReactWrapper<any, any> = null;
	const backgroundImageUrl = 'some/image.jpg';
	const infoLines: InfoLine[] = [
		{	text: 'Ad message 1' },
		{	text: 'Ad message 2', icon: 'fa-plus' },
		{	text: 'Ad message 3', icon: 'fa-times' }
	];

	it('should render without problems', () => {
		infoBox = mount(
			<InfoBox
				lines={infoLines}
				backgroundImage={backgroundImageUrl}
			/>
		);
		expect(infoBox).to.be.present();
	});

	it('should render the info lines', () => {
		for(let line of infoLines) {
			const nodeWithText = infoBox
				.findWhere(node => node.text().trim() === line.text);
			expect(nodeWithText).to.be.present();
		}
	});

	it('should render a checkmark icon for every line without a specified icon', () => {
		for(let line of infoLines) {
			if(!line.icon) {
				const nodeWithIcon = infoBox.find('li')
					.filterWhere(node => node.key() === line.text)
					.find('i');
				expect(nodeWithIcon).to.be.present();
				expect(nodeWithIcon.getDOMNode().classList.contains('fa-check'))
					.to.be.true;
			}
		}
	});

	it('should render an icon if specified for the line', () => {
		for(let line of infoLines) {
			if(line.icon) {
				const nodeWithIcon = infoBox.find('i')
					.filterWhere(node => node.getDOMNode().classList.contains(line.icon));
				expect(nodeWithIcon).to.be.present();
			}
		}
	});

	it('should have a background image', () => {
		expect(infoBox).to.have
			.style('background-image', `url(${getFullImageUrl(backgroundImageUrl)})`);
	});
});