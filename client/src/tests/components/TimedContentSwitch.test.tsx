/// <reference types="mocha"/>
/// <reference types="node"/>

import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import { spy, SinonSpy, useFakeTimers, SinonFakeTimers } from 'sinon';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { componentDebugLog } from '../TestUtil';

import { TimedContentSwitch } from '../../components/timed-content-switch/TimedContentSwitch';

chai.use(chaiEnzyme(componentDebugLog));

const expect: Chai.ExpectStatic = chai.expect;

describe('<TimedContentSwitch/>', () => {
	const TestContent: React.StatelessComponent<any> = props => (
		<section>
			<h4> { props.title } </h4>
			<main> { props.body } </main>
		</section>
	);

	let timedContentSwitch: ReactWrapper<any, any> = null;
	let clock: SinonFakeTimers = null;

	before(() => {
		clock = useFakeTimers();
	});

	it('should render without problems', () => {
		timedContentSwitch = mount(
			<TimedContentSwitch
				content={[
					<TestContent title="Test 1" body="Some content from test 1"/>,
					<TestContent title="Test 2" body="Some content from test 2"/>,
					<TestContent title="Test 3" body="Some content from test 3"/>
				]}
				switchDelay={10}
			/>
		);
		expect(timedContentSwitch).to.be.present();
	});

	it('should setup its initial state', () => {
		expect(timedContentSwitch.state())
			.to.have.property('activeIndex', 0);
	});

	it('should render the initial content', () => {
		const visibleContent = timedContentSwitch.find(TestContent)
			.filterWhere(node => node.props().title === 'Test 1' &&
				node.props().body === 'Some content from test 1');
		expect(visibleContent).to.be.present();
	});

	it('should switch the content after 10 seconds', () => {
		clock.tick(10050);
		expect(timedContentSwitch.state())
			.to.have.property('activeIndex', 1);
		clock.tick(10050);
		expect(timedContentSwitch.state())
			.to.have.property('activeIndex', 2);
		clock.tick(10050);
		expect(timedContentSwitch.state())
			.to.have.property('activeIndex', 0);
	});

	after(() => clock.restore());

});