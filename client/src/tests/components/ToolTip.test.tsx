/// <reference types="mocha"/>
/// <reference types="node"/>

import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { componentDebugLog } from '../TestUtil';
import { spy, stub, SinonSpy } from 'sinon';

import { ToolTip } from '../../components/tooltip/ToolTip';
import { IconButton } from '../../components/icon-button/IconButton';

chai.use(chaiEnzyme(componentDebugLog));

const expect: Chai.ExpectStatic = chai.expect;

describe('<ToolTip/>', () => {
	let tooltip: ReactWrapper<any, any> = null;
	let tooltipContainer: ReactWrapper<any, any> = null;
	let nodeWithToolTip = null;
	let instance = null;
	const clientX = 145;
	const clientY = 265;
	const text = 'I am a test tooltip!';
	let timeoutStub = null;

	before(() => {
		timeoutStub = stub(global, 'setTimeout')
		.callsFake((fn, timeout) => fn());
	});

	it('should render without problems', () => {
		tooltip = mount(
			<ToolTip text={text}>
				<IconButton iconClass="test"/>
			</ToolTip>
		);
		expect(tooltip).to.be.present();
	});

	it('should render its children', () => {
		const children = tooltip.find(IconButton);
		expect(children).to.be.present();
	});

	it('should render a node with a tooltip', () => {
		nodeWithToolTip = tooltip.findWhere(node => node.text().trim() === text);
		expect(nodeWithToolTip).to.be.present();
	});

	it('should setup its initial state', () => {
		expect(tooltip.state())
			.to.have.property('isVisible', false);
		expect(tooltip.state())
			.to.have.property('timerId', null);
		expect(tooltip.state())
			.to.have.property('xPos', null);
		expect(tooltip.state())
			.to.have.property('yPos', null);
	});

	it('should handle a mouse enter event', () => {
		instance = tooltip.instance();
		const handleMouseEnterSpy = spy(instance, 'handleMouseEnter');
		
		tooltip.simulate('mouseenter', { clientX, clientY });
		tooltip.update();

		expect(handleMouseEnterSpy.called).to.be.true;
		expect(timeoutStub.called).to.be.true;
		expect(tooltip.state())
			.to.have.property('isVisible', true);
		expect(tooltip.state())
			.to.have.property('xPos', clientX);
		expect(tooltip.state())
			.to.have.property('yPos', clientY);

		tooltipContainer = tooltip.find('.tooltip-main');
		expect(tooltipContainer)
			.to.have.style('left', `${clientX}px`);
		expect(tooltipContainer)
			.to.have.style('top', `${clientY}px`);
	});

	it('should handle a mouse move event', () => {
		const handleMouseMoveSpy = spy(instance, 'handleMouseMove');
		const newClientX = clientX + 50;
		const newClientY = clientY - 15;

		tooltip.simulate('mousemove', {
			clientX: newClientX,
			clientY: newClientY
		});
		tooltip.update();

		expect(tooltip.state())
			.to.have.property('xPos')
			.that.is.equal(newClientX);
		expect(tooltip.state())
			.to.have.property('yPos')
			.that.is.equal(newClientY);

		expect(tooltipContainer)
			.to.have.style('left', `${newClientX}px`);
		expect(tooltipContainer)
			.to.have.style('top', `${newClientY}px`);	
	});

	it('should handle a mouse leave event', () => {
		const handleMouseLeaveSpy = spy(instance, 'handleMouseLeave');
		tooltip.simulate('mouseleave', {});
		tooltip.update();
		
		expect(tooltip.state())
			.to.have.property('isVisible', false);
	});

	it('should render the tooltip if isVisible is true', () => {
		tooltip.setState({ isVisible: true });

		tooltip.update();
		tooltipContainer = tooltip.find('.tooltip-main');
		expect(tooltipContainer.getDOMNode()
			.classList.contains('visible')
		).to.be.true;
	});

	it('should not render the tooltip if isVisible is false', () => {
		tooltip.setState({ isVisible: false });
		tooltip.update();

		expect(tooltipContainer.getDOMNode()
			.classList.contains('visible')
		).to.be.false;
	});

	after(() => timeoutStub.restore());
});