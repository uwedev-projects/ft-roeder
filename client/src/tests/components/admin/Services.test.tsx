/// <reference types="mocha"/>
/// <reference types="node"/>

import * as React from 'react';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { spy } from 'sinon';
import { mount, ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import { componentDebugLog } from '../../TestUtil';

import { Admin } from '../../../constants/InfoMessages';

import { Provider } from 'react-redux';
import { createDefaultMockStore } from '../../../fixtures/MockStores';
import { AdminServiceList } from '../../../components/admin/services/AdminServiceList';
import { ServiceListEntry, ServiceDescription, ServiceControls } from '../../../components/admin/services/ServiceListEntry';
import { ServiceEditor } from '../../../components/admin/services/ServiceEditor';
import { RichTextEditor } from '../../../components/admin/RichTextEditor';
import { RangeSlider } from '../../../components/range-slider/RangeSlider';
import ImageManager from '../../../components/admin/images/ImageManager';
import { StaticMarkup } from '../../../components/static-markup/StaticMarkup';
import { IconButton, LabeledButton } from '../../../components/icon-button/IconButton';
import { ValidationMessages, MessageType } from '../../../constants/UIConstants';
import { replaceLastWordWithDots } from '../../../util/StringUtils';
import { getFullImageUrl, getThumbnailUrlFromImage } from '../../../util/ImageUtils';
import { stateFromHTML } from 'draft-js-import-html';
import { convertToRaw } from 'draft-js';

const { services } = require('../../../fixtures/Services');
const { images } = require('../../../fixtures/Images');

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('Services Components', () => {

	describe('<ServiceList/>', () => {
		let serviceList: ShallowWrapper<any, any> = null;
		const onServiceRemoveSpy = spy();
		const onServiceEditSpy = spy();

		it('should render without problems', () => {
			serviceList = shallow(
				<AdminServiceList
					services={services}
					onServiceEdit={onServiceEditSpy}
					onServiceRemove={onServiceRemoveSpy}
				/>
			);
			expect(serviceList).to.be.present();
		});

		it('should contain all services from props', () => {
			const entries = serviceList.find(ServiceListEntry);
			expect(entries).to.be.present();
			expect(entries).to.be.of.length(3);
		});

		it('should inform the user if no services are available', () => {
			serviceList.unmount();
			serviceList = shallow(
				<AdminServiceList
					services={[]}
					onServiceEdit={onServiceEditSpy}
					onServiceRemove={onServiceRemoveSpy}
				/>
			);
			
			expect(serviceList
				.someWhere(node => node.text().trim() === Admin.NO_SERVICES_AVAILABLE)
			).to.be.true;
		});
	});

	describe('<ServiceListEntry/>', () => {
		let serviceListEntry: ShallowWrapper<any, any> = null;
		let serviceControls: ShallowWrapper<any, any> = null;
		const onServiceRemoveSpy = spy();
		const onServiceEditSpy = spy();

		it('should render without problems', () => {
			serviceListEntry = shallow(
				<ServiceListEntry
					name={services[0].name}
					description={services[0].description}
					descImageUrl={services[0].descImageUrl}
					index={0}
					onServiceRemove={onServiceRemoveSpy}
					onServiceEdit={onServiceEditSpy}
				/>
			);
			expect(serviceListEntry).to.be.present();
		});

		it('should render the service name', () => {
			const nodeWithServiceName = serviceListEntry
				.findWhere(node => node.text().trim() === services[0].name);
			expect(nodeWithServiceName).to.be.present();
		});

		it('should render a <ServiceDescription/>', () => {
			const description = serviceListEntry.find(ServiceDescription).at(0);
			expect(description).to.be.present();
			expect(description.props())
				.to.have.property('description')
					.that.is.equal(services[0].description);
		});

		it('should render a node with a description img of the service', () => {
			const expectedUrl = getThumbnailUrlFromImage(services[0].descImageUrl, 'sm');
			const imageNode = serviceListEntry.find('img')
				.filterWhere(node => node.getNode().props.src === expectedUrl);
			expect(imageNode).to.be.present();
		});

		it('should render <ServiceControls/> to manage service list entries', () => {
			serviceControls = serviceListEntry.find(ServiceControls).at(0);
			expect(serviceControls).to.be.present();
		});

		it('should pass through the onRemove and onEdit handlers', () => {
			serviceControls.props().onServiceEdit();
			expect(onServiceEditSpy.called).to.be.true;
		});
		
		it('should pass through the onRemove and onEdit handlers', () => {
			serviceControls.props().onServiceRemove();
			expect(onServiceRemoveSpy.called).to.be.true;
		});

	});

	describe('<ServiceDescription/>', () => {
		let serviceDescription: ReactWrapper<any, any> = null;

		const rawState = convertToRaw(
			stateFromHTML(services[1].description)
		);

		it('should render without problems', () => {

			serviceDescription = mount(<ServiceDescription description={JSON.stringify(rawState)}/>);
			expect(serviceDescription).to.be.present();
		});

		it('should render the description inside a <StaticMarkup/> component', () => {
			const markup = serviceDescription.find(StaticMarkup);
			expect(markup).to.be.present();
			expect(markup.props())
				.to.have.property('content')
					.that.is.equal(JSON.stringify(rawState));
		});

	});

	describe('<ServiceControls/>', () => {
		let serviceControls: ReactWrapper<any, any> = null;
		let editButton: ReactWrapper<any, any> = null;
		let removeButton: ReactWrapper<any, any> = null;
		const onServiceEditSpy = spy();
		const onServiceRemoveSpy = spy();

		it('should render without problems', () => {
			serviceControls = mount(
				<ServiceControls
					onServiceEdit={onServiceEditSpy}
					onServiceRemove={onServiceRemoveSpy}
				/>
			);
			expect(serviceControls).to.be.present();
		});

		it('should render an <IconButton/> to edit a service', () => {
			editButton = serviceControls.find('.edit-service')
				.find(IconButton);
			expect(editButton).to.be.present();
		});

		it('should render an <IconButton/> to remove a service', () => {
			removeButton = serviceControls.find('.remove-service')
				.find(IconButton);
			expect(removeButton).to.be.present();
		});

		it('should call the onServiceEdit handler', () => {
			editButton.simulate('click', { preventDefault: spy() });
			expect(onServiceEditSpy.called).to.be.true;
		});

		it('should call the onServiceRemove handler', () => {
			removeButton.simulate('click', { preventDefault: spy() });
			expect(onServiceRemoveSpy.called).to.be.true;
		});
	});

	describe('<ServiceEditor/>', () => {
		const service = Object.assign({}, services[0], {
			description: JSON.stringify(services[0].description)
		});

		let serviceEditor: ShallowWrapper<any, any> = null;
		let instance = null;
		let imageManagerToggle: ShallowWrapper<any, any> = null;
		let storeServiceButton: ShallowWrapper<any, any> = null;
		let imageManager: ShallowWrapper<any, any> = null;
		let rangeSlider: ShallowWrapper<any, any> = null;
		let inputs = null;
		let imageChosenSpy = null;
		const submitSpy = spy().withArgs(service);
		const pushStatusMessageSpy = spy();
		const pushSpy = spy();

		before(() => {
			const provider = shallow(
				<Provider store={createDefaultMockStore()}>
					<ServiceEditor
						onSubmit={submitSpy}
						service={service}
						push={pushSpy}
						pushStatusMessage={pushStatusMessageSpy}
					/>
				</Provider>
			);

			serviceEditor = provider.dive();
			instance = serviceEditor.instance();
		});

		it('should render without problems', () => {
			expect(serviceEditor).to.be.present();
		});

		it('should render an <input/> elment for the service name', () => {
			inputs = serviceEditor.find('input');
			const nameInput = inputs.filter('#name');
			expect(nameInput).to.be.present();
		});

		it('should render a <select/> element for the service category', () => {
			const select = serviceEditor.find('select')
				.filter('#category');
			expect(select).to.be.present();
		});

		it('should render an <ImageManager/> with visibility = false', () => {
			imageManager = serviceEditor.find(ImageManager);
			expect(imageManager).to.be.present();
		});

		it('should render a <RichTextEditor/> to compose a service description', () => {
			const descriptionEditor = serviceEditor.find(RichTextEditor);
			expect(descriptionEditor).to.be.present();
		});

		it('should render a <RangeSlider/> to select the significance', () => {
			rangeSlider = serviceEditor.find(RangeSlider);
			expect(rangeSlider).to.be.present();
			expect(rangeSlider.props())
				.to.have.property('min', 1);
			expect(rangeSlider.props())
				.to.have.property('max', 5);
			expect(rangeSlider.props())
				.to.have.property('initialValue', service.significance);
			expect(rangeSlider.props())
				.to.have.property('step', 1);
		});

		it('should render an <LabeledButton/> to toggle the image manager for the description image', () => {
			imageManagerToggle = serviceEditor.find(LabeledButton).at(0);
			expect(imageManagerToggle).to.be.present();
		});

		it('should render a <LabeledButton/> to store the service', () => {
			storeServiceButton = serviceEditor.find(LabeledButton)
				.filterWhere(node => node.props().label === "Leistung speichern");
			expect(storeServiceButton).to.be.present();
		});

		it('should setup the initial state', () => {
			expect(serviceEditor.state())
				.to.have.property('service', service);
			expect(serviceEditor.state())
				.to.have.property('imageManagerVisible', false);
			expect(serviceEditor.state())
				.to.have.property('initialDescriptionState', services[0].description);
		});

		it('should call the submitHandler on save button click if the inputs are valid', () => {
			submitSpy.reset();
			storeServiceButton.simulate('click', { preventDefault: spy() });
			expect(submitSpy.called).to.be.true;
			expect(pushSpy.called).to.be.true;
			expect(pushSpy.calledWith('/admin/leistungen'))
				.to.be.true;
		});

		it('should not call the submitHandler on invalid inputs and dispatch a status message', () => {
			submitSpy.reset();
			const service = Object.assign({}, services[0]);
			service.description = '';

			serviceEditor.setState({ service });
			storeServiceButton.simulate('click', { preventDefault: spy() });
			expect(submitSpy.called).to.be.false;
			
			const errMsg = ValidationMessages.SERVICE_INVALID_DESCRIPTION;
			expect(pushStatusMessageSpy.called).to.be.true;
			expect(pushStatusMessageSpy.calledWith(errMsg, MessageType.ERROR));
		});

		it('should expand the image manager for a service image and update its state', () => {
			const expandImageManagerSpy = spy(instance, 'expandImageManager');
			imageManagerToggle.simulate('click', { preventDefault: spy() });
			expect(expandImageManagerSpy.called).to.be.true;

			serviceEditor.update();
			expect(serviceEditor.state())
				.to.have.property('imageManagerVisible', true);
		});

		it('should collapse the image manager and update its state if an image has been chosen', () => {
			imageChosenSpy = spy(instance, 'handleImageChosen');
			imageChosenSpy.call(instance, images[0]);
			expect(serviceEditor.state())
				.to.have.property('imageManagerVisible', false);
			expect(serviceEditor.state())
				.to.have.property('service')
					.that.has.property('descImageUrl')
						.that.is.equal(images[0].url);
			
			serviceEditor.update();
			const expectedUrl = getFullImageUrl(images[0].url);
			const nodeWithChosenImageUrl = serviceEditor.findWhere(node => node.text().trim() === expectedUrl);
			expect(nodeWithChosenImageUrl).to.be.present();
			imageChosenSpy.restore();
		});

		it('should collapse the image manager if the image manager got dismissed', () => {
			serviceEditor.setState({ imageManagerVisible: true });
			serviceEditor.update();
			const collapseImageManagerSpy = spy(instance, 'collapseImageManager');
			collapseImageManagerSpy.call(instance);
			expect(serviceEditor.state())
				.to.have.property('imageManagerVisible', false);
		});

		it('should handle a editor content saved event for the description', () => {
			const handleContentChangedSpy = spy(instance, 'handleEditorChange');
			const testContent = 'I am some description!';
			handleContentChangedSpy.call(instance, testContent, 'DESCRIPTION');
			expect(serviceEditor.state())
				.to.have.property('service')
					.that.has.property('description')
						.that.is.eql(testContent);
			handleContentChangedSpy.restore();
		});

		it('should handle a significance change event', () => {
			const handleSignificanceChangeSpy = spy(instance, 'handleSignificanceChange');
			handleSignificanceChangeSpy.call(instance, 5);
			expect(serviceEditor.state())
				.to.have.property('service')
					.that.has.property('significance')
						.that.is.equal(5);
		});

	});

});