/// <reference types="mocha"/>
/// <reference types="node"/>
/// <reference path="../../../index.d.ts"/>

import * as React from 'react';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { spy, SinonSpy, stub } from 'sinon';
import { mount, ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import { componentDebugLog } from '../../TestUtil';
import { getThumbnailUrlFromImage } from '../../../util/ImageUtils';

import { ImageManager, ImageManagerProps, ImageManagerState } from '../../../components/admin/images/ImageManager';
import { ImageUploadOverlay } from '../../../components/admin/images/ImageUploadOverlay';
import { ImageUploader } from '../../../components/admin/images/ImageUploader';
import { ImageList } from '../../../components/admin/images/ImageList';
import { ImageDetails } from '../../../components/admin/images/ImageDetails';
import { ImageTypes, typeTranslationMap } from '../../../constants/ImageTypes';
import { SimpleDialogue } from '../../../components/dialogue/SimpleDialogue';
import { Panel } from '../../../components/panel/Panel';
import { Slider } from '../../../components/slider/Slider';
import { IconButton } from '../../../components/icon-button/IconButton';
import { RoundButton } from '../../../components/round-button/RoundButton';

import { Admin } from '../../../constants/Labels';

const { images } = require('../../../fixtures/Images');

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('Images Components', () => {
	const imageTypes = Object.keys(ImageTypes);

	describe('<ImageUploader/>', () => {
		let imageUploader: ReactWrapper<any, any> = null;
	
		it('should render without problems', () => {
			imageUploader = mount(<ImageUploader imageType="test" onImagesSubmitted={spy()}/>);
			expect(imageUploader).to.be.present();
		});

		it('should setup it\'s initial state', () => {
			expect(imageUploader.state())
				.to.have.property('files')
				.that.is.deep.equal([]);
		});
	});
	
	describe('<ImageList/>', () => {
		let imageList: ReactWrapper<any, any> = null;
		const imageClickedSpy = spy();

		it('should render without problems', () => {
			imageList = mount(<ImageList images={images} onImageClicked={imageClickedSpy}/>);
			expect(imageList).to.be.present();
		});

		it('should setup its initial state', () => {
			expect(imageList.state())
				.to.have.property('activeIndex', null);
		});

		it('should render all images', () => {
			const renderedImages = imageList.find('.image');
			expect(renderedImages).to.be.of.length(3);
			
			for(let image of images) {
				const nodeWithName = imageList.findWhere(node => node.text().trim() === image.name);
				expect(nodeWithName).to.be.present();

				const imgUrl = getThumbnailUrlFromImage(image.url, 'xs');
				const thumbnail = imageList.findWhere(node => node.type() === 'img' && node.props().src === imgUrl);
				expect(thumbnail).to.be.present();
			}
		});

		it('should add an .active class on click', () => {
			const image = imageList.find('.image').at(0);
			image.simulate('click');
			expect(image.getDOMNode().classList.contains('active'))
				.to.be.true;
		});

		it('should the a handler function if an image is clicked', () => {
			const imgUrl = getThumbnailUrlFromImage(images[0].url, 'xs');
			const thumbnail = imageList.findWhere(node => node.type() === 'img' && node.props().src === imgUrl);
			imageClickedSpy.reset();
			thumbnail.simulate('click');
			expect(imageClickedSpy.called)
				.to.be.true;
			expect(imageClickedSpy.callCount)
				.to.equal(1);
			expect(imageClickedSpy.calledWith(images[0]))
				.to.be.true;

			const imageNode = imageList.find('.image').at(0);
			imageNode.simulate('click');
			expect(imageClickedSpy.callCount)
				.to.equal(2);
		});

		it('should render a info message if no pictures are available', () => {
			imageList.unmount();
			imageList = mount(<ImageList images={[]}/>);
			const infoMessage = 'Keine Bilder vorhanden.';
			expect(imageList.everyWhere(node => node.text().trim() === infoMessage))
				.to.be.true;
		});
	});

	describe('<ImageManager/>', () => {
		const getImageByTypeSpy: SinonSpy = spy();
		const deleteImageByIdSpy: SinonSpy = spy();
		const onImageChosenSpy: SinonSpy = spy();
		let imageDeletionConfirmedSpy: SinonSpy = null;
		let imageDeletionAbbortedSpy: SinonSpy = null;
		let dismissUploadOverlaySpy: SinonSpy = null;

		const mockImages: Map<string, Image[]> = new Map();
		mockImages.set(images[0].type, images.slice(0, 2));
		mockImages.set(images[2].type, [images[2]]);
		let imageManager: ShallowWrapper<ImageManagerProps, ImageManagerState> = null;
		let deletionDialogue = null;

		it('should render without problems', () => {
			imageManager = shallow(
				<ImageManager
					images={mockImages}
					initialType="test"
					onImageChosen={onImageChosenSpy}
					getImagesByType={getImageByTypeSpy}
					deleteImageById={deleteImageByIdSpy}
				/>
			);
			expect(imageManager).to.be.present();
			imageDeletionConfirmedSpy = spy<any>(imageManager.instance(), 'handleImageDeletionConfirmed');
			imageDeletionAbbortedSpy = spy<any>(imageManager.instance(), 'handleImageDeletionAbborted');
			dismissUploadOverlaySpy = spy<any>(imageManager.instance(), 'dismissUploadOverlay');
		});

		it('should render a <Panel/> with title set the the active type', () => {
			const activeType = imageTypes[imageManager.state().activeIndex];
			const expectedTitle = `${Admin.MANAGE_IMAGES_TITLE} - ${typeTranslationMap[activeType]}`;
			const panelWithTitle = imageManager.find(Panel)
				.filterWhere(node => node.props().title === expectedTitle);
			expect(panelWithTitle).to.be.present();
			expect(panelWithTitle.props())
				.to.have.property('title', expectedTitle);
		});

		it('should render a <Slider/>', () => {
			const slider = imageManager.find(Slider);
			expect(slider).to.be.present();
		});

		it('should render a <ImageDetails/> component', () => {
			const details = imageManager.find(ImageDetails).at(0);
			expect(details).to.be.present();
			expect(details.props().image === imageManager.state().selectedImage);
		});

		it('should render a <SimpleDialogue/>', () => {
			deletionDialogue = imageManager.find(SimpleDialogue);
			expect(deletionDialogue).to.be.present();
			expect(deletionDialogue.props())
				.to.have.property('isVisible', false);
			expect(deletionDialogue.props())
				.to.have.property('title', Admin.IMAGE_DELETION_TITLE);
		});

		it('should render a <ImageUploadOverlay/>', () => {
			const uploadOverlay = imageManager.find(ImageUploadOverlay);
			expect(uploadOverlay).to.be.present();
			expect(uploadOverlay.props())
				.to.have.property('isVisible')
				.that.is.false;
		});

		it('should setup its initial state', () => {
			expect(imageManager.state())
				.to.have.property('activeIndex', Object.keys(ImageTypes).indexOf('TEST'));
			expect(imageManager.state())
				.to.have.property('selectedImage', null);
			expect(imageManager.state())
				.to.have.property('renderConfirmationDialogue', false);
			expect(imageManager.state())
				.to.have.property('renderUploadOverlay', false);
			
			let hasFetched = {};

			for(let type of Object.keys(ImageTypes)) {
				hasFetched[type] = false;
			};

			hasFetched[imageTypes[imageTypes.length - 2]] = true;
			hasFetched[imageTypes[imageTypes.length - 1]] = true;
			hasFetched[imageTypes[0]] = true;
			imageManager.instance().componentWillMount();
			expect(imageManager.state())
				.to.have.property('hasFetched')
				.that.is.deep.equal(hasFetched);
		});

		it('should handle a remove image event', () => {
			imageManager.setState({ selectedImage: images[0] });
			const handleImageRemovedSpy = spy<any>(imageManager.instance(), 'handleImageRemoved');
			handleImageRemovedSpy.call(imageManager.instance());
			expect(handleImageRemovedSpy.called)
				.to.be.true;
			imageManager.update();
			expect(imageManager.state())
				.to.have.property('renderConfirmationDialogue', true);
		});

		it('should handle a image deletion abborted event', () => {
			imageDeletionAbbortedSpy.call(imageManager.instance());
			expect(imageDeletionAbbortedSpy.called)
				.to.be.true;
			expect(imageManager.state())
				.to.have.property('renderConfirmationDialogue')
				.that.is.false;
			
			imageManager.update();

			expect(deletionDialogue.props())
				.to.have.property('isVisible', false);
		});

		it('should handle a image deletion confirmed event', () => {
			imageManager.setState({ renderConfirmationDialogue: true });
			imageManager.update();

			imageDeletionConfirmedSpy.call(imageManager.instance());
			expect(imageDeletionConfirmedSpy.called)
				.to.be.true;
			expect(deleteImageByIdSpy.called)
				.to.be.true;
			expect(imageManager.state())
				.to.have.property('renderConfirmationDialogue', false);
		});

		it('should handle a add new image event', () => {
			const handleAddNewImageSpy = spy<any>(imageManager.instance(), 'handleAddNewImage');
			handleAddNewImageSpy.call(imageManager.instance());
			expect(handleAddNewImageSpy.called)
				.to.be.true;
			expect(imageManager.state())
				.to.have.property('renderUploadOverlay', true);
			
			imageManager.update();
			
			const uploadOverlay = imageManager.find(ImageUploadOverlay);
			expect(uploadOverlay.props())
				.to.have.property('isVisible')
				.that.is.true;
		});

		it('should handle a uploadOverlayDismiss event', () => {
			dismissUploadOverlaySpy.call(imageManager.instance());
			expect(imageManager.state())
				.to.have.property('renderUploadOverlay')
				.that.is.false;
		});

		it('should handle a imageChosen event', () => {
			onImageChosenSpy.reset();
			const handleImageChosenSpy = spy<any>(imageManager.instance(), 'handleImageChosen');
			
			imageManager.setState({ selectedImage: images[0] });
			handleImageChosenSpy.call(imageManager.instance());
		
			expect(onImageChosenSpy.called)
				.to.be.true;
		});

		it('should handle the onSlide event and fetch the next images', () => {
			const handleSlideSpy = spy<any>(imageManager.instance(), 'handleSlide');
			const loadImagesSpy = spy<any>(imageManager.instance(), 'loadImagesOfAdjacentTypes');
			handleSlideSpy.reset();
			loadImagesSpy.reset();

			handleSlideSpy.call(imageManager.instance(), true);

			expect(imageManager.state())
				.to.have.property('activeIndex', 0);
			expect(handleSlideSpy.called)
				.to.be.true;
		});

		it('should have fetched the adjacent image types', () => {
			const hasFetched = {};

			for(let type of imageTypes) {
				hasFetched[type] = false;
			};

			hasFetched[imageTypes[imageTypes.length - 2]] = true;
			hasFetched[imageTypes[imageTypes.length - 1]] = true;
			hasFetched[imageTypes[1]] = true;
			hasFetched[imageTypes[0]] = true;

			expect(imageManager.state())
				.to.have.property('hasFetched')
				.that.is.deep.equal(hasFetched);
		});
	});

	describe('<ImageDetails/>', () => {
		let imageDetails: ReactWrapper<any, any> = null;
		let removeButton = null;
		let addButton = null;
		let chooseButton = null;
		const onRemoveSpy = spy();
		const onAddSpy = spy();
		const onChosenSpy = spy();

		it('should render without problems', () => {
			imageDetails = mount(
				<ImageDetails
					image={images[0]}
					onImageRemoved={onRemoveSpy}
					onAddImageButtonClicked={onAddSpy}
					onImageChosen={onChosenSpy}
				/>
			);
			expect(imageDetails).to.be.present();
		});

		it('should render a sm thumbnail of the image', () => {
			const imgUrl = getThumbnailUrlFromImage(images[0].url, 'sm');
			const thumbnail = imageDetails.findWhere(node => node.props().src === imgUrl);
			expect(thumbnail).to.be.present();
		});

		it('should render a <RoundButton/> for removing an image', () => {
			removeButton = imageDetails.find('.remove-image');
			expect(removeButton).to.be.present();
		});

		it('should render a <RoundButton/> for adding an image', () => {
			addButton = imageDetails.find('.add-image');
			expect(addButton).to.be.present();
		});

		it('should render a <RoundButton/> for choosing the image for some other content', () => {
			chooseButton = imageDetails.find('.choose-image');
			expect(chooseButton).to.be.present();
		});

		it('should call the onImageRemoved handler function', () => {
			removeButton.simulate('click');
			expect(onRemoveSpy.called)
				.to.be.true;
		});

		it('should call the onImageAdded handler function', () => {
			addButton.simulate('click');
			expect(onAddSpy.called)
				.to.be.true;
		});

		it('should call the onImageChosen handler function', () => {
			chooseButton.simulate('click');
			expect(onChosenSpy.called)
				.to.be.true;
		});

	});
});