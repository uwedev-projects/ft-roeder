/// <reference types="mocha"/>
/// <reference types="node"/>
/// <reference path="../../../index.d.ts"/>

import * as React from 'react';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { spy } from 'sinon';
import { mount, ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import { createDefaultMockStore } from '../../../fixtures/MockStores';
import { componentDebugLog } from '../../TestUtil';

import { PostEditor } from '../../../components/admin/posts/PostEditor';
import { RichTextEditor } from '../../../components/admin/RichTextEditor';
import { LabeledButton } from '../../../components/icon-button/IconButton';
import { Provider } from 'react-redux';

const { posts } = require('../../../fixtures/Posts');
const { images } = require('../../../fixtures/Images');

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('Posts Components', () => {

	describe('<PostEditor/>', () => {
		const post = Object.assign({}, posts[0], {
			body: JSON.stringify(posts[0].body)
		});

		let postEditor: ShallowWrapper<any, any> = null;
		let bodyTextEditor: ShallowWrapper<any, any> = null;
		let paragraphTextEditor: ShallowWrapper<any, any> = null;
		let titleInput: ShallowWrapper<any, any> = null;
		let imageManagerToggle: ShallowWrapper<any, any> = null;
		let instance = null;
		const onSubmitSpy = spy();
		const pushSpy = spy();

		it('should render without problems', () => {
			postEditor = shallow(
				<PostEditor
					post={post}
					onSubmit={onSubmitSpy}
					push={pushSpy}
				/>
			);
			expect(postEditor).to.be.present();

			instance = postEditor.instance();
		});

		it('should render a <input/> for the post title', () => {
			titleInput = postEditor.find('input')
				.filterWhere(node => node.getNode().props.id === 'title');
			expect(titleInput).to.be.present();
		});

		it('should render a <RichTextEditor/>', () => {
			bodyTextEditor = postEditor.find(RichTextEditor);
			expect(bodyTextEditor).to.be.present();
			expect(bodyTextEditor.props())
				.to.have.property('placeholder')
					.that.is.equal('Verfassen Sie einen Blogeintrag ...');
		});

		it('should render a <LabeledButton/> to create the post', () => {
			const submitButton = postEditor.find(LabeledButton)
				.filterWhere(node => node.props().label === 'Blogeintrag speichern');
			expect(submitButton).to.be.present();
		});

		it('should render a <LabeledButton/> to select the display image', () => {
			const selectImageButton = postEditor.find(LabeledButton)
				.filterWhere(node => node.props().label === "Bild auswählen")
				.at(0);

			expect(selectImageButton).to.be.present();
			imageManagerToggle = selectImageButton;
			console.log(imageManagerToggle);
		});

		it('should setup its initial state', () => {
			expect(postEditor.state())
				.to.have.property('post', post);
			expect(postEditor.state())
				.to.have.property('initialBodyContent')
					.that.is.equal(posts[0].body);
			expect(postEditor.state())
				.to.have.property('imageManagerVisible')
					.that.is.false;
		});

		it('should call the onSubmit handler function and navigate to /admin/aktuelles when submit button is clicked', () => {
			const submitButton = postEditor.find(LabeledButton)
				.filterWhere(node => node.props().label === "Blogeintrag speichern");
			submitButton.simulate('click', { preventDefault: spy() });
			
			const expectedPost = Object.assign({}, postEditor.state().post, {
				body: JSON.stringify(postEditor.state().post.body)
			});

			expect(onSubmitSpy.called).to.be.true;
			expect(onSubmitSpy.calledWith(expectedPost)).to.be.true;
			expect(pushSpy.called).to.be.true;
			expect(pushSpy.calledWith('/admin/aktuelles'))
				.to.be.true;

		});

		it('should update its state on title change', () => {
			const handleTitleChangeSpy = spy(instance, 'handleTitleChange');

			titleInput.simulate('change', {
				target: { value: 'My Blogpost' }
			});

			expect(handleTitleChangeSpy.called).to.be.true;
			expect(handleTitleChangeSpy.calledWith({
				target: { value: 'My Blogpost' } 
			})).to.be.true;

			expect(postEditor.state().post)
				.to.have.property('title')
					.that.is.equal('My Blogpost');
		});

		it('should update its state on body text change', () => {
			const handleEditorContentChangeSpy = spy(instance, 'handleEditorContentChange');
			const htmlString = `<div> <h1> Hello from PostEditor! </h1> </div>`;
			
			bodyTextEditor.props().onChange(htmlString);
			expect(handleEditorContentChangeSpy.called).to.be.true;
			expect(handleEditorContentChangeSpy.calledWith(htmlString))
				.to.be.true;
			expect(postEditor.state().post)
				.to.have.property('body')
					.that.is.equal(htmlString);
		});

		it('should expand the ImageManager when toggle is clicked', () => {
			const handlerSpy = spy(instance, 'handleImageManagerToggleClicked');
			imageManagerToggle.simulate('click', { preventDefault: spy() });

			expect(handlerSpy.called).to.be.true;
			expect(postEditor.state())
				.to.have.property('imageManagerVisible')
					.that.is.true;
		});

		it('should reassign the bodyImageUrl of the post when image is chosen', () => {
			const imageChosenSpy = spy(instance, 'handleImageChosen');
			imageChosenSpy.call(instance, images[0]);

			expect(postEditor.state())
				.to.have.property('post')
					.that.has.property('bodyImageUrl')
						.that.is.equal(images[0].url);
			expect(postEditor.state())
				.to.have.property('imageManagerVisible')
					.that.is.false;
		});
	});

});