/// <reference types="mocha"/>

import * as React from 'react';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import * as sinon from 'sinon';
import { shallow, ShallowWrapper, mount, ReactWrapper } from 'enzyme';
import { componentDebugLog } from '../TestUtil';

import { Link } from 'react-router-dom';
import { SideMenu } from '../../components/side-menu/SideMenu';
import { MenuEntry } from '../../components/side-menu/MenuEntry';
import { SideMenuToggle } from '../../components/side-menu/SideMenuToggle';
import { IconButton } from '../../components/icon-button/IconButton';

import { createRouterMockStore, createAuthenticatedMockStore } from '../../fixtures/MockStores';
import { createMemoryHistory } from 'history';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import { sideMenuEntries, adminSideMenuEntries } from '../../core/Routes';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<SideMenuToggle/>', () => {
	let sideMenuToggle: ReactWrapper<any, any>;
	const mockHistory = createMemoryHistory();
	let mockStore = createAuthenticatedMockStore(mockHistory);

	it('should render without problems', () => {
		sideMenuToggle = mount(
			<Provider store={mockStore}>
				<ConnectedRouter history={mockHistory}>
					<SideMenuToggle isAuthenticated={false} activeRoute="/"/>
				</ConnectedRouter>
			</Provider>
		);
		expect(sideMenuToggle).to.be.present();
	});

	it('should be collapsed initially', () => {
		const sideMenu = sideMenuToggle.find(SideMenu);
		expect(sideMenu.props())
			.to.have.property('isExpanded', false);
	});

	it('it should toggle the side menu on click', () => {
		const sideMenu = sideMenuToggle.find(SideMenu);
		const menuButton = sideMenuToggle.find(IconButton).at(0);

		menuButton.simulate('click');
		expect(sideMenu.props())
			.to.have.property('isExpanded', true);

		menuButton.simulate('click');
		expect(sideMenu.props())
			.to.have.property('isExpanded', false);
	});
});

describe('<SideMenu/>', () => {
	let sideMenu: ReactWrapper<any, any>;
	const hideMenuSpy: sinon.SinonSpy = sinon.spy();
	const mockHistory = createMemoryHistory();
	let mockStore = createRouterMockStore(mockHistory);

	it('should render without problems', () => {
		sideMenu = mount(
			<Provider store={mockStore}>
				<ConnectedRouter history={mockHistory}>
					<SideMenu
						hideMenu={hideMenuSpy}
						isAuthenticated={false}
						activeRoute="/"
						isExpanded={true}
					/>
				</ConnectedRouter>
			</Provider>
		);
		expect(sideMenu).to.be.present();
	});

	it('should have default side menu entries if not logged in', () => {
		const entries = sideMenu.find(MenuEntry);
		expect(entries).to.be.present();
		expect(entries.length).to.be.equal(sideMenuEntries.length);
		for(let entry of sideMenuEntries) {
			expect(entries.someWhere(node => node.text().trim() === entry.name))
				.to.be.true;
		}
	});

	it('should have an entry with an active class', () => {
		const activeRoute = sideMenu.find('.active');
		expect(activeRoute).to.be.present();
		expect(activeRoute).to.be.of.length(1);
	});

	it('should have admin side menu entries if user is logged in', () => {
		sideMenu = mount(
			<Provider store={mockStore}>
				<ConnectedRouter history={mockHistory}>
					<SideMenu
						hideMenu={hideMenuSpy}
						isAuthenticated={true}
						activeRoute="/"
						isExpanded={true}
					/>
				</ConnectedRouter>
			</Provider>
		);

		const children = sideMenu.find(MenuEntry);
		expect(children).to.be.present();
		expect(children).to.be.of.length(adminSideMenuEntries.length);
		for(let entry of adminSideMenuEntries) {
			const nodeWithName = children.findWhere(node => node.text().trim() === entry.name);
			expect(nodeWithName).to.be.present();
		}
	});

	it('should have a button for dismissing the menu', () => {
		const closeButton = sideMenu.find(IconButton);
		expect(closeButton).to.be.present();

		hideMenuSpy.reset();
		closeButton.simulate('click');
		expect(hideMenuSpy.called)
			.to.be.true;
	});

	it('should pop up a dark overlay while menu is expanded', () => {
		const overlay = sideMenu.find('.dark-overlay');
		expect(overlay).to.be.present();

		hideMenuSpy.reset();
		overlay.simulate('click');
		expect(hideMenuSpy.called)
			.to.be.true;
	});
});

describe('<MenuEntry/>', () => {
	let menuEntry: ShallowWrapper<any, any>;
	let mockLink: NavMenuEntry;
	let clickSpy: sinon.SinonSpy;

	before(() => {
		mockLink = {
			name: 'TestRoute01',
			path: '/test1',
			iconClass: 'test',
			active: true
		};
		clickSpy = sinon.spy();
	});

	it('should render a <MenuEntry/> component', () => {
		menuEntry = shallow(<MenuEntry entry={mockLink} onClick={clickSpy}/>);
		expect(menuEntry).to.be.present();
	});

	it('should contain an icon', () => {
		const icon = menuEntry.find('i');
		expect(icon).to.be.present();
	});

	it('should contain a <Link/> component', () => {
		const link = menuEntry.find(Link);
		expect(link).to.be.present();
	});

	it('should call the onClick handler', () => {
		menuEntry.simulate('mouseDown');
		expect(clickSpy).to.have.property('callCount', 1);
	});

	it('should have an active class', () => {
		expect(menuEntry.hasClass('active')).to.equal(true);
	});
});