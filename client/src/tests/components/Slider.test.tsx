/// <reference types="mocha"/>
/// <reference types="node"/>

import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { componentDebugLog } from '../TestUtil';

import { Slider, Slide } from '../../components/slider/Slider';
import { IconButton } from '../../components/icon-button/IconButton';
import { RoundButton } from '../../components/round-button/RoundButton';

chai.use(chaiEnzyme(componentDebugLog));

const expect: Chai.ExpectStatic = chai.expect;

describe('<Slider/>', () => {
	let slider: ReactWrapper<any, any> = null

	const TestSlide = props => (
		<section> I am test slide number {props.index} </section>
	);

	it('should render without problems', () => {
		slider = mount(
			<Slider
				slides={[
					<TestSlide index={0}/>,
					<TestSlide index={1}/>,
					<TestSlide index={2}/>
				]}
				wrap
				initialIndex={1}
			/>
		);
		expect(slider).to.be.present();
	});

	it('should setup its initial state', () => {
		expect(slider.state())
			.to.have.property('index', 1);
		expect(slider.state())
			.to.have.property('isMovingForward', false);
		expect(slider.state())
			.to.have.property('isTransitioning', false);
	});

	it('should have 2 <RoundButtons/> for navigation', () => {
		const buttons = slider.find(RoundButton);
		expect(buttons).to.be.of.length(2);
	});

	it('should only display slide 1', () => {
		const testSlide = slider.findWhere(node => node.props().index === 1);
		expect(testSlide).to.be.present();
	});

	it('should transition to the next slide', () => {
		const nextButton = slider.find('.next-btn');
		nextButton.simulate('click');
		expect(slider.state())
			.to.have.property('isTransitioning', true);
		expect(slider.state())
			.to.have.property('index', 2);
		expect(slider.state())
			.to.have.property('isMovingForward', true);

		const activeSlide = slider.find(Slide).filterWhere(node => node.props().index === 2);
		expect(activeSlide).to.be.present();
	});

	it('should transition to the previous slide', () => {
		const prevButton = slider.find('.prev-btn');
		prevButton.simulate('click');
		expect(slider.state())
			.to.have.property('isTransitioning', true);
		expect(slider.state())
			.to.have.property('index', 1);
		expect(slider.state())
			.to.have.property('isMovingForward', false);

		const activeSlide = slider.find(Slide).filterWhere(node => node.props().index === 1);
		expect(activeSlide).to.be.present();
	});

	it('should not navigate if there is only 1 slide', () => {
		slider.unmount();
		slider = mount(
			<Slider
				slides={[
					<TestSlide index={0}/>
				]}
				wrap
				initialIndex={0}
			/>
		);

		const prevButton = slider.find('.prev-btn');
			expect(prevButton.exists()).to.be.false;
		const nextButton = slider.find('.next-btn');
			expect(nextButton.exists()).to.be.false;
	});

	it('should not be wrapping around if props.wrap is false', () => {
		slider.unmount();
		slider = mount(
			<Slider
				slides={[
					<TestSlide index={0}/>,
					<TestSlide index={1}/>,
					<TestSlide index={2}/>
				]}
				initialIndex={0}
			/>
		);

		const prevButton = slider.find('.prev-btn');
		expect(prevButton.exists()).to.be.false;
	});

});