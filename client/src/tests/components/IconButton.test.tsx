/// <reference types="mocha"/>

import * as React from 'react';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import * as sinon from 'sinon';
import { componentDebugLog } from '../TestUtil';
import { mount, ReactWrapper } from 'enzyme';
import { IconButton, LabeledButton } from '../../components/icon-button/IconButton';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;
const clickSpy: sinon.SinonSpy = sinon.spy();

describe('<IconButton/>', () =>{
	let iconButton: ReactWrapper<any, any>;
	it('should render a <IconButton/> component', () => {
		iconButton = mount(<IconButton iconClass="test" onClick={clickSpy}/>);
		expect(iconButton).to.be.present();
	});

	it('should contain an icon', () => {
		let icon: ReactWrapper<any, any> = iconButton.find('i');
		expect(icon).to.be.present();
		expect(icon.getDOMNode().classList.contains('test'))
			.to.be.true;
	});

	it('should call the onClick handler', () => {
		iconButton.simulate('click');
		expect(clickSpy).to.have.property('callCount', 1);
	});
});

describe('<LabeledButton/>', () => {
	let labeledButton: ReactWrapper<any, any> = null;
	const clickSpy = sinon.spy();
	const testLabel = 'Click Me!';

	it('should render without problems', () => {
		labeledButton = mount(<LabeledButton label={testLabel} iconClass="test" onClick={clickSpy}/>);
		expect(labeledButton).to.be.present();
	});

	it('should render an icon with the correct class', () => {
		const icon = labeledButton.find('i');
		expect(icon).to.be.present();
		expect(icon.getDOMNode().classList.contains('test'))
			.to.be.true;
	});

	it('should render the label', () => {
		const nodeWithLabel = labeledButton.findWhere(node => node.text().trim() === testLabel);
		expect(nodeWithLabel).to.be.present();
	});

	it('should call the onClick handler', () => {
		labeledButton.simulate('click');
		expect(clickSpy.called).to.be.true;
	});
});