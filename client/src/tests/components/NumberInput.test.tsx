/// <reference types="mocha"/>

import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { spy, SinonSpy } from 'sinon';
import { componentDebugLog } from '../TestUtil';

import { NumberInput } from '../../components/number-input/NumberInput';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<NumberInput/>', () => {
	let numberInput: ReactWrapper<any, any> = null;
	let input: ReactWrapper<any, any> = null;
	const onChangeSpy: SinonSpy = spy();

	it('should render without problems', () => {
		numberInput = mount(
			<NumberInput
				initialValue={50}
				onChange={onChangeSpy}
				min={20}
				max={500}
			/>
		);
		expect(numberInput).to.be.present();
	});

	it('should render a input element', () => {
		input = numberInput.find('input');
		expect(input).to.be.present();
	});

	it('should setup its initial state', () => {
		expect(numberInput.state())
			.to.have.property('value')
				.that.is.equal(50);
	});

	it('should update its state when a number is entered', () => {
		input.simulate('change', {
			target: { value: '80' }
		});

		expect(numberInput.state())
			.to.have.property('value')
				.that.is.equal(80);
	});

	it('should only accept numbers as its inputs', () => {
		const lastValue = numberInput.state().value;
		input.simulate('change', {
			target: { value: 'Test!' }
		});
		expect(numberInput.state())
			.to.have.property('value')
				.that.is.equal(lastValue);
	});

	it('should control the value of the input element', () => {
		const { value } = numberInput.state();
		expect(input).to.have.value(value.toString());
	});

	it('should call the onChange handler function with its value', () => {
		onChangeSpy.reset();
		input.simulate('change', {
			target: { value: '255' }
		});

		expect(onChangeSpy.called).to.be.true;
		expect(onChangeSpy.calledWith(255)).to.be.true;
	});

	it('should not exceed the maximum value', () => {
		input.simulate('change', {
			target: { value: '550' }
		});
		expect(numberInput.state())
			.to.have.property('value')
				.that.is.equal(500);
	});

	it('should not drop below the minimum value', () => {
		input.simulate('change', {
			target: { value: '10' }
		});
		expect(numberInput.state())
			.to.have.property('value')
				.that.is.equal(20);
	});

	it('should update its value if the initial value changes', () => {
		numberInput.setProps({ initialValue: 60 });
		expect(numberInput.state())
			.to.have.property('value')
				.that.is.equal(60);
	});

});