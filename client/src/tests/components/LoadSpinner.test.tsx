/// <reference types="mocha"/>

import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { componentDebugLog } from '../TestUtil';

import { LoadSpinner } from '../../components/load-spinner/LoadSpinner';
import { SpinnerShape } from '../../components/load-spinner/SpinnerShape';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<LoadSpinner/>', () => {
	let loadSpinner: ReactWrapper<any, any>;
	it('should render a <LoadSpinner/> component', () => {
		loadSpinner = mount(<LoadSpinner radius={50} strokeWidth={10}/>);
		expect(loadSpinner).to.be.present();
	});

	it('should contain a <SpinnerShape/>', () => {
		const spinnerShape = loadSpinner.find(SpinnerShape);
		expect(spinnerShape).to.be.present();
	});

	it('should have the expected initial state', () => {
		const expectedState = {
			motionKey: 0,
			iteration: 0,
			start: 0,
			end: 0,
			nextStart: 0,
			nextEnd: .5
		};

		expect(loadSpinner.state())
			.to.deep.equal(expectedState);
	});

});

describe('<SpinnerShape/>', () => {
	let spinnerShape: ReactWrapper<any, any>;
	const spinnerProps = {
		radius: 50,
		center: { x: 50, y: 50 },
		start: { x: 50, y: 0 },
		end: { x: 100, y: 50 },
		strokeWidth: 10
	};

	it('should render a <SpinnerShape/> component', () => {
		spinnerShape = mount(
			<SpinnerShape {...spinnerProps}/>
		);
		expect(spinnerShape).to.be.present();
	});

	it('should contain an <svg> element', () => {
		const svg = spinnerShape.find('svg');
		expect(svg).to.be.present();
	});

	it('should have the expected path', () => {
		const path = spinnerShape.find('path');
		expect(path).to.be.present();

		const expectedPath = 'M 60 10 A 50 50 0 0 1 110 60';
		expect(path).to.have.prop('d', expectedPath);
	});
});