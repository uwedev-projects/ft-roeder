/// <reference types="mocha"/>

import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { componentDebugLog } from '../TestUtil';

import { Header } from '../../components/header/Header';
import { NavBar } from '../../components/nav-bar/NavBar';
import { navBarEntries, navBarAdminEntries } from '../../core/Routes';
import { SideMenuToggle } from '../../components/side-menu/SideMenuToggle';
import { Link } from 'react-router-dom';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<Header/>', () => {
	let header: ShallowWrapper<any, any>;
	
	it('should render a <Header/> component', () => {
		header = shallow(<Header/>);
		expect(header).to.be.present();
	});

	it('should contain a <NavBar/>', () => {
		const navBar = header.find(NavBar);
		expect(navBar).to.be.present();
	});

	it('should contain a <SideMenuToggle/>', () => {
		const iconButton: ShallowWrapper<any, any> = header.find(SideMenuToggle);
		expect(iconButton).to.be.present();
	});
});

describe('<NavBar/>', () => {
	let navBar: ShallowWrapper<any, any>;
	it('should render a <NavBar> component', () => {
		navBar = shallow(<NavBar isAuthenticated={false} activeRoute="/"/>);
		expect(navBar).to.be.present();
	});

	it('should contain all nav bar entries', () => {
		const entries = navBar.find('.nav-bar-entry');
		expect(entries).to.be.of.length(navBarEntries.length);

		for(let entry of navBarEntries) {
			const link = navBar.find(Link)
				.filterWhere(node => node.props().to === entry.path);
			expect(link).to.be.present();
		};
	});

	it('should render all admin entries if user is authenticated', () => {
		navBar.setProps({ isAuthenticated: true });
		navBar.update();
		const entries = navBar.find('.nav-bar-entry');
		expect(entries).to.be.of.length(navBarAdminEntries.length);

		for(let entry of navBarAdminEntries) {
			const link = navBar.find(Link)
				.filterWhere(node => node.props().to === entry.path);
			expect(link).to.be.present();
		};
	});
});