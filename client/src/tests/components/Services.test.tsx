/// <reference types="mocha"/>

import * as React from 'react';
import { mount, ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { spy, SinonSpy } from 'sinon';
import { componentDebugLog } from '../TestUtil';
import { getFullImageUrl, getThumbnailUrlFromImage } from '../../util/ImageUtils';
import { Link, MemoryRouter } from 'react-router-dom';

const { services } = require('../../fixtures/Services');

import { ServiceOverview } from '../../components/services/ServiceOverview';
import { ServiceCard } from '../../components/services/ServiceCard';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('Services components', () => {

	describe('<ServiceCard/>', () => {
		let serviceCard: ReactWrapper<any, any> = null;
		const serviceName = 'Fenstertechnik';
		const serviceImgUrl = '/some/test-image.png';

		it('should render without problems', () => {
			serviceCard = mount(
				<MemoryRouter>
					<ServiceCard
						name={serviceName}
						imgUrl={serviceImgUrl}
					/>
				</MemoryRouter>
			);
			expect(serviceCard).to.be.present();
		});

		it('should render the service name', () => {
			const nodeWithName = serviceCard
				.findWhere(node => node.text().trim() === serviceName);
			expect(nodeWithName).to.be.present();
		});

		it('should render an medium version of the description image', () => {
			const expectedUrl = getThumbnailUrlFromImage(serviceImgUrl, 'md');
			expect(serviceCard.find('.card-content'))
				.to.have.style('background-image', `url(${expectedUrl})`);
		});

		it('should render <Link/> to the corresponding service detail page', () => {
			const link = serviceCard.find(Link);
			expect(link).to.be.present();
			expect(link.props())
				.to.have.property('to')
					.that.is.eql(`/leistungen/${serviceName}`);
		});

	});

	describe('<ServiceOverview/>', () => {
		let serviceOverview: ShallowWrapper<any, any> = null;
		
		it('should render without problems', () => {
			serviceOverview = shallow(<ServiceOverview services={services}/>);
			expect(serviceOverview).to.be.present();
		});

		it('should render a <ServiceCard/> for each service', () => {
			const serviceCards = serviceOverview.find(ServiceCard);
			expect(serviceCards).to.have.length(services.length);
			
			for(let i = 0; i < services.length; i++) {
				expect(serviceCards.at(i).props())
					.to.have.property('name')
						.that.is.equal(services[i].name);
				expect(serviceCards.at(i).props())
					.to.have.property('imgUrl')
						.that.is.equal(services[i].descImageUrl);
			}
		});
	});

});