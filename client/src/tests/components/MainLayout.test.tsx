/// <reference types="mocha"/>

import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { componentDebugLog } from '../TestUtil';

import { MainLayout } from '../../components/main-layout/MainLayout';
import NavController from '../../components/nav-controller/NavController';
import Header from '../../components/header/Header';
import { SideMenu } from '../../components/side-menu/SideMenu';
import MessagePanel from '../../components/message-panel/MessagePanel';
import { Footer } from '../../components/footer/Footer';
import { LoadSpinner } from '../../components/load-spinner/LoadSpinner';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<MainLayout/>', () => {
	let mainLayout: ShallowWrapper<any, any>;

	it('render a <MainLayout/> component', () => {
		mainLayout = shallow(<MainLayout isFetching={true}/>);
		expect(mainLayout).to.be.present();
	});

	it('should have a <NavController/> child', () => {
		const navController: ShallowWrapper<any, any> = mainLayout.find(NavController);
		expect(navController).to.be.present();
	});

	it('should have a <Header/> child', () => {
		const header: ShallowWrapper<any, any> = mainLayout.find(Header);
		expect(header).to.be.present();
	});

	it('should have a <LoadSpinner/> child', () => {
		const loadSpinner: ShallowWrapper<any, any> = mainLayout.find(LoadSpinner);
		expect(loadSpinner).to.be.present();
	});

	it('should have a <MessagePanel/> child', () => {
		const messagePanel: ShallowWrapper<any, any> = mainLayout.find(MessagePanel);
		expect(messagePanel).to.be.present();
	});

	it('should have a <Footer/> child', () => {
		const footer: ShallowWrapper<any, any> = mainLayout.find(Footer);
		expect(footer).to.be.present();
	});
});