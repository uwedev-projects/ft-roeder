/// <reference types="mocha"/>

import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { spy, SinonSpy, stub, SinonStub } from 'sinon';
import { componentDebugLog } from '../TestUtil';

import { RangeSlider, RangeArea, SliderCircle, SliderValue } from '../../components/range-slider/RangeSlider';
import { NumberInput } from '../../components/number-input/NumberInput';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;


describe('Range Slider components', () => {
	let getBoundingClientRectStub: SinonStub = null;

	describe('<RangeSlider/>', () => {
		let rangeSlider: ReactWrapper<any, any> = null;
		let rangeArea: ReactWrapper<any, any> = null;
		let sliderInstance = null;
		const min = 50;
		const max = 100;
		const initialValue = 60;
		const step = 5;
		const onChangeSpy = spy();
		let handleKeyDownSpy = null;

		it('should render without problems', () => {
			rangeSlider = mount(
				<RangeSlider
					initialValue={initialValue}
					min={min}
					max={max}
					step={step}
					onChange={onChangeSpy}
				/>
			);
			expect(rangeSlider).to.be.present();
			sliderInstance = rangeSlider.instance();
			handleKeyDownSpy = spy(sliderInstance, 'handleKeyDown');
		});

		it('should setup its initial state', () => {
			expect(rangeSlider.state())
				.to.have.property('value', initialValue);
			expect(rangeSlider.state())
				.to.have.property('lastValue', null);
			expect(rangeSlider.state())
				.to.have.property('movementStart', null);
			expect(rangeSlider.state())
				.to.have.property('steppedValue', initialValue - initialValue % step);
		});

		it('should render a <RangeArea/>', () => {
			rangeArea = rangeSlider.find(RangeArea);
			expect(rangeArea).to.be.present();
			expect(rangeArea.props())
				.to.have.property('sliderTranslation');
		});

		it('should render a <SliderValue/>', () => {
			const sliderValue = rangeSlider.find(SliderValue);
			expect(sliderValue).to.be.present();
			expect(sliderValue.props())
				.to.have.property('value')
					.that.is.equal(rangeSlider.state().value);
		});

		it('should be focusable', () => {
			expect(rangeSlider.getDOMNode().getAttribute('tabindex'))
				.to.equal('0');
		});

		it('should keep track of the last value', () => {
			const expectedLastValue = rangeSlider.state().value;
			const willReceivePropsSpy = spy(sliderInstance, 'componentWillReceiveProps');
			
			willReceivePropsSpy.call(sliderInstance, null, { value: 70 });
			
			expect(rangeSlider.state())
				.to.have.property('lastValue')
					.that.is.equal(expectedLastValue);
		});

		it('should calculate the translation correctly', () => {
			const componentDidUpdateStub = stub(sliderInstance, 'componentDidUpdate')
				.callsFake(() => {});
			rangeSlider.setState({ width: 240 });
			const getTranslationSpy = spy(sliderInstance, 'getSliderTranslation');
			const expectedTranslation = (initialValue - min) / (max - min) * 240 + 
				(RangeSlider as any).HORIZONTAL_PADDING * .5;

			expect(getTranslationSpy.call(sliderInstance, rangeSlider.state().value))
				.to.equal(expectedTranslation);
			
			componentDidUpdateStub.restore();
		});

		it('should calculate the correct value from an x position on call the onChange handler', () => {
			onChangeSpy.reset();
			let expectedValue = Math.round(
				Math.min(max, (max - min) * 80 / 240 + min)
			);
			if(expectedValue % step < step / 2)
				expectedValue -= expectedValue % step;
			else
				expectedValue += step - expectedValue % step;

			const updateValueFromPositionSpy = spy(sliderInstance, 'updateValueFromPosition');
			updateValueFromPositionSpy.call(sliderInstance, 80);
			expect(rangeSlider.state())
				.to.have.property('value')
					.that.is.equal(expectedValue);
			expect(onChangeSpy.called).to.be.true;
			expect(onChangeSpy.calledWith(expectedValue)).to.be.true;
			updateValueFromPositionSpy.restore();
		});

		it('should calculate the correct stepped value from an x position', () => {
			let expectedValue = Math.round(
				Math.min(max, (max - min) * 140 / 240 + min)
			);
			if(expectedValue % step < step / 2)
				expectedValue -= expectedValue % step;
			else
				expectedValue += step - expectedValue % step;
			const updateValueFromPositionSpy = spy(sliderInstance, 'updateValueFromPosition');
			updateValueFromPositionSpy.call(sliderInstance, 140, true);
			expect(rangeSlider.state())
				.to.have.property('value')
					.that.is.equal(expectedValue);
			updateValueFromPositionSpy.restore();
		});

		it('should store the starting position of the movement', () => {
			const handleSliderMoveStartSpy = spy(sliderInstance, 'handleSliderMoveStart');
			handleSliderMoveStartSpy.call(sliderInstance, 210);
			expect(rangeSlider.state())
				.to.have.property('movementStart', 210);
		});

		it('should set the starting position to null and update its translation on movement end', () => {
			const updateValueFromPositionSpy = spy(sliderInstance, 'updateValueFromPosition');
			const handleSliderMoveEndSpy = spy(sliderInstance, 'handleSliderMoveEnd');
			handleSliderMoveEndSpy.call(sliderInstance, 180);
			expect(rangeSlider.state())
				.to.have.property('movementStart')
					.that.is.null;
			expect(updateValueFromPositionSpy.calledWith(180, true)).to.be.true;

			updateValueFromPositionSpy.restore();
		});

		it('should set the starting position to null and update its translation on area click', () => {
			const updateValueFromPositionSpy = spy(sliderInstance, 'updateValueFromPosition');
			const handleAreaClickSpy = spy(sliderInstance, 'handleAreaClick');
			rangeSlider.setState({ movementStart: 123 });

			handleAreaClickSpy.call(sliderInstance, 335);
			expect(updateValueFromPositionSpy.calledWith(335, true)).to.be.true;
			expect(rangeSlider.state())
				.to.have.property('movementStart', null);
			
			updateValueFromPositionSpy.restore();
		});

		it('should update its internal value on slider move', () => {
			rangeSlider.setState({ movementStart: 100 });
			const updateValueFromPositionSpy = spy(sliderInstance, 'updateValueFromPosition');
			const handleSliderMoveSpy = spy(sliderInstance, 'handleSliderMove');
			handleSliderMoveSpy.call(sliderInstance, 120);
			expect(updateValueFromPositionSpy.calledWith(120)).to.be.true;
			updateValueFromPositionSpy.restore();
		});

		it('should call the onChange handler function when the value changes', () => {
			const updateValueFromPositionSpy = spy(sliderInstance, 'updateValueFromPosition');
			updateValueFromPositionSpy.call(sliderInstance, 220);

			const updatedValue = rangeSlider.state().value;
			expect(onChangeSpy.called).to.be.true;
			expect(onChangeSpy.calledWith(updatedValue)).to.be.true;

			updateValueFromPositionSpy.restore();
		});

		it('should increment the value by 1 step on right arrow key press', () => {
			const expectedValue = Math.min(max, rangeSlider.state().value + step);
			rangeSlider.simulate('keydown', {
				key: 'ArrowRight'
			});
			expect(rangeSlider.state())
				.to.have.property('value')
					.that.is.equal(expectedValue);
		});

		it('should decrement the value by 1 step on left arrow key press', () => {
			const expectedValue = Math.max(min, rangeSlider.state().value - step);
			rangeSlider.simulate('keydown', {
				key: 'ArrowLeft'
			});
			expect(rangeSlider.state())
				.to.have.property('value')
					.that.is.equal(expectedValue);
			handleKeyDownSpy.restore();
		});

		it('should handle a slider value changed event', () => {
			const handleSliderValueChange = spy(sliderInstance, 'handleSliderValueChange');
			handleSliderValueChange.call(sliderInstance, 78);
			expect(rangeSlider.state())
				.to.have.property('value')
					.that.is.equal(80);
			handleSliderValueChange.restore();
		});
	});
	
	describe('<RangeArea/>', () => {
		let rangeArea: ReactWrapper<any, any> = null;
		let sliderCircle: ReactWrapper<any, any> = null;
		let sliderValue: ReactWrapper<any, any> = null;
		const onSliderMoveStartSpy = spy();
		const onSliderMoveSpy = spy();
		const onSliderMoveEndSpy = spy();
		const onAreaClickSpy = spy();

		it('should render without problems', () => {
			rangeArea = mount(
				<RangeArea
					sliderTranslation={60}
					lastTranslation={70}
					onSliderMoveStart={onSliderMoveStartSpy}
					onSliderMove={onSliderMoveSpy}
					onSliderMoveEnd={onSliderMoveEndSpy}
					onAreaClick={onAreaClickSpy}
				/>
			);
			expect(rangeArea).to.be.present();
		});
	
		it('should render a <SliderCircle/> with correct translation', () => {
			sliderCircle = rangeArea.find(SliderCircle);
			expect(sliderCircle).to.be.present();
			expect(sliderCircle.props())
				.to.have.property('xTranslation')
					.that.is.equal(70);
		});

		it('should render a background area', () => {
			expect(rangeArea.find('.background-area'))
				.to.be.present();
		});

		it('should render a progress bar', () => {
			const progressBar = rangeArea.find('.slider-progress');
			expect(progressBar).to.be.present();
			expect(progressBar).to.have.style('width', '70px');
		});

		it('should call the onSlideMoveStart handler function', () => {
			rangeArea.simulate('mousedown', { clientX: 155 });
			expect(onSliderMoveStartSpy.called).to.be.true;
			expect(onSliderMoveStartSpy.calledWith(155)).to.be.true;

			onSliderMoveStartSpy.reset();
			
			rangeArea.simulate('touchstart', {
				changedTouches: [{ clientX: 120 }]
			});
			expect(onSliderMoveStartSpy.called).to.be.true;
			expect(onSliderMoveStartSpy.calledWith(120)).to.be.true;
		});

		it('should call the onSlideMove handler function', () => {
			rangeArea.simulate('mousemove', { clientX: 200 });
			expect(onSliderMoveSpy.called).to.be.true;
			expect(onSliderMoveSpy.calledWith(200))
				.to.be.true;

			onSliderMoveSpy.reset();
			rangeArea.simulate('touchmove', {
				changedTouches: [{ clientX: 175 }]
			});
			expect(onSliderMoveSpy.called).to.be.true;
			expect(onSliderMoveSpy.calledWith(175)).to.be.true;
		});

		it('should call the onSlideMoveEnd handler function', () => {
			rangeArea.simulate('touchend', {
				changedTouches: [{ clientX: 350 }]
			});
			expect(onSliderMoveEndSpy.called).to.be.true;
			expect(onSliderMoveEndSpy.calledWith(350)).to.be.true;
		});

		it('should call the onAreaClick handler function', () => {
			rangeArea.simulate('mouseup', { clientX: 320 });
			expect(onAreaClickSpy.called).to.be.true;
			expect(onAreaClickSpy.calledWith(320)).to.be.true;
		});
	});
	
	describe('<SliderCircle/>', () => {
		let sliderCircle: ReactWrapper<any, any> = null;
		const translation = 40;
	
		it('should render without problems', () => {
			sliderCircle = mount(<SliderCircle xTranslation={translation}/>);
			expect(sliderCircle).to.be.present();
		});
	
		it('should set its translation properly', () => {
			expect(sliderCircle).to.have
				.style('transform', `translateX(${translation}px)`);
		});
	});
	
	describe('<SlideValue/>', () => {
		let sliderValue: ReactWrapper<any, any> = null;
		let numberInput: ReactWrapper<any, any> = null;
		const onChangeSpy: SinonSpy = spy();

		it('should render without problems', () => {
			sliderValue = mount(
				<SliderValue
					value={175}
					min={50}
					max={200}
					onChange={onChangeSpy}
				/>
			);
			expect(sliderValue).to.be.present();
		});
	
		it('should render a <NumberInput/>', () => {
			numberInput = sliderValue.find(NumberInput);
			expect(numberInput).to.be.present();
			expect(numberInput.props())
				.to.have.property('min')
					.that.is.equal(50);
			expect(numberInput.props())
				.to.have.property('max')
					.that.is.equal(200);
			expect(numberInput.props())
				.to.have.property('initialValue')
					.that.is.equal(175);
		});

		it('should call the onChange handler function with the value', () => {
			numberInput.props().onChange(100);
			expect(onChangeSpy.called).to.be.true;
			expect(onChangeSpy.calledWith(100)).to.be.true;
		});
	});

});