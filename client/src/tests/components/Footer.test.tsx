/// <reference types="mocha"/>

import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { componentDebugLog } from '../TestUtil';

import { createRouterMockStore } from '../../fixtures/MockStores';
import { createMemoryHistory } from 'history';

import { Link } from 'react-router-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { Footer } from '../../components/footer/Footer';
import { footerEntries } from '../../core/Routes';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<Footer/>', () => {
	let footer: ReactWrapper<any, any>;
	
	it('should render without problems', () => {
		footer = mount(
			<Provider store={createRouterMockStore()}>
				<ConnectedRouter history={createMemoryHistory()}>
					<Footer/>
				</ConnectedRouter>
			</Provider>
		);
		expect(footer).to.be.present();
	});

	it('should render a <Link/> for each footer nav entries', () => {
		for(let entry of footerEntries) {
			const nodeWithEntry = footer.find(Link)
				.filterWhere(node => node.getDOMNode().getAttribute('href') === entry.path);
			expect(nodeWithEntry).to.be.present();
		}
	});
});