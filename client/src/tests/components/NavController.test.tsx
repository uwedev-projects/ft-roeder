/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>

import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import * as testUtils from 'redux-test-utils';
import { componentDebugLog } from '../TestUtil';

import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import { NavController } from '../../components/nav-controller/NavController';
import { protectedRoute } from '../../components/protected-route/ProtectedRoute';
import AboutPage from '../../pages/about/AboutPage';
import { DashboardPage } from '../../pages/admin/dashboard/DashboardPage';
import { createMemoryHistory } from 'history';
import { createRouterMockStore, createAuthenticatedMockStore } from '../../fixtures/MockStores';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('<NavController/>', () => {
	let navController: ReactWrapper<any, any>;
	const mockHistory = createMemoryHistory();
	const mockStore = createRouterMockStore(mockHistory);

	before(() => {
		navController = mount(
			<Provider store={mockStore}>
				<ConnectedRouter history={mockHistory}>
					<NavController/>
				</ConnectedRouter>
			</Provider>
		);
	});

	it('should render a <NavController/> component', () => {
		const navMap: Map<string, React.Component<any, any>> = new Map();
		expect(navController).to.be.present();
	});

	it('should render a <Switch/>', () => {
		const switchComponent = navController.find(Switch);
		expect(switchComponent).to.be.present();
	});

	it('render a <Route/> to the <AboutPage/> when pathname is /über-mich',() => {
		mockHistory.push('/über-mich');
		const aboutRoute = navController.find(Route)
			.filterWhere(node => node.props().path === '/über-mich');
		expect(aboutRoute).to.be.present();
	});

	after(() => {
		navController.unmount();
	});
});

describe('<ProtectedRoute/>', () => {
	let protectedDashboard: ReactWrapper<any, any>;

	const mockHistory = createMemoryHistory();
	const mockStore = createAuthenticatedMockStore(mockHistory);

	let ProtectedDashboard = protectedRoute(DashboardPage);

	before(() => {
		protectedDashboard = mount(
			<Provider store={mockStore}>
				<ConnectedRouter history={mockHistory}>
					<ProtectedDashboard/>
				</ConnectedRouter>
			</Provider>
		);
	});

	it('should render without problems', () => {
		expect(protectedDashboard).to.be.present();
	});

	it('should render the wrapped component if user is logged in', () => {
		const dashboard = protectedDashboard.find(DashboardPage);
		expect(dashboard).to.be.present();
	});

	it('should render a <Redirect/> if the user is not logged in', () => {
		protectedDashboard.unmount();
		protectedDashboard = mount(
			<Provider store={createRouterMockStore(mockHistory)}>
				<ConnectedRouter history={mockHistory}>
					<ProtectedDashboard/>
				</ConnectedRouter>
			</Provider>
		);
		
		const dashboard = protectedDashboard.find(DashboardPage);
		expect(dashboard.exists()).to.be.false;

		const redirect = protectedDashboard.find(Redirect);
		expect(redirect).to.be.present();
	});

});