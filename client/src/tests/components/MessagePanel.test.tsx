/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>

import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { stub, SinonStub } from 'sinon';
import { componentDebugLog } from '../TestUtil';

import { Motion } from 'react-motion';
import { MessagePanel, Message } from '../../components/message-panel/MessagePanel';
import { MessageType } from '../../constants/UIConstants';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

const messages: StatusMessage[] = [
	{
		text: "message1",
		type: MessageType.SUCCESS,
		duration: 1000,
		id: 'message1'
	},
	{
		text: "message2",
		type: MessageType.ERROR,
		duration: 4250,
		id: 'message2'
	}
];

describe('<MessagePanel/>', () => {
	let messagePanel: ReactWrapper<any, any>;

	it('should render without problems', () => {
		messagePanel = mount(<MessagePanel messages={[]}/>);
		expect(messagePanel).to.be.present();
	});

	it('should render an array of messages', () => {
		expect(messagePanel.find(Message)).to.be.of.length(0);

		messagePanel = mount(<MessagePanel messages={messages}/>);

		expect(messagePanel.find(Message)).to.be.of.length(2);
	});
});

describe('<Message/>', () => {
	let message: ReactWrapper<any, any> = null;
	const startTime = 1000;
	const restTime = 2000;
	let timeStub: SinonStub = null;

	before(() => {
		timeStub = stub(Date, 'now').callsFake(() => startTime);
	});

	it('should render without problems', () => {
		message = mount(<Message {...messages[0]}/>);
		expect(message).to.be.present();
	});

	it('should render a <Motion/> after mounting', () => {
		const motion = message.find(Motion);
		expect(motion).to.be.present();
	});

	it('should have a state of isFading: false', () => {
		expect(message.state())
			.to.have.property('isFading', false);
	});

	it('should contain the specified text', () => {
		const text = message.findWhere(node => node.text().trim() === messages[0].text);
		expect(text).to.be.present();
	});

	it('should have a class according to the message type', () => {
		expect(message.find('section').hasClass('success'))
			.to.be.true;
		message.unmount();
		message = mount(<Message {... messages[1]}/>);
		expect(message.find('section').hasClass('error'))
			.to.be.true;
	});

	it('should store the animation start time', () => {
		expect(message.state())
			.to.have.property('startTime', Date.now());
	});

	it('should set isFading to true on rest', () => {
		timeStub.restore();
		timeStub = stub(Date, 'now').callsFake(() => restTime);

		const motion = message.find(Motion);
		(message.instance() as any).handleMotionRested();
		message.update();

		expect(message.state())
			.to.have.property('isFading')
			.that.is.true;
	});

	it('should set the fadeOutTime on rest', () => {
		expect(message.state())
			.to.have.property('fadeOutTime', messages[1].duration - (restTime - startTime));
	});

	it('should start to fade out on rest', () => {
		const section = message.find('section');
		expect(section)
			.to.have.className('fading');
		
		const fadeOutTime = messages[1].duration - (restTime - startTime);

		expect(section)
			.to.have.style('transition', `${Math.round(fadeOutTime * 100) / 100000}s opacity ease`);
		timeStub.restore();
	});

	after(() => timeStub.restore());

});