/// <reference types="mocha"/>

import * as React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import * as chai from 'chai';
import * as chaiEnzyme from 'chai-enzyme';
import { spy, SinonSpy } from 'sinon';
import { componentDebugLog } from '../TestUtil';

import { SimpleDialogue } from '../../components/dialogue/SimpleDialogue';

chai.use(chaiEnzyme(componentDebugLog));
const expect: Chai.ExpectStatic = chai.expect;

describe('Dialogue Components', () => {

	describe('<SimpleDialogue/>', () => {
		let simpleDialogue: ReactWrapper<any, any> = null;
		const title = 'Test Dialogue';
		const message = 'Please confirm the operation.';
		const onConfirmationSpy: SinonSpy = spy();
		const onAbortionSpy: SinonSpy = spy();
		const confirmText = 'Confirm';
		const abortText = 'Abort';
		let confirmationButton = null;
		let abortionButton = null;
		let overlay = null;

		it('should render without problems', () => {
			simpleDialogue = mount(
				<SimpleDialogue
					title={title}
					message={message}
					onConfirmation={onConfirmationSpy}
					onAbortion={onAbortionSpy}
					confirmText={confirmText}
					abortText={abortText}
					isVisible={true}
				/>
			);
			expect(simpleDialogue).to.be.present();
		});

		it('should render the title', () => {
			const nodeWithTitle = simpleDialogue
				.findWhere(node => node.text().trim() === title);
			expect(nodeWithTitle).to.be.present();
		});

		it('should render the dialogue message', () => {
			const nodeWithMessage = simpleDialogue
				.findWhere(node => node.text().trim() === message);
			expect(nodeWithMessage).to.be.present();
		});

		it('should render a confirmation button', () => {
			confirmationButton = simpleDialogue.find('.confirm');
			expect(confirmationButton).to.be.present();
			expect(confirmationButton.text().trim())
				.to.equal(confirmText);
		});

		it('should render an abortion button', () => {
			abortionButton = simpleDialogue.find('.abort');
			expect(abortionButton).to.be.present();
			expect(abortionButton.text().trim())
				.to.equal(abortText);
		});

		it('should render a dark overlay', () => {
			overlay = simpleDialogue.find('.transparent-overlay');
			expect(overlay).to.be.present();
		});

		it('should call the onConfirmation handler function', () => {
			confirmationButton.simulate('click');
			expect(onConfirmationSpy.called)
				.to.be.true;
		});

		it('should call the onAbortion handler function', () => {
			abortionButton.simulate('click');
			expect(onAbortionSpy.called)
				.to.be.true;
		});

		it('should call the onAbortion handler if the overlay is clicked', () => {
			onAbortionSpy.reset();
			overlay.simulate('click');
			expect(onAbortionSpy.called)
				.to.be.true;
		});
	});

});