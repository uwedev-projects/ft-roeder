/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>

import * as chai from 'chai';
import { registerAssertions } from 'redux-actions-assertions/chai';
import { registerMiddlewares, registerInitialStoreState, buildInitialStoreState } 
															from 'redux-actions-assertions';

import * as sinon from 'sinon';
import * as uuid from 'uuid';
import thunk from 'redux-thunk';
import rootReducer from '../../reducers/Root'

import { pushStatusMessage, createStatusMessage, dismissStatusMessage } from '../../actions/UI';
import { UI } from '../../constants/ActionTypes';
import { MessageType, StatusMessages } from '../../constants/UIConstants';

const expect: any = chai.expect;

describe('UI Action Creators', () => {

	before(() => {
		registerAssertions();
		registerMiddlewares([thunk]);
		registerInitialStoreState(buildInitialStoreState(rootReducer));
	})

	describe('createStatusMessage()', () => {
		const expectedAction = {
			type: UI.CREATE_STATUS_MESSAGE,
			message: {
				id: 'test',
				text: 'test',
				duration: 1000,
				type: 'INFO'
			}
		};
		expect(createStatusMessage('test', 'test', 1000, 'INFO'))
			.to.deep.equal(expectedAction);
	});

	describe('dismissStatusMessage()', () => {
		const expectedAction = {
			type: UI.DISMISS_STATUS_MESSAGE,
			id: 'test'
		};
		expect(dismissStatusMessage('test'))
			.to.deep.equal(expectedAction);
	});

	describe('pushMessage()', () => {
		let clock: sinon.SinonFakeTimers = null;
		let uuidStub: sinon.SinonStub = null;
		const messageId = 'test'
		before(() => {
			uuidStub = sinon.stub(uuid, 'v4').callsFake(() => messageId);
			clock = sinon.useFakeTimers();
		});
		it('should create a message and dismiss it after 7.5 seconds', (done) => {
			const expectedActions = [
				createStatusMessage(messageId, 'testMessage', StatusMessages.DEFAULT_DURATION, 'INFO'),
				dismissStatusMessage(messageId)
			];
			expect(pushStatusMessage('testMessage'))
				.to.dispatch.actions(expectedActions, done);
			clock.tick(8000);
		});

		after(() => {
			(uuid.v4 as any).restore();
			clock.restore();
		})
	});

});