/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>
import * as chai from 'chai';
import * as uuid from 'uuid';
import { stub, SinonStub } from 'sinon';
import { registerAssertions } from 'redux-actions-assertions/chai';

import { networkRequest, setVisibilityState, receiveJwt, 
				 setConnectionState, setApiServer } from '../../actions/Application';
import { Application } from '../../constants/ActionTypes';

const expect: Chai.ExpectStatic = chai.expect;

describe('Application Action Creators', () => {
	before(() => registerAssertions());

	describe('networkRequest()', () => {
		let uuidStub: SinonStub = null;

		before(() => {
			uuidStub = stub(uuid, 'v4').callsFake(() => 'test');
		});

		it('should create an action of type NETWORK_REQUEST_START', () => {
			const expectedAction: NetworkRequestAction = {
				type: Application.NETWORK_REQUEST_START,
				id: 'test'
			};
			expect(networkRequest(null, true)).to.deep.equal(expectedAction);
		});

		it('should create an action of type NETWORK_REQUEST_FINISH', () => {
			const expectedAction: NetworkRequestAction = {
				type: Application.NETWORK_REQUEST_FINISH,
				id: 'test'
			};
			expect(networkRequest(null, false, 'test')).to.deep.equal(expectedAction);
		});

		it('should create an action of type NETWORK_REQUEST_FAILURE', () => {
			const expectedAction: NetworkRequestAction = {
				type: Application.NETWORK_REQUEST_FAILURE,
				error: 'error',
				id: 'test'
			};
			expect(networkRequest('error', false, 'test')).to.deep.equal(expectedAction);
		});

		after(() => {
			(uuid.v4 as any).restore();
		});
	});

	describe('setVisibilityState()', () => {
		it('should create an action to set isVisible', () => {
			const expectedAction: VisibilityAction = {
				type: Application.SET_VISIBILITY_STATE,
				isVisible: true
			};
			expect(setVisibilityState(true)).to.deep.equal(expectedAction);
		});
	});

	describe('receiveJwt()', () => {
		it('should create an action to receive a jwt', () => {
			const token = 'someToken';
			const expectedAction: JwtAction = {
				type: Application.RECEIVE_JWT,
				jwt: token
			};
			expect(receiveJwt(token)).to.deep.equal(expectedAction);
		});
	});

	describe('setConnectionState()', () => {
		it('should create an action to set isConnected', () => {
			const expectedAction: ConnectionAction = {
				type: Application.SET_CONNECTION_STATE,
				isConnected: true
			};
			expect(setConnectionState(true)).to.deep.equal(expectedAction);
		});
	});

		describe('setApiServer()', () => {
		it('should create an action to set the api server base url', () => {
			const apiServer = 'http://localhost:4200/api';
			const expectedAction: ApiServerAction = {
				type: Application.SET_API_SERVER,
				apiServer
			};
			expect(setApiServer(apiServer)).to.deep.equal(expectedAction);
		});
	});

});