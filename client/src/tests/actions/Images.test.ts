/// <reference types="mocha"/>
/// <reference types="node"/>
/// <reference path="../../index.d.ts"/>

import * as chai from 'chai';
import * as fetchMock from 'fetch-mock';
import { registerAssertions } from 'redux-actions-assertions/chai';
import { registerMiddlewares, registerInitialStoreState, buildInitialStoreState } from 'redux-actions-assertions';
import { wrapUuidV4, restoreUuidV4, getNetworkRequestActions,
				 checkThunkErrorHandling ,checkThunkDefaultErrorHandling } from '../TestUtil';
import thunk from 'redux-thunk';
import { appConfig } from '../../config';

import rootReducer from '../../reducers/Root'
import { networkRequest } from '../../actions/Application';
import { pushStatusMessage, defaultNetworkErrMessage } from '../../actions/UI';
import * as ImageActions from '../../actions/Images';
import { Data } from '../../constants/ActionTypes';
import { MessageType, StatusMessages } from '../../constants/UIConstants';

const expect: any = chai.expect;

const { images } = require('../../fixtures/Images');

describe('Images Action Creators', () => {
	const baseUrl = appConfig.apiServerBaseUrl;

	before(() => {
		registerAssertions();
		registerMiddlewares([thunk]);
		registerInitialStoreState(buildInitialStoreState(rootReducer));
	});

	describe('receiveAllImages()', () => {

		it('should create an action to receive all images', () => {
			const expectedAction = {
				type: Data.RECEIVE_ALL_IMAGES,
				images: images.slice(0, 2),
				imgType: images[0].type
			};

			expect(ImageActions.receiveAllImages(images.slice(0, 2), images[0].type))
				.to.deep.equal(expectedAction);
		});
	});

	describe('createImage()', () => {
		it('should create an action to create an image', () => {
			const expectedAction = {
				type: Data.CREATE_IMAGE,
				image: images[0]
			};
			expect(ImageActions.createImage(images[0]))
				.to.deep.equal(expectedAction);
		});
	});

	describe('createImages()', () => {
		it('should create an action to create multiple images', () => {
			const expectedAction = {
				type: Data.CREATE_IMAGES,
				images
			};
			expect(ImageActions.createImages(images))
				.to.deep.equal(expectedAction);
		});
	});

	describe('removeImage()', () => {
		it('should create an action to remove an image', () => {
			const expectedAction = {
				type: Data.REMOVE_IMAGE,
				id: images[0]._id,
				imgType: images[0].type
			};
			expect(ImageActions.removeImage(images[0]._id, images[0].type))
				.to.deep.equal(expectedAction);
		});
	});

	describe('removeAllImages()', () => {
		it('should create an action to remove all images', () => {
			const expectedAction = {
				type: Data.REMOVE_ALL_IMAGES
			};
			expect(ImageActions.removeAllImages())
				.to.deep.equal(expectedAction);
		});
	});

	describe('getImagesByType() thunk success case', () => {
		before(() => {
			wrapUuidV4('my-id');
			fetchMock.get(`${baseUrl}/images?type=${images[0].type}`, {
				images: images.slice(0, 2)	
			});
		});

		it('should dispatch all expected actions to get images of a type', (done) => {
			const expectedActions = [
				...getNetworkRequestActions(),
				ImageActions.receiveAllImages(images.slice(0, 2), images[0].type)
			];
			expect(ImageActions.getImagesByType(images[0].type))
				.to.dispatch.actions(expectedActions, done);
		});

		after(() => fetchMock.restore());
	});

	describe('getImagesByType() thunk network failure case', () => {
		before(() => fetchMock.get(`${baseUrl}/images?type=${images[0].type}`, {
			throws: 'Server not found'
		}));

		it('should dispatch all expected error actions on failure', (done) => {
			const errMsg = 'Failed to fetch Images data for type = ' + images[0].type;
			checkThunkDefaultErrorHandling(
				ImageActions.getImagesByType,
				done,
				errMsg,
				images[0].type
			);

		});

		after(() => fetchMock.restore());
	});

	describe('deleteImageById() thunk success case', () => {
		before(() => {
			fetchMock.delete(`${baseUrl}/images/${images[0]._id}?type=${images[0].type}`, {
				result: { n: 1, ok: 1 }
			});
		});

		it('should dispatch all expected actions to delete an image by id', (done) => {
			const expectedActions = [
				...getNetworkRequestActions(),
				ImageActions.removeImage(images[0]._id, images[0].type),
				pushStatusMessage(StatusMessages.IMAGES_DELETION_SUCCESS, MessageType.SUCCESS)
			];
			expect(ImageActions.deleteImageById(images[0]._id, images[0].type))
				.to.dispatch.actions(expectedActions, done);
		});

		after(() => fetchMock.restore());
	});

	describe('deleteImageById() thunk failure case', () => {
		before(() => fetchMock
			.delete(`${baseUrl}/images/${images[0]._id}?type=${images[0].type}`, 400)
		);

		it('should dispatch a status message if image could not be deleted', (done) => {
			const errMsg = StatusMessages.IMAGES_DELETION_FAILURE;
			checkThunkErrorHandling(
				ImageActions.deleteImageById,
				done,
				errMsg,
				images[0]._id,
				images[0].type
			);
		});

		after(() => fetchMock.restore());
	});

	describe('deleteImageById() thunk network failure case', () => {
		before(() => fetchMock
			.delete(`${baseUrl}/images/${images[0]._id}?type=${images[0].type}`, {
				throws: 'Server not found'
			})
		);

		it('should dispatch all expected error action on failure', (done) => {
			const errMsg = 'Failed to delete Image data.';
			checkThunkDefaultErrorHandling(
				ImageActions.deleteImageById,
				done,
				errMsg,
				images[0]._id,
				images[0].type
			);
		});

		after(() => fetchMock.restore());
	});

	after(() => restoreUuidV4());

});