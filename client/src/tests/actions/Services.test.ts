/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>

import * as chai from 'chai';
import * as fetchMock from 'fetch-mock';
import { registerAssertions } from 'redux-actions-assertions/chai';
import { registerMiddlewares, registerInitialStoreState, buildInitialStoreState } from 'redux-actions-assertions';
import { wrapUuidV4, restoreUuidV4, getNetworkRequestActions,
				 checkThunkErrorHandling ,checkThunkDefaultErrorHandling } from '../TestUtil';
import thunk from 'redux-thunk';
import { appConfig } from '../../config';

import rootReducer from '../../reducers/Root'
import { networkRequest } from '../../actions/Application';
import { pushStatusMessage, defaultNetworkErrMessage } from '../../actions/UI';
import * as ServiceActions from '../../actions/Services';
import { Data } from '../../constants/ActionTypes';
import { MessageType, StatusMessages } from '../../constants/UIConstants';

const expect: any = chai.expect;

describe('Services Action Creators', () => {
	const baseUrl = appConfig.apiServerBaseUrl;
	const service1: Service = {
		_id: 'someId',
		category: 'test',
		name: 'test category 1',
		description: 'some text'
	};

	const service2: Service = { 
		_id: 'someId',
		category: 'test',
		name: 'test category 2',
		description: 'some text'
	};

	before(() => {
		registerAssertions();
		registerMiddlewares([thunk]);
		registerInitialStoreState(buildInitialStoreState(rootReducer));
	});

	describe('receiveAllServices()', () => {
		it('should create an action to receive all services', () => {
			let expectedAction = {
				type: Data.RECEIVE_ALL_SERVICES,
				services: []
			};
			expect(ServiceActions.receiveAllServices([]))
				.to.deep.equal(expectedAction);

			expectedAction = {
				type: Data.RECEIVE_ALL_SERVICES,
				services: [ service1, service2 ]
			};
			expect(ServiceActions.receiveAllServices([service1, service2]))
				.to.deep.equal(expectedAction);
		});
	});

	describe('receiveService()', () => {
		it('should create an action to receive a single service', () => {
			const expectedAction = { type: Data.RECEIVE_SERVICE, service: service1 };
			expect(ServiceActions.receiveService(service1))
				.to.deep.equal(expectedAction);
		});
	});

	describe('updateService()', () => {
		it('should create an action to update a single service', () => {
			let expectedAction = {
				type: Data.UPDATE_SERVICE,
				service: service2
			};
			expect(ServiceActions.updateService(service2))
				.to.deep.equal(expectedAction);
		});
	});

	describe('removeService()', () => {
		it('should create an action to delete a service by id', () => {
			const expectedAction = {
				type: Data.REMOVE_SERVICE,
				id: 'test'
			};
			expect(ServiceActions.removeService('test'))
				.to.deep.equal(expectedAction);
		});
	});

	describe('removeAllServices()', () => {
		it('should create an action to delete all services', () => {
			const expectedAction = { type: Data.REMOVE_ALL_SERVICES };
			expect(ServiceActions.removeAllServices())
				.to.deep.equal(expectedAction);
		});
	});

	describe('createService()', () => {
		const expectedAction = {
			type: Data.CREATE_SERVICE,
			service: service1
		};
		expect(ServiceActions.createService(service1))
			.to.deep.equal(expectedAction);
	});

	describe('postService() thunk success case', () => {
		before(() => {
			wrapUuidV4('my-id');
			fetchMock.post(`${baseUrl}/services`, {
				status: 200,
				body: { service: service1 }
			});
		});

		it('should dispatch the expected actions and return a new service', (done) => {
			const expectedActions = [
				...getNetworkRequestActions(),
				ServiceActions.createService(service1),
				pushStatusMessage(StatusMessages.SERVICE_CREATION_SUCCESS, MessageType.SUCCESS)
			];

			expect(ServiceActions.postService(service1))
				.to.dispatch.actions(expectedActions, done);
		});

		after(() => fetchMock.restore());
	});

	describe('postService() thunk failure case', () => {
		before(() => fetchMock.post(`${baseUrl}/services`, 400));

		it('should dispatch a status message if service could not be created', (done) => {
			const errMsg = StatusMessages.SERVICE_CREATION_FAILURE;
			checkThunkErrorHandling(ServiceActions.postService, done, errMsg, service1);
		});

		after(() => fetchMock.restore());		
	});

	describe('postService() thunk network failure case', () => {

		before(() => {
			fetchMock.post(`${baseUrl}/services`, { throws: 'Server not found' });
		});

		it('should dispatch the correct error actions on failure', (done) => {
			const errMsg = 'Failed to post Service data.';
			checkThunkDefaultErrorHandling(ServiceActions.postService, done, errMsg, service1);
		});

		after(() => fetchMock.restore());
	});

	describe('getAllServices() thunk success case', () => {
		before(() => fetchMock.get(`${baseUrl}/services`, {
			services: [service1, service2]
		}));

		it('should dispatch all expected actions and return all services', (done) => {
			const expectedActions = [
				...getNetworkRequestActions(),
				ServiceActions.receiveAllServices([service1, service2])
			];

			expect(ServiceActions.getAllServices())
				.to.dispatch.actions(expectedActions, done)
		});

		after(() => fetchMock.restore());
	});

	describe('getAllServices() thunk network failure case', () => {
		before(() => fetchMock.get(`${baseUrl}/services`, { throws: 'Server not found' }));

		it('should dispatch all expected error actions on failure', (done) => {
			const errMsg = 'Failed to fetch Services data.';
			checkThunkDefaultErrorHandling(ServiceActions.getAllServices, done, errMsg);
		});

		after(() => fetchMock.restore());
	});

	describe('getServiceByName() thunk success case', () => {
		before(() => fetchMock.get(`${baseUrl}/services/${service1.name}`, {
			service: service1
		}));

		it('should dispatch all expected actions and return all services', (done) => {
			const expectedActions = [
				...getNetworkRequestActions(),
				ServiceActions.receiveService(service1)
			];

			expect(ServiceActions.getServiceByName(service1.name))
				.to.dispatch.actions(expectedActions, done)
		});

		after(() => fetchMock.restore());
	});

	describe('getServiceByName() thunk network failure case', () => {
		before(() => fetchMock.get(
			`${baseUrl}/services/${service1.name}`,
			{ throws: 'Server not found' })
		);

		it('should dispatch all expected error actions on failure', (done) => {
			const errMsg = `Failed to fetch data for Service ${service1.name}.`;
			checkThunkDefaultErrorHandling(ServiceActions.getServiceByName, done, errMsg, service1.name);
		});

		after(() => fetchMock.restore());
	});

	describe('patchServiceById() thunk success case', () => {
		before(() => fetchMock.patch(`${baseUrl}/services/${service1._id}`, {
			result: { n: 1, ok: 1 }
		}));

		it('should dispatch the expected actions to patch a service by id', (done) => {
			const expectedActions = [
				...getNetworkRequestActions(),
				ServiceActions.updateService(service1),
				pushStatusMessage(StatusMessages.SERVICE_UPDATE_SUCCESS, MessageType.SUCCESS)
			];

			expect(ServiceActions.patchServiceById(service1))
				.to.dispatch.actions(expectedActions, done);
		});

		after(() => fetchMock.restore());
	});

	describe('patchServiceById() thunk failure case', () => {
		before(() => fetchMock.patch(`${baseUrl}/services/${service1._id}`, 400));
		
		it('should dispatch a status message if service could not be patched', (done) => {
			const errMsg = StatusMessages.SERVICE_UPDATE_FAILURE;
			checkThunkErrorHandling(ServiceActions.patchServiceById, done, errMsg, service1);
		});

		after(() => fetchMock.restore());
	});

	describe('patchServiceById() thunk network failure case', () => {
		before(() => {
			fetchMock.patch(`${baseUrl}/services/${service1._id}`, { throws: 'Server not found' });
		});

		it('should dispatch all expected error actions on failure', (done) => {
			const errMsg = 'Failed to patch Service data.';
			checkThunkDefaultErrorHandling(ServiceActions.patchServiceById, done, errMsg, service1);
		});

		after(() => fetchMock.restore());
	});

	describe('deleteServiceById() thunk success case', () => {
		before(() => {
			fetchMock.delete(`${baseUrl}/services/${service1._id}`, {
				result: { n: 1, ok: 1 }
			});
		});

		it('should dispatch all expected actions to delete a service by id', (done) => {

			const expectedActions = [
				...getNetworkRequestActions(),
				ServiceActions.removeService(service1._id.toString()),
				pushStatusMessage(StatusMessages.SERVICE_DELETION_SUCCESS, MessageType.SUCCESS)
			];

			expect(ServiceActions.deleteServiceById(service1._id))
				.to.dispatch.actions(expectedActions, done);
		});

		after(() => fetchMock.restore());
	});

	describe('deleteServiceById() thunk failure case', () => {
		before(() => fetchMock.delete(`${baseUrl}/services/${service1._id}`, 400));

		it('should dispatch a status message if the deletion was not successful', (done) => {
			const errMsg = StatusMessages.SERVICE_DELETION_FAILURE;
			checkThunkErrorHandling(ServiceActions.deleteServiceById, done, errMsg, service1._id);
		});

		after(() => fetchMock.restore());
	});

	describe('deleteServiceById() thunk network failure case', () => {

		before(() => fetchMock.delete(`${baseUrl}/services/${service1._id}`, { throws: 'Server not found' }));

		it('should dispatch all expected error actions on failure', (done) => {
			const errMsg = 'Failed to delete Service data.';
			checkThunkDefaultErrorHandling(ServiceActions.deleteServiceById, done, errMsg, service1._id);
		});

		after(() => fetchMock.restore());

	});

	describe('deleteAllServices() thunk success case', () => {

		before(() => {
			fetchMock.delete(`${baseUrl}/services`, {
				result: { n: 2, ok: 1 }
			});
		});

		it('should dispatch all actions to delete all services', (done) => {
			const expectedActions = [
				...getNetworkRequestActions(),
				ServiceActions.removeAllServices(),
				pushStatusMessage(StatusMessages.SERVICE_DELETE_ALL_SUCCESS, MessageType.SUCCESS)
			];

			expect(ServiceActions.deleteAllServices())
				.to.dispatch.actions(expectedActions, done);
		});

		after(() => fetchMock.restore());
	});

	describe('deleteAllServices() thunk failure case', () => {
		before(() => fetchMock.delete(`${baseUrl}/services`, 400));

		it('should dispatch a status message if services could not be deleted', (done) => {
			const errMsg = StatusMessages.SERVICE_DELETE_ALL_FAILRE;
			checkThunkErrorHandling(ServiceActions.deleteAllServices, done, errMsg);
		});

		after(() => fetchMock.restore());
	});

	describe('deleteAllServices() thunk network failure case', () => {

		before(() => fetchMock.delete(`${baseUrl}/services`, { throws: 'Server not found' }));

		it('should dispatch all expected error actions on failure', (done) => {
			const errMsg = 'Failed to delete Services data.';
			checkThunkDefaultErrorHandling(ServiceActions.deleteAllServices, done, errMsg);
		});

		after(() => fetchMock.restore());
	});

	after(() => restoreUuidV4());

});