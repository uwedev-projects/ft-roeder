/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>

import * as chai from 'chai';
import * as sinon from 'sinon';
import { registerAssertions } from 'redux-actions-assertions/chai';
import { registerMiddlewares, registerInitialStoreState, buildInitialStoreState } 
															from 'redux-actions-assertions';
import { wrapUuidV4, restoreUuidV4 } from '../TestUtil';
import 'isomorphic-fetch';
import * as fetchMock from 'fetch-mock';
import thunk from 'redux-thunk';

import rootReducer from '../../reducers/Root'
import { createRouterMockStore } from '../../fixtures/MockStores';
import { receiveUser, login, authenticate, fetchAdminUser, signOut } from '../../actions/User';
import { deferAction } from '../../actions/Util';
import { networkRequest, deferredFinish, receiveJwt } from '../../actions/Application';
import { push } from 'react-router-redux';
import { createStatusMessage, dismissStatusMessage, pushStatusMessage, defaultNetworkErrMessage } from '../../actions/UI';
import { User } from '../../constants/ActionTypes';
import { MessageType, StatusMessages } from '../../constants/UIConstants';
import { appConfig } from '../../config';

const expect: any = chai.expect;

describe('User Action Creators', () => {

	const baseUrl: string = appConfig.apiServerBaseUrl;
	let testUser: User;
	const mockJwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXV';

	before(() => {
		registerAssertions();
		registerMiddlewares([thunk]);
		registerInitialStoreState(buildInitialStoreState(rootReducer));

		testUser = {
			credentials: { userName: 'test', password: 'test' },
			userInfo: { firstName: 'John', lastName: 'Doe' }
		};
	});

	describe('receiveUser()', () => {
		it('should create an action to receive the current user', () => {
			const expectedAction = {
				type: User.RECEIVE_USER_DATA,
				user: testUser
			};
			expect(receiveUser(testUser))
				.to.deep.equal(expectedAction);
		});

	});

	describe('authenticate()', () => {
		let clock: sinon.SinonFakeTimers = null;
		let uuidStub: sinon.SinonStub = null;
		const messageId = 'test';

		before(() => {
			fetchMock.post(baseUrl + '/auth', {
				status: 200,
				body: { jwt: mockJwt }
			})
			.catch(401);

			wrapUuidV4(messageId);
		});

		it('should dispatch NETWORK_REQUEST_START, RECEIVE_JWT' 
		+ ' & NETWORK_REQUEST_FINISH', (done) => {
			const startRequestAction = networkRequest(null, true);
			const { id } = startRequestAction;

			const finishRequestAction = deferAction(networkRequest(null, false, id), 500);
			
			const expectedActions = [
				startRequestAction,
				receiveJwt(mockJwt),
				finishRequestAction
			];
			expect(authenticate(testUser.credentials))
				.to.dispatch.actions(expectedActions, done);
		});

		it('should throw an error on 401', () => {
			clock = sinon.useFakeTimers();
			fetchMock.restore();
			fetchMock.post(baseUrl + '/auth', 401);

			try {
				expect(authenticate(testUser.credentials))
					.to.throw('401').
				clock.tick(6000);
			} catch(err) {
			}
		});

		it('should dispatch NETWORK_REQUEST_FAILURE and pushStatusMessage() on server error', (done) => {
			fetchMock.restore();
			fetchMock.post(baseUrl + '/auth', 500);

			const signInUserAction = { type: User.SIGN_IN_USER };
			const startRequestAction = networkRequest(null, true);
			const { id } = startRequestAction;

			const expectedActions = [
				signInUserAction,
				startRequestAction,
				networkRequest('Failed to fetch JSON webtoken.', false, id),
				defaultNetworkErrMessage()
			];

			expect(login(testUser.credentials))
				.with.state(createRouterMockStore().getState())
				.to.dispatch.actions(expectedActions, done);
			clock.tick(8500);
		});

		after(() => {
			clock.restore();
		});
	});

	describe('fetchAdminUser()', () => {
		before(() => {
			fetchMock.get(`${baseUrl}/admin-users/${testUser.credentials.userName}`, {
				status: 200,
				body: { adminUser: testUser }
			})
			.catch(404);
		});

		it('should dispatch NETWORK_REQUEST_START, RECEIVE_USER_DATA' + 
		' & NETWORK_REQUEST_FINISH', (done) => {
			const startRequestAction = networkRequest(null, true);
			const { id } = startRequestAction;
			const expectedActions = [
				startRequestAction,
				networkRequest(null, false, id),
				receiveUser(testUser)
			];
			expect(fetchAdminUser(testUser.credentials.userName))
				.with.state(createRouterMockStore().getState())
				.to.dispatch.actions(expectedActions, done);
		});
	});

	describe('login()', () => {
		let clock: sinon.SinonFakeTimers = null;

		it('should dispatch SIGN_IN_USER, authenticate() and fetchUser()', (done) => {
			fetchMock.restore();
			fetchMock.get(`${baseUrl}/admin-users/${testUser.credentials.userName}`, {
				status: 200,
				body: { adminUser: testUser }
			})
			.catch(404);
			
			fetchMock.post(baseUrl + '/auth', {
				status: 200,
				body: { jwt: mockJwt }
			}).catch(401);

			const expectedActions = [
				{ type: User.SIGN_IN_USER },
				authenticate(testUser.credentials),
				fetchAdminUser(testUser.credentials.userName),
				push('/admin/dashboard')
			];
			expect(login(testUser.credentials))
				.to.dispatch.actions(expectedActions, done);
		});

		it('should dispatch pushStatusMessage() on server error', (done) => {
			clock = sinon.useFakeTimers();
			fetchMock.restore();
			fetchMock.post(baseUrl + '/auth', 500);

			const signInUserAction = { type: User.SIGN_IN_USER };
			const startRequestAction = networkRequest(null, true);
			const { id } = startRequestAction;

			const expectedActions = [
				signInUserAction,
				startRequestAction,
				networkRequest('Failed to fetch JSON webtoken.', false, id),
				defaultNetworkErrMessage()
			];

			expect(login(testUser.credentials))
				.to.dispatch.actions(expectedActions, done);
			clock.tick(10000);
		});

		after(() => clock.restore());
	});

	describe('signOut()', () => {
		it('should dispatch SIGN_OUT_USER and push()', (done) => {
			const expectedActions = [
				{ type: User.SIGN_OUT_USER },
				push('/')
			];
			expect(signOut())
				.to.dispatch.actions(expectedActions, done);
		});
	});

	after(() => {
		fetchMock.restore();
		restoreUuidV4();
	});

});