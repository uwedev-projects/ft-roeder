/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>

import * as chai from 'chai';
import * as fetchMock from 'fetch-mock';
import { registerAssertions } from 'redux-actions-assertions/chai';
import { registerMiddlewares, registerInitialStoreState, buildInitialStoreState } from 'redux-actions-assertions';
import { wrapUuidV4, restoreUuidV4, getNetworkRequestActions,
				 checkThunkErrorHandling ,checkThunkDefaultErrorHandling } from '../TestUtil';
import thunk from 'redux-thunk';
import { appConfig } from '../../config';

import rootReducer from '../../reducers/Root'
import { networkRequest } from '../../actions/Application';
import { pushStatusMessage, defaultNetworkErrMessage } from '../../actions/UI';
import * as PostsActions  from '../../actions/Posts';
import { Data } from '../../constants/ActionTypes';
import { MessageType, StatusMessages } from '../../constants/UIConstants';

const expect: any = chai.expect;
const { posts } = require('../../fixtures/Posts');

describe('Posts Action Creators', () => {
	const baseUrl = appConfig.apiServerBaseUrl;

	before(() => {
		registerAssertions();
		registerMiddlewares([thunk]);
		registerInitialStoreState(buildInitialStoreState(rootReducer));
	});

	describe('retrieveNextPosts()', () => {
		it('should create an action to retrieve the next few posts', () => {
			const expectedAction = {
				type: Data.RETRIEVE_NEXT_POSTS,
				posts
			};

			expect(PostsActions.retrieveNextPosts(posts))
				.to.be.deep.equal(expectedAction);
		});
	});

	describe('createPost()', () => {
		it('should create an action to create a post', () => {
			const expectedAction = {
				type: Data.CREATE_POST,
				post: posts[0]
			};

			expect(PostsActions.createPost(posts[0]))
				.to.be.deep.equal(expectedAction);
		});
	});

	describe('updatePost()', () => {
		const post = Object.assign({}, posts[0], { title: 'Title updated!' } );
		const expectedAction = { type: Data.UPDATE_POST, post };
		expect(PostsActions.updatePost(post))
			.to.be.deep.equal(expectedAction);
	});

	describe('removePost()', () => {
		const expectedAction = { type: Data.REMOVE_POST, id: posts[1]._id };
		expect(PostsActions.removePost(posts[1]._id))
			.to.be.deep.equal(expectedAction);
	});

	describe('removePosts()', () => {
		const expectedAction = { type: Data.REMOVE_POSTS };
		expect(PostsActions.removeAllPosts())
			.to.be.deep.equal(expectedAction);
	});

	describe('getPostsLazily() thunk success case', () => {
		before(() => {
			wrapUuidV4('my-id');
			fetchMock.get(`${baseUrl}/posts?skip=0&limit=2`, {
				status: 200,
				body: { posts:
					[ posts[0], posts[1] ]
				}
			});
		});

		it('should dispatch the expected actions and return the first 2 posts', (done) => {
			const expectedActions = [
				...getNetworkRequestActions(),
				PostsActions.retrieveNextPosts([ posts[0], posts[1] ])
			];

			expect(PostsActions.getPostsLazily(0, 2))
				.to.dispatch.actions(expectedActions, done);
		});

		after(() => fetchMock.restore());
	});

	describe('getPostsLazily() thunk failure case', () => {
		before(() => fetchMock.get(`${baseUrl}/posts?skip=1&limit=2`, 400));

		it('should dispatch a status message if posts cannot be fetched', (done) => {
			const errMsg = StatusMessages.POST_FETCH_FAILURE;
			checkThunkErrorHandling(PostsActions.getPostsLazily, done, errMsg, 1, 2);
		});

		after(() => fetchMock.restore());		
	});

	describe('getPostsLazily() thunk network failure case', () => {

		before(() => {
			fetchMock.get(`${baseUrl}/posts?skip=1&limit=2`, { throws: 'Server not found' });
		});

		it('should dispatch the correct error actions on failure', (done) => {
			const errMsg = 'Failed to fetch data for Posts.';
			checkThunkDefaultErrorHandling(PostsActions.getPostsLazily, done, errMsg, 1, 2);
		});

		after(() => fetchMock.restore());
	});

	describe('postPost() thunk success case', () => {
		before(() => {
			fetchMock.post(`${baseUrl}/posts`, {
				status: 200,
				body: { post: posts[2] }
			});
		});

		it('should dispatch the expected actions and return a new post', (done) => {
			const expectedActions = [
				...getNetworkRequestActions(),
				PostsActions.createPost(posts[2]),
				pushStatusMessage(StatusMessages.POST_CREATION_SUCCESS, MessageType.SUCCESS)
			];

			expect(PostsActions.postPost(posts[2]))
				.to.dispatch.actions(expectedActions, done);
		});

		after(() => fetchMock.restore());
	});

	describe('postPost() thunk failure case', () => {
		before(() => fetchMock.post(`${baseUrl}/posts`, 400));

		it('should dispatch a status message if post could not be created', (done) => {
			const errMsg = StatusMessages.POST_CREATION_FAILRURE;
			checkThunkErrorHandling(PostsActions.postPost, done, errMsg, posts[2]);
		});

		after(() => fetchMock.restore());		
	});

	describe('postPost() thunk network failure case', () => {

		before(() => {
			fetchMock.post(`${baseUrl}/posts`, { throws: 'Server not found' });
		});

		it('should dispatch the correct error actions on failure', (done) => {
			const errMsg = 'Failed to post Post data.';
			checkThunkDefaultErrorHandling(PostsActions.postPost, done, errMsg, posts[2]);
		});

		after(() => fetchMock.restore());
	});

	describe('patchPostById() thunk success case', () => {
		before(() => fetchMock.patch(`${baseUrl}/posts/${posts[0]._id}`, {
			result: { n: 1, ok: 1 }
		}));

		it('should dispatch the expected actions to patch a post by id', (done) => {
			const expectedActions = [
				...getNetworkRequestActions(),
				PostsActions.updatePost(posts[0]),
				pushStatusMessage(StatusMessages.POST_UPDATE_SUCCESS, MessageType.SUCCESS)
			];

			expect(PostsActions.patchPostById(posts[0]))
				.to.dispatch.actions(expectedActions, done);
		});

		after(() => fetchMock.restore());
	});

	describe('patchPostById() thunk failure case', () => {
		before(() => fetchMock.patch(`${baseUrl}/posts/${posts[0]._id}`, 400));
		
		it('should dispatch a status message if post could not be patched', (done) => {
			const errMsg = StatusMessages.POST_UPDATE_FAILURE;
			checkThunkErrorHandling(PostsActions.patchPostById, done, errMsg, posts[0]);
		});

		after(() => fetchMock.restore());
	});

	describe('patchPostById() thunk network failure case', () => {
		before(() => {
			fetchMock.patch(`${baseUrl}/posts/${posts[0]._id}`, { throws: 'Server not found' });
		});

		it('should dispatch all expected error actions on failure', (done) => {
			const errMsg = 'Failed to patch Post data.';
			checkThunkDefaultErrorHandling(PostsActions.patchPostById, done, errMsg, posts[0]);
		});

		after(() => fetchMock.restore());
	});

	describe('deletePostById() thunk success case', () => {
		before(() => {
			fetchMock.delete(`${baseUrl}/posts/${posts[1]._id}`, {
				result: { n: 1, ok: 1 }
			});
		});

		it('should dispatch all expected actions to delete a post by id', (done) => {

			const expectedActions = [
				...getNetworkRequestActions(),
				PostsActions.removePost(posts[1]._id.toString()),
				pushStatusMessage(StatusMessages.POST_DELETION_SUCCESS, MessageType.SUCCESS)
			];

			expect(PostsActions.deletePostById(posts[1]._id))
				.to.dispatch.actions(expectedActions, done);
		});

		after(() => fetchMock.restore());
	});

	describe('deletePostById() thunk failure case', () => {
		before(() => fetchMock.delete(`${baseUrl}/posts/${posts[1]._id}`, 400));

		it('should dispatch a status message if the deletion was not successful', (done) => {
			const errMsg = StatusMessages.POST_DELETION_FAILURE;
			checkThunkErrorHandling(PostsActions.deletePostById, done, errMsg, posts[1]._id);
		});

		after(() => fetchMock.restore());
	});

	describe('deletePostById() thunk network failure case', () => {

		before(() => fetchMock.delete(`${baseUrl}/posts/${posts[1]._id}`, { throws: 'Server not found' }));

		it('should dispatch all expected error actions on failure', (done) => {
			const errMsg = 'Failed to delete Post data.';
			checkThunkDefaultErrorHandling(PostsActions.deletePostById, done, errMsg, posts[1]._id);
		});

		after(() => fetchMock.restore());

	});

	describe('deleteAllPosts() thunk success case', () => {

		before(() => {
			fetchMock.delete(`${baseUrl}/posts`, {
				result: { n: 2, ok: 1 }
			});
		});

		it('should dispatch all actions to delete all psots', (done) => {
			const expectedActions = [
				...getNetworkRequestActions(),
				PostsActions.removeAllPosts(),
				pushStatusMessage(StatusMessages.POST_DELETE_ALL_SUCCESS, MessageType.SUCCESS)
			];

			expect(PostsActions.deleteAllPosts())
				.to.dispatch.actions(expectedActions, done);
		});

		after(() => fetchMock.restore());
	});

	describe('deleteAllPosts() thunk failure case', () => {
		before(() => fetchMock.delete(`${baseUrl}/posts`, 400));

		it('should dispatch a status message if posts could not be deleted', (done) => {
			const errMsg = StatusMessages.POST_DELETE_ALL_FAILURE;
			checkThunkErrorHandling(PostsActions.deleteAllPosts, done, errMsg);
		});

		after(() => fetchMock.restore());
	});

	describe('deleteAllPosts() thunk network failure case', () => {

		before(() => fetchMock.delete(`${baseUrl}/posts`, { throws: 'Server not found' }));

		it('should dispatch all expected error actions on failure', (done) => {
			const errMsg = 'Failed to delete Posts data.';
			checkThunkDefaultErrorHandling(PostsActions.deleteAllPosts, done, errMsg);
		});

		after(() => fetchMock.restore());
	});

	after(() => restoreUuidV4());
});