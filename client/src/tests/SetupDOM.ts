const { jsdom } = require('jsdom');

const globalAny: any = global;
globalAny.document = jsdom('<!doctype html><html><body></body></html>')  

globalAny.window = document.defaultView;
globalAny.requestAnimationFrame = () => {};
globalAny.cancelAnimationFrame = () => {};
globalAny.navigator = globalAny.window.navigator;