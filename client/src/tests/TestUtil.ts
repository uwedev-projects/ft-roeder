import * as chai from 'chai';

import { ReactWrapper, ShallowWrapper } from 'enzyme';
import * as uuid from 'uuid';
import { stub } from 'sinon';

import * as ServiceActions from '../actions/Services';
import { networkRequest } from '../actions/Application';
import { defaultNetworkErrMessage, pushStatusMessage } from '../actions/UI';
import { MessageType } from '../constants/UIConstants';

const expect: any = chai.expect;

export const componentDebugLog = 
	(wrapper: ReactWrapper<any,any> | ShallowWrapper<any, any>) => wrapper.html();

export const wrapUuidV4 = (mockId: string) => {
	stub(uuid, 'v4')
		.callsFake(() => mockId);
};

export const restoreUuidV4 = () => { (uuid.v4 as any).restore(); };

export const checkThunkDefaultErrorHandling = (thunkAction, done, errMsg, ...thunkParams) => {
	const startRequestAction = networkRequest(null, true);
	const networkRequestFailure = networkRequest(
		errMsg,
		false,
		startRequestAction.id
	);

	const expectedActions = [
		startRequestAction,
		networkRequestFailure,
		defaultNetworkErrMessage()
	];

	expect(thunkAction(...thunkParams))
		.to.dispatch.actions(expectedActions, done);
};

export const checkThunkErrorHandling = (thunkAction, done, errMsg, ...thunkParams) => {
	const startRequestAction = networkRequest(null, true);
	const finishRequestAction = networkRequest(null, false, startRequestAction.id);

	const expectedActions = [
		startRequestAction,
		finishRequestAction,
		pushStatusMessage(errMsg, MessageType.ERROR)
	];

	expect(thunkAction(...thunkParams))
		.to.dispatch.actions(expectedActions, done);
};

export const getNetworkRequestActions = () => {
	const startRequestAction = networkRequest(null, true);
	const finishRequestAction = networkRequest(null, false, startRequestAction.id);
	return [startRequestAction, finishRequestAction];
}