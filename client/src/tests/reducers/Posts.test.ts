/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>

import { expect } from 'chai';
import * as PostsActions from '../../actions/Posts';
import { postsReducer } from '../../reducers/Posts';

const { posts } = require('../../fixtures/Posts');

describe('Posts Reducer', () => {
	it('should handle the initial state', () => {
		expect(postsReducer(undefined, ({} as any)))
			.to.deep.equal([]);
	});

	it('should handle a RETRIEVE_NEXT_POSTS action', () => {
		expect(postsReducer([], PostsActions.retrieveNextPosts([posts[1]])))
			.to.deep.equal([posts[1]]);
		expect(postsReducer([posts[0]], PostsActions.retrieveNextPosts([ posts[1], posts[2] ])))
			.to.deep.equal(posts);
	});

	it('should handle a CREATE_POST action', () => {
		expect(postsReducer([], PostsActions.createPost(posts[0])))
			.to.deep.equal([posts[0]]);
		expect(postsReducer([posts[0], posts[1]], PostsActions.createPost(posts[2])))
			.to.deep.equal(posts);
	});

	it('should handle a UPDATE_POST action', () => {
		const post = Object.assign({}, posts[0], { title: 'updated!' });
		expect(postsReducer([posts[0], posts[1]], PostsActions.updatePost(post)))
			.to.deep.equal([post, posts[1]]);
	});

	it('should handle a REMOVE_POST action', () => {
		expect(postsReducer([posts[0], posts[1]], PostsActions.removePost(posts[0]._id)))
			.to.deep.equal([posts[1]]);
	});

	it('should handle a REMOVE_ALL_POSTS action', () => {
		expect(postsReducer(posts, PostsActions.removeAllPosts()))
			.to.deep.equal([]);
	});
});