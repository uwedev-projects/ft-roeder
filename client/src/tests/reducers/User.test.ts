/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>

import { expect } from 'chai';
import { userReducer } from '../../reducers/User';
import { receiveUser } from '../../actions/User';
import { User } from '../../constants/ActionTypes';

describe('User Reducer', () => {
	const testUser = {
		credentials: { userName: 'test', password: 'test' },
		userInfo: { firstName: 'John', lastName: 'Doe' }
	};

	it('should return the initial user state', () => {
		expect(userReducer(undefined, ({} as BaseAction)))
			.to.equal(null);
	});	

	it('should handle a RECEIVE_USER action', () => {

		expect(userReducer(null, receiveUser(testUser)))
			.to.deep.equal(testUser);
	});

	it('should handle a SIGN_OUT_USER action', () => {
		expect(userReducer(testUser, { type: User.SIGN_OUT_USER }))
			.to.equal(null);
	});

});