/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>
/// <reference types="node"/>

import { expect } from 'chai';
import * as ImageActions from '../../actions/Images';
import { imagesReducer } from '../../reducers/Images';

const { images } = require('../../fixtures/Images');

describe('Images Reducer', () => {

	it('should return the initial state', () => {
		expect(imagesReducer(undefined, ({} as BaseAction)))
			.to.deep.equal(new Map());
	});

	it('should handle a CREATE_IMAGE action', () => {
		const nextState = imagesReducer(new Map(), ImageActions.createImage(images[0]));
		expect(nextState.has(images[0].type))
			.to.be.true;
		expect(nextState.size)
			.to.equal(1);
		expect(nextState.get(images[0].type))
			.to.deep.equals([images[0]]);
	});

	it('should handle a CREATE_IMAGES action', () => {
		const nextState = imagesReducer(new Map(), ImageActions.createImages(images.slice(0, 2)));
		expect(nextState.size).to.equal(1);
		expect(nextState.has(images[0].type)).to.be.true;
		expect(nextState.get(images[0].type))
			.to.be.of.length(2);
		expect(nextState.get(images[0].type))
			.to.deep.equal(images.slice(0, 2));
	});

	it('should handle a RECEIVE_ALL_IMAGES action', () => {
		let nextState = imagesReducer(
			new Map(), ImageActions.receiveAllImages(images.slice(0, 2), images[0].type)
		);
		expect(nextState.get(images[0].type))
			.to.deep.equal(images.slice(0, 2));		
		
		const previousState = new Map().set(images[2].type, [images[2]]);
		nextState = imagesReducer(
			previousState, ImageActions.receiveAllImages(images.slice(0, 2), images[0].type)
		);
		expect(nextState.get(images[0].type))
			.to.deep.equal(images.slice(0, 2));
		expect(nextState.get(images[2].type))
			.to.deep.equal([images[2]]);
	});

	it('should handle a REMOVE_IMAGE action', () => {
		const previousState = new Map().set(images[0].type, images.slice(0, 2));
		const nextState = imagesReducer(
			previousState, ImageActions.removeImage(images[0]._id, images[0].type)
		);
		expect(nextState.get(images[0].type))
			.to.deep.equal([images[1]]);
	});

	it('should handle a REMOVE_IMAGES action', () => {
		const previousState = new Map().set(images[0].type, images.slice(0, 2));
		expect(imagesReducer(previousState, ImageActions.removeAllImages()))
			.to.deep.equal(new Map());
	});

});