/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>

import { expect } from 'chai';
import { statusMessages } from '../../reducers/UI';
import { UI } from '../../constants/ActionTypes';
import { MessageType } from '../../constants/UIConstants';

describe('UI Reducers', () => {

	describe('statusMessages()', () => {
		let message1: StatusMessage = null;
		let message2: StatusMessage = null;
		before(() => {
			message1 = {
				id: 'test1',
				text: 'error message',
				duration: 1000,
				type: MessageType.ERROR
			};
			message2 = {
				id: 'test2',
				text: 'success message',
				duration: 1000,
				type: MessageType.SUCCESS
			};
		});
		
		it('should return the initial state', () => {
			expect(statusMessages(undefined, ({} as BaseAction)))
				.to.deep.equal([]);
		});

		it('should handle a CREATE_STATUS_MESSAGE action', () => {
			let messageAction = {
				type: UI.CREATE_STATUS_MESSAGE,
				message: message1
			};
			let expectedState: StatusMessage[] = [ message1 ];
			expect(statusMessages([], messageAction))
				.to.deep.equal(expectedState);

			messageAction.message = message2;
			expectedState = [ message1, message2 ];
			expect(statusMessages([ message1 ], messageAction))
				.to.deep.equal(expectedState);
		});

		it('should handle a DISMISS_STATUS_MESSAGE action', () => {
			const messageAction = {
				type: UI.DISMISS_STATUS_MESSAGE,
				id: message1.id
			};
			expect(statusMessages([ message1, message2 ], messageAction))
				.to.deep.equal([ message2 ]);
		});

		it('should not create a message identical to a current one', () => {
			const messageAction = {
				type: UI.CREATE_STATUS_MESSAGE,
				message: message1
			};
			expect(statusMessages([ message1 ], messageAction))
				.to.deep.equal([ message1 ]);
		});
	});

});