/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>

import { expect } from 'chai';
import * as ServiceActions from '../../actions/Services';
import { servicesReducer } from '../../reducers/Services';

describe('Services Reducer', () => {
	const service1: Service = {
		_id: '1',
		category: 'test',
		name: 'test category 1',
		description: 'some text'
	};

	const service2: Service = {
		_id: '2',
		category: 'test',
		name: 'test category 2',
		description: 'some text'
	};

	it('should handle the initial state', () => {
		expect(servicesReducer(undefined, ({} as BaseAction)))
			.to.deep.equal([]);
	});

	it('should handle a CREATE_SERVICE action', () => {
		expect(servicesReducer([], ServiceActions.createService(service1)))
			.to.deep.equal([service1]);
		expect(servicesReducer([service1], ServiceActions.createService(service2)))
			.to.deep.equal([service1, service2]);
	});

	it('should handle a REMOVE_SERVICE action', () => {
		const id = service1._id.toString();
		expect(servicesReducer([service1], ServiceActions.removeService(id)))
			.to.deep.equal([]);
	});

	it('should handle a REMOVE_ALL_SERVICES action', () => {
		expect(servicesReducer([service1, service2], ServiceActions.removeAllServices()))
			.to.deep.equal([]);
	});

	it('should handle an UPDATE_SERVICE action', () => {
		const updatedService = Object.assign({}, service1);
		updatedService.name = 'got updated';
		expect(servicesReducer([service1, service2], ServiceActions.updateService(updatedService)))
			.to.deep.equal([updatedService, service2]);
	});

	it('should handle a RECEIVE_ALL_SERVICES action', () => {
		expect(servicesReducer([], ServiceActions.receiveAllServices([service1, service2])))
			.to.deep.equal([service1, service2]);
	});

	it('should handle RECEIVE_SERVICE action', () => {
		expect(servicesReducer([], ServiceActions.receiveService(service1)))
			.to.deep.equal([service1]);
		expect(servicesReducer([service1], ServiceActions.receiveService(service2)))
			.to.deep.equal([service1, service2]);
	});

});