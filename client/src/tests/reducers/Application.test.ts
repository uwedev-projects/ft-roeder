/// <reference types="mocha"/>
/// <reference path="../../index.d.ts"/>

import * as uuid from 'uuid';
import { expect } from 'chai';
import { stub, SinonStub } from 'sinon';
import { apiServer, jwt, networkRequests, 
				 isVisible, isConnected } from '../../reducers/Application';
import { User } from '../../constants/ActionTypes';
import { setApiServer, receiveJwt, networkRequest, 
				 setVisibilityState, setConnectionState } from '../../actions/Application';
import { appConfig } from '../../config';

describe('Application Reducers', () => {
	
	describe('apiServer()', () => {
		it('should return the initial apiServer state', () => {
			const expectedState: string = appConfig.apiServerBaseUrl;
			expect(apiServer(undefined, ({} as BaseAction)))
				.to.equal(expectedState);
		});

		it('should handle a SET_API_SERVER action', () => {
			const expectedState: string = 'http://different-api-server.com/api';
			expect(apiServer('someApi', setApiServer(expectedState)))
				.to.equal(expectedState);
		});
	});

	describe('jwt()', () => {
		it('should return the initial jwt state', () => {
			const expectedState = null;
			expect(jwt(undefined, ({} as BaseAction)))
				.to.equal(expectedState);
		});

		it('should handle a RECEIVE_JWT action', () => {
			const expectedState: string = 'someToken';
			expect(jwt('testToken', receiveJwt(expectedState)))
				.to.equal(expectedState);
		});

		it('should handle a SIGN_OUT_USER action', () => {
			expect(jwt('someToken', { type: User.SIGN_OUT_USER }))
				.to.be.null;
		});
	});

	describe('networkRequests()', () => {
		let uuidStub: SinonStub = null;
		const request1: NetworkRequest = { id: 'test1' };
		const request2: NetworkRequest = { id: 'test2' };

		before(() => {
			uuidStub = stub(uuid, 'v4').callsFake(() => 'test');
		});

		it('should return the initial networkRequests state', () => {
			expect(networkRequests(undefined, ({} as BaseAction)))
				.to.deep.equal([]);
		});

		it('should handle a NETWORK_REQUEST_START action', () => {
			expect(networkRequests([], networkRequest(null, true)))
				.to.deep.equal([{ id: 'test' }]);
		});

		it('should handle a NETWORK_REQUEST_FINISH action', () => {
			const action = networkRequest(null, false, 'test2');
			expect(networkRequests([request1, request2], action))
				.to.deep.equal([request1]);
		});

		it('should handle a NETWORK_REQUEST_FAILURE action', () => {
			const action = networkRequest('Error!', false, 'test1');
			expect(networkRequests([request1, request2], action))
				.to.deep.equal([request2]);
		});

		after(() => {
			(uuid.v4 as any).restore();
		})
	});

	describe('isVisible()', () => {
		it('should return the initial visibility state', () => {
			expect(isVisible(undefined, ({} as BaseAction)))
				.to.equal(true);
		});

		it('should handle a SET_VISIBILITY_STATE action', () => {
			expect(isVisible(true, setVisibilityState(false)))
				.to.equal(false);
		});
	});

	describe('isConnected()', () => {
		it('should return the initial connection state', () => {
			expect(isConnected(undefined, ({} as BaseAction)))
				.to.equal(false);
		});

		it('should handle a SET_CONNECTION_STATE action', () => {
			expect(isConnected(false, setConnectionState(true)))
				.to.equal(true);
		});
	});

});