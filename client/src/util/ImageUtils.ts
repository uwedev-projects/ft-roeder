import { appConfig } from '../config';
import { Breakpoints } from '../constants/UIConstants';

const { baseUrl } = appConfig;

export const getFullImageUrl = function(imgUrl: string): string {
	return baseUrl + imgUrl;
};

export const getThumbnailUrlFromImage = function(imgUrl: string, version: string): string {
	const url = baseUrl + imgUrl;
	const dotSeparator = url.lastIndexOf('.');
	return url.slice(0, dotSeparator)
		.concat(`-${version}`, url.slice(dotSeparator));
};

export const getImageUrlForScreenWidth = function(imgUrl: string): string {
	const screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	if(screenWidth <= Breakpoints.XS)
		return getThumbnailUrlFromImage(imgUrl, 'lg');
	if(screenWidth <= Breakpoints.SM)
		return getThumbnailUrlFromImage(imgUrl, 'xl');
	else
		return getFullImageUrl(imgUrl);
};