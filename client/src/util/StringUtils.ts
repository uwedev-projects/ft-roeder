export const replaceLastWordWithDots = (text: string) => {
	if(text.charAt(text.length - 1) !== ' ') {
		const words = text.split(' ');
		words.pop();
		text = words.join(' ') + ' ';
	}
	text += '...'
	return text;
};

export const toNoun = (text: string) => text[0] + text.slice(1).toLowerCase();