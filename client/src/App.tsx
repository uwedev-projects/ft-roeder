/// <reference types="node"/>
/// <reference path="./index.d.ts"/>

import './styles/base.scss';

import 'es6-promise/auto';
import 'core-js/shim';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Store, createStore, applyMiddleware, Middleware, compose } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import thunkMiddleware from 'redux-thunk';
import * as localForage from 'localforage';
import { routerMiddleware, ConnectedRouter } from 'react-router-redux';
import { createBrowserHistory } from 'history';

import { defaultState } from './fixtures/MockStores';
import rootReducer from './reducers/Root';
import { mapEventsToDispatch } from './core/BrowserEvents';
import { storeRehydrationFilters } from './core/StoreRehydration';

import { DevTools } from './components/dev-tools/DevTools';
import MainLayout from './components/main-layout/MainLayout';

const history = createBrowserHistory();

const store: Store<any> = createStore<any>(
	rootReducer,
	compose(
		applyMiddleware(
			thunkMiddleware,
			routerMiddleware(history)
		),
		autoRehydrate(),
		DevTools.instrument(),
	)
);

mapEventsToDispatch(store);

const persistConfig = {
	storage: localForage,
	transforms: storeRehydrationFilters
};

persistStore(store, persistConfig, (err, state) => {
	ReactDOM.render(
		<Provider store={store}>
			<ConnectedRouter history={history}>
				<MainLayout/>
			</ConnectedRouter>
		</Provider>
		, document.getElementById('app')
	);
});