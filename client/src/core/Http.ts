import 'isomorphic-fetch';
import { appConfig } from '../config';

export const fetchHttp = async (url: string, options: RequestInit = {}) => {
	if(!options.headers)
	 options.headers = {};
	options.headers['Content-Type'] = 'application/json';
	return fetch(`${appConfig.apiServerBaseUrl}${url}`, options);
};

export const fetchAuthenticated = async (url: string, jwt: string, options: RequestInit = {}) => {
	if(!options.headers)
	 options.headers = {};
	if(!options.headers['Authorization'])
		options.headers['Authorization'] = 'JWT ' + jwt;
	if(!options.headers['Content-Type'])
		options.headers['Content-Type'] = 'application/json';
	return fetch(`${appConfig.apiServerBaseUrl}${url}`, options);
};

export const postFormData = async (url: string, jwt: string, body: FormData) => {
	let options = {
		headers: {
			'Authorization': 'JWT ' + jwt
		},
		method: 'POST',
		body
	};

	return fetch(`${appConfig.apiServerBaseUrl}${url}`, options);
};

export const getHttpConfig = (method: string, body: any) => {
	return {
		method,
		body: JSON.stringify(body),
		withHeaders: (headers) => {
			return Object.assign({}, getHttpConfig(method, body), { headers });
		}
	};
};