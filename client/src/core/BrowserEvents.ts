import { Store } from 'redux';
import { setVisibilityState, setConnectionState } from '../actions/Application';

export const mapEventsToDispatch = (store: Store<GlobalState>) => {
	document.addEventListener('visibilitychange', (ev: any) => {
		if(document.visibilityState === 'visible') {
			store.dispatch(setVisibilityState(true));
		}
		else {
			store.dispatch(setVisibilityState(false));
		}
	});

	if(navigator.onLine)
		store.dispatch(setConnectionState(true));
	else
		store.dispatch(setConnectionState(false));

	window.addEventListener('online', (ev: any) => {
		store.dispatch(setConnectionState(true));
	});

	window.addEventListener('offline', (ev: any) => {
		store.dispatch(setConnectionState(false));
	});
};