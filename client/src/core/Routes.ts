import { asyncComponent } from './AsyncComponent';
import { NavRoute } from '../components/nav-controller/NavController';

export const loadModule = async path => {
	try {
		const Module = await import(
			/* webpackChunkName: "[request]" */
			'../pages/' + path);
		return Module.default;
	} catch (err) {
		console.error('Failed to dynamically load module.');
		console.error(err);
	}
};

export const loadAdminModule = async path => {
	try {
		const Module = await import(
			/* webpackChunkName: "admin" */
			'../pages/admin/' + path);
		return Module.default;
	} catch (err) {
		console.error('Failed to dynamically load admin module.');
		console.error(err);
	}
};

const TestPage = asyncComponent(() => loadModule('test/TestPage'));
const HomePage = asyncComponent(() => loadModule('home/HomePage'));
const SignInPage = asyncComponent(() => loadModule('sign-in/SignInPage'));
const AboutPage = asyncComponent(() => loadModule('about/AboutPage'));
const ContactPage = asyncComponent(() => loadModule('contact/ContactPage'));
const DisclaimerPage = asyncComponent(() => loadModule('disclaimer/DisclaimerPage'));
const DataProtectionPage = asyncComponent(() => loadModule('data-protection/DataProtectionPage'));
const NewsPage = asyncComponent(() => loadModule('news/NewsPage'));
const ServicesPage = asyncComponent(() => loadModule('services/ServicesPage'));
const ServiceDetailPage = asyncComponent(() => loadModule('services/ServiceDetailPage'));
const ReferencesPage = asyncComponent(() => loadModule('references/ReferencesPage'));
const CreateServicePage = asyncComponent(() => loadAdminModule('services/CreateServicePage'));
const EditServicePage = asyncComponent(() => loadAdminModule('services/EditServicePage'));
const ManageServicesPage = asyncComponent(() => loadAdminModule('services/ManageServicesPage'));
const CreateBlogPostPage = asyncComponent(() => loadAdminModule('blog/CreateBlogPostPage'));
const ManageBlogPostsPage = asyncComponent(() => loadAdminModule('blog/ManageBlogPostsPage'));
const ManageReferencesPage = asyncComponent(() => loadAdminModule('references/ManageReferencesPage'));
const DashboardPage = asyncComponent(() => loadAdminModule('dashboard/DashboardPage'));

export const routes: any = [
	{ path: '/admin/dashboard', Component: DashboardPage, isProtected: true },
	{ path: '/admin/leistungen', Component: ManageServicesPage, isProtected: true, exact: true },
	{ path: '/admin/leistungen/bearbeiten/:name', Component: EditServicePage, isProtected: true, exact: true },
	{ path: '/admin/leistungen/neu', Component: CreateServicePage, isProtected: true },
	{ path: '/admin/aktuelles', Component: ManageBlogPostsPage, isProtected: true, exact: true },
	{ path: '/admin/aktuelles/neu', Component: CreateBlogPostPage, isProtected: true, exact: true },
	{ path: '/admin/referenzen', Component: ManageReferencesPage, isProtected: true, exact: true },
	{ path: '/leistungen/:name', Component: ServiceDetailPage, exact: true },
	{ path: '/leistungen', Component: ServicesPage, exact: true },
	{ path: '/referenzen', Component: ReferencesPage },
	{ path: '/aktuelles', Component: NewsPage },
	{ path: '/kontakt', Component: ContactPage },
	{ path: '/impressum', Component: DisclaimerPage },
	{ path: '/datenschutz', Component: DataProtectionPage },
	{ path: '/über-mich', Component: AboutPage },
	{ path: '/sign-in', Component: SignInPage },
	{ path: '/test', Component: TestPage, isProtected: true },
	{ path: '/', exact: true,  Component: HomePage }
];

export const sideMenuEntries: NavMenuEntry[] = [
	{ path: '/', name: 'Home', iconClass: 'fa fa-home'},
	{ path: '/leistungen', name: 'Leistungen', iconClass: 'fa fa-wrench'},
	{ path: '/aktuelles', name: 'Aktuelles', iconClass: 'fa fa-newspaper-o'},
	{ path: '/referenzen', name: 'Referenzen', iconClass: 'fa fa-check-circle'},
	{ path: '/über-mich', name: 'Über Mich', iconClass: 'fa fa-info-circle'},
	{ path: '/kontakt', name: 'Kontakt', iconClass: 'fa fa-address-card'},
	{ path: '/impressum', name: 'Impressum', iconClass: 'fa fa-copyright'},
	{ path: '/test', name: 'Test Page', iconClass: 'fa fa-line-chart'},
	{ path: '/sign-in', name: 'Admin Login', iconClass: 'fa fa-sign-in'}
];

export const adminSideMenuEntries: NavMenuEntry[] = [
	{ path: '/admin/dashboard', name: 'Dashboard', iconClass: 'fa fa-home' },
	{ path: '/admin/leistungen', name: 'Leistungen', iconClass: 'fa fa-wrench' },
	{ path: '/admin/aktuelles', name: 'Aktuelles', iconClass: 'fa fa-newspaper-o' },
	{ path: '/admin/referenzen', name: 'Referenzen', iconClass: 'fa fa-check-circle' },
	{ path: '/test', name: 'Test Page', iconClass: 'fa fa-line-chart' }
];

export const navBarEntries: NavMenuEntry[] = [
	{ path: '/leistungen', name: 'Leistungen'},
	{ path: '/aktuelles', name: 'Aktuelles'},
	{ path: '/referenzen', name: 'Referenzen'},
	{ path: '/über-mich', name: 'Über Mich'},
	{ path: '/kontakt', name: 'Kontakt'},
	{ path: '/impressum', name: 'Impressum'}
];

export const navBarAdminEntries: NavMenuEntry[] = [
	{ path: '/admin/dashboard', name: 'Dashboard'},
	{ path: '/admin/leistungen', name: 'Leistungen'},
	{ path: '/admin/aktuelles', name: 'Aktuelles'},
	{ path: '/admin/referenzen', name: 'Referenzen'}
];

export const footerEntries: NavMenuEntry[] = [
	{ path: '/impressum', name: 'Impressum' },
	{ path: '/datenschutz', name: 'Datenschutz' },
	{ path: '/kontakt', name: 'Kontakt'},
	{ path: '/sign-in', name: 'Admin Login' }
];