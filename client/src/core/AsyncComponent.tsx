import * as React from 'react';

export interface AsyncComponentState {
	Component: React.Component<any, any> | React.StatelessComponent<any> | React.ComponentClass<any> | React.PureComponent<any, any>;
};

export const asyncComponent = getComponent => (
	class AsyncComponent extends React.PureComponent<any, AsyncComponentState> {
		private static Component = null;

		constructor(props) {
			super(props);
			this.state = { Component: AsyncComponent.Component };
		};

		public async componentWillMount(): Promise<void> {
			if(!this.state.Component) {
				try {
					const Component = await getComponent();
					AsyncComponent.Component = Component;
				} finally {
					this.setState({ Component: AsyncComponent.Component });
				}
			}
		};

		public render(): JSX.Element {
			if(this.state.Component) {
				const Component = this.state.Component as React.ComponentClass<any>;
				return <Component { ...this.props}/>
			}
			return null;
		};
	}
);