import createFilter from 'redux-persist-transform-filter';
const { createBlacklistFilter } = require('redux-persist-transform-filter');

const appSubsetFilter = createFilter('application', ['jwt'], ['jwt']);
// const userSubsetFilter = createFilter('user', ['jwt'], ['jwt']);

const uiBlacklistKeys = ['statusMessages'];
const uiBlacklistFilter = createBlacklistFilter('ui', uiBlacklistKeys, uiBlacklistKeys);

export const storeRehydrationFilters = [
	appSubsetFilter,
	uiBlacklistFilter
];