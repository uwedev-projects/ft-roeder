import { join } from 'path';
import { Request, Response, Router, NextFunction } from 'express';
import { upload } from '../middleware/Upload';
import { unlink } from 'fs';
import { ImageModel } from '../model/Image';
import { imageTypes } from '../middleware/Upload';
import { logger } from '../util/Logger';
import { generateAllThumbnails, getThumbnailUrl, deleteImageAndThumbnails } from '../util/Helpers';

const router: Router = Router();

const postImage = async (req: Request, res: Response, next: Function) => {
	if(!req.file || !req.file.path || !req.body || !req.body.name)
		res.status(400).json({error: 'Bad Request'});
	else {
		try {
			const path = req.file.path.slice(14);
			let image: Image = {
				url: path,
				type: req.body.type || 'DEFAULT',
				name: req.file.originalname
			};

			const imageModel = new ImageModel();
			image = await imageModel.create(image);
			await generateAllThumbnails(req.file.path)
			res.json({ image });
		} catch (err) {
			logger.error('Unable to create Image');
			res.status(500).json({error: 'Unable to create Image'});
			next(err);
		}
	}
	next();
};

router.post('/image', upload.single('post'), postImage);

const postImages = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.files || Object.keys(req.files).length == 0)
		res.status(400).json({error: 'Bad Request'});
	else {
		const imagePromises: Promise<Image>[] = [];
		const thumbnailPromises: Promise<any>[] = [];

		for(const fieldName in req.files) {
			const type = fieldName.toUpperCase();
			for(const file of req.files[fieldName]) {
				logger.debug('Trying to upload image file: ', req.files);
				const image: Image = {
					url: file.path.slice(14),
					type: type || 'DEFAULT',
					name: file.originalname
				};
				const imageModel = new ImageModel();
				imagePromises.push(imageModel.create(image));
				thumbnailPromises.push(generateAllThumbnails(file.path));
			}
		}
		try {
			const images: Image[] = await Promise.all(imagePromises);
			await Promise.all(thumbnailPromises)
			res.json({ images });
		} catch (err) {
			logger.error(`Unable to create Images.`);
			res.status(500).json({ error: 'Unable to create Images' });
			next(err);
		}
	}
	next();
};

const maxCount = 12;

const postImagesFieldConfig = [
	{ name: 'paragraph', maxCount },
	{ name: 'post', maxCount },
	{ name: 'service', maxCount },
	{ name: 'reference', maxCount },
	{ name: 'slider', maxCount },
	{ name: 'test', maxCount },
	{ name: 'default', maxCount },
];

router.post('/images', upload.fields(postImagesFieldConfig), postImages);

const getImageById = async (req: Request, res: Response, next: Function) => {
	if(!req.params || !req.params['id']) 
		res.status(400).json({error: 'Bad Request'});
	else {
		try {
			const imageModel = new ImageModel();
			const images: Image[] = await imageModel.find({ _id: req.params['id']});
			if(images.length < 1)
				res.status(404).json({ error: 'Not Found' });
			else
				res.json({image: images[0]});
		} catch (err) {
			logger.error(`Unable to find Image: ${req.params['id']}`);
			res.status(500).json({ error: 'Unable to find Image' });
			next(err);
		}
	}
	next();
};

router.get('/images/:id', getImageById);

const getAllImages = async (req: Request, res: Response, next: Function) => {
	try {
		const imageModel = new ImageModel();
		let images: Image[];
		if(req.query && req.query.type) {
			const type = req.query.type.toUpperCase();
			images = await imageModel.findAll({ type });
		}
		else
			images = await imageModel.findAll();

		res.json({images});
		next();
	} catch (err) {
		logger.error(`Unable to find Images`);
		res.status(500).json({ error: 'Unable to find Images' });
		next(err);
	}
};

router.get('/images', getAllImages);

const deleteImageById = async (req: Request, res: Response, next: Function) => {
	if(!req.params || !req.params['id'])
		res.status(400).json({error: 'Bad Request'});
	else {
		try {
			logger.debug('Trying to delete image ' + req.params['id']);
			const imageModel = new ImageModel();
			const images = await imageModel.find({ _id: req.params['id'] });
			const imagePath = join(__dirname, '../../uploads/images', images[0].url);
			await deleteImageAndThumbnails(imagePath);
			const { result } = await imageModel.removeOne({ _id: req.params['id'] });
				if(result.ok)
					res.json({result});
				else
					res.status(404).json({ error: 'Not Found' });
			next();
		} catch (err) {
			logger.error(`Unable to delete Image: ${req.params['id']}`);
			res.status(500).json({ error: 'Unable to delete Image' });
			next(err);
		}
	}
};

router.delete('/images/:id', deleteImageById);

export const imageRouter = router;