import { Request, Response, Router, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';
import { appConfig } from '../util/AppConfig';
import { AdminUserModel } from '../model/AdminUser';
import { logger } from '../util/Logger';

const router: Router = Router();

router.post('/auth', async (req: Request, res: Response, next: NextFunction) => {
	if(req.body.credentials && req.body.credentials.userName && req.body.credentials.password) {
		try {
			const userName: string = req.body.credentials.userName;
			const password: string = req.body.credentials.password;
			const adminUserModel = new AdminUserModel();
			const adminUsers: AdminUser[] = await adminUserModel.find({'credentials.userName': userName});
			const admin: AdminUser = adminUsers[0];

			const passwordsAreEqual: boolean = await bcrypt.compare(password, admin.credentials.password);
			if(passwordsAreEqual) {
				const jwtOpts: jwt.SignOptions = { 
					expiresIn: appConfig.jwtLifeSpan,
					subject: admin._id.toString()
				};
				const payload = { _id: admin._id };
				jwt.sign(payload, appConfig.jwtSecret, jwtOpts, (err, token) => {
					if(err) {
						res.status(500).json({ error: err.message });
						next(err);
					}
					else
						res.json({jwt: token});
				});
				
			}
			else {
				res.status(401).json({ error: 'Invalid credentials'})
			}
		}
		catch( err ) {
			res.status(404).json({ error: err.message });
		}
	}
	else
		res.status(400).json({ error: 'Bad Request' });
	next();
});

export const authRouter = router;