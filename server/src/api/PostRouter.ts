import { Request, Response, NextFunction, Router } from 'express';
import { ObjectID } from 'mongodb';
import { logger } from '../util/Logger';
import { PostModel } from '../model/Post';

const router: Router = Router();

const getAllPosts = async (req: Request, res: Response, next: NextFunction) => {
	if(req.query.limit) {
		await getPostsWithLimit(req, res, next);
	}
	else {
		try {
			const postModel = new PostModel();
			const	posts: Post[] = await postModel.findAll();
			res.json({posts});
			next();
		} catch (err) {
			logger.error('Unable to find Posts');
			res.status(500).json({error: 'Unable to find Posts'});
			next(err);
		}
	}
};

const getPostsWithLimit = async (req: Request, res: Response, next: NextFunction) => {
	const limit = parseInt(req.query.limit);
	const skip = req.query.skip ? parseInt(req.query.skip) : null;

	if( limit <= 0 || (req.query.skip && skip < 0) || isNaN(limit) || isNaN(skip)  )
		res.status(400).json({error: 'Bad Request'});
	else {
		try {
			// const query = req.query.skip ? { date: { $gt: req.query.last} } : null;
			const postModel = new PostModel();
			const posts = await postModel.find(null, null, { sort: { date: -1 }, skip, limit });
			res.json({posts});
		} catch (err) {
			logger.error('Unable to find Posts');
			res.status(500).json({error: 'Unable to find Posts'});
			next(err);
		}		
	}
	next();
};

router.get('/posts', getAllPosts);

const getPostById = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.params || !req.params['id'])
		res.status(400).json({error: 'Bad Request'});
	else {
		try {
			const postModel = new PostModel();
			const posts: Post[] = await postModel.find({ _id: req.params['id'] });
			
			if(posts.length < 1)
				res.status(404).json({ error: 'Not Found' });
			else
				res.json({post: posts[0]});
		} catch (err) {
			logger.error(`Unable to find Post: ${req.params['id']}`);
			res.status(500).json({error: 'Unable to find Post'});
			next(err);
		};
	}
	next();
};

router.get('/posts/:id', getPostById);

const postPost = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.body || !req.body.post)
		res.status(400).json({error: 'Bad Request'});
	else {
		try {
			const postModel = new PostModel();
			const post: Post = await postModel.create(req.body.post);
			res.json({post});
		} catch (err) {
			logger.error('Unable to create post');
			res.status(500).json({ error: 'Unable to create post' });
			next(err);
		};
	}
	next();
};

router.post('/posts', postPost);

const patchPostById = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.params || !req.params['id'] || !req.body || !req.body.post)
		res.status(400).json({ error: 'Bad Request' });
	else {
		try {
			const postModel = new PostModel();
			const result = await postModel.update({ _id: req.params['id'] }, req.body.post);
			if(result.ok !== 1)
				res.status(404).json({ error: 'Not Found' });
			else
				res.json({result});
		} catch (err) {
			logger.error(`Unable to update Post: ${req.params['id']}`);
			res.status(500).json({error: 'Unable to update Post'});
			next(err);
		};
	}
	next();
};

router.patch('/posts/:id', patchPostById);

const deletePostById = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.params || !req.params['id'])
		res.status(400).json({ error: 'Bad Request' });
	else {
		try {
			const postModel = new PostModel();
			const { result } = await postModel.removeOne({ _id: req.params['id'] });
			if(result.ok)
				res.json({result});
			else
				res.status(404).json({ error: 'Not Found' });
		} catch (err) {
			logger.error(`Unable to delete Post: ${req.params['id']}`);
			res.status(500).json({error: 'Unable to delete Post'});
			next(err);
		};
	}
	next();
};

router.delete('/posts/:id', deletePostById);

const deleteAllPosts = async (req: Request, res: Response, next: NextFunction) => {
	try {
		const postModel = new PostModel();
		const { result } = await postModel.removeAll();
		res.json({result});
		next();
	} catch (err) {
		logger.error('Deletion of posts failed.');
		res.status(500).json({ error: 'Deletion of posts failed.' });
		next(err);
	}
};

router.delete('/posts', deleteAllPosts);

export const postRouter = router;