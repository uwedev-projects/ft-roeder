import { Request, Response, NextFunction, Router } from 'express';
import { logger } from '../util/Logger';
import { AdminUserModel } from '../model/AdminUser';
import { authenticate } from '../middleware/JwtStrategy';

const router: Router = Router();

const getAdminByUserName = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.params || !req.params['userName'])
		res.status(400).json({ error: 'Bad Request' });
	else {
		try {
			const adminUserModel = new AdminUserModel();
			const adminUsers: AdminUser[] = await adminUserModel.find({
				'credentials.userName': req.params['userName']
			});
			if(adminUsers.length < 1)
				res.status(404).json({ error: 'Not Found' });
			else {
				const adminUser = adminUsers[0];
				adminUser.credentials.password = null;
				res.json({ adminUser });
			}
		} catch (err) {
			logger.error(`Unable to find AdminUser: ${req.param['userName']}`);
			res.status(500).json({ error: 'Unable to find AdminUser' });
			next(err);
		}
	}
	next();
}

router.get('/admin-users/:userName', authenticate(), getAdminByUserName);

const patchAdminUserById = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.params || !req.params['id'] || !req.body || !req.body.adminUser)
		res.status(400).json({ error: 'Bad Request' });
	else {
		try {
			const adminUserModel = new AdminUserModel();
			const result = await adminUserModel.update(
				{ _id: req.params['id'] },
				req.body.adminUser
			);
			if(result.ok)
				res.json({ result });
			else
				res.status(404).json({ error: 'Not Found' });
		} catch (err) {
			logger.error(`Unable to update AdminUser ${req.params['id']}`);
			res.status(500).json({ error: 'Unable to update AdminUser' });
			next(err);
		}
	}
	next();
};

router.patch('/admin-users/:id', authenticate(), patchAdminUserById);

export const adminUserRouter = router;