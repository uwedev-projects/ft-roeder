import { Request, Response, Router, NextFunction } from 'express';
import { ServiceModel } from '../model/Service';
import { objectIdFromString } from '../util/Helpers';
import { logger } from '../util/Logger';

const router: Router = Router();

const postService = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.body || !req.body.service)
		res.status(400).json({ error: 'Bad Request' });
	else {
		try {
			const serviceModel = new ServiceModel();
			const service = await serviceModel.create(req.body.service);
			res.json({service});
		} catch (err) {
			logger.error('Unable to create service');
			res.status(500).json({ error: 'Unable to create service' });
			next(err);
		}
	}
	next();
};

router.post('/services', postService);

const getAllServices = async (req: Request, res: Response, next: NextFunction) => {
	try {
		const serviceModel = new ServiceModel();
		let services: Service[] = null;
		if(req.query && req.query.orderby && req.query.orderby === 'significance') {
			services = await serviceModel.find({}, null, {
				sort: { significance: 1 }
			});
		}
		else {
			services = await serviceModel.findAll();
		}
		res.json({services});
		next();
	} catch (err) {
			logger.error(`Unable to find Services`);
			res.status(500).json({error: 'Unable to find Services'});
			next(err);
	}
};

router.get('/services', getAllServices);

const getServiceByName = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.params || !req.params['name'])
		res.status(400).json({ error: 'Bad Request' });
	else {
		try {
			const serviceModel = new ServiceModel();
			const services = await serviceModel.find({ name: req.params['name'] });
			if(services.length < 1)
				res.status(404).json({ error: 'Not Found' });
			else
				res.json({service: services[0]});
		} catch (err) {
			logger.error(`Unable to find Service: ${req.params['name']}`);
			res.status(500).json({error: 'Unable to find Service'});
			next(err);
		}
	}
	next();
};

router.get('/services/:name', getServiceByName);

const patchServiceById = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.params || !req.params['id'] || !req.body || !req.body.service)
		res.status(400).json({ error: 'Bad Request' });
	else {
		try {
			const serviceModel = new ServiceModel();
			const result = await serviceModel.update(
				{ _id: req.params['id'] },
				req.body.service
			);
			if(result.ok)
				res.json({result});
			else
				res.status(404).json({ error: 'Not Found' });
		} catch (err) {
			logger.error(`Unable to update Service: ${req.params['id']}`);
			res.status(500).json({error: 'Unable to update Service'});
			next(err);
		}
	}
	next();
};

router.patch('/services/:id', patchServiceById);

const deleteServiceById = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.params || !req.params['id'])
		res.status(400).json({ error: 'Bad Request' });
	else {
		try {
			const serviceModel = new ServiceModel();
			const { result } = await serviceModel.removeOne({ _id: req.params['id'] });
			if(result.ok)
				res.json({result});
			else
				res.status(404).json({ error: 'Not Found' });
		} catch (err) {
			logger.error(`Unable to delete Service: ${req.params['id']}`);
			res.status(500).json({error: 'Unable to delete Service'});
			next(err);
		}
	}
	next();
};

router.delete('/services/:id', deleteServiceById);

const deleteAllServices = async (req: Request, res: Response, next: NextFunction) => {
	try {
		const serviceModel = new ServiceModel();
		const result = await serviceModel.removeAll();
		res.json({result});
		next();
	} catch (err) {
		logger.error('Deletion of services failed.');
		res.status(500).json({ error: 'Deletion of services failed.' });
		next(err);
	}
};

router.delete('/services', deleteAllServices);

export const serviceRouter = router;