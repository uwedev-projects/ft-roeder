import { Request, Response, Router, NextFunction } from 'express';
import { ServiceCategoryModel } from '../model/ServiceCategory';
import { logger } from '../util/Logger';

const router: Router = Router();

const postServiceCategory = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.body || !req.body.serviceCategory)
		res.status(400).json({ error: 'Bad Request' });
	else {
		try {
			const serviceCategoryModel = new ServiceCategoryModel();
			const serviceCategory: ServiceCategory = await serviceCategoryModel.create(
				req.body.serviceCategory
			);
			res.json({serviceCategory});
		} catch (err) {
			logger.error('Unable to create ServiceCategory');
			res.status(500).json({ error: 'Unable to create ServiceCategory' });
			next(err);
		}
	}
	next();
};

router.post('/service-categories', postServiceCategory);

const getServiceCategoryById = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.params || !req.params['id'])
		res.status(400).json({ error: 'Bad Request' });
	else {
		try {
			const serviceCategoryModel = new ServiceCategoryModel();
			const serviceCategories: ServiceCategory[] = await serviceCategoryModel.find(
				{ _id: req.params['id'] }
			);
			if(serviceCategories.length < 1)
				res.status(404).json({ error: 'Not Found' });
			else
				res.json({serviceCategory: serviceCategories[0]});
		} catch (err) {
			logger.error(`Unable to find ServiceCategory: ${req.params['id']}`);
			res.status(500).json({error: 'Unable to find ServiceCategory'});
			next(err);
		}
	}
	next();
};

router.get('/service-categories/:id', getServiceCategoryById);

const getAllServiceCategories = async (req: Request, res: Response, next: NextFunction) => {
	try {
		const serviceCategoryModel = new ServiceCategoryModel();
		const serviceCategories: ServiceCategory[] = await serviceCategoryModel.findAll();
		res.json({serviceCategories});
		next();
	} catch (err) {
		logger.error(`Unable to find ServiceCategories`);
		res.status(500).json({error: 'Unable to find ServiceCategories'});
		next(err);
	}
};

router.get('/service-categories', getAllServiceCategories);

const patchServiceCategoryById = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.params || !req.params['id'] || !req.body || !req.body.serviceCategory)
		res.status(400).json({ error: 'Bad Request' });
	else {
		try {
			const serviceCategoryModel = new ServiceCategoryModel();
			const result = await serviceCategoryModel.update(
				{ _id: req.params['id'] },
				req.body.serviceCategory
			);
			
			if(result.ok)
				res.json({result});
			else
				res.status(404).json({ error: 'Not Found' });
		} catch (err) {
			logger.error(`Unable to update ServiceCategory: ${req.params['id']}`);
			res.status(500).json({error: 'Unable to update ServiceCategory'});
			next(err);
		}
	}
	next();
};

router.patch('/service-categories/:id', patchServiceCategoryById);

const deleteServiceCategoryById = async (req: Request, res: Response, next: NextFunction) => {
	if(!req.params || !req.params['id'])
		res.status(400).json({ error: 'Bad Request' });
	else {
		try {
			const serviceCategoryModel = new ServiceCategoryModel();
			const { result } = await serviceCategoryModel.removeOne({ _id: req.params['id'] });
			if(result.ok)
				res.json({result});
			else
				res.status(404).json({ error: 'Not Found' });
		} catch (err) {
				logger.error(`Unable to delete ServiceCategory: ${req.params['id']}`);
				res.status(500).json({error: 'Unable to delete ServiceCategory'});
				next(err);
		}
	}
	next();
};

router.delete('/service-categories/:id', deleteServiceCategoryById);

const deleteAllServiceCategories = async (req: Request, res: Response, next: NextFunction) => {
	try {
		const serviceCategoryModel = new ServiceCategoryModel();
		const { result } = await serviceCategoryModel.removeAll();
		res.json({result});
		next();
	} catch (err) {
		logger.error('Deletion of ServiceCategories failed.');
		res.status(500).json({ error: 'Deletion of ServiceCategories failed.' });
		next(err);
	}
};

router.delete('/service-categories', deleteAllServiceCategories);

export const serviceCategoryRouter = router;