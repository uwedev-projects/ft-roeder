const bcrypt = require('bcrypt');

let conn = new Mongo();

print("Setting up database users ...");
let db = conn.getDB("admin");

print("Creating root role ...");
try {
    db.createUser({
    user: "root",
    pwd: "root",
    roles: [
      "root"
    ]
  });

  print("Root role created successfully.");
}
catch (err) {
    print("Root role already exists.");
    print(err.message);
}
finally {
  db.auth("root","root");
}

print('Quitting db setup...');
conn.close();