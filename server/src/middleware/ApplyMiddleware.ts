import * as express from 'express';
import * as path from 'path';
import * as cors from 'express-cors';
import * as bodyParser from 'body-parser';
import * as passport from 'passport';
import * as compression from 'compression';

import { jwtStrategy, authenticate } from './JwtStrategy';
import { mountRoutes } from './MountRoutes';
import { Request, Response, Application } from 'express';
import { appConfig } from '../util/AppConfig';
import { requestLogger, responseLogger, 
				 defaultErrorLogger, unmatchedRouteHandler } from './Logging';

export const applyMiddleware: (app: Application) => void = (app) => {
	app.use(cors({
		allowedOrigins: [...appConfig.allowedOrigins],
		headers: ['Content-Type', 'Authorization', 'X-Requested-With'],
		methods: ['GET', 'PUT', 'PATCH', 'POST', 'DELETE']
	}));
	
	app.use(bodyParser.json());

	if(process.env.NODE_ENV !== 'test') 
		app.use(requestLogger);

	app.use(compression());
	app.use(express.static('../client/dist'));
	app.use(express.static('../client/resources'));
	app.use(express.static('uploads/images'));
	app.use(passport.initialize());

	passport.use(jwtStrategy);
	
	mountRoutes(app);

	app.get('*', (req: Request, res: Response) => {
		if(!res.headersSent)
			res.sendFile(path.join(__dirname, '../../../client/dist/index.html'));
	});

	app.use(unmatchedRouteHandler);
	app.use(defaultErrorLogger);
	app.use(responseLogger);
};