import { Application } from 'express';
import { authenticate } from './JwtStrategy';
import * as routes from '../api/Index';

export const mountRoutes = (app: Application, basePath: string = '/api') => {

	app.use(basePath, routes.authRouter);
	app.use(basePath, routes.adminUserRouter);
	app.use(basePath, routes.imageRouter);
	app.use(basePath, routes.postRouter);
	app.use(basePath, routes.serviceRouter);
	app.use(basePath, routes.serviceCategoryRouter);
	// app.use('/api', authenticate(), componentsRouter);
};