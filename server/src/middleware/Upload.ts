import * as multer from 'multer';
import { Request } from 'express';
import { mkdir, access, constants } from 'fs';
import { logger } from '../util/Logger';

export enum ImageTypes {
	PARAGRAPH = 'PARAGRAPH',
	POST = 'POST',
	SERVICE = 'SERVICE',
	REFERENCE = 'REFERENCE',
	SLIDER = 'SLIDER',
	TEST = 'TEST',
	DEFAULT = 'DEFAULT'
};

export const imageTypes = Object.keys(ImageTypes);

const storage = multer.diskStorage({
	destination: (req: Express.Request, file: Express.Multer.File, cb: Function) => {
		cb(null, `uploads/images/${file.fieldname}`);
	},
	filename: (req: Express.Request, file: Express.Multer.File, cb: Function) => {
		let extension = '';
		switch(file.mimetype) {
			case 'image/jpeg':
				extension = 'jpg';
				break;
			case 'image/png':
				extension = 'png';
				break;
			case 'image/gif':
				extension = 'gif';
				break;
		};
		cb(null, `${Date.now()}.${extension}`);
	}
});

const fileFilter = (req: Express.Request, file: Express.Multer.File, cb: Function) => {
	if(imageTypes.includes(file.fieldname.toUpperCase()))
		cb(null, true);
	else
		cb(null, false);
};

const uploadConfig: multer.Options = {
	storage,
	fileFilter
};

export const upload = multer(uploadConfig);

export const setupDirectoryStructure = async () => {
	logger.debug('Trying to setup directory structure for file uploads...');
	const necessaryPermissions = constants.W_OK;
	try {
		await mkdirAsync('uploads');
		await mkdirAsync('uploads/images');

		const createFileTypeFolders = [];
		for(let imgType of imageTypes) {
			const path = `uploads/images/${imgType.toLowerCase()}`;
			createFileTypeFolders.push(mkdirAsync(path));
		}

		return await Promise.all(createFileTypeFolders);
	} catch (err) {
		if(err.code === 'EACCES') {
			logger.error('Not allowed to access filesystem.');
			throw err;
		}
	}
};

const accessAsync = async (path: string, mode: number) => new Promise(resolve => {
	access(path, mode, err => {
		if(err) {
			logger.error(`Cannot access ${path} for mode ${mode}`);
			throw err;
		}
		else
			resolve();
	});
});

const mkdirAsync = async (path: string) => new Promise(resolve => {
	mkdir(path, err => {
		if(err) {
			if(err.code != 'EEXIST')
				throw err;
			else
				return resolve();
		}
		logger.debug(`Created directory: "${path}"`);
		resolve();
	});
});