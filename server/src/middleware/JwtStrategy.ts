import * as passport from 'passport';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { appConfig } from '../util/AppConfig';
import { AdminUserModel } from '../model/AdminUser';

export const authenticate = () => passport.authenticate('jwt', {session: false});

const strategyOpts = {
	jwtFromRequest: ExtractJwt.fromAuthHeader(),
	secretOrKey: appConfig.jwtSecret
};

export const jwtStrategy = new JwtStrategy(strategyOpts, async (jwtPayload, done) => {
	try {
		const adminUserModel = new AdminUserModel();
		const adminUsers: AdminUser[] = await adminUserModel.find({'_id' : jwtPayload._id});
		return done(null, adminUsers[0]);
	}
	catch (err) {
		return done(err, false);
	}
});