import { Request, Response, NextFunction, ErrorRequestHandler } from 'express';
import { logger } from '../util/Logger';

export const requestLogger = (req: Request, res: Response, next: NextFunction) => {
	let logLine: string = `${req.method} ${req.path}\n`;
	let meta = null;
	
	if(req.get('Content-Type'))
		logLine += `Content-Type: ${req.get('Content-Type')}`;

	if(req.get('Authorization'))
		logLine += `Authorization: ${req.get('Authorization')}`;
	

	if(req.method === 'POST' || req.method === 'PATCH')
		meta = req.body;

	logger.debug(logLine, meta);
	next();
};

export const unmatchedRouteHandler = (req: Request, res: Response, next: NextFunction) => {
	if(!res.headersSent)
		res.status(404).json({error: 'Not Found'});
	next();
};

export const responseLogger = (req: Request, res: Response, next: NextFunction) => {
	let logLine = `> ${res.statusCode}`;
	if(res.statusMessage)
		logLine += ` ${res.statusMessage}`;
	logger.debug(logLine);
	return next();
};

export const defaultErrorLogger: ErrorRequestHandler = 
(err: any, req: Request, res: Response, next: NextFunction) => {
	if(err) {
		logger.error(err.message);
		logger.error(err.stack);
		res.status(500).json({ error: err.message })
	}
	next();
};