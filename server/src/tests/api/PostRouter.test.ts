import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import { spy } from 'sinon';
import { server } from '../StartHttpServer';
import { PostModel } from '../../model/Post';
import { getJwtAuthHeader } from './Helpers';

const chaiHttp: () => ChaiHttp.Agent = require('chai-http');
chai.use(chaiHttp);
chai.use(chaiAsPromised);
const expect: Chai.ExpectStatic = chai.expect;
const { posts } = require('../../../fixtures/Posts');

describe('Posts API', () => {
	let jwt: string;
	let post1Id: string;
	let post1Date: string;
	let post2Date: string;
	let post3Date: string;
	let response: ChaiHttp.Response;
	const post1 = posts[0];
	const post2 = posts[1];
	const post3 = posts[2];
	const postModel = new PostModel();
	
	const validatePost = (post: Post) => {
		expect(post).to.have.all.keys(['author', 'body', 'title', 'date', '_id', '__v']);
	};

	before(async () => {
		let post = await postModel.create(post1);
		post1Id = post._id.toString();
		post1Date = post.date;
		post = await postModel.create(post2);
		post2Date = post.date;
	});

	describe('GET /posts', () => {
		before(async () => {
			jwt = await getJwtAuthHeader();
			response = await chai.request(server).get('/api/posts');
			return await response;
		});

		it('should successfully return all posts', () => {
			expect(response).to.have.property('status', 200);
			expect(response).to.have.property('body')
				.that.has.property('posts');
			expect(response.body.posts).to.have.length(2);
			for(let post of response.body.posts) {
				validatePost(post);
			}
		});
	});

	describe('GET /posts/:id', () => {
		before(async () => {
			response = await chai.request(server).get(`/api/posts/${post1Id}`);
			return await response;
		});
		
		it('should successfully return a post by id', () => {
			expect(response).to.have.property('status', 200);
			expect(response).to.have.property('body')
				.that.has.property('post');
			validatePost(response.body.post);
		});
	});

	describe('POST /posts', () => {
		before(async () => {
			response = await chai.request(server)
				.post('/api/posts')
				.set('Authorization', jwt)
				.send({post: post3});
			return await response;
		});

		it('should return the created post', () => {
			expect(response).to.have.property('status', 200);
			expect(response).to.have.property('body')
				.that.has.property('post');
			validatePost(response.body.post);
			post3Date = response.body.post.date;
		});
	});

	describe('GET /posts?limit=2', () => {
		before(async () => {
			response = await chai.request(server).get('/api/posts?limit=2');
			return await response;
		});

		it('should successfully return a maximum of 2 posts ordered by date', () => {
			expect(response).to.have.property('status', 200);
			expect(response).to.have.property('body')
				.that.has.property('posts')
					.that.has.property('length')
						.that.is.at.most(2);
			response.body.posts
				.forEach(post => validatePost(post));
			expect(response.body.posts[0].title)
				.to.be.equal(post3.title);
			expect(response.body.posts[1].title)
				.to.be.equal(post2.title);
		});
	});

	describe('GET /posts/?skip=1&limit=1', () => {

		describe('skipping the first document', () => {
			before(async () => {
				response = await chai.request(server).get(`/api/posts?skip=1&limit=1`);
				return await response;
			});
	
			it('should successfully return 1 post that is older than post3', () => {
				expect(response).to.have.property('status', 200);
				expect(response).to.have.property('body')
					.that.has.property('posts')
						.that.has.property('length')
							.that.is.at.most(1);
	
				response.body.posts
					.forEach(post => validatePost(post));
				expect(response.body.posts[0].title)
					.to.be.equal(post2.title);
			});
		});
		
		describe('skipping the first two documents', () => {
			before(async () => {
				response = await chai.request(server).get('/api/posts?skip=2&limit=1');
				return await response;
			});

			it('should successfully return 1 post that is older than post2', () => {
				expect(response).to.have.property('status', 200);
				expect(response).to.have.property('body')
					.that.has.property('posts')
						.that.has.property('length')
							.that.is.at.most(1);
	
				response.body.posts
					.forEach(post => validatePost(post));
				expect(response.body.posts[0].title)
					.to.be.equal(post1.title);
			});
		});

		describe('with invalid query parameters', () => {
			it('should return a 400', () => {
				const request = chai.request(server)
					.get(`/api/posts?skip=abc&limit=true`);
				return expect(request)
					.to.eventually.be.rejectedWith('Bad Request');
			});
		});
	});

	describe('PATCH /posts/:id', () => {
		before(async () => {
			response = await chai.request(server)
				.patch(`/api/posts/${post1Id}`)
				.set('Authorization', jwt)
				.send({
					post: { body: 'body updated!' }
				});
			return await response;
		});

		it('should update a post by id', () => {
			expect(response).to.have.property('status', 200);
		});

	});

	describe('DELETE /posts/:id', () => {
		before(async () => {
			response = await chai.request(server)
				.del(`/api/posts/${post1Id}`)
				.set('Authorization', jwt);
			return await response;
		});

		it('should successfully delete a post', () => {
			expect(response).to.have.property('status', 200);
			expect(response).to.have.property('body')
				.that.has.property('result')
					.that.has.property('ok', 1);
		});
	});

	describe('DELETE /posts', () => {
		before(async () => {
			response = await chai.request(server)
				.del('/api/posts')
				.set('Authorization', jwt)
			return await response;
		});

		it('should confirm the deletion of all posts', () => {
			expect(response).to.have.property('status', 200);
			expect(response).to.have.property('body')
				.that.has.property('result')
					.that.has.property('n', 2);
		});
	});

	after(async () => await postModel.removeAll());

});