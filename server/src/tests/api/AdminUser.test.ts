import * as chai from 'chai';
import { server } from '../StartHttpServer';
import { AdminUserModel } from '../../model/AdminUser';
import { getJwtAuthHeader, validateResponse, 
				 validateResult, validateResults } from './Helpers';

const chaiHttp: () => ChaiHttp.Agent = require('chai-http');
chai.use(chaiHttp);
const expect: Chai.ExpectStatic = chai.expect;
const testData = require('../../../fixtures/AdminUser');

const validateAdminUser = (response: any, admin: AdminUser) => {
	expect(response)
		.to.have.property('_id');
	expect(response)
		.to.have.property('credentials');
	expect(response.credentials)
		.to.have.property('userName', admin.credentials.userName);
	expect(response.credentials)
		.to.have.property('password', null);
	expect(response)
		.to.have.property('userInfo');
	expect(response.userInfo)
		.to.have.property('firstName', admin.userInfo.firstName);
	expect(response.userInfo)
		.to.have.property('lastName', admin.userInfo.lastName);
};

describe('AdminUsers Api', () => {
	let jwt: string = null;
	let response: ChaiHttp.Response = null;
	let admin1: AdminUser = testData.testAdmin1;
	let id: string = null;
	let adminUserModel: AdminUserModel = null;

	before(async () => {
		adminUserModel = new AdminUserModel();
		const admin = await adminUserModel.create(admin1);
		id = admin._id.toString();
		jwt = await getJwtAuthHeader();
	});

	describe('GET /admin-users/:userName', () => {
		before(async () => {
			response = await chai.request(server)
				.get(`/api/admin-users/${admin1.credentials.userName}`)
				.set('Authorization', jwt);
			return await response;
		});

		it('should return an admin user by user name', () => {
			validateResponse(response);
			expect(response.body).to.have.property('adminUser');
			validateAdminUser(response.body.adminUser, admin1);
		});
	});

	describe('PATCH /admin-users:/:id', () => {
		before(async () => {
			response = await chai.request(server)
				.patch(`/api/admin-users/${id}`)
				.set('Authorization', jwt)
				.send({
					adminUser: {
						userInfo: {
							firstName: 'New First Name',
							lastName: 'New Last Name'
						}
					}
				});
			return await response;
		});

		it('should confirm the update of an admin user', () => {
			// validateResponse(response);
			// validateResult(response);
		});
	});

	after(async () => await adminUserModel.removeAll());

});