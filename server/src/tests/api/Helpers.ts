import * as jwt from 'jsonwebtoken';
import { expect } from 'chai';
import { appConfig } from '../../util/AppConfig';
import { AdminUserModel } from '../../model/AdminUser';

const testData = require('../../../fixtures/AdminUser');
const adminUserModel = new AdminUserModel();

export const getJwtAuthHeader = async () => {
	let admins: AdminUser[] = await adminUserModel.findAll();
	if(admins.length == 0) {
		admins[0] = await adminUserModel.create(testData.testAdmin1);
	}

	const jwtOpts = { 
		subject: admins[0]._id.toString(),
		expiresIn: appConfig.jwtLifeSpan,
	};

	const payload = { _id: admins[0]._id };
	const token = jwt.sign(payload, appConfig.jwtSecret, jwtOpts);
	return `JWT ${token}`;
};

export const validateResponse = (response: any) => {
	expect(response).to.have.property('status', 200);
	expect(response).to.have.property('body');
}

export const validateResult = (response: any) => {
	validateResponse(response);
	expect(response.body).to.have.property('result')
		.that.has.property('ok', 1);
};

export const validateResults = (response: any, operationCount: number) => {
	validateResponse(response);
	validateResult(response);
	expect(response.body.result).to.have.property('n', operationCount);
};