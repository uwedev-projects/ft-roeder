import * as chai from 'chai';
import { server } from '../StartHttpServer';
import { ServiceModel } from '../../model/Service';
import { ServiceCategoryModel } from '../../model/ServiceCategory';
import { getJwtAuthHeader, validateResponse, validateResult, validateResults } from './Helpers';

import { logger } from '../../util/Logger';

const chaiHttp: () => ChaiHttp.Agent = require('chai-http');
chai.use(chaiHttp);
const expect: Chai.ExpectStatic = chai.expect;

const { categories } = require('../../../fixtures/ServiceCategories');

describe('Services API', () => {
	let serviceCategoryModel: ServiceCategoryModel = null;
	let serviceModel: ServiceModel = null;
	let { services } = require('../../../fixtures/Services');
	let response: ChaiHttp.Response = null;
	let serviceCategoryId1: string;
	let serviceCategoryId2: string;
	let serviceId: string;

	let jwt: string;

	const validateService = (service: Service) => {
		expect(service).to.have.all.keys([
			'name', 
			'description',
			'descImageUrl', 
			'category', 
			'significance',
			'adTexts', 
			'_id', 
			'__v'
		]);
	};

	before(async () => {
		serviceModel = new ServiceModel();
		serviceCategoryModel = new ServiceCategoryModel();
		jwt = await getJwtAuthHeader();
		return await serviceModel.removeAll();
	});

	describe('POST /services', () => {
		before(async () => {
			response = await chai.request(server)
				.post('/api/services')
				.set('Authorization', jwt)
				.send({ service: services[0] });
			return await response;
		});

		it('should return the successfully created category', () => {
			validateResponse(response);
			expect(response.body).to.have.property('service');
			validateService(response.body.service);
		});
	});

	describe('GET /services/:name', () => {
		before(async () => {
			const service = await serviceModel.create(services[1]);
			serviceId = service._id.toString();

			response = await chai.request(server).get(`/api/services/${services[1].name}`);
			return await response;
		});

		it('should return a service by name', () => {
			validateResponse(response);
			expect(response.body).to.have.property('service');
			validateService(response.body.service);
		});
	});

	describe('GET /services', () => {
		before(async () => {
			await serviceModel.create(services[2]);
			response = await chai.request(server).get('/api/services');
			return await response;
		});

		it('should respond with all services', () => {
			validateResponse(response);
			expect(response.body).to.have.property('services')
				.that.has.length(3);
		});
	});

	describe('GET /services?orderby=significance', () => {
		before(async () => {
			response = await chai.request(server).get('/api/services?orderby=significance');
			return await response;
		});

		it('should respond with all services sorted by significance', () => {
			validateResponse(response);
			expect(response.body).to.have.property('services')
				.that.has.length(3);
			
			const { services } = response.body;
			expect(services[0].significance)
				.to.be.lte(services[1].significance);
			expect(services[1].significance)
				.to.be.lte(services[2].significance);
		});
	});

	describe('PATCH /services/:id', () => {
		before(async () => {
			const updateCriteria = {
				description: 'Description updated!'
			};
			response = await chai.request(server)
				.patch(`/api/services/${serviceId}`)
				.set('Authorization', jwt)
				.send({ service: updateCriteria });
			return await response;
		});

		it('should confirm the successful update', () => {
			validateResult(response);
		});
	});

	describe('DELETE /services/:id', () => {
		before(async () => {
			response = await chai.request(server)
				.del(`/api/services/${serviceId}`)
				.set('Authorization', jwt);
			return await response;
		});

		it('should confirm the deletion of a service', () => {
			validateResult(response);
		});
	});

	describe('DELETE /services', () => {
		before(async () => {
			await serviceModel.create(services[1]);
			
			response = await chai.request(server)
				.del('/api/services')
				.set('Authorization', jwt);
			return await response;
		});

		it('should confirm the deletion of all services', () => {
			validateResults(response, 3);
		});
	});

	after(() => {
		return Promise.all([
			serviceCategoryModel.removeAll(),
			serviceModel.removeAll()
		]);
	});
});