import * as fs from 'fs';
import * as path from 'path';
import * as chai from 'chai';
import { server } from '../StartHttpServer';
import { ImageModel } from '../../model/Image';
import { getJwtAuthHeader } from './Helpers';

const chaiHttp: () => ChaiHttp.Agent = require('chai-http');
chai.use(chaiHttp);
const expect: Chai.ExpectStatic = chai.expect;

describe('Images API', () => {
	const { images } = require('../../../fixtures/Images');
	const imageModel = new ImageModel();
	let jwt: string;
	let id: string;
	let response: ChaiHttp.Response;
	let uploadPath: string = null;

	const validateImage = (image: any) => {
		expect(image).to.have.all.keys([
			'name',
			'type',
			'url',
			'_id',
			'__v'
		]);
	};

	describe('POST /image', () => {
		const imagePath: string = path.join(__dirname, '../../../fixtures/TestImage.png');
		console.log(imagePath);
	
		before(async () => {
			await imageModel.removeAll();
			jwt = await getJwtAuthHeader();
			response = await chai.request(server).post('/api/image')
				.set('Authorization', jwt)
				.attach('post', fs.readFileSync(imagePath), 'TestImage.png')
				.field('name', images[0].name)
				.field('type', images[0].type);
		
			uploadPath = response.body.image.path;
			id = response.body.image._id;
			return await response;
		});

		it('should have a status of 200 and return an image', () => {
			expect(response).to.have.property('status', 200);
			expect(response).to.have.property('body')
				.that.has.property('image');
			validateImage(response.body.image);
		});
	});

	describe('GET /images/:id', () => {
		before(async () => {
			response = await chai.request(server).get(`/api/images/${id}`);
			return await response;
		});

		it('should get an image by id', () => {
			expect(response).to.have.property('status', 200);
			expect(response).to.have.property('body')
				.that.has.property('image');
			validateImage(response.body.image);
		});
	});

	describe('GET /images', () => {
		before(async () => {
			await imageModel.create(images[1]);
			await imageModel.create(images[2]);
			response = await chai.request(server).get('/api/images');
		});

		it('should return all images', () => {
			expect(response).to.have.property('status', 200);
			expect(response).to.have.property('body')
				.that.has.property('images')
					.that.is.of.length(3);

			for(let image of response.body.images)
				validateImage(image);
		});
	});

	describe('GET /images?type=post', () => {
		before(async () => {
			response = await chai.request(server).get('/api/images?type=post');
			return await response;
		});

		it('should only return images of type \"post\"', () => {
			expect(response).to.have.property('status', 200);
			expect(response).to.have.property('body')
				.that.has.property('images');
			expect(response.body.images).to.be.of.length(2);

			for(let image of response.body.images)
				validateImage(image);
		});
	});

	describe('DELETE /images/:id', () => {
		before(async () => {
			response = await chai.request(server).del(`/api/images/${id}`)
				.set('Authorization', jwt);
			return await response;
		});

		it('should successfully delete an image', () => {
			expect(response).to.have.property('status', 200);
			expect(response).to.have.property('body')
				.that.has.property('result');
			expect(response.body.result).to.have.property('ok', 1);
		});
	});

	after(async () => imageModel.removeAll());

});