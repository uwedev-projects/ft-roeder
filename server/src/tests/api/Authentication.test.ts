import * as chai from 'chai';
import * as jwt from 'jsonwebtoken';
import { server } from '../StartHttpServer';
import { AdminUserModel } from '../../model/AdminUser';
import { appConfig } from '../../util/AppConfig';

const chaiHttp: () => ChaiHttp.Agent = require('chai-http');
chai.use(chaiHttp);
const expect: Chai.ExpectStatic = chai.expect;
const testData = require('../../../fixtures/AdminUser');

describe('Authentication API', () => {
	const adminUserModel = new AdminUserModel();

	describe('POST /auth', () => {
		let response: ChaiHttp.Response;
		let adminId: Object;
		let token: string;
		let authHeader: string;

		before(async () => {
			await adminUserModel.create(testData.testAdmin1);
			try {
				const testAdmins = await adminUserModel.find(
					{'credentials.userName': testData.testAdmin1.credentials.userName}
				);
				adminId = testAdmins[0]._id;

			} catch (err) {
				console.log(err);
			}
			response = await chai.request(server)
				.post('/api/auth').send({ credentials: testData.testAdmin1.credentials });
			return await response;
		});

		it('should have a status of 200', () => {
			expect(response).to.have.property('status', 200);
			expect(response).to.have.property('body');
		});

		it('should respond with a valid jsonwebtoken', () => {
			expect(response).to.have.property('body')
				.that.has.property('jwt');
			const decoded: any = jwt.verify(response.body.jwt, appConfig.jwtSecret);
			token = response.body.jwt;
			expect(decoded.sub).to.equal(adminId.toString());
		});

		it('should respond with a 401 when using bad credentials', () => {
			const invalidUserAdmin: AdminUser = testData.invalidAdmin1;
			const request = chai.request(server)
				.post('/api/auth').send({ credentials: invalidUserAdmin.credentials });
			return expect(request).to.eventually.be.rejectedWith('Unauthorized');
		});

		it('should respond with a 404 when no AdminUser is found', () => {
			const invalidUserAdmin: AdminUser = testData.invalidAdmin2;
			const request = chai.request(server)
				.post('/api/auth').send({ credentials: invalidUserAdmin.credentials });
			return expect(request).to.eventually.be.rejectedWith('Not Found');
		});

		after(async () => await adminUserModel.removeAll());
	});
});