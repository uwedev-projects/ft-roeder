import * as chai from 'chai';
import { server } from '../StartHttpServer';
import { ServiceCategoryModel } from '../../model/ServiceCategory';
import { getJwtAuthHeader, validateResult, validateResponse, validateResults } from './Helpers';

const chaiHttp: () => ChaiHttp.Agent = require('chai-http');
chai.use(chaiHttp);
const expect: Chai.ExpectStatic = chai.expect;

describe('Service Categories API', () => {
	let serviceCategoryModel: ServiceCategoryModel = null;
	const { categories } = require('../../../fixtures/ServiceCategories');
	let response: ChaiHttp.Response = null;
	let jwt: string;
	let categoryId: string;

	const validateCategory = (category: ServiceCategory) => {
		expect(category).to.have.all.keys([
			'_id',
			'__v',
			'name',
			'significance'
		]);
	};

	before(async () => {
		serviceCategoryModel = new ServiceCategoryModel();
		jwt = await getJwtAuthHeader();
	});

	describe('POST /service-categories', () => {
		before(async () => {
			response = await chai.request(server)
				.post('/api/service-categories')
				.set('Authorization', jwt)
				.send({ serviceCategory: categories[0] });
			return await response;
		});

		it('should return the created category', () => {
			validateResponse(response);
			expect(response.body).to.have.property('serviceCategory');
			validateCategory(response.body.serviceCategory);
		});
	});

	describe('GET /service-categories/:id', () => {
		before(async () => {
			const category: ServiceCategory = await serviceCategoryModel.create(categories[1]);
			categoryId = category._id.toString();

			response = await chai.request(server).get(`/api/service-categories/${categoryId}`);
			return await response;
		});

		it('should return a service category by id', () => {
			validateResponse(response);
			expect(response.body).to.have.property('serviceCategory');
			validateCategory(response.body.serviceCategory);
		});
	});

	describe('GET /service-categories', () => {
		before(async () => {
			response = await chai.request(server).get('/api/service-categories');
			return await response;
		});

		it('should return all service categories', () => {
			validateResponse(response);
			expect(response.body).to.have.property('serviceCategories')
				.that.is.of.length(2);
		});
	});

	describe('PATCH /service-categories/:id', () => {
		before(async () => {
			const updateCriteria = {
				name: 'Name changed!!!'
			};
			response = await chai.request(server)
				.patch(`/api/service-categories/${categoryId}`)
				.set('Authorization', jwt)
				.send({ serviceCategory: updateCriteria });
			return await response;
		});

		it('should confirm the update', () => {
			validateResult(response);
		});
	});

	describe('DELETE /service-categories/:id', () => {
		before(async () => {
			response = await chai.request(server)
				.del(`/api/service-categories/${categoryId}`)
				.set('Authorization', jwt);
			return await response;
		});

		it('should confirm the deletion', () => {
			validateResult(response);
		});
	});

	describe('DELETE /service-categories', () => {
		before(async () => {
			await serviceCategoryModel.create(categories[1]);
			response = await chai.request(server)
				.del('/api/service-categories')
				.set('Authorization', jwt);
			return await response;
		});

		it('should confirm the deletion of all categories', () => {
			validateResult(response);
		});
	});

	after(async () => await serviceCategoryModel.removeAll());
});