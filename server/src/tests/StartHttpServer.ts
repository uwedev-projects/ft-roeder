process.env.NODE_ENV = 'test';
import { expressApp, httpServer } from '../Server';

export const app = expressApp;
export const server = httpServer;