import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import { MongooseConnection } from '../../model/MongooseConnection';

chai.use(chaiAsPromised);
const expect: Chai.ExpectStatic = chai.expect;

describe('MongooseConnection', () => {
	const connection: MongooseConnection = new MongooseConnection();

	it('should open a connection to a database specified in appConfig', () => {
		return expect(connection.open()).to.eventually.be.fulfilled;
	});

	it('should close the connection', () => {
		return expect(connection.close()).to.eventually.be.fulfilled;
	});
});