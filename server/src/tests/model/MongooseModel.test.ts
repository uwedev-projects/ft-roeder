import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import { AdminUserModel } from '../../model/AdminUser';
import { ServiceModel } from '../../model/Service';

chai.use(chaiAsPromised);
const expect: Chai.ExpectStatic = chai.expect;
const testData = require('../../../fixtures/AdminUser');
const { services } = require('../../../fixtures/Services');

describe('MongooseModel', () => {
	let testAdmin: AdminUser = testData.testAdmin1;
	let adminUserModel: AdminUserModel = null;

	before(async () => {
		adminUserModel = new AdminUserModel();
		const serviceModel = new ServiceModel();

		return await Promise.all([
			adminUserModel.removeAll(),
			serviceModel.removeAll()
		]);
	});

	describe('.create()', () => {
		it('should create a test AdminUser', () => {
			const testAdminPromise: Promise<AdminUser> = adminUserModel.create(testAdmin);
			return Promise.all([
				expect(testAdminPromise).to.eventually.be.fulfilled,
				expect(testAdminPromise).to.eventually.have.property('credentials')
					.that.has.property('userName', testAdmin.credentials.userName)
			]);
		});
	});

	describe('.find()', () => {
		it('should find a test AdminUser', () => {
			const adminUserPromise: Promise<AdminUser[]> = adminUserModel.find({ 'credentials.userName': testAdmin.credentials.userName });
			return Promise.all([
				expect(adminUserPromise)
					.to.eventually.be.fulfilled,
				expect(adminUserPromise)
					.to.eventually.be.of.length(1)
			]);
		});
	});

	describe('.findAll()', () => {
		before(() => {
			return adminUserModel.create(testData.testAdmin2);
		});

		it('should find all test AdminUsers', () => {
			const adminUsersPromise: Promise<AdminUser[]> = adminUserModel.findAll();
			return Promise.all([
				expect(adminUsersPromise).to.eventually.be.fulfilled,
				expect(adminUsersPromise).to.eventually.be.of.length(2)
			]);
		});
	});

	describe('.update()', () => {
		it('should update the userName of an AdminUser', () => {
			const adminUserPromise: Promise<any> = adminUserModel.update(
				{ 'credentials.userName': testAdmin.credentials.userName},
				{ 'credentials.userName': 'newUserName' });

			const updatedAdminUserPromise: Promise<AdminUser[]> = adminUserModel.find({'credentials.userName': 'newUserName'});
				
			return Promise.all([
				expect(adminUserPromise).to.eventually.be.fulfilled,
				expect(adminUserPromise).to.eventually.have.property('ok', 1),
				expect(updatedAdminUserPromise).to.eventually.be.fulfilled
			]);
		});
	});

	describe('.removeAll()', () => {
		it('should remove all AdminUsers', () => {
			const adminUserPromise: Promise<[AdminUser]> = adminUserModel.removeAll();
			return Promise.all([
				expect(adminUserPromise).to.eventually.be.fulfilled,
				expect(adminUserPromise).to.eventually.have.property('result')
					.that.has.property('n', 2)
			]);
		});
	});

	describe('.removeOne()', () => {
		before(() => {
			return adminUserModel.create(testAdmin);
		});

		it('should remove a test AdminUser', () => {
			const adminUserPromise: Promise<AdminUser> = adminUserModel.removeOne(
				{'credentials.userName': testAdmin.credentials.userName}
			);
			return Promise.all([
				expect(adminUserPromise).to.eventually.be.fulfilled,
				expect(adminUserPromise).to.eventually.have.property('result')
					.that.has.property('ok', 1)
			]);
		});
	});

	after(() => {
		return adminUserModel.removeAll();
	});
});