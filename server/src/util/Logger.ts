import * as winston from 'winston';
import * as fs from 'fs';

if(!fs.existsSync('/var/log/rft-server'))
	fs.mkdirSync('/var/log/rft-server');
if(!fs.existsSync('/var/log/rft-server/stdout'))
	fs.mkdirSync('/var/log/rft-server/stdout');
if(!fs.existsSync('/var/log/rft-server/stderr'))	
	fs.mkdirSync('/var/log/rft-server/stderr');

const consoleTransportOptions = {
	colorize: true,
	prettyPrint: true,
	level: 'debug',
	timestamp: true,
	humanReadableUnhandledException: true
}

const today = new Date();

const debugLogTransportOpts = {
	name: 'debug-log',
	level: 'debug',
	filename: `/var/log/rft-server/stdout/${today.toDateString()}-stdout.log`,
	maxsize: 4096000,
	timestamp: true,
	zippedArchive: true,
	prettyPrint: true
};

const errorLogTransportOpts = {
	name: 'error-log',
	level: 'error',
	filename: `/var/log/rft-server/stderr/${today.toDateString()}-stderr.log`,
	maxsize: 4096000,
	timestamp: true,
	zippedArchive: true,
	prettyPrint: true
};

const winstonConfig = {
	transports: [
		new (winston.transports.Console)(consoleTransportOptions)
	],
	exitOnError: false
};

if(process.env.NODE_ENV !== 'test') {
	winstonConfig.transports.push(
		new (winston.transports.File)(debugLogTransportOpts)
	);
	winstonConfig.transports.push(
		new (winston.transports.File)(errorLogTransportOpts)
	);
}

class RFTLogger extends winston.Logger implements Logger {
	constructor() {
		super(winstonConfig);
	};

	public error(message: string, meta?: any) {
		super.error(message, meta);
	};

	public warn(message: string, meta?: any) {
		super.warn(message, meta);
	};

	public info(message: string, meta?: any) {
		super.info(message, meta);
	};

	public verbose(message: string, meta?: any) {
		super.verbose(message, meta);
	};

	public debug(message: string, meta?: any) {
		super.debug(message, meta);
	};
};

export const logger: Logger = new RFTLogger();