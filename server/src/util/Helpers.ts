import { join } from 'path';
import { unlink } from 'fs';
import { ObjectID } from 'mongodb';
import * as sharp from 'sharp';
import { logger } from './Logger';

export const objectIdFromString = (id: string) => ObjectID.createFromHexString(id);

export enum ThumbnailVersion { XS = 'XS', SM = 'SM', MD = 'MD', LG = 'LG', XL = 'XL' };

export async function deleteImageAndThumbnails(path: string): Promise<any> {
	const imagesDeleted: Promise<any>[] = Object.keys(ThumbnailVersion)
		.map(version => deleteFromFileSystem(getThumbnailUrl(path, version)));
	imagesDeleted.push(deleteFromFileSystem(path));
	return await Promise.all(imagesDeleted);
};

export async function deleteFromFileSystem(path: string): Promise<any> {
	return new Promise((resolve, reject) => {
		unlink(path, err => {
			if(err) reject(err);
			resolve();
		});
	});
};

export function getThumbnailUrl(path: string, version: string): string {
	const dotSeparator = path.lastIndexOf('.');
	return path.slice(0, dotSeparator)
		.concat('-', version.toLowerCase(), path.slice(dotSeparator));
};

export const generateThumbnail = (path: string, version: string) => {
	const absolutePath = join(__dirname, '../../', path);
	const destination = getThumbnailUrl(absolutePath, version);
	logger.debug(`Generating thumbnails for: ${path}`);
	
	let width = null, height = null;
	switch(version) {
		case ThumbnailVersion.XS:
			width = 40;
			height = 40;
			break;
		case ThumbnailVersion.SM:
			width = 140;
			break;
		case ThumbnailVersion.MD:
			width = 480;
			break;
		case ThumbnailVersion.LG:
			width = 780;
			break;
		case ThumbnailVersion.XL:
			width = 1024;
			break;
		default: return Promise.resolve();
	};

	return sharp(absolutePath)
		.resize(width, height)
		.toFile(destination);
};

export const generateAllThumbnails = (path: string) => {
	return Promise.all(
		Object.keys(ThumbnailVersion)
			.map(version => generateThumbnail(path, version))
	);
};