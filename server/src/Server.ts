import * as express from 'express';
import * as http from 'http';
import { Router, Request, Response } from 'express';

import * as sourceMapSupport from 'source-map-support';
import { dbConnection } from './model/DbConnection';
import { setupDirectoryStructure } from './middleware/Upload';
import { logger } from './util/Logger';

import { appConfig } from './util/AppConfig';

const isTest = process.env.NODE_ENV === 'test';
const app: express.Application = express();
const port: number = isTest ? (appConfig.testPort || 4300) : (appConfig.port || 4200);
let server: http.Server = null;

const initializeApp = async () => {
	logger.info('Initializing app...');
	return await Promise.all([
		establishDbConnection(),
		setupDirectoryStructure()
	]);
};

const establishDbConnection = async () => {
	logger.info(`Connecting to ${dbConnection.getDb()} ...`);
	try {
		await dbConnection.open();
		logger.info(`Connected to ${dbConnection.getDb()}.`);
		
		if(!isTest) {
			const { setupDatabase } = require('./model/DbSetup');
			return await setupDatabase();
		}
	} catch (err) {
		logger.error(`Unable to connect to ${dbConnection.getDb()}:`);
		handleExit(err);
	}
};

const handleExit = async (err?) => {
	if(err)
		logger.error(err);
	if(server) {
		logger.info("FT Roeder Api Server shutting down ...");
		server.close();
	}
	if(dbConnection.isOpen()) {
		logger.info(`Shutting down ${dbConnection.getDb()} ..`);
		try {
			await dbConnection.close();
			logger.info('Database connection closed.');
		} catch( error ) {
			logger.warn('Database could not be closed successfully:');
			logger.warn(error);
			process.exit(1);
		}
	}
	process.exit();
};

const handleExitWrapper = (err?) => {
	handleExit(err);
};

process.on('exit', handleExitWrapper);
process.on('SIGINT', handleExitWrapper);
process.on('uncaughtException', handleExitWrapper);

if(appConfig.mode == 'dev') {
	sourceMapSupport.install();
}

initializeApp()
.then(() => {
	const { applyMiddleware } = require('./middleware/ApplyMiddleware');
	applyMiddleware(app);
})
.catch(err => {
	handleExit(err);
});

server = app.listen(port, () => {
	logger.info('FT Roeder Api Server listing on port: ' + port);
});

export const expressApp: express.Application = app;
export const httpServer: http.Server = server;