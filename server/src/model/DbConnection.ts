import { MongooseConnection } from './MongooseConnection';

export const dbConnection: DbConnection = new MongooseConnection();