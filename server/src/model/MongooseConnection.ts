import * as BluebirdPromise from 'bluebird';
import * as mongoose from 'mongoose';
import { appConfig } from '../util/AppConfig';

(<any>mongoose).Promise = BluebirdPromise;
global.Promise = BluebirdPromise;


export class MongooseConnection implements DbConnection {
	public id: number;
	static count: number = 0;
	private address: string;
	private port: number;

	private db: string;
	private authDb: string;
	private userName: string;
	private password: string;

	private baseUrl: string;
	private dbUrl: string;

	private connection: mongoose.Connection;

	constructor() {
		this.id = MongooseConnection.count;
		MongooseConnection.count++;

		const config: DbConfig = appConfig.dbConfig;

		if(process.env.NODE_ENV === 'test') {
			this.db = config.testDb;
		}
		else {
			this.db = config.db;
		}
		this.connection = null;
		this.address = config.address;
		this.port = config.port;
		this.baseUrl = 'mongodb://';
		this.authDb = config.authDb;
		this.userName = config.rootUser;
		this.password = config.rootPwd;
		this.dbUrl = this.buildConnectionUrl();
	}

	public async open(): Promise<any> {
		return new Promise<void>((resolve, reject) => {
			this.connection = mongoose.createConnection(this.dbUrl);
			this.connection.once('open', resolve);
			this.connection.on('error', reject);
		});
	};

	public async close(): Promise<any> {
		return new Promise<void>((resolve, reject) => {
			this.connection.close((err) => {
				if(err) {
					reject();
				}
				else {
					resolve();
				}
			});
		});
	}

	private buildConnectionUrl(): string {
		return this.baseUrl + this.userName + ':' + this.password + '@' + 
			this.address + ':' + this.port + '/' + this.db + '?authSource=' + this.authDb;
	}

	public setDb(db: string): void {
		this.db = db;
		this.dbUrl = this.buildConnectionUrl();
	}

	public getDb(): string {
		return this.db;
	}

	public isOpen(): boolean {
		return this.connection !== null;
	}

	public getConnectionInstance(): mongoose.Connection {
		return this.connection;
	}
}