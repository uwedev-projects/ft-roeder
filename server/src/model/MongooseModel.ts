import * as mongoose from 'mongoose';
import * as BluebirdPromise from 'bluebird';
import { dbConnection } from './DbConnection';
import { appConfig } from '../util/AppConfig';
import { logger } from '../util/Logger';

declare interface PromisifiedModel<T> extends mongoose.Model<any> {
	createAsync: (document: T) => Promise<T>;
	findByIdAsync:(id: string) => Promise<T>;
	findOneAsync:(query: Object) => Promise<T>;
	findAsync:(query?: Object, filterConfig?: any, aggregateConfig?: any) => Promise<T[]>;
	updateAsync:(query: Object, updateCriteria: Object) => Promise<any>;
	removeAsync:(query: Object) => Promise<any>;
	deleteOneAsync:(query: Object) => Promise<any>;
};

export class MongooseModel<T> implements Model<T> {
	private connectionInstance: mongoose.Connection;
	protected schema: mongoose.Schema;
	protected model: PromisifiedModel<T>;

	constructor(schemaName: string, schemaDefinition: mongoose.SchemaDefinition, collectionName: string, useEncryption?: boolean) {
		this.connectionInstance = dbConnection.getConnectionInstance();
		this.schema = new mongoose.Schema(schemaDefinition);
		if(useEncryption) {
			this.schema.plugin(require('mongoose-bcrypt'), {rounds: appConfig.hashRounds});
		}
		BluebirdPromise.promisifyAll(mongoose.model);
		this.model = <any>(this.connectionInstance.model(schemaName, this.schema, collectionName));
		BluebirdPromise.promisifyAll(this.model);
	}

	public async find(query: Object, filterConfig = {}, aggregateConfig = {}): Promise<T[]> {
		return await this.model.findAsync(query, filterConfig, aggregateConfig);
	};

	public async findAll(query: Object = {}): Promise<T[]> {
		return this.model.findAsync(query);
	};

	public async update(updateQuery: Object, updateCriteria: Object): Promise<any> {
		return this.model.updateAsync(updateQuery, updateCriteria);
	};

	public async create(entity: T): Promise<T> {
		return this.model.createAsync(entity);
	};

	public async removeOne(query: Object): Promise<any> {
		return this.model.deleteOneAsync(query);
	};

	public async removeAll(query: Object = {}): Promise<any> {
		return this.model.removeAsync(query);
	};
}