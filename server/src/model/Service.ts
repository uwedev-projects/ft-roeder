import { MongooseModel } from './MongooseModel';
import { Schema } from 'mongoose';
import { logger } from '../util/Logger';

const nameValidator = (serviceName: string) => {
	return serviceName.length >= 4 && serviceName.length <= 42;
};

const descriptionValidator = description => description.hasOwnProperty('entityMap') &&
	description.hasOwnProperty('blocks');

const schemaDefinition = {
	name: { type: String, required: true, unique: true, validate: nameValidator },
	category: { type: String, index: true },
	description: { type: Schema.Types.Mixed, required: true, validate: descriptionValidator },
	descImageUrl: { type: String },
	adTexts: [ String ],
	significance: { type: Number, default: 3, min: 1, max: 5 }
};

export class ServiceModel extends MongooseModel<Service> implements Model<Service> {
	constructor() {
		super('Service', schemaDefinition,'services');
	};
};