import { AdminUserModel } from './AdminUser';
import { appConfig } from '../util/AppConfig';
import { logger } from '../util/Logger';

export const setupDatabase = async () => {
	const adminUserModel = new AdminUserModel();
	const admins: AdminUser[] = await adminUserModel.findAll();
	
	if(admins.length < appConfig.defaultAdminUsers.length) {
		await adminUserModel.removeAll();
		logger.info('Database initialization required. Inserting default admin users..');
		for(let admin of appConfig.defaultAdminUsers) {
			await adminUserModel.create(admin);
		}
	}
};