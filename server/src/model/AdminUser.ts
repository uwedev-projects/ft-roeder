import { MongooseModel } from './MongooseModel';

const schemaDefinition = {
	credentials: {
		userName: { type: String, required: true, unique: true },
		password: { type: String, required: true, bcrypt: true }
	},
	userInfo: {
		firstName: { type: String, required: true },
		lastName: { type: String, required: true }
	}
};

export class AdminUserModel extends MongooseModel<AdminUser> implements Model<AdminUser> {
	constructor() {
		super('AdminUser', schemaDefinition,'adminUsers', true);
	};
};