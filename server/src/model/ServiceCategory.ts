import { MongooseModel } from './MongooseModel';

const schemaDefinition = {
	name: { type: String, required: true },
	significance: { type: Number, default: 3, min: 1, max: 5 }
};

export class ServiceCategoryModel extends MongooseModel<ServiceCategory> implements Model<ServiceCategory> {
	constructor() {
		super('ServiceCategory', schemaDefinition,'serviceCategories');
	};
};