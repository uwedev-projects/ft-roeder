import { MongooseModel } from './MongooseModel';
import { Schema } from 'mongoose';

const titleValidator = (title: string) => title !== '';

const bodyValidator = body => body.hasOwnProperty('entityMap') && body.hasOwnProperty('blocks');

const schemaDefinition = {
	title: { type: String, required: true, validate: titleValidator },
	author: { type: String },
	body: { type: Schema.Types.Mixed, required: true, validate: bodyValidator },
	bodyImageUrl: { type: String },
	date: { type: Date, default: Date.now, index: true }
};

export class PostModel extends MongooseModel<Post> implements Model<Post> {
	constructor() {
		super('Post', schemaDefinition,'posts');
	};
};