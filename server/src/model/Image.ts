import { MongooseModel } from './MongooseModel';

const schemaDefinition = {
	name: { type: String, required: true },
	url: { type: String, required: true, unique: true },
	type: { type: String, required: true, uppercase: true }
};

export class ImageModel extends MongooseModel<Image> implements Model<Image> {
	constructor() {
		super('Image', schemaDefinition,'images');
	};
};