declare interface DbConfig {
	address: string;
	port: number;
	rootUser: string;
	rootPwd: string;
	db: string;
	authDb: string;
	testDb: string;
}

declare interface AppConfig {
	name: string;
	mode: string;
	port: number;
	testPort: number;
	dbConfig: DbConfig;
	jwtSecret: string;
	jwtLifeSpan: string;
	hashRounds: number;
	allowedOrigins: string[];
	defaultAdminUsers: AdminUser[];
}

declare interface DbConnection {
	open:() => Promise<any>;
	close:() => Promise<any>;
	getDb:() => string;
	getConnectionInstance?:() => any;
	isOpen:() => boolean;
}

declare interface Model<T> {
	find:(query: Object, filterConfig?: any, aggregateConfig?: any) => Promise<T[]>;
	findAll:(query?: Object) => Promise<T[]>;
	create:(entity: T) => Promise<T>;
	update:(updateQuery: Object, updateCriteria: Object) => Promise<any>;
	removeOne:(query: Object) => Promise<any>;
	removeAll:(query?: Object) => Promise<any>;
}

declare interface Logger {
	error: (message: string, meta?: any) => void;
	warn: (message: string, meta?: any) => void;
	info: (message: string, meta?: any) => void;
	verbose: (message: string, meta?: any) => void;
	debug: (message: string, meta?: any) => void;
}

// # Documents

declare interface BaseDocument {
	_id?: string;
}

declare interface AdminUser extends BaseDocument {
	credentials: Credentials;
	userInfo: UserInfo;
}

declare interface Image extends BaseDocument {
	name: string;
	url: string;
	type: string;
}

declare interface Post extends BaseDocument {
	title: string;
	author?: string;
	body: string;
	bodyImageUrl?: string;
	paragraphs?: Paragraph[];
	date?: string;
}

declare interface Service extends BaseDocument {
	name: string;
	category: string;
	significance?: number;
	description: any;
	descImageUrl?: string;
	paragraphs?: Paragraph[];
	adTexts?: string[];
}

declare interface ServiceCategory extends BaseDocument {
	name: string;
	significance: number;
}

// # Subdocuments

declare interface Credentials {
	userName: string,
	password: string
}

declare interface UserInfo {
	firstName: string;
	lastName: string;
}

declare interface Paragraph {
	text: string;
	imageUrl?: string;
}

// # Enums

declare enum LogLevel { ERROR, WARN, INFO, VERBOSE, DEBUG }

declare enum ModelType {
	SERVICE = 'SERVICE',
	SERVICE_CATEGORY = 'SERVICE_CATEGORY',
	POST = 'POST',
	ADMIN_USER = 'ADMIN_USER',
	IMAGE = 'IMAGE'
}

// Misc